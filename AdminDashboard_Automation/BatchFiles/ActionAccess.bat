cd ..

call mvn test -Dsurefire.suiteXmlFiles=src\test\bvt\ActionAccess\01_SuperAdminActionAccess.xml
call mvn test -Dsurefire.suiteXmlFiles=src\test\bvt\ActionAccess\02_SupportActionAccess.xml
call mvn test -Dsurefire.suiteXmlFiles=src\test\bvt\ActionAccess\03_ProductionActionAccess.xml
call mvn test -Dsurefire.suiteXmlFiles=src\test\bvt\ActionAccess\04_FacilityAdminActionAccess.xml
call mvn test -Dsurefire.suiteXmlFiles=src\test\bvt\ActionAccess\05_ReviewDoctorActionAccess.xml
call mvn test -Dsurefire.suiteXmlFiles=src\test\bvt\ActionAccess\06_LeadTechActionAccess.xml
call mvn test -Dsurefire.suiteXmlFiles=src\test\bvt\ActionAccess\07_FieldTechActionAccess.xml
call mvn test -Dsurefire.suiteXmlFiles=src\test\bvt\ActionAccess\08_OfficePersonnelActionAccess.xml