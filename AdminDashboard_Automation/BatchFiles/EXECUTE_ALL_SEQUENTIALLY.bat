set startTime=%Time%

call PageAccess.bat
cd BatchFiles
call ActionAccess.bat
cd BatchFiles
call CharacterConstraints.bat
cd BatchFiles
call LoginModule.bat
cd BatchFiles
call PatientModule.bat
cd BatchFiles
call UserModule.bat
cd BatchFiles
call DashboardModule.bat
cd BatchFiles
call ImportValidation.bat
cd BatchFiles

echo Start Time: %startTime%
echo End Time: %Time%

pause