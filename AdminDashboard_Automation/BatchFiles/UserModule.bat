cd ..

call mvn test -Dsurefire.suiteXmlFiles=src\test\bvt\UserModule\UsersList.xml
call mvn test -Dsurefire.suiteXmlFiles=src\test\bvt\UserModule\UserCreatePage.xml
call mvn test -Dsurefire.suiteXmlFiles=src\test\bvt\UserModule\UserEditPage.xml
call mvn test -Dsurefire.suiteXmlFiles=src\test\bvt\UserModule\UserDeletionUsingSuperAdmin.xml
call mvn test -Dsurefire.suiteXmlFiles=src\test\bvt\UserModule\UserDeletionUsingSupport.xml
call mvn test -Dsurefire.suiteXmlFiles=src\test\bvt\UserModule\UserDeletionUsingFacilityAdmin.xml
call mvn test -Dsurefire.suiteXmlFiles=src\test\bvt\UserModule\UserDeletion.xml