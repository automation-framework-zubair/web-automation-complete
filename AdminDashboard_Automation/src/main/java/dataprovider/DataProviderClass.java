package dataprovider;

import helper.DataReader;
import org.testng.annotations.DataProvider;

public class DataProviderClass {

    // CREDENTIALS

    /***
     * Implementing data provider method for superAdmin's credentials
     * @return csv data list based upon rolename
     */
    @DataProvider(name = "getSuperAdminCredentials")
    public static Object[][] getCredentialsSuperAdmin(){
        return DataReader.getRoleSpecificCredentials( "credentials.csv","Super Admin");

    }

    /***
     * Implementing data provider method for Support's credentials
     * @return csv data list based upon rolename
     */
    @DataProvider(name = "getSupportCredentials")
    public static Object[][] getCredentialsSupport(){
        return DataReader.getRoleSpecificCredentials( "credentials.csv","Support");

    }

    /***
     * Implementing data provider method for Production's credentials
     * @return csv data list based upon rolename
     */
    @DataProvider(name = "getProductionCredentials")
    public static Object[][] getCredentialsProduction(){
        return DataReader.getRoleSpecificCredentials( "credentials.csv","Production");

    }

    /***
     * Implementing data provider method for Facility Admin's credentials
     * @return csv data list based upon rolename
     */
    @DataProvider(name = "getFacilityAdminCredentials")
    public static Object[][] getCredentialsFacilityAdmin(){
        return DataReader.getRoleSpecificCredentials( "credentials.csv","Facility Admin");

    }

    /***
     * Implementing data provider method for Review Doctor's credentials
     * @return csv data list based upon rolename
     */
    @DataProvider(name = "getReviewDoctorCredentials")
    public static Object[][] getCredentialsReviewDoctor(){
        return DataReader.getRoleSpecificCredentials( "credentials.csv","Review Doctor");

    }

    /***
     * Implementing data provider method for Lead Tech's credentials
     * @return csv data list based upon rolename
     */
    @DataProvider(name = "getLeadTechCredentials")
    public static Object[][] getCredentialsLeadTech(){
        return DataReader.getRoleSpecificCredentials( "credentials.csv","Lead Tech");

    }

    /***
     * Implementing data provider method for Field Tech's credentials
     * @return csv data list based upon rolename
     */
    @DataProvider(name = "getFieldTechCredentials")
    public static Object[][] getCredentialsFieldTech(){
        return DataReader.getRoleSpecificCredentials( "credentials.csv","Field Tech");

    }

    /***
     * Implementing data provider method for Office Personnel's credentials
     * @return csv data list based upon rolename
     */
    @DataProvider(name = "getOfficePersonnelCredentials")
    public static Object[][] getCredentialsOfficePersonnel(){
        return DataReader.getRoleSpecificCredentials( "credentials.csv","Office Personnel");

    }

    // PAGE ACCESS

    /***
     * Implementing data provider method for superAdmin
     * @return csv data list based upon rolename
     */
    @DataProvider(name = "getSuperAdminData")
    public static Object[][] getDataFromCSVSuperAdmin(){
        return DataReader.getRoleSpecificData( "pageAccess.csv","Super Admin");

    }

    /***
     * Implementing data provider method for support
     * @return csv data list based upon rolename
     */
    @DataProvider(name = "getSupportData")
    public static Object[][] getDataFromCSVSupport(){
        return DataReader.getRoleSpecificData("pageAccess.csv","Support");

    }

    /***
     * Implementing data provider method for production
     * @return csv data list based upon rolename
     */
    @DataProvider(name = "getProductionData")
    public static Object[][] getDataFromCSVProduction(){
        return DataReader.getRoleSpecificData("pageAccess.csv","Production");

    }

    /***
     * Implementing data provider method for facilityAdmin
     * @return csv data list based upon rolename
     */
    @DataProvider(name = "getFacilityAdminData")
    public static Object[][] getDataFromCSVFacilityAdmin(){
        return DataReader.getRoleSpecificData("pageAccess.csv","Facility Admin");

    }

    /***
     * Implementing data provider method for reviewDoctor
     * @return csv data list based upon rolename
     */
    @DataProvider(name = "getReviewDoctorData")
    public static Object[][] getDataFromCSVReviewDoctor(){
        return DataReader.getRoleSpecificData("pageAccess.csv","Review Doctor");

    }

    /***
     * Implementing data provider method for leadTech
     * @return csv data list based upon rolename
     */
    @DataProvider(name = "getLeadTechData")
    public static Object[][] getDataFromCSVLeadTech(){
        return DataReader.getRoleSpecificData("pageAccess.csv","Lead Tech");

    }

    /***
     * Implementing data provider method for fieldTech
     * @return csv data list based upon rolename
     */
    @DataProvider(name = "getFieldTechData")
    public static Object[][] getDataFromCSVFieldTech(){
        return DataReader.getRoleSpecificData("pageAccess.csv","Field Tech");

    }

    /***
     * Implementing data provider method for officePersonnel
     * @return csv data list based upon rolename
     */
    @DataProvider(name = "getOfficePersonnelData")
    public static Object[][] getDataFromCSVOfficePersonnel(){
        return DataReader.getRoleSpecificData("pageAccess.csv","Office Personnel");

    }

}
