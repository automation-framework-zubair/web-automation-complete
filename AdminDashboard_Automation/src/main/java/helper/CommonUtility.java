package helper;

import org.apache.xpath.operations.Bool;

import java.io.File;
import java.io.FileWriter;
import java.util.Scanner;

public class CommonUtility {
    private static String fileNameForLoggedInUserDetails = "loggedInUserDetails.txt";

    /***
     * Method to log messages and add them to the report
     * @param message Message to be logged
     * @param status Status of the log message (pass, fail, warn, fatal etc.)
     * */
    public static void logMessagesAndAddThemToReport(String message, String status) {
        Logger.info(message);
        if(status.toLowerCase().equals("pass")) {
            ExtentReportManager.logPass(message);
        }
        else if(status.toLowerCase().equals("fail")) {
            ExtentReportManager.logFail(message);
        }
        else if(status.toLowerCase().equals("info")) {
            ExtentReportManager.logInfo(message);
        }
        else if(status.toLowerCase().equals("warn")) {
            Logger.warn(message);
            ExtentReportManager.logWarn(message);
        }
        else if(status.toLowerCase().equals("fatal")) {
            Logger.fatal(message);
            ExtentReportManager.logFatal(message);
        }
        else if(status.toLowerCase().equals("error")) {
            Logger.error(message);
            ExtentReportManager.logError(message);
        }
        else if(status.toLowerCase().equals("debug")) {
            Logger.debug(message);
            ExtentReportManager.logDebug(message);
        }
    }

    /***
     * Method to log exceptions to the report
     * @param e exception to be logged
     * */
    public static void logExceptionsToTheReport(Exception e) {
        ExtentReportManager.logExceptions(e);

    }

    /***
     * Method to log start test case and add a test case to the report
     * @param testName Name of the test case to be logged
     */
    public static void logCreateTest(String testName) {
        Logger.startTestCase(testName);
        ExtentReportManager.logCreateTest(testName);

    }

    /***
     * Method to add a node under a test case to the report
     * @param testName Name of the test method to be logged
     */
    public static void logCreateNode(String testName) {
        ExtentReportManager.logCreateNode(testName);

    }

    /***
     * Method to add a child node under a node to the report
     * @param testName Name of the test method to be logged
     */
    public static void logCreateSubNode(String testName) {
        ExtentReportManager.logCreateSecondNode(testName);

    }

    /***
     * Method to save logged in users details to a text file
     * @param loggedInUserEmail email of the user
     * @param loggedInUserPassword password of the user
     */
    public static void saveLoggedInUserDetailsToAFile(String loggedInUserEmail, String loggedInUserPassword) {
        deleteLoggedInUserDetailsFile();
        String textToWriteToAFile = "Email : " + loggedInUserEmail + "\r\n" + "Password : " + loggedInUserPassword;
        createAndWriteTextToAFile(textToWriteToAFile, fileNameForLoggedInUserDetails);

    }

    /***
     * Method to read logged in user details
     * @param emailOrPassword to read email or password (email/password)
     */
    public static String readLoggedInUserDetailsFile(String emailOrPassword) {
        String[] loggedInUserDetails = readFile(fileNameForLoggedInUserDetails);
        String lastLoggedInUserEmail = "";
        String lastLoggedInUserPassword = "";
        String returnUserDetails = "";
        for (String text : loggedInUserDetails) {
            if(text==null) {
                break;
            }
            else if(text.contains("Email : ")) {
                String[] splitText = text.split(": ");
                lastLoggedInUserEmail = splitText[1];
            }
            else if(text.contains("Password : ")) {
                String[] splitText = text.split(": ");
                lastLoggedInUserPassword = splitText[1];
            }
        }
        if(emailOrPassword.toLowerCase().equals("email")) {
            returnUserDetails = lastLoggedInUserEmail;
        }
        else if(emailOrPassword.toLowerCase().equals("password")) {
            returnUserDetails = lastLoggedInUserPassword;
        }
        return returnUserDetails;
    }

    /***
     * Method to create a file and write to a file
     * @param fileName name of the file
     * @param text Text to be written to the file
     */
    public static void createAndWriteTextToAFile(String text, String fileName) {
        try {
            File file = new File(fileName);
            if(file.createNewFile()) {
                logMessagesAndAddThemToReport("New file created -" + fileName, "info");
            }
            else {
                logMessagesAndAddThemToReport("File could not be created or already exists - " + fileName, "info");
            }
            FileWriter fileWriter = new FileWriter(fileName, false);
            fileWriter.write(text);
            fileWriter.close();

        } catch (Exception e) {
            e.printStackTrace();
            logExceptionsToTheReport(e);
        }

    }

    /***
     * Method to read contents of a file
     * @param fileName name of the file
     */
    public static String[] readFile(String fileName) {
        String[] text = new String[10];
        try {
            File file = new File(fileName);
            Scanner scanner = new Scanner(file);
            int i = 0;
            while(scanner.hasNextLine()) {
                text[i] = scanner.nextLine();
                i++;
            }
        } catch (Exception e) {
            e.printStackTrace();
            logExceptionsToTheReport(e);
        }
        return text;
    }

    /***
     * Method to delete the logged in users details file
     */
    public static void deleteLoggedInUserDetailsFile() {
        deleteAFile(fileNameForLoggedInUserDetails);

    }

    /***
     * Method to delete a file
     * @param fileName name of the file
     */
    public static void deleteAFile(String fileName) {
        try {
            File file = new File(fileName);
            if(file.exists()) {
                if(file.delete()) {
                    logMessagesAndAddThemToReport(fileName + " file deleted", "info");
                } else {
                    logMessagesAndAddThemToReport("File could not be deleted - " + fileName, "info");
                }

            } else {
                logMessagesAndAddThemToReport("File does not exist", "info");
            }
        } catch (Exception e) {
            e.printStackTrace();
            logExceptionsToTheReport(e);
        }

    }

    /***
     * Method to check if a file exists
     * @return status if the file exists or not
     */
    public static Boolean checkIfLoggedInUserDetailsFileExists() {
        Boolean status = null;
        try {
            File file = new File(fileNameForLoggedInUserDetails);
            if(file.exists()) {
                status = Boolean.TRUE;
            }
            else if(!file.exists()) {
                status = Boolean.FALSE;
            }
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }
        return status;

    }

}
