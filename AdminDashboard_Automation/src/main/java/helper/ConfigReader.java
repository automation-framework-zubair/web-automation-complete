package helper;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConfigReader {

    public String url;
    public String forgotPasswordEmail;
    public String refreshToken_GMailAPI;
    public String clientID_GMailAPI;
    public String clientSecret_GMailAPI;
    public String rendrUrl;
    public String spreadSheetUrl;
    public String refreshToken_sheetsAPI;
    public String clientID_sheetsAPI;
    public String clientSecret_sheetsAPI;
    public String sheetNameForLoginModule;
    public String sheetNameForDashboardModule;
    public String sheetNameForImportValidation;
    public String sheetNameForPatientModule;
    public String sheetNameForAmplifiersModule;
    public String sheetNameForDevicesModule;
    public String sheetNameForFacilitiesModule;
    public String sheetNameForStudiesModule;
    public String sheetNameForUserModule;
    public String sheetNameForCharacterConstraints;

    public ConfigReader() {
        Properties prop = new Properties();
        FileInputStream input = null;
        try {
            input = new FileInputStream("src/main/resources/config.properties");
            prop.load(input);
            url = prop.getProperty("url");
            forgotPasswordEmail = prop.getProperty("forgotPasswordEmail");
            refreshToken_GMailAPI = prop.getProperty("refreshToken_GMailAPI");
            clientID_GMailAPI = prop.getProperty("clientID_GMailAPI");
            clientSecret_GMailAPI = prop.getProperty("clientSecret_GMailAPI");
            rendrUrl = prop.getProperty("rendrUrl");
            spreadSheetUrl = prop.getProperty("spreadSheetUrl");
            refreshToken_sheetsAPI = prop.getProperty("refreshToken_sheetsAPI");
            clientID_sheetsAPI = prop.getProperty("clientID_sheetsAPI");
            clientSecret_sheetsAPI = prop.getProperty("clientSecret_sheetsAPI");
            sheetNameForLoginModule = prop.getProperty("sheetNameForLoginModule");
            sheetNameForDashboardModule = prop.getProperty("sheetNameForDashboardModule");
            sheetNameForImportValidation = prop.getProperty("sheetNameForImportValidation");
            sheetNameForPatientModule = prop.getProperty("sheetNameForPatientModule");
            sheetNameForAmplifiersModule = prop.getProperty("sheetNameForAmplifierModule");
            sheetNameForDevicesModule = prop.getProperty("sheetNameForDeviceModule");
            sheetNameForFacilitiesModule = prop.getProperty("sheetNameForFacilitiesModule");
            sheetNameForStudiesModule = prop.getProperty("sheetNameForStudiesModule");
            sheetNameForUserModule = prop.getProperty("sheetNameForUserModule");
            sheetNameForCharacterConstraints = prop.getProperty("sheetNameForCharacterConstraints");
        } catch (IOException ex) {
            ex.printStackTrace();
            CommonUtility.logExceptionsToTheReport(ex);
        }

    }

}
