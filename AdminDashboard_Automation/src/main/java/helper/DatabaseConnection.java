package helper;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import org.testng.Assert;

public class DatabaseConnection {
    private static DatabaseConnection instance;
    private static Connection connection;
    private Object[][] databaseData = null;
    private String serverName = "";
    private String databaseName = "";
    private String userName = "";
    private String password = "";
    private String url = "jdbc:sqlserver://";

    /***
     * This method generates the database connection url by getting the database credentials from the
     * getDatabaseCredentials method
     */
    private void generateDatabaseConnectionUrl() {
        databaseData = DataReader.getDatabaseCredentials("databaseCredentials.csv",
                "Credentials");
        serverName = (String) databaseData[0][1];
        databaseName = (String) databaseData[0][2];
        userName = (String) databaseData[0][3];
        password = (String) databaseData[0][4];
        url = url + serverName + ";databaseName=" + databaseName + ";user=" + userName + ";password=" + password;
    }

    /***
     * Connects to the Database
     */
    private DatabaseConnection() {
        try {
            generateDatabaseConnectionUrl();
            DriverManager.registerDriver(new com.microsoft.sqlserver.jdbc.SQLServerDriver());
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            connection = DriverManager.getConnection(url);
            Logger.databaseLog("Connection established");
        } catch (ClassNotFoundException | SQLException e) {
            CommonUtility.logExceptionsToTheReport(e);
            Assert.fail("Database connection Creation failed : " + e.getMessage());
            Logger.databaseLogWarn("Database connection Creation failed : " + e.getMessage());
        }
    }

    /***
     * @return connection : Returns the connection
     */
    public Connection getConnection() {
        return connection;
    }

    public static DatabaseConnection getInstance() throws SQLException {
        if (instance == null) {
            instance = new DatabaseConnection();
        }
        else if (instance.getConnection().isClosed()) {
            instance = new DatabaseConnection();
        }
        return instance;
    }

}
