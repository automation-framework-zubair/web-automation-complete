package helper;
import com.squareup.okhttp.*;
import net.bytebuddy.matcher.CollectionOneToOneMatcher;
import org.json.JSONObject;
import org.testng.Assert;

import java.io.IOException;


public class EmailUtility {
    GoogleAPIUtility googleAPIUtility = new GoogleAPIUtility();
    ConfigReader configReader = new ConfigReader();
    String refreshToken;
    String clientID;
    String clientSecret;

    public EmailUtility() {
        refreshToken = configReader.refreshToken_GMailAPI;
        clientID = configReader.clientID_GMailAPI;
        clientSecret = configReader.clientSecret_GMailAPI;

    }

    /***
     * Method to get a list of messages (after searching using a query) by calling the GET message endpoint
     * @param email Email of the concerned user
     * @param query Query string with which the message is going to be searched
     * @return returns Message list
     */
    public String getMessagesList(String email, String query) {
        OkHttpClient client = new OkHttpClient();
        Response response = null;
        String responseBody = null;

        Request request = new Request.Builder()
                .url("https://www.googleapis.com/gmail/v1/users/" + email + "/messages?q=" + query)
                .get()
                .addHeader("Authorization", "Bearer " + googleAPIUtility.getNewAccessToken(refreshToken, clientID, clientSecret))
                .addHeader("cache-control", "no-cache")
                .build();

        try {
            response = client.newCall(request).execute();
            responseBody = response.body().string();
            if(response.isSuccessful()) {
                Assert.assertEquals(response.code(), 200, "Expected code did not match");
                CommonUtility.logMessagesAndAddThemToReport("Get MessageID was successful", "info");
            }
            else {
                Assert.fail("Get MessageID was not successful");
            }
        } catch (IOException e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }
        return responseBody;

    }

    /***
     * Method to get the full message  by ID
     * @param email email address of the concerned user
     * @[param id ID of the message
     * @return the whole message in base64url encoded format
     */
    public String getFullMessage(String email, String id) {
        OkHttpClient client = new OkHttpClient();
        Response response = null;
        String responseBody = null;

        Request request = new Request.Builder()
                .url("https://www.googleapis.com/gmail/v1/users/" + email + "/messages/" + id)
                .get()
                .addHeader("Authorization", "Bearer " + googleAPIUtility.getNewAccessToken(refreshToken, clientID, clientSecret))
                .addHeader("cache-control", "no-cache")
                .build();


        try {
            response = client.newCall(request).execute();
            responseBody = response.body().string();
            if(response.isSuccessful()) {
                Assert.assertEquals(response.code(), 200, "Expected code did not match");
                CommonUtility.logMessagesAndAddThemToReport("Get full message was successful", "info");
            }
            else {
                Assert.fail("Get full message was not successful");
            }
        } catch (IOException e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }
        return responseBody;

    }

}
