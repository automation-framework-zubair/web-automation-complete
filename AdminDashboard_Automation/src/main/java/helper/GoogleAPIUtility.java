package helper;

import com.squareup.okhttp.*;
import org.json.JSONObject;
import org.testng.Assert;

import java.io.IOException;

public class GoogleAPIUtility {

    /***
     * Method to get a new access Token by calling the token endpoint
     * @param clientID client ID of the project
     * @param refreshToken refresh token for getting a new access token
     * @param clientSecret client secret of the project
     * @return Access Token
     */
    public String getNewAccessToken(String refreshToken, String clientID, String clientSecret) {
        OkHttpClient client = new OkHttpClient();
        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, "{\n    \"" +
                "refresh_token\": \"" + refreshToken + "\",\n    \"" +
                "client_id\": \"" + clientID + "\",\n    \"" +
                "client_secret\": \"" + clientSecret + "\",\n    \"" +
                "grant_type\": \"refresh_token\"\n}");
        Request request = new Request.Builder()
                .url("https://www.googleapis.com/oauth2/v4/token")
                .post(body)
                .addHeader("Content-Type", "application/json")
                .addHeader("Accept", "*/*")
                .addHeader("Cache-Control", "no-cache")
                .addHeader("Host", "www.googleapis.com")
                .build();

        String responseBody = null;
        try {
            Response response = client.newCall(request).execute();
            responseBody = response.body().string();
            if(response.isSuccessful()) {
                Assert.assertEquals(response.code(), 200, "Expected code did not match");
                CommonUtility.logMessagesAndAddThemToReport("Get Access Token was successful", "info");
            }
            else {
                CommonUtility.logMessagesAndAddThemToReport(responseBody, "warn");
                Assert.fail("Get Access Token was not successful");
            }
        } catch (IOException e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }
        assert responseBody != null;
        JSONObject jsonObject = new JSONObject(responseBody);
        String accessToken = jsonObject.getString("access_token");
        CommonUtility.logMessagesAndAddThemToReport("Access Token : " + accessToken, "info");
        return accessToken;

    }

}
