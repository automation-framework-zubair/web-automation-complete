package helper;

import com.squareup.okhttp.*;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;

import java.util.ArrayList;
import java.util.List;

public class SpreadsheetUtility {
    GoogleAPIUtility googleAPIUtility = new GoogleAPIUtility();
    ConfigReader configReader = new ConfigReader();
    String refreshToken;
    String clientID;
    String clientSecret;

    public SpreadsheetUtility() {
        refreshToken = configReader.refreshToken_sheetsAPI;
        clientID = configReader.clientID_sheetsAPI;
        clientSecret = configReader.clientSecret_sheetsAPI;
    }

    /***
     * Method to call the GET spreadsheets.values.get endpoint
     * @param range range of the cells for which the values have to be obtained
     * @param spreadsheetID ID of the spreadsheet
     * @return Response body of the request
     */
    public String get_getSpreadsheetValues(String range, String spreadsheetID) {
        String responseBody = null;
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url("https://sheets.googleapis.com/v4/spreadsheets/" + spreadsheetID + "/values/" + range + "?majorDimension=ROWS&valueRenderOption=FORMATTED_VALUE")
                .method("GET", null)
                .addHeader("Authorization", "Bearer " + googleAPIUtility.getNewAccessToken(refreshToken, clientID, clientSecret))
                .build();
        try {
            Response response = client.newCall(request).execute();
            responseBody = response.body().string();
            CommonUtility.logMessagesAndAddThemToReport("Response body : " + responseBody, "info");
            if(response.isSuccessful()) {
                Assert.assertEquals(response.code(), 200, "Expected code did not match");
                CommonUtility.logMessagesAndAddThemToReport("Get cell values was successful", "info");
            }
            else {
                Assert.fail("Get cell values was not successful");
            }
        } catch (Exception e) {
            CommonUtility.logExceptionsToTheReport(e);
            e.printStackTrace();
        }
        return responseBody;
    }

    /***
     * Method to get cell values by a particular range
     * @param spreadsheetID ID of the spread sheet
     * @param range range of the cells for which the values have to be obtained
     * @return List of values
     */
    public List<String> readCellValuesByRange(String range, String spreadsheetID) {
        String response = get_getSpreadsheetValues(range, spreadsheetID);
        JSONObject jsonObject = new JSONObject(response);
        List<String> valuesObtained = new ArrayList<>();
        JSONArray values = jsonObject.getJSONArray("values");
        for(int i=0;i<values.length();i++) {
            JSONArray ithJsonValue = (JSONArray) values.get(i);
            String ithValue = (String) ithJsonValue.get(0);
            valuesObtained.add(ithValue);
        }
        return valuesObtained;
    }

    /***
     * Method to call the PUT spreadsheets.values.update endpoint
     * @param requestBody request body of the request
     * @param range range of the cells to be updated
     * @param spreadsheetID ID of the spreadsheet
     * @return Response body of the request
     */
    public String put_updateSpreadsheetValues(String requestBody, String range, String spreadsheetID) {
        String responseBody = null;
        OkHttpClient client = new OkHttpClient();
        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, requestBody);
        Request request = new Request.Builder()
                .url("https://sheets.googleapis.com/v4/spreadsheets/" + spreadsheetID + "/values/" + range + "?valueInputOption=USER_ENTERED")
                .method("PUT", body)
                .addHeader("Authorization", "Bearer " + googleAPIUtility.getNewAccessToken(refreshToken, clientID, clientSecret))
                .addHeader("Content-Type", "application/json")
                .build();
        try {
            Response response = client.newCall(request).execute();
            responseBody = response.body().string();
            CommonUtility.logMessagesAndAddThemToReport("Response body : " + responseBody, "info");
            if(response.isSuccessful()) {
                Assert.assertEquals(response.code(), 200, "Expected code did not match");
                CommonUtility.logMessagesAndAddThemToReport("Get cell values was successful", "info");
            }
            else {
                Assert.fail("Put cell values was not successful");
            }
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }
        return responseBody;

    }

    /***
     * Method to generate request body for updating one cell
     * @param range cell address
     * @param cellValue value to be inserted to the cell
     * @return Json request body
     */
    public String generateRequestBodyToUpdateOneCell(String range, String cellValue) {
        JSONArray values = new JSONArray();
        JSONArray value1 = new JSONArray();
        value1.put(cellValue);
        values.put(value1);
//        CommonUtility.logMessagesAndAddThemToReport("Values : " + values, "info");
        JSONObject requestJson = new JSONObject();
        requestJson.put("majorDimension", "ROWS");
        requestJson.put("range", range);
        requestJson.put("values", values);
        CommonUtility.logMessagesAndAddThemToReport("Request body : " + requestJson, "info");
        return requestJson.toString();

    }

    /***
     * Method to update cells by a range
     * @param range range of cells to be updated
     * @param requestBody request body for the PUT spreadsheet.values.update endpoint
     * @param spreadsheetID ID of the spreadsheet
     * @return Number of cells updated
     */
    public String updateCellsByRange(String range, String spreadsheetID, String requestBody) {
        String response = put_updateSpreadsheetValues(requestBody, range, spreadsheetID);
        JSONObject responseJson = new JSONObject(response);
        return responseJson.get("updatedCells").toString();

    }

    /***
     * Method to get cell address by a search value/key
     * @param searchValue value for which the search/lookup is performed
     * @param rangeToBeSearched row/column range to be searched
     * @param searchColumnOrRow (column/row) search either column or row
     * @param spreadSheetID ID of the spreadsheet
     * @return cell address of the searched value
     */
    public String getCellAddressBySearchValue(String searchValue, String rangeToBeSearched, String searchColumnOrRow, String spreadSheetID, String sheetName) {
        String cellAddressToWriteForFormula = sheetName + "W1";
        String requestBody = null;
        if(searchColumnOrRow.toLowerCase().equals("row")) {
            requestBody = generateRequestBodyToUpdateOneCell(cellAddressToWriteForFormula, "=CELL(\"address\", INDEX(" + rangeToBeSearched + ", 0, MATCH(\"" + searchValue + "\", " + rangeToBeSearched + ", 0)))");
        }
        else if(searchColumnOrRow.toLowerCase().equals("column")) {
            requestBody = generateRequestBodyToUpdateOneCell(cellAddressToWriteForFormula, "=CELL(\"address\", INDEX("+ rangeToBeSearched + ", MATCH(\"" + searchValue + "\", "+ rangeToBeSearched + ", 0), 0))");
        }

        String cellsUpdated = updateCellsByRange(cellAddressToWriteForFormula, spreadSheetID, requestBody);
        if(cellsUpdated.equals("1")) {
            CommonUtility.logMessagesAndAddThemToReport("Successfully updated", "info");
        }
        List<String> cellAddress = readCellValuesByRange(cellAddressToWriteForFormula, spreadSheetID);
        return cellAddress.get(0);

    }

    /***
     * Method to get the range containing the column headers
     * @param spreadSheetID ID of the spreadsheet
     * @param sheetName name of the sheet
     * @return range containing the column headers
     */
    public String getRangeContainingColumnHeaders(String spreadSheetID, String sheetName) {
        // GET CELL ADDRESS OF COLUMN HEADER ID
        String cellAddressOfColumnHeaderID = getCellAddressBySearchValue("ID", "$A$1:$A$1000", "column", spreadSheetID, sheetName);
        String[] rowNumberOfColumnHeaderID = cellAddressOfColumnHeaderID.split("\\$");
        return cellAddressOfColumnHeaderID + ":$W$"+ rowNumberOfColumnHeaderID[2];
//        CommonUtility.logMessagesAndAddThemToReport("Range containing the column headers : " + rangeContainingColumnHeaders,"info");

    }

    /***
     * Method to get the range that contains all the TC IDs
     * @param sheetName name of the sheet
     * @param spreadSheetID ID of the spreadsheet
     * @param rangeContainingColumnHeaders The range that is to be searched for Auto TC ID
     * @return Range containing all the TC IDs
     */
    public String getRangeContainingAllTCIDs(String rangeContainingColumnHeaders, String spreadSheetID, String sheetName) {
        // GET CELL ADDRESS OF AUTO TC ID
        String cellAddressOfAutoTCID = getCellAddressBySearchValue("Auto. TC ID", rangeContainingColumnHeaders, "row", spreadSheetID, sheetName);
//        CommonUtility.logMessagesAndAddThemToReport("Cell address of auto TC ID : " + cellAddressOfAutoTCID, "info");
        String[] columnNameOfAutoTCID = cellAddressOfAutoTCID.split("\\$");
//        CommonUtility.logMessagesAndAddThemToReport("Column name of auto TC ID : " + columnNameOfAutoTCID[1], "info");
        String rangeContainingAllTCIDs = cellAddressOfAutoTCID + ":$" + columnNameOfAutoTCID[1] + "$1000";
        return rangeContainingAllTCIDs;

    }

    /***
     * Method to get column name of Result summary header
     * @param sheetName name of the sheet
     * @param spreadSheetID ID of the spreadsheet
     * @param rangeContainingColumnHeaders The range that is to be searched for Result summary header
     * @return column name of result summary header
     */
    public String getColumnNameOfResultSummaryHeader(String rangeContainingColumnHeaders, String spreadSheetID, String sheetName) {
        // GET CELL ADDRESS OF RESULT
        String cellAddressOfResult = getCellAddressBySearchValue("Result Summary", rangeContainingColumnHeaders, "row", spreadSheetID, sheetName);
        String[] columnNameOfResult = cellAddressOfResult.split("\\$");
//        CommonUtility.logMessagesAndAddThemToReport("Column name of Result Summary : " + columnNameOfResult[1], "info");
        return columnNameOfResult[1];

    }


    /***
     * Method to update test result to the appropriate cell in google sheets
     * @param tcID ID of the test case
     * @param testResult Result of the test case (passed/failed/untested)
     * @param spreadSheetID ID of the spreadsheet
     */
    public void updateTestResultToSpreadSheet(String tcID, String testResult, String spreadSheetID, String sheetName) {
        CommonUtility.logCreateNode("UpdateTestResultToSpreadSheet");
        String rangeContainingColumnHeaders = getRangeContainingColumnHeaders(spreadSheetID, sheetName);
        String rangeContainingAllTCIDs = getRangeContainingAllTCIDs(rangeContainingColumnHeaders, spreadSheetID, sheetName);

        // GET CELL ADDRESS OF TC##
        String cellAddressOfTCID = getCellAddressBySearchValue(tcID, rangeContainingAllTCIDs, "column", spreadSheetID, sheetName);
        if(cellAddressOfTCID.equals("#N/A")) {
            CommonUtility.logMessagesAndAddThemToReport("Cell address of TC ID : " + tcID + " was not found", "warn");
        }
        else {
            String[] rowNumberOfTCID = cellAddressOfTCID.split("\\$");
            CommonUtility.logMessagesAndAddThemToReport("Row number of TC ID : " + rowNumberOfTCID[2], "info");
            String columnNameOfResult = getColumnNameOfResultSummaryHeader(rangeContainingColumnHeaders, spreadSheetID, sheetName);

            // UPDATES RESULT OF TC01
            String cellToUpdate = sheetName + columnNameOfResult + rowNumberOfTCID[2];

            String requestBody = generateRequestBodyToUpdateOneCell(cellToUpdate, testResult);
            String numOfCellsUpdated = updateCellsByRange(cellToUpdate, spreadSheetID, requestBody);
            if(numOfCellsUpdated.equals("1")) {
                CommonUtility.logMessagesAndAddThemToReport("Cell updated : " + cellToUpdate, "pass");
            }
        }
    }

    /***
     * Method to get corresponding sheet name for method names in Page & Action access modules
     * */
    public String getSheetNameForMethodName(String methodName) {
        String sheetName = "";
        if(methodName.toLowerCase().contains("dashboard")) {
            sheetName = configReader.sheetNameForDashboardModule;
        }
        else if(methodName.toLowerCase().contains("patientmodule") || methodName.toLowerCase().contains("patientlist") ||
                methodName.toLowerCase().contains("patientcreate") || methodName.toLowerCase().contains("patientedit")) {
            sheetName = configReader.sheetNameForPatientModule;
        }
        else if(methodName.toLowerCase().contains("importstudy")) {
            sheetName = configReader.sheetNameForImportValidation;
        }
        else if(methodName.toLowerCase().contains("facilitymodule") || methodName.toLowerCase().contains("facilitylist") ||
                methodName.toLowerCase().contains("facilitycreate") || methodName.toLowerCase().contains("facilityedit")) {

            sheetName = configReader.sheetNameForFacilitiesModule;
        }
        else if(methodName.toLowerCase().contains("amplifiermodule") ||methodName.toLowerCase().contains("amplifierlist") ||
                methodName.toLowerCase().contains("amplifiercreate") || methodName.toLowerCase().contains("amplifieredit")) {
            sheetName = configReader.sheetNameForAmplifiersModule;
        }
        else if(methodName.toLowerCase().contains("devicemodule") || methodName.toLowerCase().contains("devicelist") ||
                methodName.toLowerCase().contains("devicecreate") || methodName.toLowerCase().contains("deviceedit")) {
            sheetName = configReader.sheetNameForDevicesModule;
        }
        else if(methodName.toLowerCase().contains("usermodule") || methodName.toLowerCase().contains("userlist") ||
                methodName.toLowerCase().contains("usercreate") || methodName.toLowerCase().contains("useredit")) {
            sheetName = configReader.sheetNameForUserModule;
        }
        else if(methodName.toLowerCase().contains("studymodule")) {
            sheetName = configReader.sheetNameForStudiesModule;
        }
        else {
            sheetName = "Automation";
        }
        return sheetName;
    }

    /***
     * Method to get corresponding sheet name for automation suite name
     * @return sheet name
     */
    public String getSheetNameForSuiteName(String suiteName, String methodName) {
        String sheetName = "";
        if(suiteName.toLowerCase().equals("login module")) {
            sheetName = configReader.sheetNameForLoginModule;
        }
        else if(suiteName.toLowerCase().equals("dashboard module")) {
            sheetName = configReader.sheetNameForDashboardModule;
        }
        else if(suiteName.toLowerCase().equals("import validation")) {
            sheetName = configReader.sheetNameForImportValidation;
        }
        else if(suiteName.toLowerCase().equals("patient module")) {
            sheetName = configReader.sheetNameForPatientModule;
        }
        else if(suiteName.toLowerCase().equals("user deletion using super admin") ||
                suiteName.toLowerCase().equals("user deletion using support") ||
                suiteName.toLowerCase().equals("user deletion using facility admin") ||
                suiteName.toLowerCase().equals("user deletion") || suiteName.toLowerCase().equals("user create page") ||
                suiteName.toLowerCase().equals("user edit page") || suiteName.toLowerCase().equals("users page")) {
            sheetName = configReader.sheetNameForUserModule;
        }
        else if(suiteName.toLowerCase().equals("character constraints")) {
            sheetName = configReader.sheetNameForCharacterConstraints;
        }
        else if(suiteName.toLowerCase().equals("page access")) {
            sheetName = getSheetNameForMethodName(methodName);
        }
        else if(suiteName.toLowerCase().contains("action access")) {
            sheetName = getSheetNameForMethodName(methodName);
        }
        else {
            sheetName = "Automation";
        }
        return sheetName;
    }

}
