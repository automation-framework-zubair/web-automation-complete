package helper;

import org.testng.Assert;
import pages.ui.BaseClass;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class TestDataDeletion {

    private static Connection con;

    /***
     * Method to get database connection
     */
    public static void initiateConnection() {
        try {
            DatabaseConnection instance = DatabaseConnection.getInstance();
            con = instance.getConnection();
            Logger.databaseLog("Connection returned");
        }
        catch (SQLException e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
            Assert.fail("Connection not returned");
        }

    }

    /***
     * Method to delete patient by id
     * @param id ID of the patient to be deleted
     */
    public static void patientDeleteId(String id){
        initiateConnection();
        Statement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.createStatement();
            rs = stmt.executeQuery("select id from dbo.Studies where PatientID = '" + id + "'");
            String[] idDelete = new String[50];
            int i = 0;
            while (rs.next()) {
                idDelete[i] = "'" + rs.getString("id") + "'";
                System.out.println(idDelete[i]);
                i++;
            }
            for (String s : idDelete) {
                stmt.executeUpdate("delete from dbo.StudyRecordingVideos where StudyRecording_StudyID = " + s);
                stmt.executeUpdate("delete from dbo.StudyRecordings where studyid = " + s);
            }
            stmt.executeUpdate("delete from dbo.Studies where PatientID = '" + id + "'");
            Logger.databaseLog("Now deleting patient with id = " + id);
            int numOfRows = stmt.executeUpdate("delete from dbo.Patients where id = '" + id + "'");
            if(numOfRows == 1) {
                Logger.databaseLog("Deletion of patient successful");
                CommonUtility.logMessagesAndAddThemToReport("Deletion of patient successful", "pass");
            }
            else {
                Logger.databaseLogWarn("Deletion of patient unsuccessful with id = " + id);
                CommonUtility.logMessagesAndAddThemToReport("Deletion of patient unsuccessful with id = " + id, "fail");
            }
            Assert.assertEquals(numOfRows, 1, "Deletion of patient unsuccessful with id = " + id);
        }
        catch (SQLException e) {
            Logger.databaseLogWarn("Deletion of patient unsuccessful with id = " + id);
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
            Assert.fail("Deletion of patient unsuccessful");
        }
        finally {
            if(rs!=null) {
                try {
                    rs.close();
                }
                catch (SQLException e) {
                    CommonUtility.logExceptionsToTheReport(e);
                    e.printStackTrace();
                }
            }
            if(stmt!=null) {
                try {
                    stmt.close();
                }
                catch (SQLException e) {
                    CommonUtility.logExceptionsToTheReport(e);
                    e.printStackTrace();
                }
            }
            if(con!=null) {
                try {
                    if(con.isClosed()) {
                        Logger.databaseLog("Connection is already closed");
                    }
                    else {
                        con.close();
                        Logger.databaseLog("Connection closed");
                    }
                }
                catch (SQLException e) {
                    e.printStackTrace();
                    CommonUtility.logExceptionsToTheReport(e);
                }
            }
        }
    }

    /***
     * Method to delete amplifier by id
     * @param id ID of the amplifier to be deleted
     */
    public static void amplifierDeleteId(String id) {
        initiateConnection();
        Statement stmt = null;
        try {
            stmt = con.createStatement();
            String sql = "delete from dbo.Amplifiers where id ='" + id + "'";
            Logger.databaseLog("Now deleting amplifier with id = " + id);
            int numOfRows = stmt.executeUpdate(sql);
            if (numOfRows == 1) {
                Logger.databaseLog("Deletion of amplifier successful");
                CommonUtility.logMessagesAndAddThemToReport("Deletion of amplifier successful", "pass");
            }
            else {
                Logger.databaseLogWarn("Deletion of amplifier unsuccessful");
                CommonUtility.logMessagesAndAddThemToReport("Deletion of amplifier unsuccessful with id = " + id, "fail");
            }
            Assert.assertEquals(numOfRows, 1, "Deletion unsuccessful of amplifier with id = " + id);
        }
        catch (SQLException e) {
            Logger.databaseLogWarn("Deletion unsuccessful of amplifier with id = " + id);
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
            Assert.fail("Deletion unsuccessful of amplifier with id = " + id);
        }
        finally {
            if(stmt!=null) {
                try {
                    stmt.close();
                }
                catch (SQLException e) {
                    e.printStackTrace();
                    CommonUtility.logExceptionsToTheReport(e);
                }
            }
            if(con!=null) {
                try {
                    if(con.isClosed()) {
                        Logger.databaseLog("Connection is already closed");
                    }
                    else {
                        con.close();
                        Logger.databaseLog("Connection closed");
                    }
                }
                catch (SQLException e) {
                    e.printStackTrace();
                    CommonUtility.logExceptionsToTheReport(e);
                }
            }
        }
    }

    /***
     * Method to delete device by ID
     * @param id ID of the device to be deleted
     */
    public static void deviceDeleteId(String id) {
        initiateConnection();
        Statement stmt = null;
        try {
            stmt = con.createStatement();
            String sql = "delete from dbo.Devices where id ='" + id + "'";
            Logger.databaseLog("Now deleting device with id = " + id);
            int numOfRows = stmt.executeUpdate(sql);
            if (numOfRows == 1) {
                Logger.databaseLog("Deletion of device successful");
                CommonUtility.logMessagesAndAddThemToReport("Deletion of device successful", "pass");
            }
            else {
                Logger.databaseLogWarn("Deletion of device unsuccessful");
                CommonUtility.logMessagesAndAddThemToReport("Deletion of device unsuccessful with id = " + id, "fail");
            }
            Assert.assertEquals(numOfRows, 1, "Deletion unsuccessful of device with id = " + id);
        }
        catch (SQLException e) {
            Logger.databaseLogWarn("Deletion unsuccessful of device with id = " + id);
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
            Assert.fail("Deletion of Device unsuccessful");
        }
        finally {
            if(stmt!=null) {
                try {
                    stmt.close();
                }
                catch (SQLException e) {
                    CommonUtility.logExceptionsToTheReport(e);
                    e.printStackTrace();
                }
            }
            if(con!=null) {
                try {
                    if(con.isClosed()) {
                        Logger.databaseLog("Connection is already closed");
                    }
                    else {
                        con.close();
                        Logger.databaseLog("Connection closed");
                    }
                }
                catch (SQLException e) {
                    e.printStackTrace();
                    CommonUtility.logExceptionsToTheReport(e);
                }
            }
        }
    }

    /***
     * Method to delete facility by id
     * @param id ID of the facility to be deleted
     */
    public static void facilityDeleteId(String id) {
        initiateConnection();
        Statement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.createStatement();
            rs = stmt.executeQuery("select id from dbo.StudyRecordingMappings where " +
                    "facilityid = '" + id + "'");
            String[] idDelete = new String[50];
            int i = 0;
            while (rs.next()) {
                idDelete[i] = "'" + rs.getString("id") + "'";
                System.out.println(idDelete[i]);
                i++;
            }
            stmt.executeUpdate("delete from dbo.StudyRecordingVideos where facilityId = '" + id + "'");
            for (String s : idDelete) {
                stmt.executeUpdate("delete from dbo.StudyRecordingMappingItems where " +
                        "studyrecordingmappingid = " + s);
            }
            stmt.executeUpdate("delete from dbo.CalibrationPrograms where facilityId = '" + id + "'");
            stmt.executeUpdate("delete from dbo.Patients where facilityId = '" + id + "'");
            stmt.executeUpdate("delete from dbo.StudyRecordingMappings where facilityId = '" + id + "'");
            stmt.executeUpdate("delete from dbo.PhoticPrograms where facilityId = '" + id + "'");
            Logger.databaseLog("Now deleting facility with id = " + id);
            stmt.executeUpdate("delete from dbo.FacilityUsers where facilityId = '" + id + "'");
            int numOfRows = stmt.executeUpdate("delete from dbo.Facilities where id = '" + id + "'");
            if (numOfRows == 1) {
                Logger.databaseLog("Deletion of facility successful");
                CommonUtility.logMessagesAndAddThemToReport("Deletion of facility successful", "pass");
            }
            else {
                Logger.databaseLogWarn("Deletion of facility unsuccessful with id = " + id);
                CommonUtility.logMessagesAndAddThemToReport("Deletion of facility unsuccessful with id = " + id, "fail");
            }
            Assert.assertEquals(numOfRows, 1, "Deletion of facility unsuccessful with id = " + id);
        }
        catch (SQLException e) {
            Logger.databaseLogWarn("Deletion of facility unsuccessful with id = " + id);
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
            Assert.fail("Deletion of facility unsuccessful");
        }
        finally {
            if(rs!=null) {
                try {
                    rs.close();
                }
                catch (SQLException e) {
                    e.printStackTrace();
                    CommonUtility.logExceptionsToTheReport(e);
                }
            }
            if(stmt!=null) {
                try {
                    stmt.close();
                }
                catch (SQLException e) {
                    e.printStackTrace();
                    CommonUtility.logExceptionsToTheReport(e);
                }
            }
            if(con!=null) {
                try {
                    if(con.isClosed()) {
                        Logger.databaseLog("Connection is already closed");
                    }
                    else {
                        con.close();
                        Logger.databaseLog("Connection closed");
                    }
                }
                catch (SQLException e) {
                    e.printStackTrace();
                    CommonUtility.logExceptionsToTheReport(e);
                }
            }
        }
    }

    /***
     * Method to delete user by id
     * @param id ID of the user to be deleted
     */
    public static void userDeleteId(String id) {
        initiateConnection();
        Statement stmt = null;
        try {
            stmt = con.createStatement();
            stmt.executeUpdate("delete from dbo.FacilityInvitations where invitedID = '" + id + "'");
            stmt.executeUpdate("delete from dbo.FacilityUsers where userid = '" + id + "'");
            stmt.executeUpdate("delete from dbo.SharedStudies where userid = '" + id + "'");
            stmt.executeUpdate("delete from dbo.UserProfiles where id = '" + id + "'");
            Logger.databaseLog("Now deleting user with id = " + id);
            stmt.executeUpdate("delete from dbo.AspNetUserRoles where userid = '" + id + "'");
            int numOfRows = stmt.executeUpdate("delete from dbo.AspNetUsers where id = '" + id + "'");
            if(numOfRows == 1) {
                Logger.databaseLog("Deletion of user successful");
                CommonUtility.logMessagesAndAddThemToReport("Deletion of user successful", "pass");
            }
            else {
                Logger.databaseLogWarn("Deletion of user unsuccessful with id = " + id);
                CommonUtility.logMessagesAndAddThemToReport("Deletion of user unsuccessful with id = " + id, "fail");
            }
            Assert.assertEquals(numOfRows, 1, "Deletion of user unsuccessful with id = " + id);
        }
        catch (SQLException e) {
            Logger.databaseLogWarn("Deletion of user unsuccessful with id = " + id);
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
            Assert.fail("Deletion of user unsuccessful");
        }
        finally {
            if(stmt!=null) {
                try {
                    stmt.close();
                }
                catch (SQLException e) {
                    e.printStackTrace();
                    CommonUtility.logExceptionsToTheReport(e);
                }
            }
            if(con!=null) {
                try {
                    if(con.isClosed()) {
                        Logger.databaseLog("Connection is already closed");
                    }
                    else {
                        con.close();
                        Logger.databaseLog("Connection closed");
                    }
                }
                catch (SQLException e) {
                    e.printStackTrace();
                    CommonUtility.logExceptionsToTheReport(e);
                }
            }
        }
    }
}
