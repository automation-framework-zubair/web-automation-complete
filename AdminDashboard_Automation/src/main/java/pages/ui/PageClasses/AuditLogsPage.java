package pages.ui.PageClasses;

import helper.CommonUtility;
import org.openqa.selenium.By;
import pages.ui.BaseClass;

public class AuditLogsPage extends BaseClass {

    // Start: Audit Logs page web locators
    By studyMonitoringIcon = By.xpath("//title[text()='Rendr Review - Study Monitoring History']/../..//descendant::i[contains(@class, 'icon-electrotek-monitoring mr-3')]");
    By allAuditLogsIcon = By.xpath("//i[@title='All audit logs']");
    By tableForAllAuditLogs = By.xpath("//title[text()='Rendr Review - Study Audit Logs']/../..//descendant::table");
    // End: Audit Logs page web locators

    /***
     * Method to verify if user is successfully navigated to the Audit Logs page
     */
    public void verifyAuditLogsPageNavigation(String auditLogsPageAccess) {
        verifyListPageNavigation(auditLogsPageAccess, studyMonitoringIcon, "Audit Logs Page Title");

    }

    /***
     * Method to click on All Audit logs button if it is present
     */
    public void clickOnAllAuditLogsButton(String allAuditLogsPageAccess) {
        if(allAuditLogsPageAccess.toLowerCase().equals("yes")) {
            clickOn(allAuditLogsIcon, 30, "All Audit Logs Button - ");
        }
        else if(allAuditLogsPageAccess.toLowerCase().equals("no")) {
            isNotExpectedElementVisible(allAuditLogsIcon, 10, "All Audit logs icon should not be present - ");
            CommonUtility.logMessagesAndAddThemToReport("User does not have access to All Audit Logs page ", "pass");
        }

    }

    /***
     * Method to check if all audit logs page navigation was successful
     */
    public void allAuditLogsPageNavigation(String allAuditLogsPageAccess) {
        verifyListPageNavigation(allAuditLogsPageAccess, tableForAllAuditLogs, "All Audit Logs Page ");
    }

}
