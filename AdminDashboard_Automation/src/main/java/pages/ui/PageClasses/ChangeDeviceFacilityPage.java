package pages.ui.PageClasses;

import org.openqa.selenium.By;
import org.testng.Assert;
import pages.ui.BaseClass;

public class ChangeDeviceFacilityPage extends BaseClass {

    // Start: Change Device Facility locators
    By pageTitle = By.xpath("//h3[text()='Change Device Facility']");
    By loadingIcon = By.xpath("//td[text()='Loading...']");
    By facilitySelectDropdown = By.id("Facility");
    By saveButton = By.xpath("//input[@type='submit' and @value='Save']");
    // End: Change Device Facility locators

    /***
     * Method to verify that the user has access to the Change Device Facility Page
     * @param changeDeviceFacility (yes/no)
     * */
    public void verifyChangeDeviceFacilityPageNavigation(String changeDeviceFacility) {
        isNotExpectedElementVisible(loadingIcon, 30, "Data load");
        verifyListPageNavigation(changeDeviceFacility, pageTitle, "Change Device Facility Page");

    }

    /***
     * Method to select the facility to which the device will belong to
     * @param actionAccess if user has access to change device facility
     * @param facilityName Name of the facility
     * */
    public void changeDeviceFacility(String actionAccess, String facilityName) {
        if(actionAccess.toLowerCase().equals("yes")) {
            Assert.assertTrue(isExpectedElementVisible(facilitySelectDropdown, 30, "Facility select dropdown"));
            clickOn(facilitySelectDropdown, 30, "click on facility selection dropdown");
            clickOn(By.xpath("//select[@id='Facility']//option[contains(text(),'" + facilityName + "')]"), 30, "select facility");
            clickOn(saveButton, 30, "click on save button");
            MatchToastMessageValue("Facility changed!");
        } else if(actionAccess.toLowerCase().equals("no")) {
            Assert.assertTrue(isNotExpectedElementVisible(pageTitle, 30, "Change Device Facility"));
        }
    }

}
