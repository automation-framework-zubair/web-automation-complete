package pages.ui.PageClasses.CreateEditPages;

import helper.CommonUtility;
import helper.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;

public class AmplifierCreatePage extends BaseClassCreateEdit {

    // Start: Amplifier create  screen web locators
    By pageTitle = By.xpath("//h3[text()='Create Amplifier']");
    By ampNameTextBox = By.id("Name");
    By serialNumberTextBox = By.id("SerialNumber");
    By saveButton = By.id("btn_submit");
    By bluetoothIDTextBox = By.id("BluetoothID");
    By usbIDTextBox = By.id("UsbID");
    // End: Amplifier create  screen web locators

    // Start: Amplifier create page web elements
    @FindBy(id = "AmplifierTypeID")
    WebElement amplifierTypeDropdown;

    @FindBy(id = "FacilityID")
    WebElement facilityNameDropdown;

    @FindBy(id = "Enabled")
    WebElement enabledDropDown;
    // End: Amplifier create page web elements


    /**
     * Method to verify if user is successfully navigated to the Create Amplifier page
     * @param createAmplifierPageAccess (yes/no)
     */
    public void verifyCreateAmplifierPageNavigation(String createAmplifierPageAccess) {
        verifyListPageNavigation(createAmplifierPageAccess, pageTitle, "Create Amplifier page");

    }

    /***
     * Implementing create amplifier functionality
     * @param createAmplifierValue
     * @param ampName
     * @param serialNumber
     * @param ampType
     * @param facilityName
     */
    public void createAmplifier(String createAmplifierValue, String ampName, String serialNumber, String bluetoothID, String usbID, String ampType, String enabled, String facilityName, String toastMessage) {
        if (createAmplifierValue.toLowerCase().equals("yes")) {
            try {
                insertValueToTextbox(ampNameTextBox, ampName, "Amplifier name" );
                insertValueToTextbox(serialNumberTextBox, serialNumber, "Serial Number");
                insertValueToTextbox(bluetoothIDTextBox, bluetoothID, "Bluetooth ID");
                insertValueToTextbox(usbIDTextBox, usbID, "USB ID");
                dropdownByVisibleText(amplifierTypeDropdown, ampType);
                dropdownByVisibleText(enabledDropDown, enabled);
                dropdownByVisibleText(facilityNameDropdown, facilityName);
                buttonClickByLocator(saveButton);
                MatchToastMessageValue(toastMessage);

            } catch (Exception e) {
                Assert.fail();
                e.printStackTrace();
                CommonUtility.logExceptionsToTheReport(e);
            }

        } else if (createAmplifierValue.toLowerCase().equals("no")) {
            try {
                Assert.assertTrue(isNotExpectedElementVisible(pageTitle, 10, "Amplifier not created - "));
            } catch (Exception e) {
                Assert.fail();
                e.printStackTrace();
                CommonUtility.logExceptionsToTheReport(e);
            }

        } else {
            CommonUtility.logMessagesAndAddThemToReport("Wrong text provided in the CSV file for create amplifier", "fail");
            Assert.fail();

        }

    }

    /**
     * Method to verify if the has successfully navigated to the create amplifier page (no param)
     */
    public void verifyCreateAmplifierPageNavigation() {
        try {
            Assert.assertTrue(isExpectedElementVisible(pageTitle, 10, "Create amplifier page"));
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Implementing type amplifier's name
     * @param ampName
     */
    public void typeAmplifierName(String ampName) {
        try {
            clearText(ampNameTextBox);
            sendKeys(ampNameTextBox, ampName, "Amplifier Name text box");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Implementing type amplifier's serial number
     * @param ampSerialNumber
     */
    public void typeSerialNumber(String ampSerialNumber) {
        try {
            clearText(serialNumberTextBox);
            sendKeys(serialNumberTextBox, ampSerialNumber, "Amplifier Serial number text box");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Implementing character length verification
     * @param textFieldName
     * @param expectedCharLength
     */
    public void verifyCharacterLengthCreateAmplifier(String textFieldName, int expectedCharLength){
        if(textFieldName.toLowerCase().equals("name")){
            verifyCharacterLength(ampNameTextBox, textFieldName, expectedCharLength);
        }
        else if(textFieldName.toLowerCase().equals("serial number")){
            verifyCharacterLength(serialNumberTextBox, textFieldName, expectedCharLength);
        }
        else {
            CommonUtility.logMessagesAndAddThemToReport("Wrong field name provided", "fail");
            Assert.fail();
        }

    }

}
