package pages.ui.PageClasses.CreateEditPages;

import helper.CommonUtility;
import helper.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;

public class AmplifierEditPage extends BaseClassCreateEdit {

    // Start: Amplifier edit  screen web locators
    By pageTitle = By.xpath("//h3[text()='Edit Amplifier']");
    By loadingIcon = By.xpath("//td[text()='Loading...']");
    By ampNameTextBox = By.id("Name");
    By serialNumberTextBox = By.id("SerialNumber");
    By saveButton = By.id("btn_submit");
    By bluetoothIDTextBox = By.id("BluetoothID");
    By usbIDTextBox = By.id("UsbID");
    // End: Amplifier edit  screen web locators

    // Start: Amplifier edit page web elements
    @FindBy(id = "AmplifierTypeID")
    WebElement amplifierTypeDropdown;

    @FindBy(id = "Enabled")
    WebElement enabledDropDown;
    // End: Amplifier edit page web elements

    /**
     * Method to verify if user is successfully navigated to the Edit Amplifier page
     * @param editAmplifierPageAccess (yes/no)
     */
    public void verifyEditAmplifierPageNavigation(String editAmplifierPageAccess){
        isNotExpectedElementVisible(loadingIcon, 30, "Data load");
        verifyListPageNavigation(editAmplifierPageAccess, pageTitle, "Edit Amplifier page");
    }

    /***
     * Implementing edit patient functionality
     * @param editAmplifierValue
     * @param ampName
     * @param serialNumber
     * @param ampType
     */
    public void editAmplifier(String editAmplifierValue, String ampName, String serialNumber, String bluetoothID, String usbID, String ampType, String enabled, String toastMessage) {
        if (editAmplifierValue.toLowerCase().equals("yes")) {
            try {
                insertValueToTextbox(ampNameTextBox, ampName, "Amplifier name");
                insertValueToTextbox(serialNumberTextBox, serialNumber, "Serial number");
                insertValueToTextbox(bluetoothIDTextBox, bluetoothID, "Bluetooth ID");
                insertValueToTextbox(usbIDTextBox, usbID, "USB ID");
                dropdownByVisibleText(amplifierTypeDropdown, ampType);
                dropdownByVisibleText(enabledDropDown, enabled);
                buttonClickByLocator(saveButton);
                MatchToastMessageValue(toastMessage);

            } catch (Exception e) {
                e.printStackTrace();
                CommonUtility.logExceptionsToTheReport(e);
            }

        } else if (editAmplifierValue.toLowerCase().equals("no")) {
            try {
                Assert.assertTrue(isNotExpectedElementVisible(pageTitle, 10, "Edit amplifier page should not appear - "));
            } catch (Exception e) {
                e.printStackTrace();
                CommonUtility.logExceptionsToTheReport(e);
            }

        } else {
            CommonUtility.logMessagesAndAddThemToReport("Wrong text provided in the CSV file for edit amplifier", "fail");
            Assert.fail();

        }

    }

    /**
     * Method to verify if user is successfully navigated to the edit amplifier page (without param)
     */
    public void verifyEditAmplifierPageNavigation( ){
        try {
            Assert.assertTrue(isExpectedElementVisible(pageTitle, 10, "Edit Amplifier page"));
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Implementing type amplifier's name
     * @param ampName
     */
    public void typeAmplifierName(String ampName) {
        try {
            clearText(ampNameTextBox);
            sendKeys(ampNameTextBox, ampName, "Amplifier Name text box");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Implementing type amplifier's serial number
     * @param ampSerialNumber
     */
    public void typeSerialNumber(String ampSerialNumber) {
        try {
            clearText(serialNumberTextBox);
            sendKeys(serialNumberTextBox, ampSerialNumber, "Amplifier Serial number text box");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Implementing character length verification
     * @param textFieldName
     * @param expectedCharLength
     */
    public void verifyCharacterLengthEditAmplifier(String textFieldName, int expectedCharLength){
        if(textFieldName.toLowerCase().equals("name")){
            verifyCharacterLength(ampNameTextBox, textFieldName, expectedCharLength);
        }
        else if(textFieldName.toLowerCase().equals("serial number")){
            verifyCharacterLength(serialNumberTextBox, textFieldName, expectedCharLength);
        }
        else {
            CommonUtility.logMessagesAndAddThemToReport("Wrong field name provided", "fail");
            Assert.fail();
        }

    }

}

