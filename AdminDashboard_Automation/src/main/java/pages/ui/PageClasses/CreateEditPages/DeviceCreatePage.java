package pages.ui.PageClasses.CreateEditPages;

import helper.CommonUtility;
import helper.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;

public class DeviceCreatePage extends BaseClassCreateEdit {

    // Start: Device create  screen web locators
    By pageTitle = By.xpath("//h3[text()='Create Device']");
    By deviceNameTextBox = By.id("Name");
    By partNumberTextBox = By.id("PartNumber");
    By configurationTextBox = By.id("Configuration");
    By serialNumberTextBox = By.id("SerialNumber");
    By saveButton = By.id("btn_submit");
    // End: Device create  screen web locators

    // Start: Device create page web elements
    @FindBy(id = "FacilityID")
    WebElement facilityNameDropdown;
    // End: Device create page web elements

    /**
     * Method to verify if user is successfully navigated to the Create Device page
     * @param createDevicePageAccess (Yes/No)
     */
    public void verifyCreateDevicePageNavigation(String createDevicePageAccess){
        verifyListPageNavigation(createDevicePageAccess, pageTitle, "Create Device page");

    }

    /***
     * Implementing create device functionality
     * @param createDeviceValue
     * @param deviceName
     * @param partNumber
     * @param configValue
     * @param serialNumber
     * @param facilityName
     * @param toastMessage
     */
    public void createDevice(String createDeviceValue, String deviceName, String partNumber, String configValue,
                                String serialNumber, String facilityName, String toastMessage) {
        if (createDeviceValue.toLowerCase().equals("yes")) {
            try {
                insertValueToTextbox(deviceNameTextBox, deviceName, "Device name" );
                insertValueToTextbox(partNumberTextBox, partNumber, "Serial Number");
                insertValueToTextbox(configurationTextBox, configValue, "Configuration value");
                insertValueToTextbox(serialNumberTextBox, serialNumber, "Serial Number");
                dropdownByVisibleText(facilityNameDropdown, facilityName);
                buttonClickByLocator(saveButton);
                MatchToastMessageValue(toastMessage);

            } catch (Exception e) {
                e.printStackTrace();
                CommonUtility.logExceptionsToTheReport(e);
            }

        } else if (createDeviceValue.toLowerCase().equals("no")) {
            try {
                Assert.assertTrue(isNotExpectedElementVisible(pageTitle, 10, "Create device page should not appear -"));
            } catch (Exception e) {
                e.printStackTrace();
                CommonUtility.logExceptionsToTheReport(e);
            }

        } else {
            CommonUtility.logMessagesAndAddThemToReport("Wrong text provided in the CSV file for create device", "fail");
            Assert.fail();

        }

    }

    /**
     * Method to verify if the has successfully navigated to the create device page (no param)
     */
    public void verifyCreateDevicePageNavigation() {
        try {
            Assert.assertTrue(isExpectedElementVisible(pageTitle, 10, "Create device page"));
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Implementing type device's name
     * @param deviceName
     */
    public void typeName(String deviceName) {
        try {
            clearText(deviceNameTextBox);
            sendKeys(deviceNameTextBox, deviceName, "Device Name text box");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Implementing type device's part number
     * @param partNumber
     */
    public void typePartNumber(String partNumber) {
        try {
            clearText(partNumberTextBox);
            sendKeys(partNumberTextBox, partNumber, "Part Number text box");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Implementing type device's configuration
     * @param configValue
     */
    public void typeConfiguration(String configValue) {
        try {
            clearText(configurationTextBox);
            sendKeys(configurationTextBox, configValue, "Configuration text box");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Implementing type device's serial number
     * @param serialNumber
     */
    public void typeSerialNumber(String serialNumber) {
        try {
            clearText(serialNumberTextBox);
            sendKeys(serialNumberTextBox, serialNumber, "Serial number text box");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /***
     * Implementing character length verification
     * @param textFieldName
     * @param expectedCharLength
     */
    public void verifyCharacterLengthCreateDevice(String textFieldName, int expectedCharLength){
        if(textFieldName.toLowerCase().equals("name")){
            verifyCharacterLength(deviceNameTextBox, textFieldName, expectedCharLength);
        }
        else if(textFieldName.toLowerCase().equals("part number")){
            verifyCharacterLength(partNumberTextBox, textFieldName, expectedCharLength);
        }
        else if(textFieldName.toLowerCase().equals("configuration")){
            verifyCharacterLength(configurationTextBox, textFieldName, expectedCharLength);
        }
        else if(textFieldName.toLowerCase().equals("serial number")){
            verifyCharacterLength(serialNumberTextBox, textFieldName, expectedCharLength);
        }
        else {
            CommonUtility.logMessagesAndAddThemToReport("Wrong field name provided", "fail");
            Assert.fail();
        }

    }

}
