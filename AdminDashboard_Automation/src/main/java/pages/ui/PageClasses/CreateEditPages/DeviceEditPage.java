package pages.ui.PageClasses.CreateEditPages;

import helper.CommonUtility;
import helper.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.Reporter;

public class DeviceEditPage extends BaseClassCreateEdit {

    // Start: Device edit  screen web locators
    By pageTitle = By.xpath("//h3[text()='Edit Device']");
    By loadingIcon = By.xpath("//td[text()='Loading...']");
    By deviceNameTextBox = By.id("Name");
    By partNumberTextBox = By.id("PartNumber");
    By configurationTextBox = By.id("Configuration");
    By serialNumberTextBox = By.id("SerialNumber");
    By saveButton = By.id("btn_submit");
    // End: Device edit  screen web locators

    /**
     * Method to verify if user is successfully navigated to the Edit Device page
     * @param editDevicePageAccess (Yes/No)
     */
    public void verifyEditDevicePageNavigation(String editDevicePageAccess){
        isNotExpectedElementVisible(loadingIcon, 30, "Data load");
        verifyListPageNavigation(editDevicePageAccess, pageTitle, "Edit Device page");

    }

    /***
     * Implementing edit device functionality
     * @param editDeviceValue
     * @param deviceName
     * @param partNumber
     * @param configValue
     * @param serialNumber
     * @param toastMessage
     */
    public void editDevice(String editDeviceValue, String deviceName, String partNumber, String configValue,
                             String serialNumber, String toastMessage) {
        if (editDeviceValue.toLowerCase().equals("yes")) {
            try {
                insertValueToTextbox(deviceNameTextBox, deviceName, "Device name" );
                insertValueToTextbox(partNumberTextBox, partNumber, "Serial Number");
                insertValueToTextbox(configurationTextBox, configValue, "Configuration value");
                insertValueToTextbox(serialNumberTextBox, serialNumber, "Serial Number");
                buttonClickByLocator(saveButton);
                MatchToastMessageValue(toastMessage);

            } catch (Exception e) {
                e.printStackTrace();
                CommonUtility.logExceptionsToTheReport(e);
            }

        } else if (editDeviceValue.toLowerCase().equals("no")) {
            try {
                Assert.assertTrue(isNotExpectedElementVisible(pageTitle, 10, "Edit device page should not appear -"));
            } catch (Exception e) {
                e.printStackTrace();
                CommonUtility.logExceptionsToTheReport(e);
            }

        } else {
            CommonUtility.logMessagesAndAddThemToReport("Wrong text provided in the CSV file for edit device", "fail");
            Assert.fail();

        }

    }

    /**
     * Method to verify if user is successfully navigated to the edit device page (without param)
     */
    public void verifyEditDevicePageNavigation( ){
        try {
            Assert.assertTrue(isExpectedElementVisible(pageTitle, 10, "Edit Device page"));
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Implementing type device's name
     * @param deviceName
     */
    public void typeName(String deviceName) {
        try {
            clearText(deviceNameTextBox);
            sendKeys(deviceNameTextBox, deviceName, "Device Name text box");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Implementing type device's part number
     * @param partNumber
     */
    public void typePartNumber(String partNumber) {
        try {
            clearText(partNumberTextBox);
            sendKeys(partNumberTextBox, partNumber, "Part Number text box");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Implementing type device's configuration
     * @param configValue
     */
    public void typeConfiguration(String configValue) {
        try {
            clearText(configurationTextBox);
            sendKeys(configurationTextBox, configValue, "Configuration text box");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /***
     * Implementing type device's serial number
     * @param serialNumber
     */
    public void typeSerialNumber(String serialNumber) {
        try {
            clearText(serialNumberTextBox);
            sendKeys(serialNumberTextBox, serialNumber, "Serial number text box");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Implementing character length verification
     * @param textFieldName
     * @param expectedCharLength
     */
    public void verifyCharacterLengthEditDevice(String textFieldName, int expectedCharLength){
        if(textFieldName.toLowerCase().equals("name")){
            verifyCharacterLength(deviceNameTextBox, textFieldName, expectedCharLength);
        }
        else if(textFieldName.toLowerCase().equals("part number")){
            verifyCharacterLength(partNumberTextBox, textFieldName, expectedCharLength);
        }
        else if(textFieldName.toLowerCase().equals("configuration")){
            verifyCharacterLength(configurationTextBox, textFieldName, expectedCharLength);
        }
        else if(textFieldName.toLowerCase().equals("serial number")){
            verifyCharacterLength(serialNumberTextBox, textFieldName, expectedCharLength);
        }
        else {
            CommonUtility.logMessagesAndAddThemToReport("Wrong field name provided", "fail");
            Assert.fail();
        }

    }

}
