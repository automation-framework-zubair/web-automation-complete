package pages.ui.PageClasses.CreateEditPages;

import helper.CommonUtility;
import helper.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.Reporter;

public class FacilityCreatePage extends BaseClassCreateEdit {

    // Start: Facility Create screen web locators
    By pageTitle = By.xpath("//h3[text()='Create Facility']");
    By facilityNameTextBox = By.xpath("//input[@id='Name']");
    By domainNameTextBox = By.xpath("//input[@id='Domain']");
    By saveButton = By.xpath("//input[@type='submit']");
    // End: Facility Create screen web locators

    // Start: Facility create page web elements
    // End: Facility create page web elements

    /**
     * Method to verify if user is successfully navigated to the Create Facility page
     * @param createFacilityPageAccess (Yes/No)
     */
    public void verifyCreateFacilityPageNavigation(String createFacilityPageAccess){
        verifyListPageNavigation(createFacilityPageAccess, pageTitle, "Create Facility page");

    }

    /***
     * Implementing create facility functionality
     * @param createFacilityValue
     * @param facilityName
     * @param domainName
     * @param toastMessage
     */
    public void createFacility(String createFacilityValue, String facilityName, String domainName, String toastMessage){
        if (createFacilityValue.toLowerCase().equals("yes")) {
            try {
                insertValueToTextbox(facilityNameTextBox, facilityName, "Facility name");
                insertValueToTextbox(domainNameTextBox, domainName, "Domain name");
                buttonClickByLocator(saveButton);
                MatchToastMessageValue(toastMessage);

            } catch (Exception e) {
                e.printStackTrace();
                CommonUtility.logExceptionsToTheReport(e);
            }

        } else if (createFacilityValue.toLowerCase().equals("no")) {
            try {
                Assert.assertTrue(isNotExpectedElementVisible(pageTitle, 10, "Create Facility page should not appear"));
            } catch (Exception e) {
                e.printStackTrace();
                CommonUtility.logExceptionsToTheReport(e);
            }

        } else {
            CommonUtility.logMessagesAndAddThemToReport("Wrong text provided in the CSV file for create facility", "fail");
            Assert.fail();

        }

    }

    /**
     * Method to verify if user is successfully navigated to the Create Facility page without param
     */
    public void verifyCreateFacilityPageNavigation( ){
        try {
            Assert.assertTrue(isExpectedElementVisible(pageTitle, 10, "Create Facility page"));
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Implementing type facility name
     * @param facilityName
     */
    public void typeFacilityName(String facilityName) {
        try {
            clearText(facilityNameTextBox);
            sendKeys(facilityNameTextBox, facilityName, "Facility Name text box");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Implementing type facility name
     * @param domainName
     */
    public void typeDomainName(String domainName) {
        try {
            clearText(domainNameTextBox);
            sendKeys(domainNameTextBox, domainName, "Domain Name text box");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Implementing save button click functionality
     */
    public void clickSaveButton() {
        try {
            clickOn(saveButton, "Save Button");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Implementing character length verification
     * @param textFieldName
     * @param expectedCharLength
     */
    public void verifyCharacterLengthCreateFacility(String textFieldName, int expectedCharLength){
        if(textFieldName.toLowerCase().equals("facility")){
            verifyCharacterLength(facilityNameTextBox, textFieldName, expectedCharLength);
        }
        else if(textFieldName.toLowerCase().equals("domain")){
            verifyCharacterLength(domainNameTextBox, textFieldName, expectedCharLength);
        }
        else {
            CommonUtility.logMessagesAndAddThemToReport("Wrong field name provided", "fail");
            Assert.fail();
        }

    }

}
