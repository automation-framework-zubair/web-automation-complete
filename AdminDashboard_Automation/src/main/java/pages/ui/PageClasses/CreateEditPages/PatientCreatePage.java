package pages.ui.PageClasses.CreateEditPages;

import helper.CommonUtility;
import helper.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;

public class PatientCreatePage extends BaseClassCreateEdit {

    // Start: Patient create  screen web locators
    By pageTitle = By.xpath("//h3[text()='Create Patient']");
    By firstNameTextBox = By.id("FirstName");
    By middleNameTextBox = By.id("MiddleName");
    By lastNameTextBox = By.id("LastName");
    By dobTextBox = By.xpath("//label[text()='DOB*']/../../descendant::input");
    By ssnTextBox = By.id("SSN");
    By patientIDTextBox = By.id("PatientID");
    By saveButton = By.id("btn_submit");
    // End: Patient create  screen web locators

    // Start: Patient create page web elements
    @FindBy(id = "Sex")
    WebElement sexDropdown;
    // End: Patient create page web elements

    //Getting current date value
    String currentDate = GetCurrentDate();

    /**
     * Method to verify if user is successfully navigated to the Create Patient page (with param)
     * @param createPatientPageAccess (Yes/No)
     */
    public void verifyCreatePatientPageNavigation(String createPatientPageAccess) {
        verifyListPageNavigation(createPatientPageAccess, pageTitle, "Create Patient page");

    }

    /**
     * Method to verify if the has successfully navigated to the create patient page (no param)
     */
    public void verifyCreatePatientPageNavigation() {
        try {
            Assert.assertTrue(isExpectedElementVisible(pageTitle, 10, "Create patient page"));
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Implementing type patient's first name
     * @param firstName
     */
    public void typeFirstName(String firstName) {
        try {
            clearText(firstNameTextBox);
            sendKeys(firstNameTextBox, firstName, "First Name text box");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Implementing type patient's middle name
     * @param middleName
     */
    public void typeMiddleName(String middleName) {
        try {
            clearText(middleNameTextBox);
            sendKeys(middleNameTextBox, middleName, "Middle Name text box");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Implementing type patient's last name
     * @param lastName
     */
    public void typeLastName(String lastName) {
        try {
            clearText(lastNameTextBox);
            sendKeys(lastNameTextBox, lastName, "Last Name text box");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Implementing type patient's DOB
     */
    public void typeDOB(String DOB) {
        try {
            clearText(dobTextBox);
            sendKeys(dobTextBox, DOB, "DOB text box");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Implementing type patient's SSN
     * @param ssnValue
     */
    public void typeSSN(String ssnValue) {
        try {
            clearText(ssnTextBox);
            sendKeys(ssnTextBox, ssnValue, "SSN text box");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Implementing type patient ID
     * @param patientID
     */
    public void typePatientID(String patientID) {
        try {
            clearText(patientIDTextBox);
            sendKeys(patientIDTextBox, patientID, "Patient ID text box");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Implementing select sex value functionality
     * @param sexValue
     */
    public void selectSexValueFromDropdown(String sexValue) {
        try {
            dropdownByVisibleText(sexDropdown, sexValue);
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Implementing save button click functionality
     */
    public void clickSaveButton() {
        try {
            clickOn(saveButton, "Save Button");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Implementing create patient functionality
     * @param createPatientValue
     * @param firstName
     * @param middleName
     * @param lastName
     * @param sexValue
     * @param ssnValue
     * @param patientIDValue
     * @param toastMessage
     */
    public void createPatient(String createPatientValue, String firstName, String middleName, String lastName,
                              String sexValue, String ssnValue, String patientIDValue, String toastMessage) {
        if (createPatientValue.toLowerCase().equals("yes")) {
            try {
                insertValueToTextbox(firstNameTextBox, firstName, "First name");
                insertValueToTextbox(middleNameTextBox, middleName, "Middle name");
                insertValueToTextbox(lastNameTextBox, lastName, "Last name");
                dropdownByVisibleText(sexDropdown, sexValue);
                insertValueToTextbox(ssnTextBox, ssnValue, "SSN value");
                insertValueToTextbox(patientIDTextBox, patientIDValue, "Patient ID");
                insertValueToTextbox(dobTextBox, currentDate, "DOB");
                buttonClickByLocator(saveButton);
                MatchToastMessageValue(toastMessage);
            } catch (Exception e) {
                e.printStackTrace();
                CommonUtility.logExceptionsToTheReport(e);
            }

        } else if (createPatientValue.toLowerCase().equals("no")) {
            try {
                Assert.assertTrue(isNotExpectedElementVisible(pageTitle, 10, "Create Patient page"));
            } catch (Exception e) {
                e.printStackTrace();
                CommonUtility.logExceptionsToTheReport(e);
            }

        } else {
            CommonUtility.logMessagesAndAddThemToReport("Wrong text provided in the CSV file for create patient", "fail");
            Assert.fail();

        }

    }

}