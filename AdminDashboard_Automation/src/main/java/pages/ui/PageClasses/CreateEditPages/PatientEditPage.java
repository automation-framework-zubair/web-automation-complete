package pages.ui.PageClasses.CreateEditPages;

import helper.CommonUtility;
import helper.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;

public class PatientEditPage extends BaseClassCreateEdit {

    // Start: Patient edit  screen web locators
    By pageTitle = By.xpath("//h3[text()='Edit Patient']");
    By loadingIcon = By.xpath("//td[text()='Loading...']");
    By firstNameTextBox = By.id("FirstName");
    By middleNameTextBox = By.id("MiddleName");
    By lastNameTextBox = By.id("LastName");
    By dobTextBox = By.xpath("//label[text()='DOB*']/../../descendant::input");
    By ssnTextBox = By.id("SSN");
    By patientIDTextBox = By.id("PatientID");
    By saveButton = By.id("btn_submit");
    // End: Patient edit  screen web locators

    // Start: Patient edit page web elements
    @FindBy(id = "Sex")
    WebElement sexDropdown;
    // End: Patient edit page web elements

    //Getting current date value
    String currentDate = GetCurrentDate();

    /**
     * Method to verify if user is successfully navigated to the Edit Patient page(with param)
     * @param editPatientPageAccess (Yes/No)
     */
    public void verifyEditPatientPageNavigation(String editPatientPageAccess) {
        isNotExpectedElementVisible(loadingIcon, 30, "Data load");
        verifyListPageNavigation(editPatientPageAccess, pageTitle, "Edit Patient page");

    }

    /**
     * Method to verify if user is successfully navigated to the Edit Patient page(without param)
     */
    public void verifyEditPatientPageNavigation() {
        try {
            isNotExpectedElementVisible(loadingIcon, 30, "Data load");
            Assert.assertTrue(isExpectedElementVisible(pageTitle, 10, "Edit Patient page"));
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Implementing type patient's first name
     * @param firstName
     */
    public void typeFirstName(String firstName) {
        try {
            clearText(firstNameTextBox);
            sendKeys(firstNameTextBox, firstName, "First Name text box");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Implementing type patient's middle name
     * @param middleName
     */
    public void typeMiddleName(String middleName) {
        try {
            clearText(middleNameTextBox);
            sendKeys(middleNameTextBox, middleName, "Middle Name text box");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Implementing type patient's last name
     * @param lastName
     */
    public void typeLastName(String lastName) {
        try {
            clearText(lastNameTextBox);
            sendKeys(lastNameTextBox, lastName, "Last Name text box");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Implementing type patient's DOB
     */
    public void typeDOB() {
        try {
            clearText(dobTextBox);
            sendKeys(dobTextBox, currentDate, "DOB text box");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Implementing type patient's SSN
     * @param ssnValue
     */
    public void typeSSN(String ssnValue) {
        try {
            clearText(ssnTextBox);
            sendKeys(ssnTextBox, ssnValue, "SSN text box");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Implementing select sex value
     * @param sexValue
     */
    public void selectSexValueFromDropdown(String sexValue) {
        try {
            dropdownByVisibleText(sexDropdown, sexValue);
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Implementing type patient ID
     * @param patientID
     */
    public void typePatientID(String patientID) {
        try {
            clearText(patientIDTextBox);
            sendKeys(patientIDTextBox, patientID, "Patient ID text box");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Implementing save button click functionality
     */
    public void clickSaveButton() {
        try {
            clickOn(saveButton, "Save button");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Implementing edit patient functionality
     * @param editPatientValue
     * @param firstName
     * @param middleName
     * @param lastName
     * @param sexValue
     * @param ssnValue
     * @param patientIDValue
     * @param toastMessage
     */
    public void editPatient(String editPatientValue, String firstName, String middleName, String lastName,
                            String sexValue, String ssnValue, String patientIDValue, String toastMessage) {
        if (editPatientValue.toLowerCase().equals("yes")) {
            try {
                insertValueToTextbox(firstNameTextBox, firstName, "First name");
                CommonUtility.logMessagesAndAddThemToReport("First Name edited", "info");

                insertValueToTextbox(middleNameTextBox, middleName, "Middle name");
                CommonUtility.logMessagesAndAddThemToReport("Middle Name edited", "info");
                insertValueToTextbox(lastNameTextBox, lastName, "Last name");
                CommonUtility.logMessagesAndAddThemToReport("Last Name edited", "info");
                dropdownByVisibleText(sexDropdown, sexValue);
                CommonUtility.logMessagesAndAddThemToReport("Sex value edited", "info");
                insertValueToTextbox(ssnTextBox, ssnValue, "SSN value");
                CommonUtility.logMessagesAndAddThemToReport("SSN edited", "info");
                insertValueToTextbox(patientIDTextBox, patientIDValue, "Patient ID");
                CommonUtility.logMessagesAndAddThemToReport("Patient ID edited", "info");
                insertValueToTextbox(dobTextBox, currentDate, "DOB");
                CommonUtility.logMessagesAndAddThemToReport("DOB edited", "info");
                buttonClickByLocator(saveButton);
                MatchToastMessageValue(toastMessage);
            } catch (Exception e) {
                e.printStackTrace();
                CommonUtility.logExceptionsToTheReport(e);
            }

        } else if (editPatientValue.toLowerCase().equals("no")) {
            try {
                Assert.assertTrue(isNotExpectedElementVisible(pageTitle, 10, "Edit Patient page"));
            } catch (Exception e) {
                e.printStackTrace();
                CommonUtility.logExceptionsToTheReport(e);
            }

        } else {
            CommonUtility.logMessagesAndAddThemToReport("Wrong text provided in the CSV file for edit patient", "fail");
            Assert.fail();

        }

    }

}
