package pages.ui.PageClasses.CreateEditPages;

import helper.CommonUtility;
import helper.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class UserCreatePage extends BaseClassCreateEdit {

    // Start: User create  screen web locators
    By pageTitle = By.xpath("//h3[text()='Create User']");
    By titleTextBox = By.id("Title");
    By firstNameTextBox = By.id("FirstName");
    By middleNameTextBox = By.id("MiddleName");
    By lastNameTextBox = By.id("LastName");
    By suffixTextbox = By.id("Suffix");
    By emailTextBox = By.id("Email");
    By passwordTextBox = By.id("Password");
    By confirmPasswordTextBox = By.id("ConfirmPassword");
    By saveButton = By.id("btn_submit");
    By showPasswordIcon = By.xpath("//input[@id='Password']/../span[contains(@class,'icon')]");
    By saveButtonDisabled = By.xpath("//input[@id='btn_submit' and @disabled]");
    By errorToastMessageForDuplicateEmail = By.xpath("//div[contains(@class, 'toast-container')]/descendant::i[contains(@class, 'icon error')]/..//div[text()='Email is already in use by user']");
    By roleDropdownLocator = By.xpath("//select[@id='RoleId']");
    // End: User create  screen web locators

    // Start: User create page web elements
    @FindBy(id = "RoleId")
    WebElement roleDropdown;
    // End: User create page web elements

    /**
     * Method to verify if user is successfully navigated to the Create User page
     * @param createUserPageAccess (Yes/No)
     */
    public void verifyCreateUserPageNavigation(String createUserPageAccess){
        verifyListPageNavigation(createUserPageAccess, pageTitle, "Create user page");

    }

    /***
     * Implementing create user functionality
     * @param createUserValue
     * @param titleValue
     * @param firstNamevalue
     * @param middleNameValue
     * @param lastNameValue
     * @param suffixValue
     * @param emailValue
     * @param passwordValue
     * @param confirmPasswordValue
     * @param roleValue
     * @param toastMessage
     */
    public void createUser(String createUserValue, String titleValue, String firstNamevalue, String middleNameValue, String lastNameValue, String suffixValue,
                             String emailValue, String passwordValue, String confirmPasswordValue, String roleValue, String toastMessage) {
        if (createUserValue.toLowerCase().equals("yes")) {
            try {
                sleep(2);
                insertValueToTextbox(titleTextBox, titleValue, "Title" );
                insertValueToTextbox(firstNameTextBox, firstNamevalue, "First name");
                insertValueToTextbox(middleNameTextBox, middleNameValue, "Middle name");
                insertValueToTextbox(lastNameTextBox, lastNameValue, "Last name");
                insertValueToTextbox(suffixTextbox, suffixValue, "Suffix");
                insertValueToTextbox(emailTextBox, emailValue, "Email");
                insertValueToTextbox(passwordTextBox, passwordValue, "Password");
                insertValueToTextbox(confirmPasswordTextBox, confirmPasswordValue, "Confirm password");
                dropdownByVisibleText(roleDropdown, roleValue);
                buttonClickByLocator(saveButton);
                MatchToastMessageValue(toastMessage);
            } catch (Exception e) {
                e.printStackTrace();
                CommonUtility.logExceptionsToTheReport(e);
            }

        } else if (createUserValue.toLowerCase().equals("no")) {
            try {
                Assert.assertTrue(isNotExpectedElementVisible(pageTitle, 10, "Create user page"));
            } catch (Exception e) {
                e.printStackTrace();
                CommonUtility.logExceptionsToTheReport(e);
            }

        } else {
            CommonUtility.logMessagesAndAddThemToReport("Wrong text provided in the CSV file for create user", "fail");
            Assert.fail();

        }

    }

    /**
     * Method to verify if the has successfully navigated to the create user page (no param)
     */
    public void verifyCreateUserPageNavigation() {
        try {
            Assert.assertTrue(isExpectedElementVisible(pageTitle, 60, "Create user page"));
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Implementing type user's title
     * @param titleValue
     */
    public void typeTitle(String titleValue) {
        try {
            clearText(titleTextBox);
            sendKeys(titleTextBox, titleValue, "Title text box");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Implementing type user's first name
     * @param firstName
     */
    public void typeFirstName(String firstName) {
        try {
            clearText(firstNameTextBox);
            sendKeys(firstNameTextBox, firstName, "First Name text box");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Implementing type user's middle name
     * @param middleName
     */
    public void typeMiddleName(String middleName) {
        try {
            clearText(middleNameTextBox);
            sendKeys(middleNameTextBox, middleName, "Middle Name text box");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Implementing type user's last name
     * @param lastName
     */
    public void typeLastName(String lastName) {
        try {
            clearText(lastNameTextBox);
            sendKeys(lastNameTextBox, lastName, "Last Name text box");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Implementing type user's suffix
     * @param suffixValue
     */
    public void typeSuffix(String suffixValue) {
        try {
            clearText(suffixTextbox);
            sendKeys(suffixTextbox, suffixValue, "Suffix text box");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Implementing type user's password
     * @param passwordValue
     */
    public void typePassword(String passwordValue) {
        try {
            buttonClickByLocator(showPasswordIcon);
            sendKeys(passwordTextBox, Keys.CONTROL + "a" + Keys.DELETE, "Type control A");
            sendKeys(passwordTextBox, passwordValue, "Password text box");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Implementing type user's confirm password
     * @param confirmPasswordValue
     */
    public void typeConfirmPassword(String confirmPasswordValue) {
        try {
            clearText(confirmPasswordTextBox);
            sendKeys(confirmPasswordTextBox, confirmPasswordValue, "Confirm password text box");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Implementing type user's email
     * @param emailValue
     */
    public void typeEmail(String emailValue) {
        try {
            clearTextUsingKeyboard(emailTextBox);
            sendKeys(emailTextBox, emailValue, "Email text box");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Implementing selecting role from drop-down
     * @param rolevalue
     */
    public void selectValueFromDropDown(String rolevalue){
        try {
            dropdownByVisibleText(roleDropdown, rolevalue);
            CommonUtility.logMessagesAndAddThemToReport("Role selected", "info");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }
    }

    /**
     * Implementing save button click
     */
    public void saveButtonClick(){
        try {
            buttonClickByLocator(saveButton);
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }
    }

    /***
     * Method to check save button is disabled
     */
    public void checkSaveButtonIsDisabled() {
        Assert.assertTrue(isExpectedElementVisible(saveButtonDisabled, 30, "Save button Disabled"));

    }

    /***
     * Method to verify that error message is displayed for duplicate email
     */
    public void checkErrorMessageForDuplicateEmail() {
        Assert.assertTrue(isExpectedElementVisible(errorToastMessageForDuplicateEmail, 30, "Error message for duplicate email"));
    }

    /***
     * Method to verify that name field is now separated to Title, First Name, Middle Name, Last Name & Suffix
     */
    public void verifyCreateUserPageContainsSeparateNameFields() {
        Assert.assertTrue(verifyLabelOfTextFieldExists("Title"));
        Assert.assertTrue(isExpectedElementVisible(titleTextBox, 30, "Title text box"));
        Assert.assertTrue(verifyLabelOfTextFieldExists("FirstName"));
        Assert.assertTrue(isExpectedElementVisible(firstNameTextBox, 30, "FirstName text box"));
        Assert.assertTrue(verifyLabelOfTextFieldExists("MiddleName"));
        Assert.assertTrue(isExpectedElementVisible(middleNameTextBox, 30, "MiddleName text box"));
        Assert.assertTrue(verifyLabelOfTextFieldExists("LastName"));
        Assert.assertTrue(isExpectedElementVisible(lastNameTextBox, 30, "LastName text box"));
        Assert.assertTrue(verifyLabelOfTextFieldExists("Suffix"));
        Assert.assertTrue(isExpectedElementVisible(suffixTextbox, 30, "Suffix text box"));

    }

    /***
     * Method to verify that the a label of a particular text field exists
     * @param value the identifier of the text field label
     */
    public Boolean verifyLabelOfTextFieldExists(String value) {
        By textFieldLabelLocator = By.xpath("//label[@for='" + value + "']");
        return isExpectedElementVisible(textFieldLabelLocator, 30, "Text Field Label ");
    }

    /***
     * Method to verify that the label of required field contains asterisk
     * @param value the identifier of the text field label
     */
    public Boolean verifyLabelOfRequiredFieldContainsAsterisk(String value) {
        Boolean status = Boolean.FALSE;
        By textFieldLabelLocator = By.xpath("//label[@for='" + value + "']");
        String label = getText(textFieldLabelLocator, "Text Field " + value);
        if(label.contains("*")) {
            status = Boolean.TRUE;
        }
        return status;
    }


    /***
     * Implementing character length verification
     * @param textFieldName
     * @param expectedCharLength
     */
    public void verifyCharacterLengthCreateUser(String textFieldName, int expectedCharLength){
        if(textFieldName.toLowerCase().equals("title")){
            verifyCharacterLength(titleTextBox, textFieldName, expectedCharLength);
        }
        else if(textFieldName.toLowerCase().equals("first name")){
            verifyCharacterLength(firstNameTextBox, textFieldName, expectedCharLength);
        }
        else if(textFieldName.toLowerCase().equals("middle name")){
            verifyCharacterLength(middleNameTextBox, textFieldName, expectedCharLength);
        }
        else if(textFieldName.toLowerCase().equals("last name")){
            verifyCharacterLength(lastNameTextBox, textFieldName, expectedCharLength);
        }
        else if(textFieldName.toLowerCase().equals("suffix")){
            verifyCharacterLength(suffixTextbox, textFieldName, expectedCharLength);
        }
        else if(textFieldName.toLowerCase().equals("email")){
            verifyCharacterLength(emailTextBox, textFieldName, expectedCharLength);
        }
        else {
            CommonUtility.logMessagesAndAddThemToReport("Wrong field name provided", "fail");
            Assert.fail();
        }

    }


    /***
     * Method to clear title text field
     */
    public void clearTitleTextField() {
        try {
            clearTextUsingKeyboard(titleTextBox);
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Method to clear first name text field
     */
    public void clearFirstNameTextField() {
        try {
            clearTextUsingKeyboard(firstNameTextBox);
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Method to clear middle name text field
     */
    public void clearMiddleNameTextField() {
        try {
            clearTextUsingKeyboard(middleNameTextBox);
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Method to clear last name text field
     */
    public void clearLastNameTextField() {
        try {
            clearTextUsingKeyboard(lastNameTextBox);
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Method to clear suffix text field
     */
    public void clearSuffixTextField() {
        try {
            clearTextUsingKeyboard(suffixTextbox);
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Method to clear password text field
     */
    public void clearPasswordTextField() {
        try {
            clearTextUsingKeyboard(passwordTextBox);
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Method to clear confirm password text field
     */
    public void clearConfirmPasswordTextField() {
        try {
            clearTextUsingKeyboard(confirmPasswordTextBox);
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Method to clear email text field
     */
    public void clearEmailTextField() {
        try {
            clearTextUsingKeyboard(emailTextBox);
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Method to verify roles are displayed in a particular role in the dropdown
     */
    public void checkRolesAreDisplayedInAParticularOrderInDropdown() {
        List<String> roleOrder = new ArrayList<String>();
        roleOrder.add("Please select a role");
        roleOrder.add("SuperAdmin");
        roleOrder.add("Support");
        roleOrder.add("Production");
        roleOrder.add("FacilityAdmin");
        roleOrder.add("ReviewDoctor");
        roleOrder.add("LeadTech");
        roleOrder.add("FieldTech");
        roleOrder.add("OfficePersonnel");
        String roles = getText(roleDropdownLocator, "Roles list");
        CommonUtility.logMessagesAndAddThemToReport("Role order from dropdown:\n" + roles, "info");
        List<String> roleOrderFromDropdown = new ArrayList<String>(Arrays.asList(roles.split("\\r?\\n")));
        Assert.assertEquals(roleOrderFromDropdown, roleOrder, "Roles order did not match");

    }

}
