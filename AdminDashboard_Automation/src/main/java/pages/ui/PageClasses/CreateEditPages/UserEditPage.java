package pages.ui.PageClasses.CreateEditPages;

import helper.CommonUtility;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class UserEditPage extends BaseClassCreateEdit {

    // Start: User edit  screen web locators
    By pageHeader = By.xpath("//h3[text()='Edit User']");
    By pageTitle = By.xpath("//title[text()='Rendr Review - Edit User']");
    By loadingIcon = By.xpath("//td[text()='Loading...']");
    By titleTextBox = By.id("Title");
    By firstNameTextBox = By.id("FirstName");
    By middleNameTextBox = By.id("MiddleName");
    By lastNameTextBox = By.id("LastName");
    By suffixTextbox = By.id("Suffix");
    By emailTextBox = By.id("Email");
    By saveButton = By.id("btn_submit");
    By passwordTextBox = By.id("Password");
    By confirmPasswordTextBox = By.id("ConfirmPassword");
    By saveButtonDisabled = By.xpath("//input[@id='btn_submit' and @disabled]");
    By roleDropdownLocator = By.xpath("//select[@id='RoleId']");
    // End: User edit  screen web locators

    // Start: User edit page web elements
    @FindBy(id = "RoleId")
    WebElement roleDropdown;
    // End: User edit page web elements

    /**
     * Method to verify if user is successfully navigated to the Edit User page
     * @param editUserPageAccess (Yes/No)
     */
    public void verifyEditUserPageNavigation(String editUserPageAccess){
        isNotExpectedElementVisible(loadingIcon, 30, "Data load");
        verifyListPageNavigation(editUserPageAccess, pageHeader, "Edit user page");

    }

    /***
     * Implementing create user functionality
     * @param createUserValue
     * @param titleValue
     * @param firstNamevalue
     * @param middleNameValue
     * @param lastNameValue
     * @param suffixValue
     * @param emailValue
     * @param roleValue
     * @param toastMessage
     */
    public void editUser(String createUserValue, String titleValue, String firstNamevalue, String middleNameValue, String lastNameValue, String suffixValue,
                           String emailValue, String roleValue, String toastMessage) {
        if (createUserValue.toLowerCase().equals("yes")) {
            try {
                insertValueToTextbox(titleTextBox, titleValue, "Title" );
                insertValueToTextbox(firstNameTextBox, firstNamevalue, "First name");
                insertValueToTextbox(middleNameTextBox, middleNameValue, "Middle name");
                insertValueToTextbox(lastNameTextBox, lastNameValue, "Last name");
                insertValueToTextbox(suffixTextbox, suffixValue, "Suffix");
                insertValueToTextbox(emailTextBox, emailValue, "Email");
                dropdownByVisibleText(roleDropdown, roleValue);
                buttonClickByLocator(saveButton);
                MatchToastMessageValue(toastMessage);
            } catch (Exception e) {
                e.printStackTrace();
                CommonUtility.logExceptionsToTheReport(e);
            }

        } else if (createUserValue.toLowerCase().equals("no")) {
            try {
                Assert.assertTrue(isNotExpectedElementVisible(pageHeader, 10, "Edit user page"));
            } catch (Exception e) {
                e.printStackTrace();
                CommonUtility.logExceptionsToTheReport(e);
            }

        } else {
            CommonUtility.logMessagesAndAddThemToReport("Wrong text provided in the CSV file for edit user", "fail");
            Assert.fail();

        }

    }

    /**
     * Method to verify if the has successfully navigated to the edit user page (no param)
     */
    public void verifyEditUserPageNavigation() {
        try {
            Assert.assertTrue(isExpectedElementVisible(pageHeader, 30, "Edit user page"));
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Method to verify that the page Title is proper for edit User page
     */
    public void verifyPageTitleOfEditUserPage() {
        Assert.assertTrue(isExpectedElementVisible(pageTitle, 30, "Edit User page title"));

    }

    /***
     * Implementing type user's title
     * @param titleValue
     */
    public void typeTitle(String titleValue) {
        try {
            sendKeys(titleTextBox, Keys.CONTROL + "a" + Keys.DELETE, "Type control A");
            sendKeys(titleTextBox, titleValue, "Title text box");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Implementing type user's first name
     * @param firstName
     */
    public void typeFirstName(String firstName) {
        try {
            sendKeys(firstNameTextBox, Keys.CONTROL + "a" + Keys.DELETE, "Type control A");
            sendKeys(firstNameTextBox, firstName, "First Name text box");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Implementing type user's middle name
     * @param middleName
     */
    public void typeMiddleName(String middleName) {
        try {
            sendKeys(middleNameTextBox, Keys.CONTROL + "a" + Keys.DELETE, "Type control A");
            sendKeys(middleNameTextBox, middleName, "Middle Name text box");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Implementing type user's last name
     * @param lastName
     */
    public void typeLastName(String lastName) {
        try {
            sendKeys(lastNameTextBox, Keys.CONTROL + "a" + Keys.DELETE, "Type control A");
            sendKeys(lastNameTextBox, lastName, "Last Name text box");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Implementing type user's suffix
     * @param suffixValue
     */
    public void typeSuffix(String suffixValue) {
        try {
            sendKeys(suffixTextbox, Keys.CONTROL + "a" + Keys.DELETE, "Type control A");
            sendKeys(suffixTextbox, suffixValue, "Suffix text box");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Implementing type user's email
     * @param emailValue
     */
    public void typeEmail(String emailValue) {
        try {
            sendKeys(emailTextBox, Keys.CONTROL + "a" + Keys.DELETE, "Type control A");
            sendKeys(emailTextBox, emailValue, "Email text box");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Implementing selecting role from drop-down
     * @param roleValue
     */
    public void selectValueFromDropDown(String roleValue){
        try {
            dropdownByVisibleText(roleDropdownLocator, roleValue);
            CommonUtility.logMessagesAndAddThemToReport("Role selected" + roleValue, "info");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }
    }

    /***
     * Method to verify password & confirm password fields are not present
     */
    public void verifyPasswordFieldsAreNotPresent() {
        Assert.assertTrue(isNotExpectedElementVisible(passwordTextBox, 20, "Password Text Box"));
        Assert.assertTrue(isNotExpectedElementVisible(confirmPasswordTextBox, 20, "Confirm Password Text Box"));

    }

    /***
     * Method to click on save button
     */
    public void clickOnSaveButton() {
        buttonClickByLocator(saveButton);

    }

    /***
     * Method to check save button is disabled
     */
    public void checkSaveButtonIsDisabled() {
        Assert.assertTrue(isExpectedElementVisible(saveButtonDisabled, 30, "Save button Disabled"));

    }

    /***
     * Implementing character length verification
     * @param textFieldName
     * @param expectedCharLength
     */
    public void verifyCharacterLengthEditUser(String textFieldName, int expectedCharLength){
        if(textFieldName.toLowerCase().equals("title")){
            verifyCharacterLength(titleTextBox, textFieldName, expectedCharLength);
        }
        else if(textFieldName.toLowerCase().equals("first name")){
            verifyCharacterLength(firstNameTextBox, textFieldName, expectedCharLength);
        }
        else if(textFieldName.toLowerCase().equals("middle name")){
            verifyCharacterLength(middleNameTextBox, textFieldName, expectedCharLength);
        }
        else if(textFieldName.toLowerCase().equals("last name")){
            verifyCharacterLength(lastNameTextBox, textFieldName, expectedCharLength);
        }
        else if(textFieldName.toLowerCase().equals("suffix")){
            verifyCharacterLength(suffixTextbox, textFieldName, expectedCharLength);
        }
        else if(textFieldName.toLowerCase().equals("email")){
            verifyCharacterLength(emailTextBox, textFieldName, expectedCharLength);
        }
        else {
            CommonUtility.logMessagesAndAddThemToReport("Wrong field name provided", "fail");
            Assert.fail();
        }

    }

    /***
     * Method to check that a particular role is present in the drop down
     */
    public void checkIfUserRoleIsPresentInTheDropdown(String userRole) {
        By roleLocator = By.xpath("//select[@id='RoleId']//option[text()='" + userRole + "']");
        Assert.assertTrue(isExpectedElementVisible(roleLocator, 30, "User role present in dropdown - " + userRole));

    }

    /***
     * Method to check that a particular role is present in the drop down
     */
    public void checkIfUserRoleIsNotPresentInTheDropdown(String userRole) {
        By roleLocator = By.xpath("//select[@id='RoleId']//option[text()='" + userRole + "']");
        Assert.assertTrue(isNotExpectedElementVisible(roleLocator, 30, "User role should not be present in dropdown - " + userRole));

    }

    /***
     * Method to check that save button is not disabled
     */
    public void checkThatSaveButtonIsNotDisabled() {
        Assert.assertTrue(isExpectedElementVisible(saveButton, 30, "Save button"));
        Assert.assertTrue(isNotExpectedElementVisible(saveButtonDisabled, 30, "Save button disabled"));

    }

}