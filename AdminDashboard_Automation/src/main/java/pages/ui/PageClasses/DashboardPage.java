package pages.ui.PageClasses;

import helper.CommonUtility;
import helper.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.Reporter;
import pages.ui.BaseClass;

public class DashboardPage extends BaseClass {

    // Start: Dashboard web locators
    By dashboardMenuLocator = By.xpath("//li[@class='nav-item']/a[text()='Dashboard']");
    By studiesForReviewCard = By.xpath("//div[@title='Studies For Review']");
    By studiesInProgressCard = By.xpath("//div[@title='Studies In Progress']");
    By totalStudiesCard = By.xpath("//div[@title='Total Studies']");
    By loadingIcon = By.xpath("//*[text()='Loading...']");
    By latestStudiesCard = By.xpath("//div[@class='container row']");
    By latestStudiesTable = By.xpath("//table[@class='table']");
    private By inProgressFilterControlLocator = By.xpath("//span[text()='In Progress']");
    By pendingReviewFilterControlLocator = By.xpath("//span[text()='Pending Review']");
    By anyStatusFilterControlLocator = By.xpath("//span[text()='Any Status ']");
    By iconErrorForToastMessage = By.xpath("//i[contains(@class, 'icon error')]");
    By closeIconErrorToastMessage = By.xpath("//i[contains(@class, 'icon error')]/..//i[contains(@class, 'close-icon')]");
    By userNotAssociatedToAnyFacilityMessage = By.xpath("//h3[text()='You are not associated with any facilities.']");
    // End: Dashboard web locators

    /**
     * Method to verify if dashboard is visible
     * @param dashboardAccess (Yes/No)
     */
    public void isDashboardMenuVisible(String dashboardAccess){
        isNotExpectedElementVisible(loadingIcon, 30, "Data load");
        if(dashboardAccess.toLowerCase().equals("yes")) {
            try {
                Assert.assertTrue(isExpectedElementVisible(dashboardMenuLocator, 10, "Dashboard menu should be visible"));
            } catch (Exception e) {
                e.printStackTrace();
                CommonUtility.logExceptionsToTheReport(e);
            }

        } else if(dashboardAccess.toLowerCase().equals("no")) {
            try {
                Assert.assertTrue(isNotExpectedElementVisible(dashboardMenuLocator, 10, "Dashboard menu should not be visible"));
            } catch (Exception e) {
                e.printStackTrace();
                CommonUtility.logExceptionsToTheReport(e);
            }

        } else {
            CommonUtility.logMessagesAndAddThemToReport("Wrong text provided in the CSV file of dashboard menu", "fail");
            Assert.fail();

        }

    }

    /***
     * Method to verify In Progress studies are displayed when In Progress card is clicked
     */
    public void verifyClickingInProgressCard() {
        clickOn(studiesInProgressCard, 30, "Studies In Progress card");
        Assert.assertTrue(isExpectedElementVisible(inProgressFilterControlLocator, 30, "In Progress Filter"));

    }

    /***
     * Method to verify Pending Review studies are displayed when Pending Review card is clicked
     */
    public void verifyClickingStudiesForReviewCard() {
        clickOn(studiesForReviewCard, 30, "Studies For Review card");
        Assert.assertTrue(isExpectedElementVisible(pendingReviewFilterControlLocator, 30, "Pending Review Filter"));

    }

    /***
     * Method to verify all studies are shown when Total studies card is clicked
     */
    public void verifyClickingTotalStudiesCard() {
        clickOn(totalStudiesCard, 30, "Total Studies card");
        Assert.assertTrue(isExpectedElementVisible(anyStatusFilterControlLocator, 30, "Any status filter"));

    }

    /**
     * Method to verify the presence of studies for review card
     * @return
     */
    public boolean isStudiesForReviewCardPresent() {
        return isExpectedElementVisible(studiesForReviewCard, 10, "Studies For Review card");

    }

    /**
     * Method to verify the presence of studies in progress card
     * @return
     */
    public boolean isStudiesInProgressCardPresent() {
        return isExpectedElementVisible(studiesInProgressCard, 10, "Studies in progress card.");

    }


    /**
     * Method to verify the presence of total studies card
     * @return
     */
    public boolean isTotalStudiesCardPresent() {
        return isExpectedElementVisible(totalStudiesCard, 10, "Total studies card");

    }

    /**
     * Method to verify if study info box is visible
     * @param infoBoxAccess (Yes/No)
     */
    public void verifyInfoBox(String infoBoxAccess){
        isNotExpectedElementVisible(loadingIcon, 30, "Data load");
        if(infoBoxAccess.toLowerCase().toLowerCase().equals("yes")) {
            try {
                Assert.assertTrue(isExpectedElementVisible(studiesInProgressCard, 10, "Info Box"));
            } catch (Exception e) {
                e.printStackTrace();
                CommonUtility.logExceptionsToTheReport(e);
            }

        } else if(infoBoxAccess.toLowerCase().equals("no")) {
            try {
                Assert.assertTrue(isNotExpectedElementVisible(studiesInProgressCard, 10, "Info Box"));
            } catch (Exception e) {
                e.printStackTrace();
                CommonUtility.logExceptionsToTheReport(e);
            }

        } else {
            CommonUtility.logMessagesAndAddThemToReport("Wrong text provided in the CSV file for Info box", "fail");
            Assert.fail();

        }

    }

    /**
     * Method to verify if latest study card and table are visible
     * @param latestStudiesAccess (Yes/No)
     */
    public void verifyLatestStudiesCard(String latestStudiesAccess){
        isNotExpectedElementVisible(loadingIcon, 30, "Data load");
        if(latestStudiesAccess.toLowerCase().equals("yes")) {
            try {
                Assert.assertTrue(isExpectedElementVisible(latestStudiesCard, 10, "Latest studies card"));
                Assert.assertTrue(isExpectedElementVisible(latestStudiesTable, 10, "Latest studies table"));
            } catch (Exception e) {
                e.printStackTrace();
                CommonUtility.logExceptionsToTheReport(e);
            }

        } else if(latestStudiesAccess.toLowerCase().equals("no")) {
            try {
                Assert.assertTrue(isNotExpectedElementVisible(latestStudiesCard, 10, "Latest studies card"));
                Assert.assertTrue(isNotExpectedElementVisible(latestStudiesTable, 10, "Latest studies table"));
            } catch (Exception e) {
                e.printStackTrace();
                CommonUtility.logExceptionsToTheReport(e);
            }

        } else {
            CommonUtility.logMessagesAndAddThemToReport("Wrong text provided in the CSV file for Latest studies", "fail");
            Assert.fail();

        }

    }

    /**
     * Method to verify if the has successfully navigated to the dashboard page
     */
    public void verifyDashboardPageNavigation(){
        try {
            isNotExpectedElementVisible(loadingIcon,30, "Data load");
            Assert.assertTrue(isExpectedElementVisible(dashboardMenuLocator, 30, "Dashboard page"));
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Method to verify presence of  error toast message
     */
    public void verifyErrorToastMessage() {
        isExpectedElementVisible(iconErrorForToastMessage, 5, "Error Toast Message");

    }

    /***
     * Method to close Error message
     */
    public void clickOnErrorToastMessage() {
        clickOn(closeIconErrorToastMessage, 30, "Close icon of error toast message");

    }

    /***
     * Method to verify absence of  error toast message
     */
    public boolean verifyAbsenceOfErrorToastMessage() {
        return isNotExpectedElementVisible(iconErrorForToastMessage, 30, "Error toast message is not present");

    }

    /***
     * Method to verify that user is not associated with any facility message appears after logging in
     */
    public void checkThatUserNotAssociatedWithAnyFacilityMessageAppears() {
        isNotExpectedElementVisible(loadingIcon, 30, "Loading icon disappearance - ");
        Assert.assertTrue(isExpectedElementVisible(userNotAssociatedToAnyFacilityMessage, 30, "User not Associated with any facility message - "));

    }

}
