package pages.ui.PageClasses;

import helper.CommonUtility;
import helper.Logger;
import helper.TestDataDeletion;

public class DataDeletionUsingDB {

    /***
     * Method for deleting a patient using ID
     * @param id ID of the patient to be deleted
     */
    public static void patientDelete(String id) {
        TestDataDeletion.patientDeleteId(id);

    }

    /***
     * Method for deleting an amplifier using ID
     * @param id ID of the amplifier to be deleted
     */
    public static void amplifierDelete(String id) {
        TestDataDeletion.amplifierDeleteId(id);

    }

    /***
     * Method for deleting a device using ID
     * @param id ID of the device to be deleted
     */
    public static void deviceDelete(String id) {
        TestDataDeletion.deviceDeleteId(id);

    }

    /***
     * Method for deleting a facility using ID
     * @param id ID of the facility to be deleted
     */
    public static void facilityDelete(String id) {
        if(id.equals("e3dd1742-335e-438e-8d8d-65df10447ea3")) {
            CommonUtility.logMessagesAndAddThemToReport("You tried to delete MobileMedTek Facility!", "warn");
            CommonUtility.logMessagesAndAddThemToReport("MobileMedTek facility cannot be deleted", "info");
        }
        else {
            TestDataDeletion.facilityDeleteId(id);
        }

    }

    /***
     * Method for deleting a user using ID
     * @param id ID of the user to be deleted
     */
    public static void userDelete(String id) {
        if(id.equals("2E449249-26B7-4DDB-8ED2-92E1655EA217")) {
            CommonUtility.logMessagesAndAddThemToReport("You tried to delete Default Super Admin", "warn");
            CommonUtility.logMessagesAndAddThemToReport("Default Super Admin cannot be deleted", "info");
        }
        else {
            TestDataDeletion.userDeleteId(id);
        }

    }

}
