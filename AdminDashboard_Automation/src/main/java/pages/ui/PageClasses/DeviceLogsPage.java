package pages.ui.PageClasses;

import org.openqa.selenium.By;
import pages.ui.BaseClass;

public class DeviceLogsPage extends BaseClass {

    // Start: Device Logs page locators
    By pageTitle = By.xpath("//h3[text()='Device Logs']");
    By loadingIcon = By.xpath("//td[text()='Loading...']");
    //End: Device logs page locators

    /***
     * Method to verify that the user has access to the Device Logs page
     * @param deviceLogs (yes/no)
     * */
    public void verifyDeviceLogsPageNavigation(String deviceLogs) {
        isNotExpectedElementVisible(loadingIcon, 30, "Data load");
        verifyListPageNavigation(deviceLogs, pageTitle, "Device Logs");

    }

}
