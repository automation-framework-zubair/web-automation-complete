package pages.ui.PageClasses;

import helper.CommonUtility;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import pages.ui.BaseClass;

public class HeaderPanel extends BaseClass {

    //Start: Header panel web elements
    @FindBy(className = "img-avatar")
    WebElement profileButton;

    @FindBy(xpath = "//button[text()='Sign Out']")
    WebElement signOutButton;

    @FindBy(xpath = "//a[text()='Your Profile']")
    WebElement yourProfileButton;

    @FindBy(xpath = "//header/span[contains(@class, 'facility')]/div")
    WebElement facilityDropdown;

    @FindBy(xpath = "//span[contains(@class, 'facility')]/descendant::span")
    WebElement currentFacility;
    // End: Header panel web elements

    // Start: Header panel locators
    By profileButtonLocator = By.className("img-avatar");
    By signOutButtonLocator = By.xpath("//button[text()='Sign Out']");
    By yourProfileButtonLocator = By.xpath("//a[text()='Your Profile']");
    By userNameInHeader = By.xpath("//ul[contains(@class, 'ml-auto navbar')]//span");

    /**
     * Method to get the Logged in user's title
     * @return logged in user's title
     */
    public String getUserLoggedInTitle() {
        return profileButton.getAttribute("title");

    }

    /**
     * To sign out
     */
    public void signOut() {
        explicitlyWait(30).until(ExpectedConditions.elementToBeClickable(profileButton));
        clickWithJavaScript(profileButtonLocator);
        explicitlyWait(30).until(ExpectedConditions.elementToBeClickable(signOutButton));
        clickWithJavaScript(signOutButtonLocator);

    }

    /**
     * Navigate to Profile page
     */
    public void yourProfileButtonClick() {
        explicitlyWait(30).until(ExpectedConditions.elementToBeClickable(profileButton));
        profileButton.click();
        explicitlyWait(30).until(ExpectedConditions.elementToBeClickable(yourProfileButton));
        yourProfileButton.click();

    }

    /**
     * Implementing select facility from facility dropdown
     * @param facilityName
     */
    public void selectFacilityFromDropdown(String facilityName) {

        if(!currentFacility.getText().toLowerCase().equals(facilityName.toLowerCase())) {
            try {
                By facilityLocator = By.xpath("//button[@role='menuitem']//div[contains(text(),'" + facilityName + "')]");
                facilityDropdown.click();
                scrollVerticallyTillAppears(facilityLocator);
                clickOn(facilityLocator,10, "Facility Selection");
            } catch (Exception e) {
                e.printStackTrace();
                CommonUtility.logExceptionsToTheReport(e);
            }
        }

    }

    /***
     * Method to check that the user name in the header (top-right corner) matches with the actual user name
     * @param userLastName Last name of the user
     * @param userFirstName First name of the user
     */
    public void checkThatUserNameInHeaderMatchesWithActualUserName(String userLastName, String userFirstName) {
        String userName = userLastName + " " + userFirstName;
        Assert.assertTrue(isExpectedElementVisible(userNameInHeader, 30, "User name in header"));
        String userNameDisplayedInHeader = getText(userNameInHeader, 30, "User name in header");
        Assert.assertEquals(userNameDisplayedInHeader, userName, "User name in the header does not match with the correct one");

    }

}