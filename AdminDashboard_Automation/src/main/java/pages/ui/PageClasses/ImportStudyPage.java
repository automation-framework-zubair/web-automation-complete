package pages.ui.PageClasses;

import helper.CommonUtility;
import helper.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;
import pages.ui.BaseClass;

public class ImportStudyPage extends BaseClass {

    // Start: Import Study page web locators
    By pageTitle = By.xpath("//h3[contains(text(), 'Import Study')]");
    By selectFolderButton = By.xpath("//label[contains(@class, 'btn-success') and text()='Select Folder']");
    By uploadFile = By.id("input");
    By selectedPatientHeader = By.xpath("//h4[text()='Selected Patient']");
    By selectedStudyHeader = By.xpath("//h4[text()='Selected Study']");
    By continueButton = By.xpath("//button[contains(@class, 'btn-success') and text()='Continue']");
    By progressBar = By.xpath("//div[@class='progress-container']");
    By successfulMessageForImportStudy = By.xpath("//p[text()='Study has successfully been imported. You may close this tab.']");
    By errorMessageForEDFAndBDF = By.xpath("//span[text()='Can not have both .EDF and .BDF files.']");
    By errorMessageForMP4AndAVI = By.xpath("//p[contains(@class,'text-danger') and text()='Only one type of video file is allowed.']");
    By errorMessageForDifferentTVXThanVideo = By.xpath("//p[contains(@class,'text-danger') and text()='Each video file must have a corresponding TVS file with the same name.']");
    By firstErrorMessageForTVSWithNoVideo = By.xpath("//p[contains(@class,'text-danger') and text()='No video file, but a .TVS file was found']");
    By secondErrorMessageForTVSWithNoVideo = By.xpath("//p[contains(@class,'text-danger') and text()='There must be one and only one .TVS file per video file.']");
    By errorMessageForMultipleTVSWithOneVideo = By.xpath("//p[contains(@class,'text-danger') and text()='There must be one and only one .TVS file per video file.']");
    By errorMessageForMultipleEDFFiles = By.xpath("//span[text()='Can not have more than one .EDF file.']");
    By errorMessageForMultiplePhoticChannels = By.xpath("//span[text()='Multiple photic channels detected in data file.']");
    By errorMessageForNoDataFile = By.xpath("//span[text()='No data file found.']");
    By errorMessageForNoTVXOrTEVFile = By.xpath("//p[contains(@class, 'text-danger') and text()='No .TEV / .TVX file found.']");
    By errorMessageForHeadersNotMatching = By.xpath("//p[contains(@class, 'text-danger') and text()='Headers of data files and events files do not match.']");
    By errorMessageForPatientID = By.xpath("//span[text()='The Patient ID must be the same in all data files.']");
    By errorMessageForSignalCount = By.xpath("//span[text()='The Signal Count must be the same in all data files.']");
    By errorMessageForRecordID = By.xpath("//span[text()='The Record ID must be the same in all data files.']");
    By errorMessageForDataVersionFormat = By.xpath("//span[text()='The Data Version Format must be the same in all data files.']");
    By errorMessageForDataRecordDuration = By.xpath("//span[text()='The Data Record Duration must be the same in all data files.']");
    By errorMessageForSamplesPerDataRecord = By.xpath("//span[text()='The Samples Per Data Record must be the same in all data files.']");
    By errorMessageForSampleRateDifferentIn1File = By.xpath("//span[contains(text(), 'Each channel must use the same sample rate.')]");
    By errorMessageFor33Channels = By.xpath("//span[contains(text(), 'The header file contains more data channels (33) than the maximum supported by database (32).')]");
    By errorMessageForWrongFileNamingConvention = By.xpath("//span[contains(text(), 'Unable to find')]");
    By errorMessageForTEVAndBDFOrEDFDifferentNames = By.xpath("//p[contains(@class, 'text-danger') and text()='Each .EDF / .BDF file must have a corresponding .TEV file with the same name.']");
    By errorMessageForNoTEVFile = By.xpath("//p[contains(@class, 'text-danger') and text()='No .TEV / .TVX file found. There must be one and only one .TEV / .TVX file per data file.']");
    By errorMessageForTEVTVXDifferentNamesForBDF = By.xpath("//p[contains(@class, 'text-danger') and text()='Each .EDF / .BDF file must have a corresponding .TEV file with the same name. The .TVX file must have the same name as the first .EDF / .BDF file']");
    By errorMessageForTVXDifferentNameForBDF = By.xpath("//p[contains(@class, 'text-danger') and text()='The .TVX file must have the same name as the first .EDF / .BDF file']");
    By errorMessageForTEVTVXDifferentNamesForEDF = By.xpath("//p[contains(@class, 'text-danger') and text()='Each .EDF / .BDF file must have a corresponding .TEV file with the same name. The .TVX file must have the same name as the first .EDF / .BDF file']");
    By errorMessageForTVXDifferentNameForEDF = By.xpath("//p[contains(@class, 'text-danger') and text()='The .TVX file must have the same name as the first .EDF / .BDF file']");
    By errorMessageForMoreThanOneTVX = By.xpath("//p[contains(@class, 'text-danger') and text()='Only one .TVX file is allowed. No .TEV / .TVX file found. There must be one and only one .TEV / .TVX file per data file.']");
    By errorMessageForMoreThanTEVPerDataFile = By.xpath("//p[contains(@class, 'text-danger') and text()='There must be one and only one .TEV / .TVX file per data file.']");
    By contactSupportMessage = By.xpath("//p[text()='If you have any questions, please ']");
    By contactSupportLink = By.xpath("//a[@href='/help']");
    By warningMessageForDifferentPatientNames = By.xpath("//div[contains(@class, 'alert-warning')]");

    //previous error message
    By errorMessageForDifferentTEVAndTVXNames = By.xpath("//p[contains(@class, 'text-danger') and text()=‘.TEV &.TVX files, when both included, must have the same names.’]");
    // End: Import Study page web locators

    /***
     * Method to verify Import Study Page navigation for Page Access
     * @param importStudyPageAccess actionAccess (yes/no)
     */
    public void verifyImportStudyPageNavigation(String importStudyPageAccess) {
        verifyListPageNavigation(importStudyPageAccess, pageTitle, "Import Study Page");

    }

    /***
     * Method to verify Import Study Page navigation for Page Access
     */
    public void verifyImportStudyPageNavigation() {
        Assert.assertTrue(isExpectedElementVisible(pageTitle, 30, "Import Study Page"));

    }

    /***
     * Method to close Import Study Page
     * @param importStudyPageAccess actionAccess (yes/no)
     */
    public void closeImportStudyPage(String importStudyPageAccess) {
        if(importStudyPageAccess.toLowerCase().equals("yes")) {
            int opened = getNumberOfOpenedTabs();
            CommonUtility.logMessagesAndAddThemToReport("No of opened tabs: " + String.valueOf(opened), "info");
            if(opened>1) {
                closeCurrentTab();
                changeTabFocus(0);
                CommonUtility.logMessagesAndAddThemToReport("Import Study tab closed", "info");
            } else {
                CommonUtility.logMessagesAndAddThemToReport("Only 1 browser tab is opened", "info");
            }
        } else if(importStudyPageAccess.toLowerCase().equals("no")) {
            CommonUtility.logMessagesAndAddThemToReport("User does not have access to Import Study Page", "info");
        }
    }

    /***
     * Method to close Import Study Page
     */
    public void closeImportStudyPage() {
        int opened = getNumberOfOpenedTabs();
        CommonUtility.logMessagesAndAddThemToReport("No of opened tabs: " + String.valueOf(opened), "info");
        if(opened>1) {
            closeCurrentTab();
            changeTabFocus(0);
            CommonUtility.logMessagesAndAddThemToReport("Import Study tab closed", "info");
        } else {
            CommonUtility.logMessagesAndAddThemToReport("Only 1 browser tab is opened", "info");
        }

    }


    /***
     * Method to click on Select Folder button
     * @param folderName Name of the folder which contains the files to be uploaded
     */
    public void uploadFilesForImportStudy(String folderName) {
        try {
            String currentWorkingDirectory = System.getProperty("user.dir");
            String FilesToBeUploaded = currentWorkingDirectory + "\\StudiesForImport\\" + folderName;
            uploadFile(uploadFile, FilesToBeUploaded);
            CommonUtility.logMessagesAndAddThemToReport("Files selected for upload", "info");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }
    }

    /***
     * Method to verify a button exists that allows the user to select folder
     */
    public void verifyButtonExistsForSelectingFolderForImport() {
        Assert.assertTrue(isExpectedElementVisible(selectFolderButton, 30, "Select folder button"));

    }

    /***
     * Method to verify Patient And Study Information is shown
     */
    public void verifyPatientAndStudyInformationIsDisplayed() {
        Assert.assertTrue(isExpectedElementVisible(selectedPatientHeader, 30, "Selected Patient Header"));
        Assert.assertTrue(isExpectedElementVisible(selectedStudyHeader, 30, "Selected Study Header"));

    }

    /***
     * Method to click on Continue button
     */
    public void clickOnContinueButton() {
        sleep(5);
        clickOn(continueButton, 30, "Continue button");

    }

    /***
     * Method to check if progress of uploading process is shown
     */
    public void verifyProgressOfUploadIsDisplayed() {
        Assert.assertTrue(isExpectedElementVisible(progressBar, 30, "Progress Bar"));

    }

    /***
     * Method to verify successful Message for Import Study
     */
    public void verifyPresenceOfSuccessfulMessageForImportStudy() {
        Assert.assertTrue(isExpectedElementVisible(successfulMessageForImportStudy, 30, "Successful Message for Import Study"));

    }

    /***
     * Method to verify presence of error message for uploading a folder containing both EDF and BDF files
     */
    public void verifyErrorMessageForBothEDFAndBDF() {
        Assert.assertTrue(isExpectedElementVisible(errorMessageForEDFAndBDF, 30, "Error message for uploading a folder containing both EDF and BDF files "));

    }

    /***
     * Method to verify absence of continue button
     */
    public void verifyAbsenceOfContinueButton() {
        Assert.assertTrue(isNotExpectedElementVisible(continueButton, 30, "Continue button should not appear"));

    }

    /***
     * Method to verify presence of error message for uploading a folder containing both MP4 and AVI files
     */
    public void verifyErrorMessageForBothMP4AndAVI() {
        Assert.assertTrue(isExpectedElementVisible(errorMessageForMP4AndAVI, 30, "Error message for uploading a folder containing both MP4 and AVI files "));

    }

    /***
     * Method to verify presence of error message for uploading video file with different file name than the TVS file
     */
    public void verifyErrorMessageForTVSFileWithDifferentNameThanVideo() {
        Assert.assertTrue(isExpectedElementVisible(errorMessageForDifferentTVXThanVideo, 30, "error message for uploading video file with different file name than the TVS file"));

    }

    /***
     * Method to verify presence of error messages for uploading a folder with tvs file but no video file
     */
    public void verifyErrorMessageForTVSWithNoVideo() {
        Assert.assertTrue(isExpectedElementVisible(firstErrorMessageForTVSWithNoVideo, 30, "error messages for uploading a folder with tvs file but no video file"));
        Assert.assertTrue(isExpectedElementVisible(secondErrorMessageForTVSWithNoVideo, 30, "error messages for uploading a folder with tvs file but no video file"));

    }

    /***
     * Method to verify error message for uploading multiple tvs files with one video file
     */
    public void verifyErrorMessageForMultipleTVSFilesWithOneVideo() {
        Assert.assertTrue(isExpectedElementVisible(errorMessageForMultipleTVSWithOneVideo, 30, "error message for uploading multiple tvs files with one video file"));
    }

    /***
     * Method to verify error message for uploading a folder containing multiple EDF files
     */
    public void verifyErrorMessageForMultipleEDFFiles() {
        Assert.assertTrue(isExpectedElementVisible(errorMessageForMultipleEDFFiles, 30, "error message for uploading a folder containing multiple EDF files"));

    }

    /***
     * Method to verify error message for uploading a study with multiple photic channels
     */
    public void verifyErrorMessageForMultiplePhoticChannels() {
        Assert.assertTrue(isExpectedElementVisible(errorMessageForMultiplePhoticChannels, 30, "error message for uploading a study with multiple photic channels"));

    }

    /***
     * Method to verify error message for not having a BDF or EDF file
     */
    public void verifyErrorMessageForNotHavingEDForBDFFile() {
        Assert.assertTrue(isExpectedElementVisible(errorMessageForNoDataFile, 30, "error message for no data file"));
        Assert.assertTrue(isExpectedElementVisible(errorMessageForNoTVXOrTEVFile, 30, "error message for no tev/tvx file"));

    }

    /***
     * Method to verify error message for headers of TEV/TVX not matching with EDF/BDF files
     */
    public void verifyErrorMessageForHeadersOfTEVOrTVXNotMatchingWithEDFOrBDF() {
        Assert.assertTrue(isExpectedElementVisible(errorMessageForHeadersNotMatching, 30, "error message for headers of TEV/TVX not matching with EDF/BDF files"));

    }

    /***
     * Method to verify error message if signal count is not same in all BDF files
     */
    public void verifyErrorMessageIfSignalCountNotSameForAllBDFFiles() {
        Assert.assertTrue(isExpectedElementVisible(errorMessageForSignalCount, 30, "error message if signal count is not same in all BDF files"));

    }

    /***
     * Method to verify error message if patient ID is not same in all BDF files
     */
    public void verifyErrorMessageIfPatientIDNotSameForAllBDFFiles() {
        Assert.assertTrue(isExpectedElementVisible(errorMessageForPatientID, 30, "error message if patient ID is not same in all BDF files"));

    }

    /***
     * Method to verify error message if record ID is not same in all BDF files
     */
    public void verifyErrorMessageIfRecordIDNotSameForAllBDFFiles() {
        Assert.assertTrue(isExpectedElementVisible(errorMessageForRecordID, 30, "error message if record ID is not same in all BDF files"));

    }

    /***
     * Method to verify error message if Data Version Format is not same in all BDF files
     */
    public void verifyErrorMessageIfDataVersionFormatNotSameForAllBDFFiles() {
        Assert.assertTrue(isExpectedElementVisible(errorMessageForDataVersionFormat, 30, "error message if Data Version Format is not same in all BDF files"));

    }

    /***
     * Method to verify error message if Data Record Duration is not same in all BDF files
     */
    public void verifyErrorMessageIfDataRecordDurationNotSameForAllBDFFiles() {
        Assert.assertTrue(isExpectedElementVisible(errorMessageForDataRecordDuration, 30, "error message if Data Record Duration is not same in all BDF files"));

    }

    /***
     * Method to verify error message if samples per data record is not same in all BDF files
     */
    public void verifyErrorMessageIfSamplesPerDataRecordNotSameForAllBDFFiles() {
        Assert.assertTrue(isExpectedElementVisible(errorMessageForSamplesPerDataRecord, 30, "error message if samples per data record is not same in all BDF files"));

    }

    /***
     * Method to verify error message if sample rate of one channel does not match with sample rate of the other channels in EDF or BDF files
     */
    public void verifyErrorMessageIfSampleRateDoesNotMatchWithOtherChannels() {
        Assert.assertTrue(isExpectedElementVisible(errorMessageForSampleRateDifferentIn1File, 30, "error message if sample rate of one channel does not match with sample rate of the other channels in EDF or BDF files"));

    }

    /***
     * Method to verify error message if study contains 33 channels instead of the maximum 32 channels
     */
    public void verifyErrorMessageForStudyContaining33Channels() {
        Assert.assertTrue(isExpectedElementVisible(errorMessageFor33Channels, 30, "error message if study contains 33 channels instead of the maximum 32 channels"));

    }

    /***
     * Method to verify error message for wrong file naming convention/sequence for multiple BDF files
     */
    public void verifyErrorMessageForWrongFileNamingConvention() {
        Assert.assertTrue(isExpectedElementVisible(errorMessageForWrongFileNamingConvention, 30, "error message for wrong file naming convention for multiple BDF files"));

    }

    /***
     * Method to verify error message for EDF/BDF having different names than TEV
     */
    public void verifyErrorMessageForEDFOrBDFHavingDifferentNamesThanTEV() {
        Assert.assertTrue(isExpectedElementVisible(errorMessageForTEVAndBDFOrEDFDifferentNames, 30, "error message for EDF/BDF having different names than TEV"));

    }

    /***
     * Method to verify error message for EDF/BDF having no TEV file
     */
    public void verifyErrorMessageForEDFOrBDFHavingNoTEVFile() {
        Assert.assertTrue(isExpectedElementVisible(errorMessageForNoTEVFile, 30, "error message for EDF/BDF having no TEV file"));

    }

    /***
     * Method to verify error message for BDF having different name than TEV and TVX
     */
    public void verifyErrorMessageForBDFHavingDifferentNamesThanTEVAndTVX() {
        Assert.assertTrue(isExpectedElementVisible(errorMessageForTEVTVXDifferentNamesForBDF, 30, " error message for BDF having different name than TEV and TVX"));

    }

    /***
     * Method to verify error message for BDF having different name than TVX
     */
    public void verifyErrorMessageForBDFHavingDifferentNameThanTVX() {
        Assert.assertTrue(isExpectedElementVisible(errorMessageForTVXDifferentNameForBDF, 30, "error message for BDF having different name than TVX"));

    }

    /***
     * Method to verify error message for EDF having different name than TEV and TVX
     */
    public void verifyErrorMessageForEDFHavingDifferentNamesThanTEVAndTVX() {
        Assert.assertTrue(isExpectedElementVisible(errorMessageForTEVTVXDifferentNamesForEDF, 30, " error message for EDF having different name than TEV and TVX"));

    }

    /***
     * Method to verify error message for EDF having different name than TVX
     */
    public void verifyErrorMessageForEDFHavingDifferentNameThanTVX() {
        Assert.assertTrue(isExpectedElementVisible(errorMessageForTVXDifferentNameForEDF, 30, "error message for EDF having different name than TVX"));

    }

    /***
     * Method to verify error message for folder not having data file
     */
    public void verifyErrorMessageForFolderNotHavingDataFile() {
        Assert.assertTrue(isExpectedElementVisible(errorMessageForNoDataFile, 30, "error message for folder not having data file"));

    }

    /***
     * Method to verify error message for folder with multiple TVX files
     */
    public void verifyErrorMessageForFolderHavingMultipleTVXFiles() {
        Assert.assertTrue(isExpectedElementVisible(errorMessageForMoreThanOneTVX, 30, "error message for folder with multiple TVX files"));

    }

    /***
     * Method to verify error message for folder with more than 1 TEV files per data file
     */
    public void verifyErrorMessageForFolderHavingMoreThanOneTEVFilePerDataFile() {
        Assert.assertTrue(isExpectedElementVisible(errorMessageForMoreThanTEVPerDataFile, 30, "error message for folder with more than 1 TEV files per data file"));

    }

    /***
     * Method to verify presence of contact support message in case of import rejection
     */
    public void verifyContactSupportMessageForImportRejection() {
        Assert.assertTrue(isExpectedElementVisible(contactSupportMessage, 30, "contact support message in case of import rejection"));

    }

    /***
     * Method to click on contact support link
     */
    public void clickOnContactSupportLink() {
        clickOn(contactSupportLink, 30, "Contact Support Link");

    }

    /***
     * Method to verify that folder has been rejected for import
     */
    public void verifyFolderRejectedForImport() {
        verifyAbsenceOfContinueButton();
        verifyButtonExistsForSelectingFolderForImport();

    }

    /***
     * Method to verify warning is displayed for patient name not matching with BDF header
     */
    public void verifyWarningForPatientNameNotMatchingWithBDF() {
        String warningMessage = getText(warningMessageForDifferentPatientNames, 30, "Warning message for patient name BDF header");
        Assert.assertEquals(warningMessage, "Warning! Patient name does not match BDF header.", "Warning message did not match- BDF header");
    }

    /***
     * Method to verify warning is displayed for patient name not matching with EDF header
     */
    public void verifyWarningForPatientNameNotMatchingWithEDF() {
        String warningMessage = getText(warningMessageForDifferentPatientNames, 30, "Warning message for patient name EDF header");
        Assert.assertEquals(warningMessage, "Warning! Patient name does not match EDF header.", "Warning message did not match- EDF header");
    }

    /***
     * Method to verify error message is displayed for different TEV and TVX names
     * PREVIOUS ERROR MESSAGE
     * */
    public void verifyErrorMessageForDifferentTEVTVXNames() {
        Assert.assertTrue(isExpectedElementVisible(errorMessageForDifferentTEVAndTVXNames, 10, "Error message for different TEV and TVX names"));

    }

}
