package pages.ui.PageClasses.ListPages;

import helper.CommonUtility;
import org.openqa.selenium.By;
import org.testng.Assert;

public class AmplifierPage extends BaseClassList {

    // Start: Amplifier page web locators
    By pageTitle = By.xpath("//h3[text()='Amplifiers']");
    By createButton = By.xpath("//h3[text()='Amplifiers']/..//a[text()='Create']");
    By editButton = By.xpath("//tr[1]//a[@title='Edit']");
    By loadingIcon = By.xpath("//td[text()='Loading...']");
    // End: Amplifier page web locators

    /**
     * Method to verify if user is successfully navigated to the Amplifiers page
     * @param amplifierPageAccess (yes/no)
     */
    public void verifyAmplifierPageNavigation(String amplifierPageAccess){
        isNotExpectedElementVisible(loadingIcon, 30, "Loading icon disappearance");
        verifyListPageNavigation(amplifierPageAccess, pageTitle, "Amplifier page");

    }

    /**
     * Method to verify if the user has access to enter create amplifier page
     * @param createAmplifierAccess (Yes/No)
     */
    public void verifyCreateButtonClick(String createAmplifierAccess){
        verifySubPageNavigation(createAmplifierAccess, createButton, "Create amplifier button ");

    }

    /**
     * Method to verify if the user has access to enter edit amplifier page
     * @param editAmplifierAccess (Yes/No)
     */
    public void verifyEditButtonClick(String editAmplifierAccess){
        isNotExpectedElementVisible(loadingIcon, 30, "Data load");
        verifySubPageNavigation(editAmplifierAccess, editButton, "Edit amplifier button");

    }

    /**
     * Method to verify if the has successfully navigated to the amplifiers page(without param)
     */
    public void verifyAmplifierPageNavigation(){
        try {
            isNotExpectedElementVisible(loadingIcon,30, "Data load");
            Assert.assertTrue(isExpectedElementVisible(pageTitle, 10, "Amplifier page"));
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /**
     * Method to navigate to the Create Amplifier page
     */
    public void goToCreateAmplifierPage() {
        try {
            isExpectedElementVisible(createButton, 30, "Create Button");
            clickOn(createButton, 30, "Create Button click");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /**
     * Method to navigate to the Edit page of first amplifier instance
     */
    public void goToEditAmplifierPage() {
        try {
            isExpectedElementVisible(editButton, 30, "Edit Button");
            clickOn(editButton, 30, "Edit Button click");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

}
