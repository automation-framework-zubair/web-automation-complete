package pages.ui.PageClasses.ListPages;

import browserutility.DriverCommand;
import helper.CommonUtility;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.Reporter;
import pages.ui.BaseClass;

import java.util.ArrayList;
import java.util.List;

public class BaseClassList extends BaseClass {
    DriverCommand driverCommand;
    public BaseClassList(){
        driverCommand = new DriverCommand();

    }

    /***
     * Method to get all data of a desired column
     * @param columnName
     */
    public List getDataByColumnName(String columnName){
        WebElement table = driverCommand.getDriver().findElement(By.xpath("//table[contains(@class,'table table-hover sortable-table')]"));
        int numOfRow = table.findElements(By.tagName("tr")).size();

        String first_part = "//table[contains(@class,'table table-hover sortable-table')]//tr[";
        String second_part = "]/td[";
        String third_part = "]";

        List<WebElement> tableHeaders = driverCommand.getDriver().findElements(By.xpath("//table[contains(@class,'table table-hover sortable-table')]//tr[1]/th"));
        int indexValue = 1;
        for (WebElement header : tableHeaders) {
            System.out.print(header);
            if(header.getText().equals(columnName)){
                break;
            }
            else {
                indexValue++;
            }
        }

        List<String> columndata=new ArrayList<String>();

        for (int i = 1; i<numOfRow; i++){
            String final_xpath = first_part + i + second_part + indexValue + third_part;
            String test_name = driverCommand.getDriver().findElement(By.xpath(final_xpath)).getText();
            columndata.add(test_name);
        }
        return columndata;

    }

    /***
     * Method to get all data split by a string of a desired column
     * @param columnName : Name of the column
     * @param splitByText : Split the data by the text
     */
    public List getDataByColumnName(String columnName, String splitByText) {
        WebElement table = driverCommand.getDriver().findElement(By.xpath("//table[contains(@class, 'table table-hover sortable-table')]"));
        int numOfRow = table.findElements(By.tagName("tr")).size();

        String first_part = "//table[contains(@class,'table table-hover sortable-table')]//tr[";
        String second_part = "]/td[";
        String third_part = "]";

        List<WebElement> tableHeaders = driverCommand.getDriver().findElements(By.xpath("//table[contains(@class,'table table-hover sortable-table')]//tr[1]/th"));
        int indexValue = 1;
        for(WebElement header : tableHeaders) {
            System.out.println(header);
            if(header.getText().equals(columnName)){
                break;
            }
            else {
                indexValue++;
            }
        }

        List<String> columnData = new ArrayList<>();

        for(int i = 1; i<numOfRow; i++){
            String final_xpath = first_part + i + second_part + indexValue + third_part;
            String test_name = driverCommand.getDriver().findElement(By.xpath(final_xpath)).getText();
            String[] splitTestName = test_name.split(splitByText);
            columnData.add(splitTestName[0]);
        }
        return columnData;
    }

    /***
     * Finds specific data in the table and return the whole row containing the data as a list.
     * @param tableHead
     * @param tableRow
     * @param colName Name of the column of the data you are looking for
     * @param targetData The data you are looking for
     * @return list of data
     */
    public ArrayList<String> findInTable(By tableHead, By tableRow, String colName, String targetData) {
        List col = driverCommand.getDriver().findElements(tableHead);
        List rows = driverCommand.getDriver().findElements(tableRow);

        ArrayList<String> list = new ArrayList<String>(col.size());

        int nameColumn = 0;
        int targetDataCol = 0;

        for (int i = 1; i <= col.size(); i++){
            String headValue = null;
            headValue = driverCommand.getDriver().findElement(By.xpath(stringFromBy(tableHead)+"["+ i + "]")).getText();
            if(headValue.equals(colName)){
                nameColumn = i;
                for (int j = 1; j <= rows.size(); j++) {
                    String rowValue = null;
                    rowValue = driverCommand.getDriver().findElement(By.xpath(stringFromBy(tableRow) + "[" + j + "]/td[" + nameColumn + "]")).getText();
                    if(rowValue.equals(targetData)){
                        scrollVerticallyTillAppears(By.xpath(stringFromBy(tableRow) + "[" + j + "]/td[" + nameColumn + "]"));
                        targetDataCol = j;
                        for (int k = 1; k <= col.size(); k++) {
                            String targetRow = null;
                            targetRow = driverCommand.getDriver().findElement(By.xpath(stringFromBy(tableRow) + "[" + targetDataCol + "]/td[" + k + "]")).getText();
                            System.out.println(targetRow);
                            list.add(targetRow);
                        }
                        break;
                    }
                }
                break;
            }
        }
        return list;
    }

    public ArrayList<String> getTableHeaderColumns(By tableHead) {
        List col = driverCommand.getDriver().findElements(tableHead);
        ArrayList<String> list = new ArrayList<String>(col.size());
        for (int i = 1; i <= col.size(); i++){
            String headValue = null;
            headValue = driverCommand.getDriver().findElement(By.xpath("//tr"+stringFromBy(tableHead) + "[" + i + "]")).getText();
            list.add(headValue);
        }
        return list;

    }

    /***
     * Method to find and get a button from a table
     * @param tableHead
     * @param tableRow
     * @param colName
     * @param targetData
     * @param buttonName
     * @return locator of the button
     */
    public By getButtonFromTable(By tableHead, By tableRow, String colName, String targetData, String buttonName) {
        By button = null;
        List col = driverCommand.getDriver().findElements(tableHead);
        List rows = driverCommand.getDriver().findElements(tableRow);
        int nameColumn = 0;
        int targetDataCol = 0;
        for (int i = 1; i <= col.size(); i++){
            String headValue = null;
            headValue = driverCommand.getDriver().findElement(By.xpath(stringFromBy(tableHead)+"[" + i + "]")).getText();
            headValue = headValue.toLowerCase();
            if(headValue.equals(colName.toLowerCase())){
                nameColumn = i;
                for (int j = 1; j <= rows.size(); j++) {
                    String rowValue = null;
                    rowValue = driverCommand.getDriver().findElement(By.xpath(stringFromBy(tableRow)+"[" + j + "]/td["+nameColumn+"]")).getText();
                    rowValue = rowValue.toLowerCase();
                    if(rowValue.equals(targetData.toLowerCase())){
                        targetDataCol=j;
                        for (int k = 1; k <= col.size(); k++) {
                            String targetRow = null;
                            targetRow = driverCommand.getDriver().findElement(By.xpath(stringFromBy(tableRow)+"[" + targetDataCol + "]/td[" + k + "]")).getText();
                            if(targetRow.toLowerCase().contains(buttonName.toLowerCase())) {
                                scrollHorizontallyTillAppears(By.xpath(stringFromBy(tableRow) + "[" + targetDataCol + "]/td[" + k + "]"));
                                button = By.xpath(stringFromBy(tableRow) + "[" + targetDataCol + "]/a[text()='Edit']");
                            }
                        }
                        break;
                    }
                }
                break;
            }
        }
        return button;

    }

    /**
     * Method to click edit icon by instance name
     * @param actionAccess (Yes/No)
     * @param instanceName
     */
    public void clickEditIconByInstanceName(String actionAccess, String instanceName, String pageTitle) {
        isNotExpectedElementVisible(By.xpath("//td[text()='Loading...']"), 30, "Data load");
        CommonUtility.logMessagesAndAddThemToReport("Instance Name :" + instanceName, "info");
        if (actionAccess.toLowerCase().equals("yes")) {
            try {
                By editIcon = By.xpath("//td[contains(@title,'" + instanceName + "') or contains(text(), '" + instanceName + "')]/..//a[@title='Edit']");
                Assert.assertTrue(isExpectedElementVisible(editIcon, 30, pageTitle + "Edit icon"));
                scrollVerticallyTillAppears(editIcon);
                clickWithJavaScript(editIcon);
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (actionAccess.toLowerCase().equals("no")) {
            try {
                if(pageTitle.toLowerCase().equals("patients")) {
                    Assert.assertTrue(isNotExpectedElementVisible(By.xpath("//span[text()='" + pageTitle + "']/../../descendant::a[@title='Edit']"), 10, pageTitle + "Edit icon"));
                }
                else if (pageTitle.toLowerCase().equals("amplifiers")) {
                    Assert.assertTrue(isNotExpectedElementVisible(By.xpath("//h3[text()='" + pageTitle + "']/../descendant::a[@title='Edit']"), 10, pageTitle + "Edit icon"));
                }
                else {
                    Assert.assertTrue(isNotExpectedElementVisible(By.xpath("//h3[contains(text(),'" + pageTitle + "')]/..//a[@title='Edit']"), 10, pageTitle + "Edit icon"));
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            Reporter.log("Wrong text provided in the CSV file of edit facility");
            Assert.fail();

        }

    }

    /***
     * Method to select an item by instance name
     * @param instanceName Name of the instance to be selected
     * @param pageTitle Title of the page
     */
    public void selectAnItemByInstanceName(String instanceName, String pageTitle) {
        try {
            if(pageTitle.toLowerCase().equals("patients")) {
                By selectItem = By.xpath("//div[contains(@class, 'patient') and contains(text(), '" + instanceName + "')]");
                isExpectedElementVisible(selectItem, 30, "Item to be selected");
                scrollVerticallyTillAppears(selectItem);
                clickWithJavaScript(selectItem);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }

    }

    /***
     * Method to click on edit icon for selected item
     * @param instanceName Name of the instance whose edit icon is going to be clicked
     * @param pageTitle Title of the page
     * @param actionAccess If the user has access to the edit icon or not
     */
    public void clickOnEditIconForSelectedItem(String actionAccess, String instanceName, String pageTitle) {
        if(actionAccess.toLowerCase().equals("yes")) {
            try {
                if(pageTitle.toLowerCase().equals("patients")) {
                    selectAnItemByInstanceName(instanceName, pageTitle);
                    By editIcon = By.xpath("//i[@class='icon-electrotek-pencil' and contains(@title, 'Edit " + instanceName + "')]");
                    Assert.assertTrue(isExpectedElementVisible(editIcon, 30, "item selected or not"));
                    clickOn(editIcon, 30, "Edit Icon");

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if(actionAccess.toLowerCase().equals("no")) {
            try {
                if(pageTitle.toLowerCase().equals("patients")) {
                    Assert.assertTrue(isNotExpectedElementVisible(By.xpath("//span[text()='" + pageTitle + "']/../../../..//i[@class='icon-electrotek-pencil' and contains(@title, 'Edit')]"), 30, "Edit Icon"));
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Reporter.log("Wrong text provided in the CSV file of edit facility");
            Assert.fail();
        }

    }

    /***
     * Method to click on icon for a particular page
     * @param actionAccess If the user has access to the icon
     * @param locator Locator of the icon to be clicked
     * @param logMsg Log message
     * */
    public void clickOnIconByLocator(String actionAccess, By locator, String logMsg) {
        if(actionAccess.toLowerCase().equals("yes")) {
            try {
                Assert.assertTrue(isExpectedElementVisible(locator, 30,logMsg));
                scrollVerticallyTillAppears(locator);
                clickWithJavaScript(locator);
            } catch (Exception e) {
                e.printStackTrace();
                CommonUtility.logExceptionsToTheReport(e);
            }
        } else if(actionAccess.toLowerCase().equals("no")) {
            try {
                Assert.assertTrue(isNotExpectedElementVisible(locator, 30, logMsg));
            } catch (Exception e) {
                e.printStackTrace();
                CommonUtility.logExceptionsToTheReport(e);
            }
        }

    }

    /***
     * Method to check that a modal has appeared with a particular title
     * @param modalTitle Title of the modal
     */
    public void checkThatAParticularModalIsPresentWithTitle(String modalTitle) {
        By modalTitleLocator = By.xpath("//h5[@class='modal-title' and text()='" + modalTitle + "']");
        Assert.assertTrue(isExpectedElementVisible(modalTitleLocator, 30, modalTitle + " Modal"));

    }

    /***
     * Method to check that a particular modal is not present
     * @param modalTitle Title of the modal
     */
    public void checkThatAParticularModalIsNotPresentWithTitle(String modalTitle) {
        By modalTitleLocator = By.xpath("//h5[@class='modal-title'] and text()='" + modalTitle + "'");
        Assert.assertTrue(isNotExpectedElementVisible(modalTitleLocator, 30, modalTitle + " Modal"));

    }

}