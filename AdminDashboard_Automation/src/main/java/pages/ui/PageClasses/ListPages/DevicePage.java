package pages.ui.PageClasses.ListPages;

import helper.CommonUtility;
import org.openqa.selenium.By;
import org.testng.Assert;

public class DevicePage extends BaseClassList {

    // Start: Device screen locators
    By pageTitle = By.xpath("//h3[text()=' Devices']");
    By createButton = By.xpath("//h3[text()=' Devices']/..//a[text()='Create']");
    By editButton = By.xpath("//tr[1]//a[@title='Edit']");
    By loadingIcon = By.xpath("//td[text()='Loading...']");
    By deviceLogsFirstRow = By.xpath("//tr[1]//descendant::a[@title='Logs']");
    By changeDeviceFacilityFirstRow = By.xpath("//tr[1]//descendant::a[@title='Change Device Facility']");
    // End: Device screen locators

    /**
     * Method to verify if user is successfully navigated to the Devices page
     * @param devicePageAccess (Yes/No)
     */
    public void verifyDevicesPageNavigation(String devicePageAccess){
        isNotExpectedElementVisible(loadingIcon, 30, "Data load");
        verifyListPageNavigation(devicePageAccess, pageTitle, "Device page");

    }

    /**
     * Method to verify if the user has access to enter create device page
     * @param createDeviceAccess (Yes/No)
     */
    public void verifyCreateButtonClick(String createDeviceAccess){
        verifySubPageNavigation(createDeviceAccess, createButton, "Create device button");

    }

    /**
     * Method to verify if the user has access to enter edit device page
     * @param editDeviceAccess (Yes/No)
     */
    public void verifyEditButtonClick(String editDeviceAccess){
        isNotExpectedElementVisible(loadingIcon, 30, "Data load");
        verifySubPageNavigation(editDeviceAccess, editButton, "Edit device button");

    }

    /**
     * Method to verify if the has successfully navigated to the devices page(without param)
     */
    public void verifyDevicesPageNavigation(){
        try {
            isNotExpectedElementVisible(loadingIcon,30, "Data load");
            Assert.assertTrue(isExpectedElementVisible(pageTitle, 10, "Device page"));
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /**
     * Method to navigate to the Create Device page
     */
    public void goToCreateDevicePage() {
        try {
            isExpectedElementVisible(createButton, 30, "Create Button");
            clickOn(createButton, 30, "Create Button click");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /**
     * Method to navigate to the Edit page of first device instance
     */
    public void goToEditDevicePage() {
        try {
            isExpectedElementVisible(editButton, 30, "Edit Button");
            clickOn(editButton, 30, "Edit Button click");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Method to check if user has access to the Device Logs page
     * @param deviceLogs (yes/no)
     * */
    public void verifyDeviceLogsPage(String deviceLogs) {
        isNotExpectedElementVisible(loadingIcon, 30, "Data Load");
        verifySubPageNavigation(deviceLogs, deviceLogsFirstRow, "Device Logs button");

    }

    /***
     * Method to check if user has access to the Change Device Facility Page
     * @param changeDeviceFacility (yes/no)
     * */
    public void verifyChangeDeviceFacilityPage(String changeDeviceFacility) {
        isNotExpectedElementVisible(loadingIcon, 30, "Data Load");
        verifySubPageNavigation(changeDeviceFacility, changeDeviceFacilityFirstRow, "Change Device Facility button");

    }

    /***
     * Method to click on Change Device Facility icon by Device name
     * @param actionAccess if the user has access to the icon
     * @param deviceName Name of the device whose facility is going to be changed
     * */
    public void clickOnChangeDeviceFacilityIconByDeviceName(String actionAccess, String deviceName) {
        isNotExpectedElementVisible(loadingIcon, 30, "Data load");
        By changeDeviceFacilityIcon = By.xpath("//td[contains(text(), '" + deviceName + "')]/..//a[@title='Change Device Facility']");
        clickOnIconByLocator(actionAccess, changeDeviceFacilityIcon, "Change Device Facility Icon");

    }

}
