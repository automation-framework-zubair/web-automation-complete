package pages.ui.PageClasses.ListPages;

import com.google.api.client.repackaged.org.apache.commons.codec.binary.Base64;
import helper.CommonUtility;
import helper.EmailUtility;
import helper.Logger;
import org.json.JSONObject;

public class EmailService {
    EmailUtility emailUtility = new EmailUtility();

    /***
     * Method to convert the raw message body to plain text
     * @param rawFullMessage contains the whole message body in base64url encoded format
     * @return returns the message in plain text
     * */
    public String decodeRawMessageBodyToPlainText(String rawFullMessage) {
        JSONObject jsonObject = new JSONObject(rawFullMessage);
        JSONObject payload = (JSONObject)jsonObject.get("payload");
        JSONObject body = (JSONObject)payload.get("body");
        String messageData = body.getString("data");
        Base64 base64url = new Base64(true);
        byte[] emailBytes = base64url.decode(messageData);
        String plainTextMessage = new String(emailBytes);
        CommonUtility.logMessagesAndAddThemToReport("Plain text message \n" + plainTextMessage, "info");
        return plainTextMessage;

    }

    /***
     * Method to get ID of the first message from a list of messages (after searching using a query)
     * @param email Email of the concerned user
     * @param query Query string with which the message is going to be searched
     * @return returns first message ID
     */
    public String getMessageID(String email, String query) {
        String messagesList = emailUtility.getMessagesList(email, query);
        String[] splitTextForID = messagesList.split("\"id\": \"");
        String[] messageID = splitTextForID[1].split("\",");
        CommonUtility.logMessagesAndAddThemToReport("Message ID : " + messageID[0], "info");
        return messageID[0];

    }

    /***
     * Method to get plain text message by id
     * */
    public String getPlainTextMessage(String email, String id) {
        String rawMessage = emailUtility.getFullMessage(email, id);
        String plainTextMessage = decodeRawMessageBodyToPlainText(rawMessage);
        return plainTextMessage;

    }

    /***
     * Method to get the account recovery link
     * @param id ID of the message
     * @param email email address of the concerned user
     * @return Account Recovery Link
     */
    public String getAccountRecoveryLink(String email, String id) {
        String rawMessage = emailUtility.getFullMessage(email, id);
        String plainTextMessage = decodeRawMessageBodyToPlainText(rawMessage);
        String[] splitTextForAccountRecovery = plainTextMessage.split("link:");
        String[] AccountRecoveryLink = splitTextForAccountRecovery[1].split("If you did not");
        CommonUtility.logMessagesAndAddThemToReport("Account Recovery Link: " + AccountRecoveryLink[0], "info");
        return AccountRecoveryLink[0];

    }

}
