package pages.ui.PageClasses.ListPages;

import helper.CommonUtility;
import org.openqa.selenium.By;
import org.testng.Assert;

public class FacilityPage extends BaseClassList {

    // Start: Facility screen locators
    By pageTitle = By.xpath("//h3[text()=' Facilities']");
    By createButton = By.xpath("//h3[text()=' Facilities']/..//a[text()='Create']");
    By editFacilityButton = By.xpath("//tr[1]//span[@class='userlist-action-icons']");
    By loadingIcon = By.xpath("//td[text()='Loading...']");
    // End: Facility screen locators

    /**
     * Method to verify if user is successfully navigated to the Facilities page
     * @param facilityPageAccess (Yes/No)
     */
    public void verifyFacilityPageNavigation(String facilityPageAccess) {
        isNotExpectedElementVisible(loadingIcon, 30, "Data load");
        verifyListPageNavigation(facilityPageAccess, pageTitle, "Facility page");

    }

    /**
     * Method to verify if the user has access to enter create Facility page
     * @param createFacilityAccess (Yes/No)
     */
    public void verifyCreateButtonClick(String createFacilityAccess) {
        verifySubPageNavigation(createFacilityAccess, createButton, "Create facility button");

    }

    /**
     * Method to verify if the user has access to enter edit patient page
     * @param editFacilityAccess (Yes/No)
     */
    public void verifyEditButtonClick(String editFacilityAccess) {
        isNotExpectedElementVisible(loadingIcon, 30, "Data load");
        verifySubPageNavigation(editFacilityAccess, editFacilityButton, "Edit facility button");

    }

    /**
     * Method to verify if user is successfully navigated to the Facilities page no param
     */
    public void verifyFacilityPageNavigation() {
        try {
            isNotExpectedElementVisible(loadingIcon,30, "Data load");
            Assert.assertTrue(isExpectedElementVisible(pageTitle, 30, "Facility page"));
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /**
     * Method to navigate to the Create Facility page
     */
    public void goToCreateFacilityPage() {
        try {
            isExpectedElementVisible(createButton, 30, "Create Button");
            clickOn(createButton, 30, "Create Button click");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /**
     * Method to navigate to the Edit Facility page
     */
    public void goToEditFacilityPage() {
        try {
            isExpectedElementVisible(editFacilityButton, 30, "Edit Button");
            clickOn(editFacilityButton, 30, "Edit Button click");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

}

