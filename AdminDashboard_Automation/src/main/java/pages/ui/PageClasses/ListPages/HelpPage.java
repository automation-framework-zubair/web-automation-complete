package pages.ui.PageClasses.ListPages;

import org.openqa.selenium.By;
import org.testng.Assert;

public class HelpPage extends BaseClassList{
    // Start: Facility screen locators
    By pageTitle = By.xpath("//h3[contains(text(),'Help')]");
    By loadingIcon = By.xpath("//td[text()='Loading...']");
    // End: Facility screen locators

    /**
     * Method to verify if user is successfully navigated to the Help page
     */
    public void verifyHelpPageNavigation() {
        Assert.assertTrue(isExpectedElementVisible(pageTitle, 30, "Help Page header"));

    }
}
