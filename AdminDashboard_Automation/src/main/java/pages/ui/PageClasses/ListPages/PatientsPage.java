package pages.ui.PageClasses.ListPages;

import helper.CommonUtility;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.openqa.selenium.WebElement;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class PatientsPage extends BaseClassList {

    // Start: Patient page web locators
    By pageTitle = By.xpath("//span[text()='Patients']");
    By createButton = By.xpath("//span[text()='Patients']/../../../..//a[@title='Create patient']");
    By editButton_FirstRow = By.xpath("//span[text()='Patients']/../../../..//i[@class='icon-electrotek-pencil' and contains(@title, 'Edit')]");
    By loadingIcon = By.xpath("//td[text()='Loading...']");
    By searchingIcon = By.xpath("//td[text()='Searching...']");
    By searchTextBar = By.xpath("//div[@class='form-group']/descendant::input");
    By clearButton = By.xpath("//button[text()='clear']");
    By nameHedaer = By.xpath("//th[text()='Name']");
    By nameSortAscendingIcon = By.xpath("//th[text()='Name']/i[@class='fa fa-sort-up']");
    By nameSortDescendingIcon = By.xpath("//th[text()='Name']/i[@class='fa fa-sort-down']");
    By patientIDHedaer = By.xpath("//th[text()='Patient ID']");
    By patientIDSortAscendingIcon = By.xpath("//th[text()='Patient ID']/i[@class='fa fa-sort-up']");
    By patientIDDescendingIcon = By.xpath("//th[text()='Patient ID']/i[@class='fa fa-sort-down']");
    By dobHedaer = By.xpath("//th[text()='DOB']");
    By dobSortAscendingIcon = By.xpath("//th[text()='DOB']/i[@class='fa fa-sort-up']");
    By dobSortDescendingIcon = By.xpath("//th[text()='DOB']/i[@class='fa fa-sort-down']");
    By patientDocumentsTab = By.xpath("//a[contains(@class, 'nav-link') and text()='Documents']");
    By dragAndDropText = By.xpath("//p[contains(@class, 'drop-message') and text()='Drag and drop to upload']");
    By auditTrailTab = By.xpath("//a[contains(@class, 'nav-link') and text()='Audit Trail']");
    By firstColumnNameOfPatientDocumentsTable = By.xpath("//div[contains(@class, 'patient-documents')]//descendant::th[text()='Name']");
    By patientDocumentsPanelUnderAuditTrail = By.xpath("//div[contains(@class,'accordion__button') and text()='Patient Documents']");
    By firstColumnNameOfPatientDocumentsTableUnderAuditTrail = By.xpath("//div[contains(@class,'accordion__button') and text()='Patient Documents']/../..//th[text()='Date']");
    By patientRecordPanelAuditTrail = By.xpath("//div[contains(@class,'accordion__button') and text()='Patient Record']");
    By firstColumnNameOfPatientRecordsTable = By.xpath("//div[contains(@class,'accordion__button') and text()='Patient Record']/../..//th[text()='Date']");
    By importIcon = By.xpath("//i[contains(@class,'icon-electrotek-import') and contains(@title,'Import study for')]");
    // End: Patient page web locators

    //Start: Patient page web elements
    @FindBy(xpath = "//tr/th[1]")
    WebElement firstColumn;

    @FindBy(xpath = "//tr/th[2]")
    WebElement secondColumn;

    @FindBy(xpath = "//tr/th[3]")
    WebElement thirdColumn;

    @FindBy(xpath = "//tr/th[4]")
    WebElement fourthColumn;

    @FindBy(xpath = "//tr/th[5]")
    WebElement fifthColumn;
    // End: Patient page web elements

    /**
     * Method to verify if user is successfully navigated to the Patients page
     * @param patientPageAccess (Yes/No)
     */
    public void verifyPatientPageNavigation(String patientPageAccess){
        isNotExpectedElementVisible(loadingIcon, 30, "Data load");
        verifyListPageNavigation(patientPageAccess, pageTitle, "Patient page");

    }

    /**
     * Method to verify if the user has access to enter create patient page
     * @param createPatientAccess (Yes/No)
     */
    public void verifyCreateButtonClick(String createPatientAccess){
        verifySubPageNavigation(createPatientAccess, createButton, "Create patient button");

    }

    /**
     * Method to verify if the user has access to enter edit patient page
     * @param editPatientAccess (Yes/No)
     */
    public void verifyEditButtonClick(String editPatientAccess){
        isNotExpectedElementVisible(loadingIcon, 30, "Data load");
        verifySubPageNavigation(editPatientAccess, editButton_FirstRow, "Edit patient button");

    }

    /**
     * Method to verify if the has successfully navigated to the patients page(without param)
     */
    public void verifyPatientPageNavigation(){
        try {
            isNotExpectedElementVisible(loadingIcon,30, "Data load");
            Assert.assertTrue(isExpectedElementVisible(pageTitle, 10, "Patient page"));
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Method to verify that patient Documents tab is present or not
     */
    public void verifyPatientDocumentTabIsPresent() {
        try {
            isNotExpectedElementVisible(loadingIcon,30, "Data load");
            isExpectedElementVisible(patientDocumentsTab, 30, "Patient Documents Tab");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Method to verify that patient Documents tab is present or not
     */
    public void verifyPatientDocumentTabIsNotPresent() {
        try {
            isNotExpectedElementVisible(loadingIcon,30, "Data load");
            isNotExpectedElementVisible(patientDocumentsTab, 30, "Patient Documents Tab");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Method to verify click on Patient Documents tab
     */
    public void verifyClickOnPatientDocumentsTab() {
        try {
            clickOn(patientDocumentsTab, 30, "Patient Documents Tab");
            Assert.assertTrue(isExpectedElementVisible(dragAndDropText, 30, "Drag and drop text"));
            Assert.assertTrue(isExpectedElementVisible(firstColumnNameOfPatientDocumentsTable, 30, "First column Name"));
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Method to verify that Audit Trail Tab is present or not
     */
    public void verifyAuditTrailTabIsPresent() {
        try {
            isNotExpectedElementVisible(loadingIcon,30, "Data load");
            isExpectedElementVisible(auditTrailTab, 30, "Patients Audit Trail Tab");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Method to verify that Audit Trail Tab is present or not
     */
    public void verifyAuditTrailTabIsNotPresent() {
        try {
            isNotExpectedElementVisible(loadingIcon,30, "Data load");
            isNotExpectedElementVisible(auditTrailTab, 30, "Patients Audit Trail Tab");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Method to verify that click on Audit Trail Tab
     */
    public void verifyClickOnAuditTrailTab() {
        try {
            clickOn(auditTrailTab, 30, "Audit Trail tab");
            Assert.assertTrue(isExpectedElementVisible(patientDocumentsPanelUnderAuditTrail, 30, "Patient Documents Panel Audit Trail Tab"));
            Assert.assertTrue(isExpectedElementVisible(patientRecordPanelAuditTrail, 30, "Patient Records"));
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Method to verify click on Patient Documents Panel under Audit Trail tab
     */
    public void verifyClickOnPatientDocumentsPanelInAuditTrailTab() {
        try {
            clickOn(patientDocumentsPanelUnderAuditTrail, 30, "Patient Documents Panel Audit Trail Tab");
            Assert.assertTrue(isExpectedElementVisible(firstColumnNameOfPatientDocumentsTableUnderAuditTrail, 30, "First Column Name"));
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }
    }

    /***
     * Method to verify Click on Patient Records tab under Audit Trail tab
     */
    public void verifyClickOnPatientRecordsPanel() {
        try {
            clickOn(patientRecordPanelAuditTrail, 30, "Patient Records Panel");
            Assert.assertTrue(isExpectedElementVisible(firstColumnNameOfPatientRecordsTable, 30, "First Column Name"));
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }
    }

    /***
     * Method to verify import button is present
     */
    public void verifyImportButtonIsPresent() {
        Assert.assertTrue(isExpectedElementVisible(importIcon, 30, "Import Icon"));

    }

    /***
     * Method to verify click on the import Icon
     * @param importStudyPageAccess actionAccess (yes/no)
     */
    public void verifyClickOnImportIcon(String importStudyPageAccess) {
        if(importStudyPageAccess.toLowerCase().equals("yes")) {
            verifySubPageNavigation(importStudyPageAccess, importIcon, "Import Icon");
            sleep(2);
            changeTabFocus(1);
        } else if(importStudyPageAccess.toLowerCase().equals("no")) {
            verifySubPageNavigation(importStudyPageAccess, importIcon, "Import Icon");
        }

    }

    /***
     * Method to click on Import Icon by Patient Name
     * @param importStudyPageAccess actionAccess (yes/no)
     * @param patientName Name of the patient
     * */
    public void clickOnImportIconByPatientName(String importStudyPageAccess, String patientName) {
        if(importStudyPageAccess.toLowerCase().equals("yes")) {
            selectAnItemByInstanceName(patientName, "Patients");
            By importIcon = By.xpath("//h4[contains(text(), '" + patientName + "')]/..//i[@class='icon-electrotek-import']");
            clickOnIconByLocator(importStudyPageAccess, importIcon, "Import Icon for " + patientName);
            changeTabFocus(1);
        } else if(importStudyPageAccess.toLowerCase().equals("no")) {
            verifySubPageNavigation(importStudyPageAccess, importIcon, "Import Icon");
        }
    }

    /**
     * Method to verify if the column orders are proper (First Column)
     * @param expectedColumnName
     */
    public void verifyFirstColumnName(String expectedColumnName) {
        try {
            String actualColumnName = firstColumn.getText();
            Assert.assertEquals(actualColumnName, expectedColumnName, "First Column");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Method to verify if the column orders are proper (Second Column)
     * @param expectedColumnName
     */
    public void verifySecondColumnName(String expectedColumnName){
        try {
            String actualColumnName = secondColumn.getText();
            Assert.assertEquals(actualColumnName, expectedColumnName, "Second Column");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Method to verify if the column orders are proper (Third Column)
     * @param expectedColumnName
     */
    public void verifyThirdColumnName(String expectedColumnName){
        try {
            String actualColumnName = thirdColumn.getText();
            Assert.assertEquals(actualColumnName, expectedColumnName, "Third Column");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Method to verify if the column orders are proper (Fourth Column)
     * @param expectedColumnName
     */
    public void verifyFourthColumnName(String expectedColumnName){
        try {
            String actualColumnName = fourthColumn.getText();
            Assert.assertEquals(actualColumnName, expectedColumnName, "Fourth Column");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Method to verify if the column orders are proper (Fifth Column)
     * @param expectedColumnName
     */
    public void verifyFifthColumnName(String expectedColumnName){
        try {
            String actualColumnName = fifthColumn.getText();
            Assert.assertEquals(actualColumnName, expectedColumnName, "Fifth Column");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Method to navigate to the Create Patient page
     */
    public void goToCreatePatientPage() {
        try {
            isExpectedElementVisible(createButton, 30, "Create Button");
            clickOn(createButton, 30, "Create Button click");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /**
     * Method to implement search functionality
     */
    public void searchForPatient(String searchKeyword) {
        try {
            clearText(searchTextBar);
            sendKeys(searchTextBar, searchKeyword, "Search text bar, text =" + searchKeyword);
            isNotExpectedElementVisible(searchingIcon, 30, "Searching text");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /**
     * Method to verify search functionality
     * @param instanceNameToSelect name to select
     * @param searchKeyword text with which it is searched
     * @param patientInformationType the type of information that the patient is being searched with
     */
    public void verifySearchedPatientByKeyword(String searchKeyword, String instanceNameToSelect, String patientInformationType) {
        try {
            Assert.assertTrue(isExpectedElementVisible(By.xpath("//div[contains(@class,'results')]//descendant::div[contains(text(), '" + instanceNameToSelect + "')]"),30, instanceNameToSelect + "Search not found"));
            selectAnItemByInstanceName(instanceNameToSelect,  "Patients");
            By expectedSearchTextLocator = By.xpath("//div[contains(@class,'view')]//descendant::p[contains(text(), '" + patientInformationType + "')]");
            String actualSearchedText = getText(expectedSearchTextLocator, 30, "Expected Search Text");
            Assert.assertTrue(actualSearchedText.contains(searchKeyword), "Searched text did not match with keyword, keyword: " + searchKeyword);
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }
    }

    /**
     * Method to verify search functionality for Patient Name
     * @param patientName Name of the patient
     * @param instanceNameToSelect name to select
     */
    public void verifySearchedPatientName(String patientName, String instanceNameToSelect) {
        try {
            Assert.assertTrue(isExpectedElementVisible(By.xpath("//div[contains(@class,'results')]//descendant::div[contains(text(), '" + instanceNameToSelect + "')]"),30, instanceNameToSelect + "Search not found"));
            selectAnItemByInstanceName(instanceNameToSelect,  "Patients");
            Assert.assertTrue(isExpectedElementVisible(By.xpath("//div[contains(@class,'view')]//descendant::h4[contains(text(),'" + patientName + "')]"),30, patientName + "Searched result"));
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /**
     * Method to click clear button
     */
    public void clickOnSearchClearButton() {
        try {
            clickOn(clearButton, "Clear button");
            isNotExpectedElementVisible(loadingIcon, 30, "Loading text");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Method to click edit icon
     * @param patientName
     */
    public void clickOnEditButton(String patientName) {
        try {
            selectAnItemByInstanceName(patientName, "Patients");
            clickOnEditIconForSelectedItem("yes", patientName, "Patients");
        }
        catch (Exception e)
        {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /**
     * Method to verify initial sort icon
     */
    public void verifyInitialSortIndicationByName() {
        try {
            isNotExpectedElementVisible(loadingIcon,30, "Loading icon");
            Assert.assertTrue(isExpectedElementVisible(nameSortAscendingIcon, 30, "Name sort ascending order"));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    /**
     * Method to change Name sort
     */
    public void changeNameSort() {
        try {
            clickOn(nameHedaer, 30, "Name header");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    /**
     * Method to change patient id sort
     */
    public void changePatientIDSort() {
        try {
            clickOn(patientIDHedaer, 30, "Patient ID header");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    /**
     * Method to change DOB sort
     */
    public void changeDOBSort() {
        try {
            clickOn(dobHedaer, 30, "DOB header");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    /**
     * Method to verify column list sort
     * @param attributeValue(Name/DOB/Patient ID)
     * @param sortType(Ascending/Descending)
     */
    public void verifySortedData(String attributeValue, String sortType) {

        List<String> columnData =  getDataByColumnName(attributeValue);
        List<String> columnDataRaw = getDataByColumnName(attributeValue);
        if(sortType.equals("Ascending")) {
            Collections.sort(columnData, new Comparator<String>() {
                @Override
                public int compare(String o1, String o2) {
                    return o1.compareToIgnoreCase(o2);
                }
            });
        }
        else if(sortType.equals("Descending")){
            Collections.sort(columnData, new Comparator<String>() {
                @Override
                public int compare(String o1, String o2) {
                    return o2.compareToIgnoreCase(o1);
                }
            });
        }

        try {
            Assert.assertEquals(columnData, columnDataRaw, attributeValue + " " + sortType + " " + "did not match");
        }
        catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }
    }

    /**
     * Method to verify column list sort if only a certain part of the value is used in sorting
     * @param attributeValue(Name/DOB/Patient ID)
     * @param sortType(Ascending/Descending)
     * @param splitText : Text to split by
     */
    public void verifySortedData(String attributeValue, String sortType, String splitText) {

        List<String> columnData =  getDataByColumnName(attributeValue, splitText);
        List<String> columnDataRaw = getDataByColumnName(attributeValue, splitText);

        if(sortType.equals("Ascending")) {
            Collections.sort(columnData, new Comparator<String>() {
                @Override
                public int compare(String o1, String o2) {
                    return o1.compareToIgnoreCase(o2);

                }
            });
        }
        else if(sortType.equals("Descending")){
            Collections.sort(columnData, new Comparator<String>() {
                @Override
                public int compare(String o1, String o2) {
                    return o2.compareToIgnoreCase(o1);

                }
            });
        }

        try {
            Assert.assertEquals(columnData, columnDataRaw, attributeValue + " " + sortType + " " + "did not match");
        }
        catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }
    }

    /**
     * Method to verify column list sort for DOB column
     * @param attributeValue : Column header
     * @param sortType : Ascending or Descending
     * @param DOBOrNot : Parameter to check if the list to be sorted is DOB
     */
    public void verifySortedData(String attributeValue, String sortType, boolean DOBOrNot){
        if(DOBOrNot == false){
            verifySortedData(attributeValue, sortType);
        }
        else {
            List<String> columnData = getDataByColumnName(attributeValue);
            List<String> columnDataRaw = getDataByColumnName(attributeValue);
            if(sortType.equals("Ascending")){
                Collections.sort(columnData, new Comparator<String>() {
                    DateFormat f = new SimpleDateFormat("MM/dd/yyyy");
                    @Override
                    public int compare(String o1, String o2) {
                        int i = 0;
                        try {
                            i = f.parse(o1).compareTo(f.parse(o2));
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        return i;
                    }
                });
            }
            else if(sortType.equals("Descending")){
                Collections.sort(columnData, new Comparator<String>() {
                    DateFormat f = new SimpleDateFormat("MM/dd/yyyy");
                    @Override
                    public int compare(String o1, String o2) {
                        int i = 0;
                        try {
                            i = f.parse(o2).compareTo(f.parse(o1));
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        return i;
                    }
                });
            }
            try {
                Assert.assertEquals(columnData, columnDataRaw, attributeValue + " " + sortType + " " + "did not match");
            } catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    /**
     * Method to navigate to the Edit Patient page of first patient
     */
    public void goToEditPatientPage() {
        try {
            isExpectedElementVisible(editButton_FirstRow, 30, "Edit Button");
            clickOn(editButton_FirstRow, 30, "Edit Button click");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

}
