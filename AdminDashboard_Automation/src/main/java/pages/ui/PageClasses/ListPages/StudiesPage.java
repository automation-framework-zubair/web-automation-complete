package pages.ui.PageClasses.ListPages;

import org.openqa.selenium.By;
import org.testng.Assert;

public class StudiesPage extends BaseClassList {

    // Start: Studies page web locators
    By pageTitle = By.xpath("//h3[text()=' Studies']");
    By openButton = By.xpath("//tr[1]/td[contains(@class, 'actions-column')]/a[@title='Open']");
    By loadingIcon = By.xpath("//td[text()='Loading...']");
    By auditLogsFirstRow = By.xpath("//tr[1]//a[@title='Audit Logs']");
    By sharedUsersFirstRow = By.xpath("//tr[1]//a[@title='Shared Users']");
    By shareStudyFirstRow = By.xpath("//tr[1]//descendant::i[@title='Share Study']");
    By shareStudyModalHeader = By.xpath("//h5[@class='modal-title' and text()='Share Study']");
    By editableDropdown = By.xpath("//div[contains(@class, 'css-1hwfws3')]");
    By shareButton = By.xpath("//button[contains(@class, 'btn-success') and text()='Share']");
    // End: Studies page web locators

    /**
     * Method to verify if user is successfully navigated to the Studies page
     * @param studyPageAccess (Yes/No)
     */
    public void verifyStudiesPageNavigation(String studyPageAccess){
        isNotExpectedElementVisible(loadingIcon, 30, "Disappearance of loading icon");
        verifyListPageNavigation(studyPageAccess, pageTitle, "Study page");
    }

    /**
     * Method to verify if the user has access to enter remote viewer page
     * @param openStudyAccess (Yes/No)
     */
    public void verifyOpenStudyButtonClick(String openStudyAccess){
        isNotExpectedElementVisible(loadingIcon, 30, "Disappearance of loading icon");
        verifySubPageNavigation(openStudyAccess, openButton, "Open study button");

    }

    /***
     * Method to verify that if the user has access to the auditLogs pafe
     * @param auditLogsAccess (yes/no)
     */
    public void verifyAuditLogsButtonClick(String auditLogsAccess) {
        isNotExpectedElementVisible(loadingIcon, 30, "Disappearance of loading icon");
        verifySubPageNavigation(auditLogsAccess, auditLogsFirstRow, "Audit Logs button");

    }

    /***
     * Method to check if user has access to the Shared Users page
     * @param sharedUsers (yes/no)
     * */
    public void verifySharedUsersPage(String sharedUsers) {
        isNotExpectedElementVisible(loadingIcon, 30, "Disappearance of loading icon");
        verifySubPageNavigation(sharedUsers, sharedUsersFirstRow, "Shared Users Button");

    }

    public void verifyShareStudyButtonIsPresentFirstRow() {
        isNotExpectedElementVisible(loadingIcon, 30, "Disappearance of loading icon");
        Assert.assertTrue(isExpectedElementVisible(shareStudyFirstRow, 30, "Share study icon first row"));

    }

    public void clickOnShareStudyButtonFirstRow() {
        clickOn(shareStudyFirstRow,"Share study icon first row");

    }

    public void verifyShareStudyModal() {
        Assert.assertTrue(isExpectedElementVisible(shareStudyModalHeader, 30, "Share Study Modal header"));

    }

    public void selectUserForSharingStudyFromDropDown(String userName) {
        By selectUserFromDropDown = By.xpath("//div[contains(@class, 'css-26l3qy-menu')]//descendant::div[text()='" + userName + "']");
        clickOn(editableDropdown, "Editable dropdown");
        clickOn(selectUserFromDropDown, 30, "Select user from dropdown - " + userName);

    }

    public void shareStudyWithReviewDoctor() {
        selectUserForSharingStudyFromDropDown("ReviewDoctor, Rendr");
    }

    public void clickOnShareButtonOnShareStudyModal() {
        clickOn(shareButton, "Share button on share study modal");

    }

    public void verifyToastMessageAppearsForSuccessfullySharingStudy() {
        MatchToastMessageValue("Study successfully shared");

    }

}
