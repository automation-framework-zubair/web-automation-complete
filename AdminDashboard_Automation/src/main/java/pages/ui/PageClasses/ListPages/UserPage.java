package pages.ui.PageClasses.ListPages;

import helper.CommonUtility;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

public class UserPage extends BaseClassList {

    // Start: User screen locators
    By pageTitle = By.xpath("//h3[text()=' Users']");
    By createButton = By.xpath("//h3[text()=' Users']/..//a[text()='Create']");
    By editUserButton = By.xpath("//tr[1]//a[@title='Edit']");
    By loadingIcon = By.xpath("//td[text()='Loading...']");
    By getResetPassword = By.xpath("//div[contains(@class, 'toast-container')]//descendant::div[contains(@class, 'body')]");
    By closeIconResetPasswordToastMessage = By.xpath("//i[contains(@class,'close-icon')]");
    By yesButtonResetPassword = By.xpath("//button[text()='Yes']");
    By userAuditLogsButtonFirstRow = By.xpath("//tr[1]//a[contains(@class, 'icon fas fa-list') or @title='Audit Logs']");
    By resetPasswordIconFirstRow = By.xpath("//tr[1]//i[@title='Reset Password']");
    By userDeletionConfirmationMessage = By.xpath("//div[contains(@class,'modal-bool msg')]");
    By systemRoleNone = By.xpath("//tr[contains(@class, 'user-info-row')]//dt[contains(text(),'System Roles:')]/..//dd[text()='None']");
    By facilityUserCard = By.xpath("//div[contains(@class, 'facility-user-cards')]");
    // End: User screen locators

    /**
     * Method to verify if user is successfully navigated to the Users page
     * @param userPageAccess (Yes/No)
     */
    public void verifyUserPageNavigation(String userPageAccess){
        isNotExpectedElementVisible(loadingIcon, 30, "Data load");
        verifyListPageNavigation(userPageAccess, pageTitle, "User page");

    }

    /**
     * Method to verify if the user has access to enter create user page
     * @param createUserAccess (Yes/No)
     */
    public void verifyCreateButtonClick(String createUserAccess){
        verifySubPageNavigation(createUserAccess, createButton, "Create user button");

    }

    /**
     * Method to verify if the user has access to enter edit user page
     * @param editUserAccess (Yes/No)
     */
    public void verifyEditButtonClick(String editUserAccess){
        isNotExpectedElementVisible(loadingIcon, 30, "Loading icon disappearance -");
        verifySubPageNavigation(editUserAccess, editUserButton, "Edit user button");

    }

    /***
     * Method to verify if the user has access to enter Audit logs page or user monitoring page
     * */
    public void verifyAuditLogsButtonClick(String userAuditLogsAccess) {
        isNotExpectedElementVisible(loadingIcon, 30, "Loading icon disappearance -");
        verifySubPageNavigation(userAuditLogsAccess, userAuditLogsButtonFirstRow, "User Audit Logs button (first row) - ");

    }

    /**
     * Method to verify if the has successfully navigated to the users page(without param)
     */
    public void verifyUserPageNavigation(){
        try {
            isNotExpectedElementVisible(loadingIcon,30, "Loading icon disappearance -");
            Assert.assertTrue(isExpectedElementVisible(pageTitle, 30, "User page"));
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /**
     * Method to navigate to the Create User page
     */
    public void goToCreateUserPage() {
        try {
            isExpectedElementVisible(createButton, 30, "Create Button");
            clickOn(createButton, 30, "Create Button click");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /**
     * Method to navigate to the Edit page of first user instance
     */
    public void goToEditUserPage() {
        try {
            isExpectedElementVisible(editUserButton, 30, "Edit Button");
            clickOn(editUserButton, 30, "Edit Button click");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /**
     * Method to click on reset password icon
     * @param firstName First Name of the user
     * @param lastName  Last Name of the user
     * @param middleName Middle Name of the user
     */
    public void resetPasswordIconClickByUserNames(String firstName, String lastName, String middleName){
        By resetPasswordIcon = By.xpath("//td[contains(@title, '" + lastName + "') or contains(@title, '" + firstName + "') or contains(@title, '" + middleName + "')]/..//i[@title='Reset Password']");
        try {
            Assert.assertTrue(isExpectedElementVisible(resetPasswordIcon, 30, "Reset Password Icon"));
            scrollVerticallyTillAppears(resetPasswordIcon);
            Assert.assertTrue(isExpectedElementVisible(resetPasswordIcon, 30, "Reset Password Icon"));
            clickWithJavaScript(resetPasswordIcon);
        } catch (Exception e){
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Method to verify click on reset password icon
     */
    public void resetPasswordIconClick() {
        isNotExpectedElementVisible(loadingIcon, 30, "loading icon disappearance - ");
        Assert.assertTrue(isExpectedElementVisible(resetPasswordIconFirstRow, 30, "Reset password icon first row"));
        try {
            clickOn(resetPasswordIconFirstRow, "Reset password icon first row");
        } catch (Exception e) {
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Method to check that reset password icon is present
     */
    public void checkResetPasswordIconIsPresent() {
        isNotExpectedElementVisible(loadingIcon, 30, "loading icon disappearance - ");
        Assert.assertTrue(isExpectedElementVisible(resetPasswordIconFirstRow, 30, "Reset password icon"));

    }

    /***
     * Method to check that reset password icon is present
     */
    public void checkResetPasswordIconIsNotPresent() {
        isNotExpectedElementVisible(loadingIcon, 30, "loading icon disappearance - ");
        Assert.assertTrue(isNotExpectedElementVisible(resetPasswordIconFirstRow, 30, "Reset password icon"));

    }

    /**
     * Method to get the newly reset password by at first splitting the message using ':' and then omitting the preceding the blank space to isolate the reset password
     * @return resetPassword : contains the value of the reset password
     */
    public String getResetPassword(){
        String resetPassword = "";
        try {
            Assert.assertTrue(isExpectedElementVisible(getResetPassword, 30, "Reset Password Toast Message"));
            String textToastMessage = getText(getResetPassword, 30, "Reset Password Toast message");
            String[] splitText = textToastMessage.split(":");
            String[] splitText2 = splitText[1].split(" ");
            Assert.assertTrue(textToastMessage.contains("password has been reset to:"));
            resetPassword = splitText2[1];
        } catch (Exception e){
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }
        return resetPassword;

    }

    /**
     * Method to close the toast message of Reset password
     */
    public void closeResetPasswordToastMessage(){
        try {
            clickOn(closeIconResetPasswordToastMessage, 30, "close Icon");
        } catch (Exception e){
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Method to check that a particular user exists in the user list
     * @param userLastName Last name of the user
     */
    public void verifyUserExistsInUsersList(String userLastName) {
        By userLocator = By.xpath("//td[contains(@title, '" + userLastName + "')]");
        isNotExpectedElementVisible(loadingIcon,30, "Loading icon disappearance -");
        Assert.assertTrue(isExpectedElementVisible(userLocator, 30, userLastName + " in users list"));

    }

    /***
     * Method to check that a deleted user exists in the users list
     * @param userLastName Last name of the user
     */
    public void verifyDeletedUserExistsInUserListWithDifferentShade(String userLastName) {
        By userLocator = By.xpath("//tr[contains(@class, 'user_row deleted')]//td[contains(@title, '" + userLastName + "') or contains(text(),'" + userLastName + "')]");
        isNotExpectedElementVisible(loadingIcon,30, "Loading icon disappearance -");
        Assert.assertTrue(isExpectedElementVisible(userLocator, 30, userLastName + " in users list"));

    }

    /***
     * Method to check Edit icon is present for a particular user
     * @param userLastName Last name of the user
     * */
    public void checkEditIconIsPresentForUser(String userLastName) {
        By userEditIconLocator = By.xpath("//td[contains(@title,'" + userLastName + "') or contains(text(), '" + userLastName + "')]/..//a[@title='Edit']");
        isNotExpectedElementVisible(loadingIcon,30, "Loading icon disappearance -");
        Assert.assertTrue(isExpectedElementVisible(userEditIconLocator, 30, "User Edit Icon " + userLastName));

    }

    /***
     * Method to check Edit icon is not present for a particular user
     * @param userLastName Last name of the user
     */
    public void checkEditIconIsNotPresentForUser(String userLastName) {
        By userEditIconLocator = By.xpath("//td[contains(@title,'" + userLastName + "') or contains(text(), '" + userLastName + "')]/..//a[@title='Edit']");
        isNotExpectedElementVisible(loadingIcon,30, "Loading icon disappearance -");
        Assert.assertTrue(isNotExpectedElementVisible(userEditIconLocator, 30, "User Edit Icon should not be present - " + userLastName));

    }

    /***
     * Method to check that User Page did not appear
     */
    public void checkUserPageDidNotAppear() {
        Assert.assertTrue(isNotExpectedElementVisible(pageTitle, 30, "User page should not appear -"));

    }

    /***
     * Method to click on the delete icon for a user by their user name
     * @param userLastName Last name of the user
     */
    public void clickOnDeleteIconByUserLastName(String userLastName) {
        By userDeleteIcon = By.xpath("//td[contains(@title, '" + userLastName + "') or contains(text(), '" + userLastName + "')]/..//i[@title='Delete']");
        isNotExpectedElementVisible(loadingIcon,30, "Loading icon disappearance -");
        isExpectedElementVisible(userDeleteIcon, 30, "User Delete icon for - " + userLastName);
        clickOn(userDeleteIcon, 30, "User Delete Icon for - " + userLastName);

    }

    /***
     * Method to click on the info icon for a user by their user name
     * @param userLastName Last name of the user
     */
    public void clickOnInfoIconByUserLastName(String userLastName) {
        By userInfoIcon = By.xpath("//td[contains(@title, '" + userLastName + "') or contains(text(), '" + userLastName + "')]/..//i[@title='Info']");
        isNotExpectedElementVisible(loadingIcon,30, "Loading icon disappearance -");
        isExpectedElementVisible(userInfoIcon, 30, "User Info icon for - " + userLastName);
        clickOn(userInfoIcon, 30, "User Info Icon for - " + userLastName);

    }

    /***
     * Method to check if appropriate system role is displayed for user
     * @param userRole Role of the user in question
     */
    public void checkIfAppropriateSystemRoleDisplayedForUser(String userRole) {
        By userRoleDisplayed = By.xpath("//tr[contains(@class, 'user-info-row')]//dt[contains(text(),'System Roles:')]/..//dd[text()='" + userRole + "']");
        isNotExpectedElementVisible(loadingIcon,30, "Loading icon disappearance -");
        Assert.assertTrue(isExpectedElementVisible(userRoleDisplayed, 30, "User Role displayed or not - " + userRole));

    }

    /***
     * Method to check if appropriate facility role is displayed for user
     * @param userRole Role of the user in question
     */
    public void checkIfAppropriateFacilityRoleDisplayedForUser(String userRole) {
        By userRoleDisplayed = By.xpath("//div[contains(@class, 'facility-user-cards')]//li[text()='"+ userRole + "']");
        isNotExpectedElementVisible(loadingIcon,30, "Loading icon disappearance -");
        isExpectedElementVisible(systemRoleNone, 30, "System role displayed as none" + userRole);
        Assert.assertTrue(isExpectedElementVisible(userRoleDisplayed, 30, "User Role displayed or not - " + userRole));

    }

    /***
     * Method to check that facility user card is not displayed
     */
    public void checkThatFacilityUserCardIsNotDisplayed() {
        isNotExpectedElementVisible(loadingIcon,30, "Loading icon disappearance -");
        Assert.assertTrue(isNotExpectedElementVisible(facilityUserCard, 30, "Facility user card should not be displayed -"));

    }

    /***
     * Method to check that user does not exist in the users list
     * @param userLastName Last name of the user
     */
    public void checkThatUserIsNotPresentInUsersList(String userLastName) {
        isNotExpectedElementVisible(loadingIcon,30, "Loading icon disappearance -");
        By userLocator = By.xpath("//td[contains(@title, '" + userLastName + "') or contains(text(), '" + userLastName + "')]");
        Assert.assertTrue(isNotExpectedElementVisible(userLocator, 30, "User should not be present in users list - " + userLastName));

    }

    /***
     * Method to check that Delete icon is not present for a particular user
     * @param userLastName Last name of the user
     */
    public void checkThatDeleteIconIsNotPresentForUser(String userLastName) {
        By userDeleteIcon = By.xpath("//td[contains(@title, '" + userLastName + "') or contains(text(), '" + userLastName + "')]/..//i[@title='Delete']");
        By userLocator = By.xpath("//td[contains(@title, '" + userLastName + "') or contains(text(), '" + userLastName + "')]");
        isNotExpectedElementVisible(loadingIcon,30, "Loading icon disappearance -");
        isExpectedElementVisible(userLocator, 30, "User should be present in users list - " + userLastName);
        Assert.assertTrue(isNotExpectedElementVisible(userDeleteIcon, 30, "User Delete icon should not be present for - " + userLastName));

    }

    /***
     * Method to check that deleting user's confirmation message is displayed
     */
    public void checkThatUserDeletionConfirmationMessageIsDisplayed() {
        String confirmationMessage = getText(userDeletionConfirmationMessage, 30, "User Deletion confirmation message");
        Assert.assertTrue(confirmationMessage.contains("Are you sure you wish to delete the user"));

    }

    /***
     * Method to check that user names are truncated in confirm user delete modal for long user names
     */
    public void checkThatLongUserNamesAreTruncatedInConfirmUserDeleteModal() {
        Assert.assertTrue(isExpectedElementVisible(userDeletionConfirmationMessage, 30, "User Deletion confirmation message"));
        WebElement element = driverCommand.getDriver().findElement(userDeletionConfirmationMessage);
        Assert.assertEquals(element.getCssValue("text-overflow"), "ellipsis", "User names are not truncated");

    }
}
