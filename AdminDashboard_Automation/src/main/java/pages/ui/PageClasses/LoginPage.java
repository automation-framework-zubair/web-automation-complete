package pages.ui.PageClasses;

import browserutility.Browser;
import helper.CommonUtility;
import helper.EmailUtility;
import helper.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;
import pages.ui.BaseClass;

public class LoginPage extends BaseClass {

    // Start: Login screen web elements
    @FindBy(id = "email")
    WebElement usernmaeTextBox;

    @FindBy(id = "email")
    WebElement emailTextBox;

    @FindBy(id = "password")
    WebElement passwordTextBox;

    @FindBy(id = "confirmPassword")
    WebElement confirmPasswordTextBox;

    @FindBy(xpath = "//div[contains(@class, 'col-sm-6')]")
    WebElement successfulMessageElement;

    // End: Login screen web elements

    // Start: Login screen web locators
    By usernameTextBoxLocator = By.id("email");

    By passwordTextBoxLocator = By.id("password");

    By confirmPasswordTextBoxLocator = By.id("confirmPassword");

    By resetPasswordButtonLocator = By.xpath("//input[@value=\"Reset Password\"]");

    By titleText = By.xpath("//div[contains(@class,'container-fluid')]/descendant::h2[text()='Login']");

    By ForgotPasswordTitle = By.xpath("//h2[text()='Recover Your Account']");

    By loginError = By.xpath("//div[contains(@class,'toast-container')]/descendant::div[@class='body']");

    By successfulMessage = By.xpath("//div[contains(@class, 'col-sm-6')]");

    By sendResetButton = By.xpath("//input[@value='Send Reset']");

    By passwordDisplayedAsDots = By.xpath("//input[@id='password' and @type='password']");

    By passwordAsPlainText = By.xpath("//input[@id='password' and @type='text']");

    By showPasswordButton = By.xpath("//span[contains(@class, 'icon-electrotek-eye-open') and @title='Show password']");

    By hidePasswordButton = By.xpath("//span[contains(@class, 'icon-electrotek-eye-closed') and @title='Hide password']");

    By rendrIconEnabled = By.xpath("//a[@href='/Dashboard' and contains(@class, 'navbar-brand')]");

    By rendrIconDisabled = By.xpath("//a[contains(@class, 'navbar-brand')]");

    By forgotPassword = By.xpath("//a[text()='Forgot Password?']");

    By loginButton = By.xpath("//input[@value='Login']");

    By sendResetButtonDisabled = By.xpath("//input[@value='Send Reset' and @disabled]");

    By emailTextBoxLocator = By.id("email");

    By accountRecoveryPageTitle = By.xpath("//h2[text()='Account Recovery']");

    By successfulResetPasswordPageTitle = By.xpath("//h2[text()='Reset Password']");

    By successfulResetPasswordMessage = By.xpath("//p[contains(text(),'Your password has been successfully reset! Please')]");

    By clientSideMessageForInvalidEmail = By.xpath("//span[text()='Valid Email is required']");

    By clientSideMessageForInvalidPassword = By.xpath("//span[text()='Password that is 8 characters is required']");

    By rendrPlatformLink = By.xpath("//a[text()='rendr PLATFORM']");

    // End: Login screen web locators

    // Start: Page specific constant variable
    String dashboardPageTitle = "Rendr Dashboard";
    String dashboardPageURL = "/Dashboard";
    // End: Page specific constant variable

    /***
     * Implementing type user name functionality
     * @param userName
     */
    public void typeUserName(String userName){
        clearTextUsingKeyboard(usernameTextBoxLocator);
        usernmaeTextBox.sendKeys(userName);

    }

    /***
     * Implementing type password functionality
     * @param password
     */
    public void typePassword(String password){
        clearTextUsingKeyboard(passwordTextBoxLocator);
        passwordTextBox.sendKeys(password);

    }

    /***
     * Implementing typing confirm password
     * @param password
     */
    public void typeConfirmPassword(String password) {
        clearTextUsingKeyboard(confirmPasswordTextBoxLocator);
        confirmPasswordTextBox.sendKeys(password);

    }

    /***
     * Implementing click login button functionality
     */
    public void clickLoginButton(){
        clickOn(loginButton, 30, "Login button");

    }

    /***
    * Implementing click on Forgot Password link functionality
    */
    public void clickForgotPasswordLink(){
        clickOn(forgotPassword, 30, "Forgot Password link");

    }

    /***
     * Implementing login page appearance verification
     */
    public void verifyLoginPage(){
        Assert.assertTrue(isExpectedElementVisible(titleText,30, "Login Page"));

    }

    /***
     * Method to verify login page appeared or not
     * */
    public Boolean verifyLoginPageAppearance() {
        return isExpectedElementVisible(titleText,10, "Login Page");
    }

    /***
     * Implementing login button disappearance verification
     */
    public void loginButtonAppearance(){
        Assert.assertTrue(isNotExpectedElementVisible(titleText,20, "Login button should not be present -"));

    }

    /**
     * Method to get error message shown fo invalid credentials
     */
    public void checkInvalidCredentialsMessage(){
        String actualMessage = getText(loginError,10,"Invalid credentials toast message");
        String expectedMessage = "The user name or password is incorrect.";
        Assert.assertEquals(actualMessage, expectedMessage, "Invalid credentials toast message did not match");

    }

    /***
     * Implementing forgot password page navigation verification
     */
    public void verifyForgotPasswordPage(){
        Assert.assertTrue(isExpectedElementVisible(ForgotPasswordTitle, 30, "Recover Your Account Page"));

    }

    /***
     * Method to enter email in the text box
     */
    public void typeEmailAddress(String email){
        clearTextUsingKeyboard(emailTextBoxLocator);
        emailTextBox.sendKeys(email);

    }

    /***
     * Method to click on the send reset button
     */
    public void sendResetButtonClick(){
        clickOn(sendResetButton, 30, "send reset button");

    }

    /***
     * Method to verify if successful message appears or not
     */
    public void verifySuccessfulMessageForForgotPassword(String email){
        String expectedMessage = "inbox for instructions from us on how to reset your password. If you do not see an email after 10 minutes check the spam folder just in case";
        String actualMessage = successfulMessageElement.getText();
        CommonUtility.logMessagesAndAddThemToReport("Actual message: " + actualMessage, "info");
        Assert.assertTrue(actualMessage.contains(email));
        Assert.assertTrue(actualMessage.contains(expectedMessage));

    }

    /***
     * Method to check that password is not visible
     */
    public void checkPasswordNotVisible() {
        Assert.assertTrue(isNotExpectedElementVisible(passwordAsPlainText, 30, "Password as plain text should not appear -"));
        Assert.assertTrue(isExpectedElementVisible(passwordDisplayedAsDots, 30, "Password as dots"));

    }

    /***
     * Method to check that password is visible
     */
    public void checkPasswordIsVisible() {
        Assert.assertTrue(isNotExpectedElementVisible(passwordDisplayedAsDots, 30, "Password as dots should not appear -"));
        Assert.assertTrue(isExpectedElementVisible(passwordAsPlainText, 30, "Password as plain text"));

    }

    /***
     * Method to click on the show password button
     */
    public void showPasswordButtonClick() {
        clickOn(showPasswordButton, 30, "show password button");

    }

    /***
     * Method to click on the hide password button
     */
    public void hidePasswordButtonClick() {
        clickOn(hidePasswordButton, 30, "hide password button");

    }

    /***
     * Method to check if rendr icon is disabled in the login page
     */
    public void checkRendrIconDisabled() {
        Assert.assertTrue(isExpectedElementVisible(rendrIconDisabled, 30, "rendr icon"));
        Assert.assertTrue(isNotExpectedElementVisible(rendrIconEnabled, 30, "rendr should be disabled -"));

    }

    /***
     * Method to check if forgot password link is visible
     */
    public void verifyForgotPasswordLinkVisible() {
        Assert.assertTrue(isExpectedElementVisible(forgotPassword, 30, "Forgot Password link"));

    }

    /***
     * Method to verify if send reset button is disabled
     */
    public void verifySendResetButtonDisabled(String logMsg) {
        Assert.assertTrue(isExpectedElementVisible(sendResetButtonDisabled, 30, logMsg));

    }

    /***
     * Method to open the login screen in a new tab
     * @param url Url of Portal
     */
    public void openLoginScreenInNewTab(String url) {
        openNewTab();
        changeTabFocus(1);
        Browser.goToUrl(url);

    }

    /***
     * Method to check if login page is not present
     */
    public void verifyLoginPageNotVisible() {
        Assert.assertTrue(isNotExpectedElementVisible(titleText, 30, "login page should not appear"));

    }

    /***
     * Method to verify account recovery page navigation
     */
    public void verifyAccountRecoveryPageNavigation() {
        Assert.assertTrue(isExpectedElementVisible(accountRecoveryPageTitle, 30, "Account Recovery page"));

    }

    /***
     * Method to verify Account Recovery page is not present
     */
    public void verifyAccountRecoveryPageIsNotVisible() {
        Assert.assertTrue(isNotExpectedElementVisible(accountRecoveryPageTitle, 30, "Account recovery page should not appear"));

    }

    /***
     * Method to click on Reset Password button
     */
    public void clickOnResetPasswordButton() {
        clickOn(resetPasswordButtonLocator, 30, "Reset Password Button");

    }

    /***
     * Method to verify successful reset password page navigation
     */
    public void verifySuccessfulResetPasswordPageNavigation() {
        Assert.assertTrue(isExpectedElementVisible(successfulResetPasswordPageTitle, 30, "Successful Reset Password Page"));
        Assert.assertTrue(isExpectedElementVisible(successfulResetPasswordMessage, 30, "Successful Reset Password Message"));

    }

    /***
     * Method to check that client side error message is shown for invalid email
     */
    public void verifyClientSideErrorMessageForInvalidEmail() {
        Assert.assertTrue(isExpectedElementVisible(clientSideMessageForInvalidEmail, 30, "Client Side Error message"));

    }

    /***
     * Method to check that client side error message is shown for invalid password
     */
    public void verifyClientSideErrorMessageForInvalidPassword() {
        Assert.assertTrue(isExpectedElementVisible(clientSideMessageForInvalidPassword, 30, "Client side error message"));

    }

    /***
     * Method to click on the rendr PLATFORM link in the home page
     */
    public void verifyClickOnRendrHyperLink() {
        clickOn(rendrPlatformLink, 30, "rendr HyperLink");

    }

    /***
     * Method to check current url matches the lifelines url
     * @param expectedUrl Expected url of the lifelines page
     */
    public void verifyCurrentUrlMatchesRendrUrl(String expectedUrl) {
        String currentUrl = Browser.getCurrentUrl();
        Assert.assertEquals(currentUrl, expectedUrl, "The url did not match with the expected url");

    }

}
