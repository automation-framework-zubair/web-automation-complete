package pages.ui.PageClasses;

import helper.CommonUtility;
import helper.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.Reporter;
import pages.ui.BaseClass;

public class MenuSidebarPage extends BaseClass {

    // Start: Menu Side bar screen locators
    By patientPagePane = By.xpath("//a[text()='Patients']");
    By studiesPagePane = By.xpath("//a[text()='Studies']");
    By facilitiesPagePane = By.xpath("//a[text()='Facilities']");
    By amplifiersPagePane = By.xpath("//a[text()='Amplifiers']");
    By devicesPagePane = By.xpath("//a[text()='Devices']");
    By usersPagePane = By.xpath("//a[text()='Users']");
    By helpPagePane = By.xpath("//a[text()='Help']");
    By dashboardPagePane = By.xpath("//a[text()='Dashboard']");
    // End: Menu Side bar screen locators

    /***
     * Method to navigate to Dashboard page
     */
    public void goToDashboardPage() {
        clickOn(dashboardPagePane, 30, "Dashboard Page pane");

    }

    /**
     * method to navigate to patients page
     */
    public void goToPatientsPage() {
        clickOn(patientPagePane, "Patient page pane");

    }

    /**
     * method to navigate to studies page
     */
    public void goToStudiesPage() {
        clickOn(studiesPagePane, "Studies page pane");

    }

    /**
     * method to navigate to facilities page
     */
    public void goToFacilitiesPage() {
        clickOn(facilitiesPagePane, "Facilities page pane");

    }

    /**
     * method to navigate to amplifiers page
     */
    public void goToAmplifiersPage() {
        clickOn(amplifiersPagePane, "Amplifiers page pane");

    }

    /**
     * method to navigate to devices page
     */
    public void goToDevicesPage() {
        clickOn(devicesPagePane, "Devices page pane");

    }

    /**
     * method to navigate to users page
     */
    public void goToUsersPage() {
        clickOn(usersPagePane, "Users page pane");

    }

    /**
     * method to navigate to help page
     */
    public void goToHelpPage() {
        clickOn(helpPagePane, "Help page pane");

    }

    /**
     * method to verify patients list access
     * @param patientPageAccess (Yes/No)
     */
    public void patientPageVerification(String patientPageAccess) {
        if(patientPageAccess.toLowerCase().equals("yes")) {
            try {
                Assert.assertTrue(clickVerify(patientPagePane, "Patient page pane"));
            } catch (Exception e) {
                e.printStackTrace();
                CommonUtility.logExceptionsToTheReport(e);
            }

        } else if(patientPageAccess.toLowerCase().equals("no")) {
            try {
                Assert.assertTrue(isNotExpectedElementVisible(patientPagePane,10, "Patient page pane"));
            } catch (Exception e) {
                e.printStackTrace();
                CommonUtility.logExceptionsToTheReport(e);
            }

        } else {
            CommonUtility.logMessagesAndAddThemToReport("Wrong text provided in the CSV file of patient page", "fail");
            Assert.fail();

        }

    }

    /**
     * method to verify studies list access
     * @param studyPageAccess (Yes/No)
     */
    public void studyPageVerification(String studyPageAccess) {
        if(studyPageAccess.toLowerCase().equals("yes")) {
            try {
                Assert.assertTrue(clickVerify(studiesPagePane, "Study page pane"));
            } catch (Exception e) {
                e.printStackTrace();
                CommonUtility.logExceptionsToTheReport(e);
            }

        } else if(studyPageAccess.toLowerCase().equals("no")) {
            try {
                Assert.assertTrue(isNotExpectedElementVisible(studiesPagePane,10, "Study page pane"));
            } catch (Exception e) {
                e.printStackTrace();
                CommonUtility.logExceptionsToTheReport(e);
            }
            
        } else {
            CommonUtility.logMessagesAndAddThemToReport("Wrong text provided in the CSV file of patient page", "fail");
            Assert.fail();

        }

    }

    /**
     * method to verify facilities list access
     * @param facilityPageAccess (Yes/No)
     */
    public void facilityPageVerification(String facilityPageAccess) {
        if(facilityPageAccess.toLowerCase().equals("yes")) {
            try {
                Assert.assertTrue(clickVerify(facilitiesPagePane, "Facility page pane"));
            } catch (Exception e) {
                e.printStackTrace();
                CommonUtility.logExceptionsToTheReport(e);
            }

        } else if(facilityPageAccess.toLowerCase().equals("no")) {
            try {
                Assert.assertTrue(isNotExpectedElementVisible(facilitiesPagePane,10, "Facility page pane"));
            } catch (Exception e) {
                e.printStackTrace();
                CommonUtility.logExceptionsToTheReport(e);
            }

        } else {
            CommonUtility.logMessagesAndAddThemToReport("Wrong text provided in the CSV file of patient page", "fail");
            Assert.fail();

        }

    }

    /**
     * method to verify amplifiers list access
     * @param amplifierPageAccess (Yes/No)
     */
    public void amplifierPageVerification(String amplifierPageAccess) {
        if(amplifierPageAccess.toLowerCase().equals("yes")) {
            try {
                Assert.assertTrue(clickVerify(amplifiersPagePane, "Amplifier page pane - "));
            } catch (Exception e) {
                e.printStackTrace();
                CommonUtility.logExceptionsToTheReport(e);
            }

        } else if(amplifierPageAccess.toLowerCase().equals("no")) {
            try {
                Assert.assertTrue(isNotExpectedElementVisible(amplifiersPagePane,10, "Amplifier page should not appear - "));
            } catch (Exception e) {
                e.printStackTrace();
                CommonUtility.logExceptionsToTheReport(e);
            }

        } else {
            CommonUtility.logMessagesAndAddThemToReport("Wrong text provided in the CSV file of patient page", "fail");
            Assert.fail();

        }

    }

    /**
     * method to verify devices list access
     * @param devicePageAccess (Yes/No)
     */
    public void devicePageVerification(String devicePageAccess) {
        if(devicePageAccess.toLowerCase().equals("yes")) {
            try {
                Assert.assertTrue(clickVerify(devicesPagePane, "Device page pane"));
            } catch (Exception e) {
                e.printStackTrace();
                CommonUtility.logExceptionsToTheReport(e);
            }

        } else if(devicePageAccess.toLowerCase().equals("no")) {
            try {
                Assert.assertTrue(isNotExpectedElementVisible(devicesPagePane,10, "Device page pane"));
            } catch (Exception e) {
                e.printStackTrace();
                CommonUtility.logExceptionsToTheReport(e);
            }

        } else {
            CommonUtility.logMessagesAndAddThemToReport("Wrong text provided in the CSV file of patient page", "fail");
            Assert.fail();

        }

    }

    /**
     * method to verify User list access
     * @param userPageAccess (Yes/No)
     */
    public void userPageVerification(String userPageAccess) {
        if(userPageAccess.toLowerCase().equals("yes")) {
            try {
                Assert.assertTrue(clickVerify(usersPagePane, "User page pane"));
            } catch (Exception e) {
                e.printStackTrace();
                CommonUtility.logExceptionsToTheReport(e);
            }

        } else if(userPageAccess.toLowerCase().equals("no")) {
            try {
                Assert.assertTrue(isNotExpectedElementVisible(usersPagePane,10, "User page pane"));
            } catch (Exception e) {
                e.printStackTrace();
                CommonUtility.logExceptionsToTheReport(e);
            }

        } else {
            CommonUtility.logMessagesAndAddThemToReport("Wrong text provided in the CSV file of patient page", "fail");
            Assert.fail();

        }

    }

    /**
     * method to verify Dashboard page access
     * @param dashboardPageAccess (Yes/No)
     */
    public void dashboardPageVerification(String dashboardPageAccess) {
        if(dashboardPageAccess.toLowerCase().equals("yes")) {
            try {
                Assert.assertTrue(clickVerify(dashboardPagePane, "Dashboard page pane"));
            } catch (Exception e) {
                e.printStackTrace();
                CommonUtility.logExceptionsToTheReport(e);
            }

        } else if(dashboardPageAccess.toLowerCase().equals("no")) {
            try {
                Assert.assertTrue(isNotExpectedElementVisible(dashboardPagePane,10, "Dashboard page pane"));
            } catch (Exception e) {
                e.printStackTrace();
                CommonUtility.logExceptionsToTheReport(e);
            }

        } else {
            CommonUtility.logMessagesAndAddThemToReport("Wrong text provided in the CSV file of patient page", "fail");
            Assert.fail();

        }

    }

}
