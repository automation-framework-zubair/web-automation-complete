package pages.ui.PageClasses;

import helper.CommonUtility;
import org.openqa.selenium.By;
import org.testng.Assert;
import pages.ui.BaseClass;

public class ProfilePage extends BaseClass {

    // Start: Profile page web locators
    By profilePic = By.xpath("//div[@id='profile_pic']");
    By showPasswordButtonForNewPasswordField = By.xpath("//input[@id='NewPassword']/..//descendant::span[contains(@class, 'show-password')]");
    By showPasswordButtonForConfirmPasswordField = By.xpath("//input[@id='ConfirmPassword']/..//descendant::span[contains(@class, 'show-password')]");
    By showPasswordButtonForCurrentPasswordField = By.xpath("//input[@id='CurrentPassword']/..//descendant::span[contains(@class, 'show-password')]");
    By passwordStrengthErrorMessageForRepeatedText = By.xpath("//div[text()='Repeats like \"aaa\" are easy to guess']");
    By passwordStrengthErrorMessageForCommonPassword = By.xpath("//div[text()='This is a very common password']");
    By newPasswordField = By.id("NewPassword");
    By confirmPasswordField = By.id("ConfirmPassword");
    By currentPasswordField = By.id("CurrentPassword");
    By doesNotMatchPasswordErrorMessage = By.xpath("//div[text()='Does not match Password']");
    By errorMessageForIncorrectCurrentPassword = By.xpath("//div[text()='Unable to change password']");
    By changePasswordButton = By.xpath("//input[@id='btn_submit' and @value='Change Password']");
    By passwordChangedToastMessage = By.xpath("//div[text()='Password has been changed']");
    By monitoredStudiesButton = By.xpath("//a[contains(@class, 'btn-success') and text()='Monitored Studies']");
    By lastNameOfTheUserLocator = By.xpath("//dt[text()='Last Name']/..//dd");
    // End: Profile page web locators

    /***
     * To validate Profile page navigation
     */
    public void verifyProfilePageNavigation(){

        try{
            Assert.assertTrue(isExpectedElementVisible(profilePic, 10,"Profile page"));
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * To get the user role
     * @param userRole
     */
    public void verifyUserRole(String userRole){
        try{
            Assert.assertTrue(isExpectedElementVisible(By.xpath("//div[@id='user_description' and text() = '" + userRole + "']"), 30, "Proper user role appeared - " + userRole));
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Method to validate show password buttons are present
     */
    public void verifyShowPasswordButtonsArePresent() {
        Assert.assertTrue(isExpectedElementVisible(showPasswordButtonForNewPasswordField, 30, "Show password button for new password field"));
        Assert.assertTrue(isExpectedElementVisible(showPasswordButtonForConfirmPasswordField, 30, "Show password button for confirm password field"));
        Assert.assertTrue(isExpectedElementVisible(showPasswordButtonForCurrentPasswordField, 30, "Show password button for current password field"));

    }

    /***
     * Method to enter text into the New Password field
     */
    public void typeNewPassword(String text) {
        try {
            clearTextUsingKeyboard(newPasswordField);
            sendKeys(newPasswordField, text, "New Password field");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Method to enter text into the Confirm password field
     */
    public void typeConfirmPassword(String text) {
        try {
            clearTextUsingKeyboard(confirmPasswordField);
            sendKeys(confirmPasswordField, text, "Confirm password field");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Method to enter text into the current password field
     */
    public void typeCurrentPassword(String text) {
        try {
            clearTextUsingKeyboard(currentPasswordField);
            sendKeys(currentPasswordField, text, "Current password field");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtility.logExceptionsToTheReport(e);
        }

    }

    /***
     * Method to check error message appears for repeated text
     */
    public void verifyErrorMessageForRepeatedText() {
        Assert.assertTrue(isExpectedElementVisible(passwordStrengthErrorMessageForRepeatedText, 30, "Error message for repeated text"));

    }

    /***
     * Method to check error message appears for common password
     */
    public void verifyErrorMessageForCommonPassword() {
        Assert.assertTrue(isExpectedElementVisible(passwordStrengthErrorMessageForCommonPassword, 30, "Error message for repeated text"));

    }

    /***
     * Method to check error message appears for password not matching in the new and confirm password fields
     */
    public void verifyErrorMessageForMismatchedPasswordOfNewAndConfirmPasswordFields() {
        Assert.assertTrue(isExpectedElementVisible(doesNotMatchPasswordErrorMessage, 30, "Error message if the passwords do not match"));

    }

    /***
     * Method to check error message does not appear if passwords entered in new and confirm password fields matches
     */
    public void verifyErrorMessageDoesNotAppearForMatchedPasswords() {
        Assert.assertTrue(isNotExpectedElementVisible(doesNotMatchPasswordErrorMessage, 30, "Error message should not appear"));

    }

    /***
     * Method to check error message appears for incorrect current password
     */
    public void verifyErrorMessageForIncorrectCurrentPassword() {
        Assert.assertTrue(isExpectedElementVisible(errorMessageForIncorrectCurrentPassword, 30, "Error message for incorrect current password"));

    }

    /***
     * Method to click on the change password button
     */
    public void clickOnChangePasswordButton() {
        scrollVerticallyTillAppears(changePasswordButton);
        clickOn(changePasswordButton, 30, "Change Password Button");

    }

    /***
     * Method to check appearance of successful toast message indicating that password has been changed
     */
    public void verifyToastMessageForSuccessfulPasswordChange() {
        Assert.assertTrue(isExpectedElementVisible(passwordChangedToastMessage, 30, "Successful password change toast message"));

    }

    /***
     * Method to click on monitored studies button
     * @param monitoredStudiesAccess
     */
    public void clickOnMonitoredStudiesButton(String monitoredStudiesAccess) {
        verifySubPageNavigation(monitoredStudiesAccess, monitoredStudiesButton, "Monitored Studies button");

    }

    /***
     * Method to get the last name of the currently logged in user
     * @return user's last name
     * */
    public String getLastNameOfTheCurrentlyLoggedInUser() {
        return getText(lastNameOfTheUserLocator, "Last name of the logged in user");

    }

}
