package pages.ui.PageClasses;

import org.openqa.selenium.By;
import pages.ui.BaseClass;

public class SharedUsersPage extends BaseClass {

    // Start: Shared Users page locators
    By pageTitle = By.xpath("//h3[text()='Shared Users']");
    By loadingIcon = By.xpath("//td[text()='Loading...']");
    //End: Shared Users page locators

    /***
     * Method to verify that the user has access to the Shared Users page
     * @param sharedUsers (yes/no)
     * */
    public void verifySharedUsersPageNavigation(String sharedUsers) {
        isNotExpectedElementVisible(loadingIcon, 30, "Data load");
        verifyListPageNavigation(sharedUsers, pageTitle, "Shared Users");

    }

}
