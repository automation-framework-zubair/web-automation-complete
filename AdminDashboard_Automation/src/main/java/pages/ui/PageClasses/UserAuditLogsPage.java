package pages.ui.PageClasses;

import helper.CommonUtility;
import org.openqa.selenium.By;
import org.testng.Assert;
import pages.ui.BaseClass;

public class UserAuditLogsPage extends BaseClass {

    // Start: User Audit Logs page web locators
    By userMonitoringIcon = By.xpath("//title[text()='Rendr Review - User Monitoring History']/../..//descendant::i[contains(@class, 'icon-electrotek-monitoring mr-3')]");
    By userMonitoringHeader = By.xpath("//h3[contains(text(), 'Monitoring History')]");
    // End: User Audit Logs page web locators


    /***
     * Method to verify if user is successfully navigated to the User Audit Logs page
     */
    public void verifyUserAuditLogsPageNavigation(String userAuditLogsPageAccess) {
        verifyListPageNavigation(userAuditLogsPageAccess, userMonitoringHeader, "User Audit Logs Page Header");

    }

    /***
     * Method to verify if user is navigated to the user audit logs page for the currently logged in user
     * @param userLastName Last name of the user
     * @param userAuditLogsPageAccess If user has access to monitored studies / user audit logs page
     */
    public void verifyNavigationToUserAuditLogsPageOfCurrentUser(String userLastName, String userAuditLogsPageAccess) {
        By userAuditLogsPageHeader = By.xpath("//h3[contains(text(), 'Monitoring History') and contains(@title, '" + userLastName + "')]");
        verifyListPageNavigation(userAuditLogsPageAccess, userAuditLogsPageHeader, "User Audit Logs page for - " + userLastName);

    }

}
