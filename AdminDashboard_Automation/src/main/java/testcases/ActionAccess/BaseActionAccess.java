package testcases.ActionAccess;
import helper.CommonUtility;
import pages.ui.BaseClass;
import testcases.BaseTest;

import helper.Logger;
import org.testng.Reporter;
import pages.PagesFactory;
import pages.ui.PageClasses.*;
import pages.ui.PageClasses.CreateEditPages.*;
import pages.ui.PageClasses.ListPages.*;
import testcases.DataDeletion;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

public class BaseActionAccess extends BaseTest {

    LoginPage loginPage;
    DashboardPage dashboardPage;
    ProfilePage profilePage;
    MenuSidebarPage menuSidebarPage;
    PatientsPage patientsPage;
    HeaderPanel headerPanel;
    PatientCreatePage patientCreatePage;
    PatientEditPage patientEditPage;
    ImportStudyPage importStudyPage;
    FacilityPage facilityPage;
    FacilityCreatePage facilityCreatePage;
    FacilityEditPage facilityEditPage;
    AmplifierPage amplifierPage;
    AmplifierCreatePage amplifierCreatepage;
    AmplifierEditPage amplifierEditPage;
    DevicePage devicePage;
    DeviceCreatePage deviceCreatePage;
    DeviceEditPage deviceEditPage;
    ChangeDeviceFacilityPage changeDeviceFacilityPage;
    UserPage userPage;
    UserCreatePage userCreatePage;
    UserEditPage userEditPage;

    Format f = new SimpleDateFormat("MMddyyyy_hhmm");
    Format g = new SimpleDateFormat("hhmmss");
    String dateTime = f.format(new Date());
    String time = g.format(new Date());
    DataDeletion dataDeletion = new DataDeletion();

    /***
     * Implementing test case to validate Login page
     * @param email
     * @param password
     */
    public void actionAccessScriptForLogin(String email, String password)
    {
        loginPage = PagesFactory.getLoginPage();

        loginPage.verifyLoginPage();
        loginPage.typeUserName(email);
        loginPage.typePassword(password);
        CommonUtility.logMessagesAndAddThemToReport("User Credentials Given", "info");
        CommonUtility.logMessagesAndAddThemToReport("User email : " + email, "info");
        loginPage.clickLoginButton();
        loginPage.loginButtonAppearance();
        CommonUtility.saveLoggedInUserDetailsToAFile(email, password);

    }

    /***
     * Implementing test case to validate user role
     * @param userRole
     */
    public void actionAccessScriptForUserRole(String userRole)
    {
        dashboardPage = PagesFactory.getDashboardPage();
        headerPanel = PagesFactory.getheaderPanel();
        profilePage = PagesFactory.getProfilePage();

        headerPanel.yourProfileButtonClick();

        profilePage.verifyProfilePageNavigation();
        profilePage.verifyUserRole(userRole);

    }

    /***
     * Implementing test case to validate patient module (list) access
     * @param patientsList
     */
    public void actionAccessScriptForPatientModule_List(String patientsList)
    {
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        patientsPage = PagesFactory.getPatientsPage();

        menuSidebarPage.patientPageVerification(patientsList);
        patientsPage.verifyPatientPageNavigation(patientsList);

    }

    /***
     * Implementing test case to for dashboard page navigation
     * @param dashboardValue
     */
    public void actionAccessScriptForDashboardModule(String dashboardValue)
    {
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        dashboardPage = PagesFactory.getDashboardPage();

        menuSidebarPage.dashboardPageVerification(dashboardValue);

        dashboardPage.verifyDashboardPageNavigation();

    }

    /***
     * Implementing test case to validate patient module (create) access
     * @param patientsList
     * @param createPatient
     */
    public void actionAccessScriptForPatientModule_Create(String patientsList, String createPatient)
    {
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        patientsPage = PagesFactory.getPatientsPage();
        patientCreatePage = PagesFactory.getPatientCreatePage();
        patientEditPage = PagesFactory.getPatientEditPage();

        menuSidebarPage.patientPageVerification(patientsList);

        patientsPage.verifyPatientPageNavigation(patientsList);
        patientsPage.verifyCreateButtonClick(createPatient);

        patientCreatePage.verifyCreatePatientPageNavigation(createPatient);
        patientCreatePage.createPatient(createPatient, dateTime + "FN", "MN", "LN",
                "Male", "159753842", "", "Patient created!");

    }

    /***
     * Implementing test case to validate patient module (edit) access
     * @param patientsList
     * @param editPatient
     */
    public void actionAccessScriptForPatientModule_Edit(String patientsList, String editPatient)
    {
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        patientsPage = PagesFactory.getPatientsPage();
        patientCreatePage = PagesFactory.getPatientCreatePage();
        patientEditPage = PagesFactory.getPatientEditPage();

        menuSidebarPage.patientPageVerification(patientsList);
        patientsPage.verifyPatientPageNavigation(patientsList);
        patientsPage.clickOnEditIconForSelectedItem(editPatient, dateTime, "Patients");

        patientEditPage.verifyEditPatientPageNavigation(editPatient);
        patientEditPage.editPatient(editPatient, dateTime + "FN_Edited", "MN_Edited", "LN_Edited",
                "Female", "260817130", "", "Patient saved!");

    }

    /***
     * Implementing test case to validate import study access
     * @param patientsList if user has access to Patients list
     * @param importStudy if has access to import study
     * */
    public void actionAccessScriptForPatientModule_ImportStudy(String patientsList, String importStudy)
    {
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        patientsPage = PagesFactory.getPatientsPage();
        importStudyPage = PagesFactory.getImportStudyPage();

        menuSidebarPage.patientPageVerification(patientsList);
        patientsPage.verifyPatientPageNavigation(patientsList);
        patientsPage.clickOnImportIconByPatientName(importStudy, dateTime);
        importStudyPage.verifyImportStudyPageNavigation(importStudy);
        if(importStudy.toLowerCase().equals("yes")) {
            importStudyPage.uploadFilesForImportStudy("ActionAccess_ImportStudy");
            importStudyPage.verifyPatientAndStudyInformationIsDisplayed();
            importStudyPage.clickOnContinueButton();
            importStudyPage.verifyProgressOfUploadIsDisplayed();
            importStudyPage.verifyPresenceOfSuccessfulMessageForImportStudy();
        }
        importStudyPage.closeImportStudyPage(importStudy);

    }

    /***
     * Implementing test case to validate facility module (list) list access
     * @param facilityList
     */
    public void actionAccessScriptForFacilityModule_List(String facilityList)
    {
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        facilityPage = PagesFactory.getFacilityPage();

        menuSidebarPage.facilityPageVerification(facilityList);

        facilityPage.verifyFacilityPageNavigation(facilityList);
    }

    /***
     * Implementing test case to validate facility module (create) access
     * @param facilityList
     * @param createFacility
     */
    public void actionAccessScriptForFacilityModule_Create(String facilityList, String createFacility, String role)
    {
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        facilityPage = PagesFactory.getFacilityPage();
        facilityCreatePage = PagesFactory.getFacilityCreatePage();

        menuSidebarPage.facilityPageVerification(facilityList);

        facilityPage.verifyFacilityPageNavigation(facilityList);
        facilityPage.verifyCreateButtonClick(createFacility);

        facilityCreatePage.verifyCreateFacilityPageNavigation(createFacility);
        facilityCreatePage.createFacility(createFacility, dateTime + role, dateTime + role, "Facility created!");

    }

    /***
     * Implementing test case to validate facility module (edit) access
     * @param facilityList
     * @param editFacility
     */
    public void actionAccessScriptForFacilityModule_Edit(String facilityList, String editFacility, String role)
    {
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        facilityPage = PagesFactory.getFacilityPage();
        facilityEditPage = PagesFactory.getFacilityEditPage();

        menuSidebarPage.facilityPageVerification(facilityList);

        facilityPage.verifyFacilityPageNavigation(facilityList);
        facilityPage.clickEditIconByInstanceName(editFacility, dateTime, "Facilities");

        facilityEditPage.verifyEditFacilityPageNavigation(editFacility);
        facilityEditPage.editFacility(editFacility, dateTime + role + "_Edited", dateTime + role + "_Edited", "After 10 days",
                                    dateTime + "Test Note Template", "Facility saved!");

    }

    /***
     * Implementing test case to validate facility module edit access for FACILITY ADMIN (Single facility) - We also don't want to change the name domain of MobileMedTek facility!
     * @param facilityList
     * @param editFacility
     */
    public void actionAccessScriptForFacilityModule_Edit_FA(String facilityList, String editFacility, String role)
    {
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        facilityPage = PagesFactory.getFacilityPage();
        facilityEditPage = PagesFactory.getFacilityEditPage();

        menuSidebarPage.facilityPageVerification(facilityList);

        facilityPage.verifyFacilityPageNavigation(facilityList);
        facilityPage.clickEditIconByInstanceName(editFacility, "Lifelines Neuro Company", "Facilities");

        facilityEditPage.verifyEditFacilityPageNavigation(editFacility);
        facilityEditPage.editFacility_FA(editFacility, "After 10 days", dateTime + role + "Test Note Template", "Facility saved!");

    }

    /***
     * Implementing test case to validate amplifier module (list) access
     * @param amplifierList
     */
    public void actionAccessScriptForAmplifierModule_List(String amplifierList) {
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        amplifierPage = PagesFactory.getAmplifierPage();

        menuSidebarPage.amplifierPageVerification(amplifierList);

        amplifierPage.verifyAmplifierPageNavigation(amplifierList);
    }

    /***
     * Implementing test case to validate amplifier module (creat)e access
     * @param amplifierList
     * @param createAmplifier
     */
    public void actionAccessScriptForAmplifierModule_Create(String amplifierList, String createAmplifier, String role) {
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        amplifierPage = PagesFactory.getAmplifierPage();
        amplifierCreatepage = PagesFactory.getAmplifierCreatePage();

        menuSidebarPage.amplifierPageVerification(amplifierList);

        amplifierPage.verifyAmplifierPageNavigation(amplifierList);
        amplifierPage.verifyCreateButtonClick(createAmplifier);

        amplifierCreatepage.verifyCreateAmplifierPageNavigation(createAmplifier);
        amplifierCreatepage.createAmplifier(createAmplifier, dateTime + role, time + role, "bluetooth ID", "usb ID", "Rendr", "Yes", "Lifelines Neuro Company", "Amplifier created!");

    }

    /***
     * Implementing test case to validate amplifier module (edit) access
     * @param amplifierList
     * @param editAmplifier
     */
    public void actionAccessScriptForAmplifierModule_Edit(String amplifierList, String editAmplifier, String role) {
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        amplifierPage = PagesFactory.getAmplifierPage();
        amplifierEditPage = PagesFactory.getAmplifierEditPage();

        menuSidebarPage.amplifierPageVerification(amplifierList);

        amplifierPage.verifyAmplifierPageNavigation(amplifierList);
        amplifierPage.clickEditIconByInstanceName(editAmplifier, dateTime, "Amplifiers");

        amplifierEditPage.verifyEditAmplifierPageNavigation(editAmplifier);
        amplifierEditPage.editAmplifier(editAmplifier, dateTime + role + "_Edited", time + role + "ED", "edit", "edit", "Trackit Mk3", "No", "Amplifier saved!");

    }

    /***
     * Implementing test case to validate device module (list) access
     * @param deviceList
     */
    public void actionAccessScriptForDeviceModule_List(String deviceList) {
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        devicePage = PagesFactory.getDevicePage();

        menuSidebarPage.devicePageVerification(deviceList);

        devicePage.verifyDevicesPageNavigation(deviceList);

    }

    /***
     * Implementing test case to validate device module (create) access
     * @param deviceList
     * @param createDevice
     */
    public void actionAccessScriptForDeviceModule_Create(String deviceList, String createDevice) {
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        devicePage = PagesFactory.getDevicePage();
        deviceCreatePage = PagesFactory.getCreateDevicePage();

        menuSidebarPage.devicePageVerification(deviceList);

        devicePage.verifyDevicesPageNavigation(deviceList);
        devicePage.verifyCreateButtonClick(createDevice);

        deviceCreatePage.verifyCreateDevicePageNavigation(createDevice);
        deviceCreatePage.createDevice(createDevice, dateTime + "Device Name", "Part Number", "Configuration",
                "Serial Number", "Lifelines Neuro Company", "Device Created!");

    }

    /***
     * Implementing test case to validate device module (edit) access
     * @param deviceList
     * @param editDevice
     */
    public void actionAccessScriptForDeviceModule_Edit(String deviceList, String editDevice) {
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        devicePage = PagesFactory.getDevicePage();
        deviceEditPage = PagesFactory.getEditDevicePage();

        menuSidebarPage.devicePageVerification(deviceList);

        devicePage.verifyDevicesPageNavigation(deviceList);
        devicePage.clickEditIconByInstanceName(editDevice, dateTime, "Devices");

        deviceEditPage.verifyEditDevicePageNavigation(editDevice);
        deviceEditPage.editDevice(editDevice, dateTime + "DN_Edited", "PN_Edited", "CNFG_Edited", "SN_Edited", "Device saved!");
    }

    /***
     * Implementing test case to validate change device facility access
     * @param deviceList user access to devices list (yes/no)
     * @param changeDeviceFacility user access to change device facility (yes/no)
     */
    public void actionAccessScriptForDeviceModule_ChangeFacility(String deviceList, String changeDeviceFacility) {
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        devicePage = PagesFactory.getDevicePage();
        deviceEditPage = PagesFactory.getEditDevicePage();
        changeDeviceFacilityPage = PagesFactory.getChangeDeviceFacilityPage();
        headerPanel = PagesFactory.getheaderPanel();

        menuSidebarPage.devicePageVerification(deviceList);
        devicePage.verifyDevicesPageNavigation(deviceList);
        devicePage.clickOnChangeDeviceFacilityIconByDeviceName(changeDeviceFacility, dateTime);
        changeDeviceFacilityPage.verifyChangeDeviceFacilityPageNavigation(changeDeviceFacility);
        changeDeviceFacilityPage.changeDeviceFacility(changeDeviceFacility, dateTime);
        menuSidebarPage.devicePageVerification(deviceList);
        if(changeDeviceFacility.toLowerCase().equals("yes")) {
            headerPanel.selectFacilityFromDropdown(dateTime);
            devicePage.clickOnChangeDeviceFacilityIconByDeviceName(changeDeviceFacility, dateTime);
            changeDeviceFacilityPage.verifyChangeDeviceFacilityPageNavigation(changeDeviceFacility);
            changeDeviceFacilityPage.changeDeviceFacility(changeDeviceFacility, "Lifelines Neuro Company");
            menuSidebarPage.devicePageVerification(deviceList);
            headerPanel.selectFacilityFromDropdown("Lifelines Neuro Company");
        } else if(changeDeviceFacility.toLowerCase().equals("no")) {
            CommonUtility.logMessagesAndAddThemToReport("User does not have access to Change Device Facility", "info");
        }
    }

    /***
     * Implementing test case to validate user module (list) access
     * @param userList
     */
    public void actionAccessScriptForUserModule_List(String userList) {
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        userPage = PagesFactory.getUserPage();

        menuSidebarPage.userPageVerification(userList);
        userPage.verifyUserPageNavigation(userList);

    }

    /***
     * Implementing test case to validate user module (create) access
     * @param userList
     * @param createUser
     */
    public void actionAccessScriptForUserModule_Create(String userList, String createUser, String role) {
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        userPage = PagesFactory.getUserPage();
        userCreatePage = PagesFactory.getUserCreatePage();

        menuSidebarPage.userPageVerification(userList);

        userPage.verifyUserPageNavigation(userList);
        userPage.verifyCreateButtonClick(createUser);

        userCreatePage.verifyCreateUserPageNavigation(createUser);
        userCreatePage.createUser(createUser, "Title", "First Name", "Middle Name", "Last Name", "Suffix",
                dateTime + role + "@enosisbd.com", "Enosis123", "Enosis123", "ReviewDoctor", "User created!");

    }

    /***
     * Implementing test case to validate user module( edit) access
     * @param userList
     * @param editUser
     */
    public void actionAccessScriptForUserModule_Edit(String userList, String editUser, String role) {
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        userPage = PagesFactory.getUserPage();
        userEditPage = PagesFactory.getUserEditPage();

        menuSidebarPage.userPageVerification(userList);

        userPage.verifyUserPageNavigation(userList);
        userPage.clickEditIconByInstanceName(editUser, dateTime, "Users");

        userEditPage.verifyEditUserPageNavigation(editUser);
        userEditPage.editUser(editUser, "Title_Edited", "First Name_Edited", "Middle Name_Edited", "Last Name_Edited",
                              "Suffix_Edited", dateTime + role + "@yahoo.com", "LeadTech", "User saved!");

    }

    /***
     * For user sign out
     */
    public void actionAccessScriptForUserSignOut() {
        headerPanel = PagesFactory.getheaderPanel();
        loginPage = PagesFactory.getLoginPage();
        CommonUtility.logMessagesAndAddThemToReport("Now signing out", "info");
        headerPanel.signOut();
        loginPage.verifyLoginPage();
        CommonUtility.deleteLoggedInUserDetailsFile();

    }

    /***
     * For deleting patients that were created during test execution
     * @param editPatient If user is allowed access to edit patient page
     */
    public void patientDeletion(String editPatient) {
        dataDeletion.patientDeletion(editPatient, dateTime);

    }

    /***
     * For deleting amplifiers that were created during test execution
     * @param editAmplifier If user is allowed access to edit Amplifier page
     */
    public void amplifierDeletion(String editAmplifier) {
        dataDeletion.amplifierDeletion(editAmplifier, dateTime);

    }

    /***
     * For deleting devices that were created during test execution
     * @param editDevice if user is allowed access to edit Device page
     */
    public void deviceDeletion(String editDevice) {
        dataDeletion.deviceDeletion(editDevice, dateTime);

    }

    /***
     * For deleting Facilities that were created during test execution
     * @param editFacility if user is allowed access to edit facility page
     */
    public void facilityDeletion(String editFacility) {
        dataDeletion.facilityDeletion(editFacility, dateTime);

    }

    /***
     * For deleting users that were created during test execution
     * @param editUser if user is allowed access to edit user page
     */
    public void userDeletion(String editUser) {
        dataDeletion.userDeletion(editUser, dateTime);

    }

    /***
     * For verifying presence or error toast message
     */
    public void verifyPresenceOfErrorToastMessage() {
        dashboardPage = PagesFactory.getDashboardPage();
        dashboardPage.verifyErrorToastMessage();

    }

    /***
     * For clicking on the error toast message
     */
    public void clickOnErrorToastMessage() {
        dashboardPage = PagesFactory.getDashboardPage();
        dashboardPage.clickOnErrorToastMessage();

    }

    /***
     * For verifying absence or error toast message
     */
    public boolean verifyAbsenceOfErrorToastMessage() {
        dashboardPage = PagesFactory.getDashboardPage();
        return dashboardPage.verifyAbsenceOfErrorToastMessage();

    }

    /***
     * Method to close second tab in case of test case failure
     */
    public void closeSecondTab() {
        importStudyPage = PagesFactory.getImportStudyPage();
        importStudyPage.closeImportStudyPage("yes");

    }

}
