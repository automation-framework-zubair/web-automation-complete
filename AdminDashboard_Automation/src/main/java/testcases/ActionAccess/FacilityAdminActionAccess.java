package testcases.ActionAccess;

import dataprovider.DataProviderClass;
import helper.CommonUtility;
import helper.Logger;
import org.testng.Reporter;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

public class FacilityAdminActionAccess extends BaseActionAccess {

    String userRoleValue;
    String emailValue;
    String passwordValue;
    String dashboardValue;
    String infoBoxValue;
    String latestStudiesValue;
    String patientsListValue;
    String createPatientValue;
    String editPatientValue;
    String importStudyValue;
    String patientDocumentsTabValue;
    String patientAuditTrailTabValue;
    String createDeviceValue;
    String editDeviceValue;
    String devicesListValue;
    String deviceLogsValue;
    String changeDeviceFacilityValue;
    String amplifiersListValue;
    String createAmplifierValue;
    String editAmplifierValue;
    String facilitiesListValue;
    String createFacilityValue;
    String editFacilityValue;
    String studiesListValue;
    String openStudyValue;
    String auditLogsValue;
    String sharedUsersValue;
    String usersListValue;
    String createUserValue;
    String editUserValue;

    /***
     * Getting credentials for FacilityAdmin
     * @param userRole User Role
     * @param email User Email
     * @param password User Password
     */
    @Test(dataProvider = "getFacilityAdminCredentials", dataProviderClass = DataProviderClass.class, priority = 1)
    public void getCredentialsForFacilityAdmin(String userRole, String email, String password) {
        CommonUtility.logCreateTest("getCredentialsForFacilityAdmin");
        userRoleValue = userRole;
        emailValue = email;
        passwordValue = password;

    }

    /***
     * Getting data from csv file for FacilityAdmin
     * parameters : each page & action access value
     */
    @Test(dataProvider = "getFacilityAdminData", dataProviderClass = DataProviderClass.class, priority = 2)
    public void getDataForFacilityAdmin(String userRole, String dashboard, String infoBox, String latestStudies,
                                        String patientsList, String createPatient, String editPatient, String importStudy, String patientDocumentsTab, String patientAuditTrailTab,
                                        String createDevice, String editDevice, String devicesList, String deviceLogs, String changeDeviceFacility, String amplifiersList, String createAmplifier,
                                        String editAmplifier, String facilitiesList, String createFacility, String editFacility,
                                        String studiesList, String openStudy, String auditLogs, String sharedUsers, String usersList, String createUser, String editUser, String userAuditLogs) {
        CommonUtility.logCreateTest("getDataForFacilityAdmin");
        userRoleValue = userRole;
        dashboardValue = dashboard;
        infoBoxValue = infoBox;
        latestStudiesValue = latestStudies;
        patientsListValue = patientsList;
        createPatientValue = createPatient;
        editPatientValue = editPatient;
        importStudyValue = importStudy;
        patientDocumentsTabValue = patientDocumentsTab;
        patientAuditTrailTabValue = patientAuditTrailTab;
        createDeviceValue = createDevice;
        editDeviceValue = editDevice;
        devicesListValue = devicesList;
        deviceLogsValue = deviceLogs;
        changeDeviceFacilityValue = changeDeviceFacility;
        amplifiersListValue = amplifiersList;
        createAmplifierValue = createAmplifier;
        editAmplifierValue = editAmplifier;
        facilitiesListValue = facilitiesList;
        createFacilityValue = createFacility;
        editFacilityValue = editFacility;
        studiesListValue = studiesList;
        openStudyValue = openStudy;
        auditLogsValue = auditLogs;
        sharedUsersValue = sharedUsers;
        usersListValue = usersList;
        createUserValue = createUser;
        editUserValue = editUser;

    }

    /***
     * Implementing login and user role verification for FacilityAdmin
     * Passing email and password to BaseActionAccess method actionAccessScriptForLogin
     * Passing user role authorization value to BaseActionAccess method actionAccessScriptForUserRole
     */
    @Test(priority = 3)
    public void facilityAdminUser_LoginRoleVerification() {
        CommonUtility.logCreateTest("TC219_facilityAdminUser_LoginRoleVerification");
        actionAccessScriptForLogin(emailValue, passwordValue);
        actionAccessScriptForUserRole(userRoleValue);

    }

    /***
     * Implementing patient list authorization for FacilityAdmin
     * Passing patient list authorization value to BaseActionAccess method actionAccessScriptForPatientModule_List
     */
    @Test(priority = 4)
    public void TC220_facilityAdminUser_PatientListAction() {
        CommonUtility.logCreateTest("TC220_facilityAdminUser_PatientListAction");
        actionAccessScriptForPatientModule_List(patientsListValue);

    }

    /***
     * Implementing patient create action authorization for FacilityAdmin
     * Passing patient list value, create patient value to BaseActionAccess method actionAccessScriptForPatientModule_Create
     */
    @Test(priority = 5)
    public void TC221_facilityAdminUser_PatientCreateAction() {
        CommonUtility.logCreateTest("TC221_facilityAdminUser_PatientCreateAction");
        actionAccessScriptForPatientModule_Create(patientsListValue, createPatientValue);

    }

    /***
     * Implementing patient edit action authorization for FacilityAdmin
     * Passing patient list value, edit patient value to BaseActionAccess method actionAccessScriptForPatientModule_Edit
     */
    @Test(priority = 6)
    public void TC222_facilityAdminUser_PatientEditAction() {
        CommonUtility.logCreateTest("TC222_facilityAdminUser_PatientEditAction");
        actionAccessScriptForPatientModule_Edit(patientsListValue, editPatientValue);

    }

    /***
     * Implementing import study action authorization for FacilityAdmin
     */
    @Test(priority = 7, groups = "importStudy")
    public void TC223_facilityAdminUser_ImportStudyAction() {
        CommonUtility.logCreateTest("TC223_facilityAdminUser_ImportStudyAction");
        actionAccessScriptForPatientModule_ImportStudy(patientsListValue, importStudyValue);

    }

    /***
     * Implementing facility list authorization for FacilityAdmin
     * Passing facility list authorization value to BaseActionAccess method actionAccessScriptForFacilityModule_List
     */
    @Test(priority = 8)
    public void TC224_facilityAdminUser_FacilityListAction() {
        CommonUtility.logCreateTest("TC224_facilityAdminUser_FacilityListAction");
        actionAccessScriptForFacilityModule_List(facilitiesListValue);

    }

    /***
     * Implementing facility create action authorization for FacilityAdmin
     * Passing facility list value, create facility value to BaseActionAccess method actionAccessScriptForFacilityModule_Create
     */
    @Test(priority = 9)
    public void TC225_facilityAdminUser_FacilityCreateAction() {
        CommonUtility.logCreateTest("TC225_facilityAdminUser_FacilityCreateAction");
        actionAccessScriptForFacilityModule_Create(facilitiesListValue, createFacilityValue, "FA");

    }

    /***
     * Implementing facility edit action authorization for FacilityAdmin
     * Passing facility list value, edit facility value to BaseActionAccess method actionAccessScriptForFacilityModule_Edit
     */
    @Test(priority = 10)
    public void TC226_facilityAdminUser_FacilityEditAction() {
        CommonUtility.logCreateTest("TC226_facilityAdminUser_FacilityEditAction");
        actionAccessScriptForFacilityModule_Edit_FA(facilitiesListValue, editFacilityValue, "FacilityAdmin");

    }

    /***
     * Implementing amplifier list authorization for FacilityAdmin
     * Passing amplifier list authorization value to BaseActionAccess method actionAccessScriptForAmplifierModule_List
     */
    @Test(priority = 11)
    public void TC227_facilityAdminUser_AmplifierListAction() {
        CommonUtility.logCreateTest("TC227_facilityAdminUser_AmplifierListAction");
        actionAccessScriptForAmplifierModule_List(amplifiersListValue);

    }

    /***
     * Implementing amplifier create action authorization for FacilityAdmin
     * Passing amplifer list value, create amplifier value to BaseActionAccess method actionAccessScriptForAmplifierModule_Create
     */
    @Test(priority = 12)
    public void TC228_facilityAdminUser_AmplifierCreateAction() {
        CommonUtility.logCreateTest("TC228_facilityAdminUser_AmplifierCreateAction");
        actionAccessScriptForAmplifierModule_Create(amplifiersListValue, createAmplifierValue, "FA");

    }

    /***
     * Implementing amplifier edit action authorization for FacilityAdmin
     * Passing amplifier list value, edit amplifier value to BaseActionAccess method actionAccessScriptForAmplifierModule_Edit
     */
    @Test(priority = 13)
    public void TC229_facilityAdminUser_AmplifierEditAction() {
        CommonUtility.logCreateTest("TC229_facilityAdminUser_AmplifierEditAction");
        actionAccessScriptForAmplifierModule_Edit(amplifiersListValue, editAmplifierValue, "FA");

    }

    /***
     * Implementing device list authorization for FacilityAdmin
     * Passing device list authorization value to BaseActionAccess method actionAccessScriptForDeviceModule_List
     */
    @Test(priority = 14)
    public void TC230_facilityAdminUser_DeviceListAction() {
        CommonUtility.logCreateTest("TC230_facilityAdminUser_DeviceListAction");
        actionAccessScriptForDeviceModule_List(devicesListValue);

    }

    /***
     * Implementing device create action authorization for FacilityAdmin
     * Passing device list value, create device value to BaseActionAccess method actionAccessScriptForDeviceModule_Create
     */
    @Test(priority = 15)
    public void TC231_facilityAdminUser_DeviceCreateAction() {
        CommonUtility.logCreateTest("TC231_facilityAdminUser_DeviceCreateAction");
        actionAccessScriptForDeviceModule_Create(devicesListValue, createDeviceValue);

    }

    /***
     * Implementing device edit action authorization for FacilityAdmin
     * Passing device list value, edit device value to BaseActionAccess method actionAccessScriptForDeviceModule_Edit
     */
    @Test(priority = 16)
    public void TC232_facilityAdminUser_DeviceEditAction() {
        CommonUtility.logCreateTest("TC232_facilityAdminUser_DeviceEditAction");
        actionAccessScriptForDeviceModule_Edit(devicesListValue, editDeviceValue);

    }

    /***
     * Implementing user list authorization for FacilityAdmin
     * Passing user list authorization value to BaseActionAccess method actionAccessScriptForUserModule_List
     */
    @Test(priority = 18)
    public void TC234_facilityAdminUser_UserListAction() {
        CommonUtility.logCreateTest("TC234_facilityAdminUser_UserListAction");
        actionAccessScriptForUserModule_List(usersListValue);

    }

    /***
     * Implementing user create action authorization for FacilityAdmin
     * Passing user list value, create user value to BaseActionAccess method actionAccessScriptForUserModule_Create
     */
    @Test(priority = 19)
    public void TC235_facilityAdminUser_UserCreateAction() {
        CommonUtility.logCreateTest("TC235_facilityAdminUser_UserCreateAction");
        actionAccessScriptForUserModule_Create(usersListValue, createUserValue, "FA");

    }

    /***
     * Implementing user edit action authorization for FacilityAdmin
     * Passing user list value, edit user value to BaseActionAccess method actionAccessScriptForUserModule_Edit
     */
    @Test(priority = 20)
    public void TC236_facilityAdminUser_UserEditAction() {
        CommonUtility.logCreateTest("TC236_facilityAdminUser_UserEditAction");
        actionAccessScriptForUserModule_Edit(usersListValue, editUserValue, "FA");

    }

    /***
     * For deleting facilities that were created during test execution
     */
    @Test(groups = "ExcludedTemporarily", priority = 23)
    public void afterTest_facilityAdminUser_FacilityDeletion() {
        CommonUtility.logCreateTest("t03_facilityAdminUser_FacilityDeletion");
        facilityDeletion(editFacilityValue);

    }

    /***
     * For deleting users that were created during test execution
     */
    @Test(priority = 22)
    public void afterTest_facilityAdminUser_UserDeletion() {
        CommonUtility.logCreateTest("t02_facilityAdminUser_UserDeletion");
        userDeletion(editUserValue);

    }

    /***
     * For deleting patients that were created during test execution
     */
    @Test(priority = 21)
    public void afterTest_facilityAdminUser_PatientDeletion() {
        CommonUtility.logCreateTest("t01_facilityAdminUser_PatientDeletion");
        patientDeletion(editPatientValue);

    }

}
