package testcases.ActionAccess;

import dataprovider.DataProviderClass;
import helper.CommonUtility;
import helper.Logger;
import org.testng.Reporter;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

public class FieldTechActionAccess extends BaseActionAccess {

    String userRoleValue;
    String emailValue;
    String passwordValue;
    String dashboardValue;
    String infoBoxValue;
    String latestStudiesValue;
    String patientsListValue;
    String createPatientValue;
    String editPatientValue;
    String importStudyValue;
    String patientDocumentsTabValue;
    String patientAuditTrailTabValue;
    String createDeviceValue;
    String editDeviceValue;
    String devicesListValue;
    String deviceLogsValue;
    String changeDeviceFacilityValue;
    String amplifiersListValue;
    String createAmplifierValue;
    String editAmplifierValue;
    String facilitiesListValue;
    String createFacilityValue;
    String editFacilityValue;
    String studiesListValue;
    String openStudyValue;
    String auditLogsValue;
    String sharedUsersValue;
    String usersListValue;
    String createUserValue;
    String editUserValue;

    /***
     * Getting credentials for FieldTech
     * @param userRole User Role
     * @param email User Email
     * @param password User Password
     */
    @Test(dataProvider = "getFieldTechCredentials", dataProviderClass = DataProviderClass.class, priority = 1)
    public void getCredentialsForFieldTech(String userRole, String email, String password) {
        CommonUtility.logCreateTest("getCredentialsForFieldTech");
        userRoleValue = userRole;
        emailValue = email;
        passwordValue = password;

    }

    /***
     * Getting data from csv file for FieldTech
     * parameters : each page & action access value
     */
    @Test(dataProvider = "getFieldTechData", dataProviderClass = DataProviderClass.class, priority = 2)
    public void getDataForFieldTech(String userRole, String dashboard, String infoBox, String latestStudies,
                                    String patientsList, String createPatient, String editPatient, String importStudy, String patientDocumentsTab, String patientAuditTrailTab,
                                    String createDevice, String editDevice, String devicesList, String deviceLogs, String changeDeviceFacility, String amplifiersList, String createAmplifier,
                                    String editAmplifier, String facilitiesList, String createFacility, String editFacility,
                                    String studiesList, String openStudy, String auditLogs, String sharedUsers, String usersList, String createUser, String editUser, String userAuditLogs) {
        CommonUtility.logCreateTest("getDataForFieldTech");
        userRoleValue = userRole;
        dashboardValue = dashboard;
        infoBoxValue = infoBox;
        latestStudiesValue = latestStudies;
        patientsListValue = patientsList;
        createPatientValue = createPatient;
        editPatientValue = editPatient;
        importStudyValue = importStudy;
        patientDocumentsTabValue = patientDocumentsTab;
        patientAuditTrailTabValue = patientAuditTrailTab;
        createDeviceValue = createDevice;
        editDeviceValue = editDevice;
        devicesListValue = devicesList;
        deviceLogsValue = deviceLogs;
        changeDeviceFacilityValue = changeDeviceFacility;
        amplifiersListValue = amplifiersList;
        createAmplifierValue = createAmplifier;
        editAmplifierValue = editAmplifier;
        facilitiesListValue = facilitiesList;
        createFacilityValue = createFacility;
        editFacilityValue = editFacility;
        studiesListValue = studiesList;
        openStudyValue = openStudy;
        auditLogsValue = auditLogs;
        sharedUsersValue = sharedUsers;
        usersListValue = usersList;
        createUserValue = createUser;
        editUserValue = editUser;

    }

    /***
     * Implementing login and user role verification for FieldTech
     * Passing email and password to BaseActionAccess method actionAccessScriptForLogin
     * Passing user role authorization value to BaseActionAccess method actionAccessScriptForUserRole
     */
    @Test(priority = 3)
    public void fieldTechUser_LoginRoleVerification() {
        CommonUtility.logCreateTest("TC273_fieldTechUser_LoginRoleVerification");
        actionAccessScriptForLogin(emailValue, passwordValue);
        actionAccessScriptForUserRole(userRoleValue);

    }

    /***
     * Implementing patient list authorization for FieldTech
     * Passing patient list authorization value to BaseActionAccess method actionAccessScriptForPatientModule_List
     */
    @Test(priority = 4)
    public void TC274_fieldTechUser_PatientListAction() {
        CommonUtility.logCreateTest("TC274_fieldTechUser_PatientListAction");
        actionAccessScriptForPatientModule_List(patientsListValue);

    }

    /***
     * Implementing patient create action authorization for FieldTech
     * Passing patient list value, create patient value to BaseActionAccess method actionAccessScriptForPatientModule_Create
     */
    @Test(priority = 5)
    public void TC275_fieldTechUser_PatientCreateAction() {
        CommonUtility.logCreateTest("TC275_fieldTechUser_PatientCreateAction");
        actionAccessScriptForPatientModule_Create(patientsListValue, createPatientValue);

    }

    /***
     * Implementing patient edit action authorization for FieldTech
     * Passing patient list value, edit patient value to BaseActionAccess method actionAccessScriptForPatientModule_Edit
     */
    @Test(priority = 6)
    public void TC276_fieldTechUser_PatientEditAction() {
        CommonUtility.logCreateTest("TC276_fieldTechUser_PatientEditAction");
        actionAccessScriptForPatientModule_Edit(patientsListValue, editPatientValue);

    }

    /***
     * Implementing import study action authorization for FieldTech
     */
    @Test(priority = 7, groups = "importStudy")
    public void TC277_fieldTechUser_ImportStudyAction() {
        CommonUtility.logCreateTest("TC277_fieldTechUser_ImportStudyAction");
        actionAccessScriptForPatientModule_ImportStudy(patientsListValue, importStudyValue);

    }

    /***
     * Implementing facility list authorization for FieldTech
     * Passing facility list authorization value to BaseActionAccess method actionAccessScriptForFacilityModule_List
     */
    @Test(priority = 8)
    public void TC278_fieldTechUser_FacilityListAction() {
        CommonUtility.logCreateTest("TC278_fieldTechUser_FacilityListAction");
        actionAccessScriptForFacilityModule_List(facilitiesListValue);

    }

    /***
     * Implementing facility create action authorization for FieldTech
     * Passing facility list value, create facility value to BaseActionAccess method actionAccessScriptForFacilityModule_Create
     */
    @Test(priority = 9)
    public void TC279_fieldTechUser_FacilityCreateAction() {
        CommonUtility.logCreateTest("TC279_fieldTechUser_FacilityCreateAction");
        actionAccessScriptForFacilityModule_Create(facilitiesListValue, createFacilityValue, "FT");

    }

    /***
     * Implementing facility edit action authorization for FieldTech
     * Passing facility list value, edit facility value to BaseActionAccess method actionAccessScriptForFacilityModule_Edit
     */
    @Test(priority = 10)
    public void TC280_fieldTechUser_FacilityEditAction() {
        CommonUtility.logCreateTest("TC280_fieldTechUser_FacilityEditAction");
        actionAccessScriptForFacilityModule_Edit(facilitiesListValue, editFacilityValue, "FT");

    }

    /***
     * Implementing amplifier list authorization for FieldTech
     * Passing amplifier list authorization value to BaseActionAccess method actionAccessScriptForAmplifierModule_List
     */
    @Test(priority = 11)
    public void TC281_fieldTechUser_AmplifierListAction() {
        CommonUtility.logCreateTest("TC281_fieldTechUser_AmplifierListAction");
        actionAccessScriptForAmplifierModule_List(amplifiersListValue);

    }

    /***
     * Implementing amplifier create action authorization for FieldTech
     * Passing amplifer list value, create amplifier value to BaseActionAccess method actionAccessScriptForAmplifierModule_Create
     */
    @Test(priority = 12)
    public void TC282_fieldTechUser_AmplifierCreateAction() {
        CommonUtility.logCreateTest("TC282_fieldTechUser_AmplifierCreateAction");
        actionAccessScriptForAmplifierModule_Create(amplifiersListValue, createAmplifierValue, "FT");

    }

    /***
     * Implementing amplifier edit action authorization for FieldTech
     * Passing amplifier list value, edit amplifier value to BaseActionAccess method actionAccessScriptForAmplifierModule_Edit
     */
    @Test(priority = 13)
    public void TC283_fieldTechUser_AmplifierEditAction() {
        CommonUtility.logCreateTest("TC283_fieldTechUser_AmplifierEditAction");
        actionAccessScriptForAmplifierModule_Edit(amplifiersListValue, editAmplifierValue, "FT");

    }

    /***
     * Implementing device list authorization for FieldTech
     * Passing device list authorization value to BaseActionAccess method actionAccessScriptForDeviceModule_List
     */
    @Test(priority = 14)
    public void TC284_fieldTechUser_DeviceListAction() {
        CommonUtility.logCreateTest("TC284_fieldTechUser_DeviceListAction");
        actionAccessScriptForDeviceModule_List(devicesListValue);

    }

    /***
     * Implementing device create action authorization for FieldTech
     * Passing device list value, create device value to BaseActionAccess method actionAccessScriptForDeviceModule_Create
     */
    @Test(priority = 15)
    public void TC285_fieldTechUser_DeviceCreateAction() {
        CommonUtility.logCreateTest("TC285_fieldTechUser_DeviceCreateAction");
        actionAccessScriptForDeviceModule_Create(devicesListValue, createDeviceValue);

    }

    /***
     * Implementing device edit action authorization for FieldTech
     * Passing device list value, edit device value to BaseActionAccess method actionAccessScriptForDeviceModule_Edit
     */
    @Test(priority = 16)
    public void TC286_fieldTechUser_DeviceEditAction() {
        CommonUtility.logCreateTest("TC286_fieldTechUser_DeviceEditAction");
        actionAccessScriptForDeviceModule_Edit(devicesListValue, editDeviceValue);

    }

    /***
     * Implementing user list authorization for FieldTech
     * Passing user list authorization value to BaseActionAccess method actionAccessScriptForUserModule_List
     */
    @Test(priority = 18)
    public void TC288_fieldTechUser_UserListAction() {
        CommonUtility.logCreateTest("TC288_fieldTechUser_UserListAction");
        actionAccessScriptForUserModule_List(usersListValue);

    }

    /***
     * Implementing user create action authorization for FieldTech
     * Passing user list value, create user value to BaseActionAccess method actionAccessScriptForUserModule_Create
     */
    @Test(priority = 19)
    public void TC289_fieldTechUser_UserCreateAction() {
        CommonUtility.logCreateTest("TC289_fieldTechUser_UserCreateAction");
        actionAccessScriptForUserModule_Create(usersListValue, createUserValue, "FT");

    }

    /***
     * Implementing user edit action authorization for FieldTech
     * Passing user list value, edit user value to BaseActionAccess method actionAccessScriptForUserModule_Edit
     */
    @Test(priority = 20)
    public void TC290_fieldTechUser_UserEditAction() {
        CommonUtility.logCreateTest("TC290_fieldTechUser_UserEditAction");
        actionAccessScriptForUserModule_Edit(usersListValue, editUserValue, "FT");

    }

    /***
     * For deleting Patients that have been created during test execution
     */
    @Test(priority = 21)
    public void afterTest_fieldTechUser_PatientDeletion() {
        CommonUtility.logCreateTest("t01_fieldTechUser_PatientDeletion");
        patientDeletion(editPatientValue);

    }

}
