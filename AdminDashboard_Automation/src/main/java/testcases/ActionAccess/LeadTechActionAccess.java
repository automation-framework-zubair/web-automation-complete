package testcases.ActionAccess;

import dataprovider.DataProviderClass;
import helper.CommonUtility;
import helper.Logger;
import org.testng.Reporter;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

public class LeadTechActionAccess extends BaseActionAccess {

    String userRoleValue;
    String emailValue;
    String passwordValue;
    String dashboardValue;
    String infoBoxValue;
    String latestStudiesValue;
    String patientsListValue;
    String createPatientValue;
    String editPatientValue;
    String importStudyValue;
    String patientDocumentsTabValue;
    String patientAuditTrailTabValue;
    String createDeviceValue;
    String editDeviceValue;
    String devicesListValue;
    String deviceLogsValue;
    String changeDeviceFacilityValue;
    String amplifiersListValue;
    String createAmplifierValue;
    String editAmplifierValue;
    String facilitiesListValue;
    String createFacilityValue;
    String editFacilityValue;
    String studiesListValue;
    String openStudyValue;
    String auditLogsValue;
    String sharedUsersValue;
    String usersListValue;
    String createUserValue;
    String editUserValue;

    /***
     * Getting credentials for LeadTech
     * @param userRole User Role
     * @param email User Email
     * @param password User Password
     */
    @Test(dataProvider = "getLeadTechCredentials", dataProviderClass = DataProviderClass.class, priority = 1)
    public void getCredentialsForLeadTech(String userRole, String email, String password) {
        CommonUtility.logCreateTest("getCredentialsForLeadTech");
        userRoleValue = userRole;
        emailValue = email;
        passwordValue = password;

    }

    /***
     * Getting data from csv file for LeadTech
     * parameters : each page & action access value
     */
    @Test(dataProvider = "getLeadTechData", dataProviderClass = DataProviderClass.class, priority = 2)
    public void getDataForLeadTech(String userRole, String dashboard, String infoBox, String latestStudies,
                                   String patientsList, String createPatient, String editPatient, String importStudy, String patientDocumentsTab, String patientAuditTrailTab,
                                   String createDevice, String editDevice, String devicesList, String deviceLogs, String changeDeviceFacility, String amplifiersList, String createAmplifier,
                                   String editAmplifier, String facilitiesList, String createFacility, String editFacility,
                                   String studiesList, String openStudy, String auditLogs, String sharedUsers, String usersList, String createUser, String editUser, String userAuditLogs) {
        CommonUtility.logCreateTest("getDataForLeadTech");
        userRoleValue = userRole;
        dashboardValue = dashboard;
        infoBoxValue = infoBox;
        latestStudiesValue = latestStudies;
        patientsListValue = patientsList;
        createPatientValue = createPatient;
        editPatientValue = editPatient;
        importStudyValue = importStudy;
        patientDocumentsTabValue = patientDocumentsTab;
        patientAuditTrailTabValue = patientAuditTrailTab;
        createDeviceValue = createDevice;
        editDeviceValue = editDevice;
        devicesListValue = devicesList;
        deviceLogsValue = deviceLogs;
        changeDeviceFacilityValue = changeDeviceFacility;
        amplifiersListValue = amplifiersList;
        createAmplifierValue = createAmplifier;
        editAmplifierValue = editAmplifier;
        facilitiesListValue = facilitiesList;
        createFacilityValue = createFacility;
        editFacilityValue = editFacility;
        studiesListValue = studiesList;
        openStudyValue = openStudy;
        auditLogsValue = auditLogs;
        sharedUsersValue = sharedUsers;
        usersListValue = usersList;
        createUserValue = createUser;
        editUserValue = editUser;

    }

    /***
     * Implementing login and user role verification for LeadTech
     * Passing email and password to BaseActionAccess method actionAccessScriptForLogin
     * Passing user role authorization value to BaseActionAccess method actionAccessScriptForUserRole
     */
    @Test(priority = 3)
    public void leadTechUser_LoginRoleVerification() {
        CommonUtility.logCreateTest("TC255_leadTechUser_LoginRoleVerification");
        actionAccessScriptForLogin(emailValue, passwordValue);
        actionAccessScriptForUserRole(userRoleValue);

    }

    /***
     * Implementing patient list authorization for LeadTech
     * Passing patient list authorization value to BaseActionAccess method actionAccessScriptForPatientModule_List
     */
    @Test(priority = 4)
    public void TC256_leadTechUser_PatientListAction() {
        CommonUtility.logCreateTest("TC256_leadTechUser_PatientListAction");
        actionAccessScriptForPatientModule_List(patientsListValue);

    }

    /***
     * Implementing patient create action authorization for LeadTech
     * Passing patient list value, create patient value to BaseActionAccess method actionAccessScriptForPatientModule_Create
     */
    @Test(priority = 5)
    public void TC257_leadTechUser_PatientCreateAction() {
        CommonUtility.logCreateTest("TC257_leadTechUser_PatientCreateAction");
        actionAccessScriptForPatientModule_Create(patientsListValue, createPatientValue);

    }

    /***
     * Implementing patient edit action authorization for LeadTech
     * Passing patient list value, edit patient value to BaseActionAccess method actionAccessScriptForPatientModule_Edit
     */
    @Test(priority = 6)
    public void TC258_leadTechUser_PatientEditAction() {
        CommonUtility.logCreateTest("TC258_leadTechUser_PatientEditAction");
        actionAccessScriptForPatientModule_Edit(patientsListValue, editPatientValue);

    }

    /***
     * Implementing import study action authorization for LeadTech
     */
    @Test(priority = 7, groups = "importStudy")
    public void TC259_leadTechUser_ImportStudyAction() {
        CommonUtility.logCreateTest("TC259_leadTechUser_ImportStudyAction");
        actionAccessScriptForPatientModule_ImportStudy(patientsListValue, importStudyValue);

    }

    /***
     * Implementing facility list authorization for LeadTech
     * Passing facility list authorization value to BaseActionAccess method actionAccessScriptForFacilityModule_List
     */
    @Test(priority = 8)
    public void TC260_leadTechUser_FacilityListAction() {
        CommonUtility.logCreateTest("TC260_leadTechUser_FacilityListAction");
        actionAccessScriptForFacilityModule_List(facilitiesListValue);

    }

    /***
     * Implementing facility create action authorization for LeadTech
     * Passing facility list value, create facility value to BaseActionAccess method actionAccessScriptForFacilityModule_Create
     */
    @Test(priority = 9)
    public void TC261_leadTechUser_FacilityCreateAction() {
        CommonUtility.logCreateTest("TC261_leadTechUser_FacilityCreateAction");
        actionAccessScriptForFacilityModule_Create(facilitiesListValue, createFacilityValue, "LT");

    }

    /***
     * Implementing facility edit action authorization for LeadTech
     * Passing facility list value, edit facility value to BaseActionAccess method actionAccessScriptForFacilityModule_Edit
     */
    @Test(priority = 10)
    public void TC262_leadTechUser_FacilityEditAction() {
        CommonUtility.logCreateTest("TC262_leadTechUser_FacilityEditAction");
        actionAccessScriptForFacilityModule_Edit(facilitiesListValue, editFacilityValue, "LT");

    }

    /***
     * Implementing amplifier list authorization for LeadTech
     * Passing amplifier list authorization value to BaseActionAccess method actionAccessScriptForAmplifierModule_List
     */
    @Test(priority = 11)
    public void TC263_leadTechUser_AmplifierListAction() {
        CommonUtility.logCreateTest("TC263_leadTechUser_AmplifierListAction");
        actionAccessScriptForAmplifierModule_List(amplifiersListValue);

    }

    /***
     * Implementing amplifier create action authorization for LeadTech
     * Passing amplifer list value, create amplifier value to BaseActionAccess method actionAccessScriptForAmplifierModule_Create
     */
    @Test(priority = 12)
    public void TC264_leadTechUser_AmplifierCreateAction() {
        CommonUtility.logCreateTest("TC264_leadTechUser_AmplifierCreateAction");
        actionAccessScriptForAmplifierModule_Create(amplifiersListValue, createAmplifierValue, "LT");

    }

    /***
     * Implementing amplifier edit action authorization for LeadTech
     * Passing amplifier list value, edit amplifier value to BaseActionAccess method actionAccessScriptForAmplifierModule_Edit
     */
    @Test(priority = 13)
    public void TC265_leadTechUser_AmplifierEditAction() {
        CommonUtility.logCreateTest("TC265_leadTechUser_AmplifierEditAction");
        actionAccessScriptForAmplifierModule_Edit(amplifiersListValue, editAmplifierValue, "LT");

    }

    /***
     * Implementing device list authorization for LeadTech
     * Passing device list authorization value to BaseActionAccess method actionAccessScriptForDeviceModule_List
     */
    @Test(priority = 14)
    public void TC266_leadTechUser_DeviceListAction() {
        CommonUtility.logCreateTest("TC266_leadTechUser_DeviceListAction");
        actionAccessScriptForDeviceModule_List(devicesListValue);

    }

    /***
     * Implementing device create action authorization for LeadTech
     * Passing device list value, create device value to BaseActionAccess method actionAccessScriptForDeviceModule_Create
     */
    @Test(priority = 15)
    public void TC267_leadTechUser_DeviceCreateAction() {
        CommonUtility.logCreateTest("TC267_leadTechUser_DeviceCreateAction");
        actionAccessScriptForDeviceModule_Create(devicesListValue, createDeviceValue);

    }

    /***
     * Implementing device edit action authorization for LeadTech
     * Passing device list value, edit device value to BaseActionAccess method actionAccessScriptForDeviceModule_Edit
     */
    @Test(priority = 16)
    public void TC268_leadTechUser_DeviceEditAction() {
        CommonUtility.logCreateTest("TC268_leadTechUser_DeviceEditAction");
        actionAccessScriptForDeviceModule_Edit(devicesListValue, editDeviceValue);

    }

    /***
     * Implementing user list authorization for LeadTech
     * Passing user list authorization value to BaseActionAccess method actionAccessScriptForUserModule_List
     */
    @Test(priority = 18)
    public void TC270_leadTechUser_UserListAction() {
        CommonUtility.logCreateTest("TC270_leadTechUser_UserListAction");
        actionAccessScriptForUserModule_List(usersListValue);

    }

    /***
     * Implementing user create action authorization for LeadTech
     * Passing user list value, create user value to BaseActionAccess method actionAccessScriptForUserModule_Create
     */
    @Test(priority = 19)
    public void TC271_leadTechUser_UserCreateAction() {
        CommonUtility.logCreateTest("TC271_leadTechUser_UserCreateAction");
        actionAccessScriptForUserModule_Create(usersListValue, createUserValue, "LT");

    }

    /***
     * Implementing user edit action authorization for LeadTech
     * Passing user list value, edit user value to BaseActionAccess method actionAccessScriptForUserModule_Edit
     */
    @Test(priority = 20)
    public void TC272_leadTechUser_UserEditAction() {
        CommonUtility.logCreateTest("TC272_leadTechUser_UserEditAction");
        actionAccessScriptForUserModule_Edit(usersListValue, editUserValue, "LT");

    }

    /***
     * For deleting Patients that have been created during test execution
     */
    @Test(priority = 21)
    public void afterTest_leadTechUser_PatientDeletion() {
        CommonUtility.logCreateTest("t01_leadTechUser_PatientDeletion");
        patientDeletion(editPatientValue);

    }

}
