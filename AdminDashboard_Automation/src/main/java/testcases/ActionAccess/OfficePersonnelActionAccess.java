package testcases.ActionAccess;

import dataprovider.DataProviderClass;
import helper.CommonUtility;
import org.testng.annotations.Test;

public class OfficePersonnelActionAccess extends BaseActionAccess {

    String userRoleValue;
    String emailValue;
    String passwordValue;
    String dashboardValue;
    String infoBoxValue;
    String latestStudiesValue;
    String patientsListValue;
    String createPatientValue;
    String editPatientValue;
    String importStudyValue;
    String patientDocumentsTabValue;
    String patientAuditTrailTabValue;
    String createDeviceValue;
    String editDeviceValue;
    String devicesListValue;
    String deviceLogsValue;
    String changeDeviceFacilityValue;
    String amplifiersListValue;
    String createAmplifierValue;
    String editAmplifierValue;
    String facilitiesListValue;
    String createFacilityValue;
    String editFacilityValue;
    String studiesListValue;
    String openStudyValue;
    String auditLogsValue;
    String sharedUsersValue;
    String usersListValue;
    String createUserValue;
    String editUserValue;

    /***
     * Getting credentials for OfficePersonnel
     * @param userRole User Role
     * @param email User Email
     * @param password User Password
     */
    @Test(dataProvider = "getOfficePersonnelCredentials", dataProviderClass = DataProviderClass.class, priority = 1)
    public void getCredentialsForOfficePersonnel(String userRole, String email, String password) {
        CommonUtility.logCreateTest("getCredentialsForOfficePersonnel");
        userRoleValue = userRole;
        emailValue = email;
        passwordValue = password;

    }

    /***
     * Getting data from csv file for OfficePersonnel
     * parameters : each page & action access value
     */
    @Test(dataProvider = "getOfficePersonnelData", dataProviderClass = DataProviderClass.class, priority = 2)
    public void getDataForOfficePersonnel(String userRole, String dashboard, String infoBox, String latestStudies,
                                          String patientsList, String createPatient, String editPatient, String importStudy, String patientDocumentsTab, String patientAuditTrailTab,
                                          String createDevice, String editDevice, String devicesList, String deviceLogs, String changeDeviceFacility, String amplifiersList, String createAmplifier,
                                          String editAmplifier, String facilitiesList, String createFacility, String editFacility,
                                          String studiesList, String openStudy, String auditLogs, String sharedUsers, String usersList, String createUser, String editUser, String userAuditLogs) {
        CommonUtility.logCreateTest("getDataForOfficePersonnel");
        userRoleValue = userRole;
        dashboardValue = dashboard;
        infoBoxValue = infoBox;
        latestStudiesValue = latestStudies;
        patientsListValue = patientsList;
        createPatientValue = createPatient;
        editPatientValue = editPatient;
        importStudyValue = importStudy;
        patientDocumentsTabValue = patientDocumentsTab;
        patientAuditTrailTabValue = patientAuditTrailTab;
        createDeviceValue = createDevice;
        editDeviceValue = editDevice;
        devicesListValue = devicesList;
        deviceLogsValue = deviceLogs;
        changeDeviceFacilityValue = changeDeviceFacility;
        amplifiersListValue = amplifiersList;
        createAmplifierValue = createAmplifier;
        editAmplifierValue = editAmplifier;
        facilitiesListValue = facilitiesList;
        createFacilityValue = createFacility;
        editFacilityValue = editFacility;
        studiesListValue = studiesList;
        openStudyValue = openStudy;
        auditLogsValue = auditLogs;
        sharedUsersValue = sharedUsers;
        usersListValue = usersList;
        createUserValue = createUser;
        editUserValue = editUser;

    }

    /***
     * Implementing login and user role verification for OfficePersonnel
     * Passing email and password to BaseActionAccess method actionAccessScriptForLogin
     * Passing user role authorization value to BaseActionAccess method actionAccessScriptForUserRole
     */
    @Test(priority = 3)
    public void officePersonnelUser_LoginRoleVerification() {
        CommonUtility.logCreateTest("TC291_officePersonnelUser_LoginRoleVerification");
        actionAccessScriptForLogin(emailValue, passwordValue);
        actionAccessScriptForUserRole(userRoleValue);

    }

    /***
     * Implementing patient list authorization for OfficePersonnel
     * Passing patient list authorization value to BaseActionAccess method actionAccessScriptForPatientModule_List
     */
    @Test(priority = 4)
    public void TC292_officePersonnelUser_PatientListAction() {
        CommonUtility.logCreateTest("TC292_officePersonnelUser_PatientListAction");
        actionAccessScriptForPatientModule_List(patientsListValue);

    }

    /***
     * Implementing patient create action authorization for OfficePersonnel
     * Passing patient list value, create patient value to BaseActionAccess method actionAccessScriptForPatientModule_Create
     */
    @Test(priority = 5)
    public void TC293_officePersonnelUser_PatientCreateAction() {
        CommonUtility.logCreateTest("TC293_officePersonnelUser_PatientCreateAction");
        actionAccessScriptForPatientModule_Create(patientsListValue, createPatientValue);

    }

    /***
     * Implementing patient edit action authorization for OfficePersonnel
     * Passing patient list value, edit patient value to BaseActionAccess method actionAccessScriptForPatientModule_Edit
     */
    @Test(priority = 6)
    public void TC294_officePersonnelUser_PatientEditAction() {
        CommonUtility.logCreateTest("TC294_officePersonnelUser_PatientEditAction");
        actionAccessScriptForPatientModule_Edit(patientsListValue, editPatientValue);

    }

    /***
     * Implementing import study action authorization for OfficePersonnel
     */
    @Test(priority = 7, groups = "importStudy")
    public void TC295_officePersonnelUser_ImportStudyAction() {
        CommonUtility.logCreateTest("TC295_officePersonnelUser_ImportStudyAction");
        actionAccessScriptForPatientModule_ImportStudy(patientsListValue, importStudyValue);

    }

    /***
     * Implementing facility list authorization for OfficePersonnel
     * Passing facility list authorization value to BaseActionAccess method actionAccessScriptForFacilityModule_List
     */
    @Test(priority = 8)
    public void TC296_officePersonnelUser_FacilityListAction() {
        CommonUtility.logCreateTest("TC296_officePersonnelUser_FacilityListAction");
        actionAccessScriptForFacilityModule_List(facilitiesListValue);

    }

    /***
     * Implementing facility create action authorization for OfficePersonnel
     * Passing facility list value, create facility value to BaseActionAccess method actionAccessScriptForFacilityModule_Create
     */
    @Test(priority = 9)
    public void TC297_officePersonnelUser_FacilityCreateAction() {
        CommonUtility.logCreateTest("TC297_officePersonnelUser_FacilityCreateAction");
        actionAccessScriptForFacilityModule_Create(facilitiesListValue, createFacilityValue, "OP");

    }

    /***
     * Implementing facility edit action authorization for OfficePersonnel
     * Passing facility list value, edit facility value to BaseActionAccess method actionAccessScriptForFacilityModule_Edit
     */
    @Test(priority = 10)
    public void TC298_officePersonnelUser_FacilityEditAction() {
        CommonUtility.logCreateTest("TC298_officePersonnelUser_FacilityEditAction");
        actionAccessScriptForFacilityModule_Edit(facilitiesListValue, editFacilityValue, "OP");

    }

    /***
     * Implementing amplifier list authorization for OfficePersonnel
     * Passing amplifier list authorization value to BaseActionAccess method actionAccessScriptForAmplifierModule_List
     */
    @Test(priority = 11)
    public void TC299_officePersonnelUser_AmplifierListAction() {
        CommonUtility.logCreateTest("TC299_officePersonnelUser_AmplifierListAction");
        actionAccessScriptForAmplifierModule_List(amplifiersListValue);

    }

    /***
     * Implementing amplifier create action authorization for OfficePersonnel
     * Passing amplifer list value, create amplifier value to BaseActionAccess method actionAccessScriptForAmplifierModule_Create
     */
    @Test(priority = 12)
    public void TC300_officePersonnelUser_AmplifierCreateAction() {
        CommonUtility.logCreateTest("TC300_officePersonnelUser_AmplifierCreateAction");
        actionAccessScriptForAmplifierModule_Create(amplifiersListValue, createAmplifierValue, "OP");

    }

    /***
     * Implementing amplifier edit action authorization for OfficePersonnel
     * Passing amplifier list value, edit amplifier value to BaseActionAccess method actionAccessScriptForAmplifierModule_Edit
     */
    @Test(priority = 13)
    public void TC301_officePersonnelUser_AmplifierEditAction() {
        CommonUtility.logCreateTest("TC301_officePersonnelUser_AmplifierEditAction");
        actionAccessScriptForAmplifierModule_Edit(amplifiersListValue, editAmplifierValue, "OP");

    }

    /***
     * Implementing device list authorization for OfficePersonnel
     * Passing device list authorization value to BaseActionAccess method actionAccessScriptForDeviceModule_List
     */
    @Test(priority = 14)
    public void TC302_officePersonnelUser_DeviceListAction() {
        CommonUtility.logCreateTest("TC302_officePersonnelUser_DeviceListAction");
        actionAccessScriptForDeviceModule_List(devicesListValue);

    }

    /***
     * Implementing device create action authorization for OfficePersonnel
     * Passing device list value, create device value to BaseActionAccess method actionAccessScriptForDeviceModule_Create
     */
    @Test(priority = 15)
    public void TC303_officePersonnelUser_DeviceCreateAction() {
        CommonUtility.logCreateTest("TC303_officePersonnelUser_DeviceCreateAction");
        actionAccessScriptForDeviceModule_Create(devicesListValue, createDeviceValue);

    }

    /***
     * Implementing device edit action authorization for OfficePersonnel
     * Passing device list value, edit device value to BaseActionAccess method actionAccessScriptForDeviceModule_Edit
     */
    @Test(priority = 16)
    public void TC304_officePersonnelUser_DeviceEditAction() {
        CommonUtility.logCreateTest("TC304_officePersonnelUser_DeviceEditAction");
        actionAccessScriptForDeviceModule_Edit(devicesListValue, editDeviceValue);

    }

    /***
     * Implementing user list authorization for OfficePersonnel
     * Passing user list authorization value to BaseActionAccess method actionAccessScriptForUserModule_List
     */
    @Test(priority = 18)
    public void TC306_officePersonnelUser_UserListAction() {
        CommonUtility.logCreateTest("TC306_officePersonnelUser_UserListAction");
        actionAccessScriptForUserModule_List(usersListValue);

    }

    /***
     * Implementing user create action authorization for OfficePersonnel
     * Passing user list value, create user value to BaseActionAccess method actionAccessScriptForUserModule_Create
     */
    @Test(priority = 19)
    public void TC307_officePersonnelUser_UserCreateAction() {
        CommonUtility.logCreateTest("TC307_officePersonnelUser_UserCreateAction");
        actionAccessScriptForUserModule_Create(usersListValue, createUserValue, "OP");

    }

    /***
     * Implementing user edit action authorization for OfficePersonnel
     * Passing user list value, edit user value to BaseActionAccess method actionAccessScriptForUserModule_Edit
     */
    @Test(priority = 20)
    public void TC308_officePersonnelUser_UserEditAction() {
        CommonUtility.logCreateTest("TC308_officePersonnelUser_UserEditAction");
        actionAccessScriptForUserModule_Edit(usersListValue, editUserValue, "OP");

    }

    /***
     * For deleting Patients that have been created during test execution
     */
    @Test(priority = 21)
    public void afterTest_officePersonnelUser_PatientDeletion() {
        CommonUtility.logCreateTest("t01_officePersonnelUser_PatientDeletion");
        patientDeletion(editPatientValue);

    }

}
