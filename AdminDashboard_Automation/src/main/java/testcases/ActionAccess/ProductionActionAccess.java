package testcases.ActionAccess;

import dataprovider.DataProviderClass;
import helper.CommonUtility;
import helper.Logger;
import org.testng.Reporter;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

public class ProductionActionAccess extends BaseActionAccess {

    String userRoleValue;
    String emailValue;
    String passwordValue;
    String dashboardValue;
    String infoBoxValue;
    String latestStudiesValue;
    String patientsListValue;
    String createPatientValue;
    String editPatientValue;
    String importStudyValue;
    String patientDocumentsTabValue;
    String patientAuditTrailTabValue;
    String createDeviceValue;
    String editDeviceValue;
    String devicesListValue;
    String deviceLogsValue;
    String changeDeviceFacilityValue;
    String amplifiersListValue;
    String createAmplifierValue;
    String editAmplifierValue;
    String facilitiesListValue;
    String createFacilityValue;
    String editFacilityValue;
    String studiesListValue;
    String openStudyValue;
    String auditLogsValue;
    String sharedUsersValue;
    String usersListValue;
    String createUserValue;
    String editUserValue;

    /***
     * Getting credentials for Production
     * @param userRole User Role
     * @param email User Email
     * @param password User Password
     */
    @Test(dataProvider = "getProductionCredentials", dataProviderClass = DataProviderClass.class, priority = 1)
    public void getCredentialsForProduction(String userRole, String email, String password) {
        CommonUtility.logCreateTest("getCredentialsForProduction");
        userRoleValue = userRole;
        emailValue = email;
        passwordValue = password;

    }

    /***
     * Getting data from csv file for Production
     * parameters : each page & action access value
     */
    @Test(dataProvider = "getProductionData", dataProviderClass = DataProviderClass.class, priority = 2)
    public void getDataForProduction(String userRole, String dashboard, String infoBox, String latestStudies,
                                     String patientsList, String createPatient, String editPatient, String importStudy, String patientDocumentsTab, String patientAuditTrailTab,
                                     String createDevice, String editDevice, String devicesList, String deviceLogs, String changeDeviceFacility, String amplifiersList, String createAmplifier,
                                     String editAmplifier, String facilitiesList, String createFacility, String editFacility,
                                     String studiesList, String openStudy, String auditLogs, String sharedUsers, String usersList, String createUser, String editUser, String userAuditLogs) {
        CommonUtility.logCreateTest("getDataForProduction");
        userRoleValue = userRole;
        dashboardValue = dashboard;
        infoBoxValue = infoBox;
        latestStudiesValue = latestStudies;
        patientsListValue = patientsList;
        createPatientValue = createPatient;
        editPatientValue = editPatient;
        importStudyValue = importStudy;
        patientDocumentsTabValue = patientDocumentsTab;
        patientAuditTrailTabValue = patientAuditTrailTab;
        createDeviceValue = createDevice;
        editDeviceValue = editDevice;
        devicesListValue = devicesList;
        deviceLogsValue = deviceLogs;
        changeDeviceFacilityValue = changeDeviceFacility;
        amplifiersListValue = amplifiersList;
        createAmplifierValue = createAmplifier;
        editAmplifierValue = editAmplifier;
        facilitiesListValue = facilitiesList;
        createFacilityValue = createFacility;
        editFacilityValue = editFacility;
        studiesListValue = studiesList;
        openStudyValue = openStudy;
        auditLogsValue = auditLogs;
        sharedUsersValue = sharedUsers;
        usersListValue = usersList;
        createUserValue = createUser;
        editUserValue = editUser;

    }

    /***
     * Implementing login and user role verification for production
     * Passing email and password to BaseActionAccess method actionAccessScriptForLogin
     * Passing user role authorization value to BaseActionAccess method actionAccessScriptForUserRole
     */
    @Test(priority = 3)
    public void productionUser_LoginRoleVerification() {
        CommonUtility.logCreateTest("TC201_productionUser_LoginRoleVerification");
        actionAccessScriptForLogin(emailValue, passwordValue);
        actionAccessScriptForUserRole(userRoleValue);

    }

    /***
     * Implementing patient list authorization for production
     * Passing patient list authorization value to BaseActionAccess method actionAccessScriptForPatientModule_List
     */
    @Test(priority = 4)
    public void TC202_productionUser_PatientListAction() {
        CommonUtility.logCreateTest("TC202_productionUser_PatientListAction");
        actionAccessScriptForPatientModule_List(patientsListValue);

    }

    /***
     * Implementing patient create action authorization for production
     * Passing patient list value, create patient value to BaseActionAccess method actionAccessScriptForPatientModule_Create
     */
    @Test(priority = 5)
    public void TC203_productionUser_PatientCreateAction() {
        CommonUtility.logCreateTest("TC203_productionUser_PatientCreateAction");
        actionAccessScriptForPatientModule_Create(patientsListValue, createPatientValue);

    }

    /***
     * Implementing patient edit action authorization for production
     * Passing patient list value, edit patient value to BaseActionAccess method actionAccessScriptForPatientModule_Edit
     */
    @Test(priority = 6)
    public void TC204_productionUser_PatientEditAction() {
        CommonUtility.logCreateTest("TC204_productionUser_PatientEditAction");
        actionAccessScriptForPatientModule_Edit(patientsListValue, editPatientValue);

    }

    /***
     * Implementing import study action authorization for Production
     */
    @Test(priority = 7, groups = "importStudy")
    public void TC205_productionUser_ImportStudyAction() {
        CommonUtility.logCreateTest("TC205_productionUser_ImportStudyAction");
        actionAccessScriptForPatientModule_ImportStudy(patientsListValue, importStudyValue);

    }

    /***
     * Implementing facility list authorization for production
     * Passing facility list authorization value to BaseActionAccess method actionAccessScriptForFacilityModule_List
     */
    @Test(priority = 8)
    public void TC206_productionUser_FacilityListAction() {
        CommonUtility.logCreateTest("TC206_productionUser_FacilityListAction");
        actionAccessScriptForFacilityModule_List(facilitiesListValue);

    }

    /***
     * Implementing facility create action authorization for production
     * Passing facility list value, create facility value to BaseActionAccess method actionAccessScriptForFacilityModule_Create
     */
    @Test(priority = 9)
    public void TC207_productionUser_FacilityCreateAction() {
        CommonUtility.logCreateTest("TC207_productionUser_FacilityCreateAction");
        actionAccessScriptForFacilityModule_Create(facilitiesListValue, createFacilityValue, "PRO");

    }

    /***
     * Implementing facility edit action authorization for production
     * Passing facility list value, edit facility value to BaseActionAccess method actionAccessScriptForFacilityModule_Edit
     */
    @Test(priority = 10)
    public void TC208_productionUser_FacilityEditAction() {
        CommonUtility.logCreateTest("TC208_productionUser_FacilityEditAction");
        actionAccessScriptForFacilityModule_Edit(facilitiesListValue, editFacilityValue, "PRO");

    }

    /***
     * Implementing amplifier list authorization for production
     * Passing amplifier list authorization value to BaseActionAccess method actionAccessScriptForAmplifierModule_List
     */
    @Test(priority = 11)
    public void TC209_productionUser_AmplifierListAction() {
        CommonUtility.logCreateTest("TC209_productionUser_AmplifierListAction");
        actionAccessScriptForAmplifierModule_List(amplifiersListValue);

    }

    /***
     * Implementing amplifier create action authorization for production
     * Passing amplifer list value, create amplifier value to BaseActionAccess method actionAccessScriptForAmplifierModule_Create
     */
    @Test(priority = 12)
    public void TC210_productionUser_AmplifierCreateAction() {
        CommonUtility.logCreateTest("TC210_productionUser_AmplifierCreateAction");
        actionAccessScriptForAmplifierModule_Create(amplifiersListValue, createAmplifierValue, "PRO");

    }

    /***
     * Implementing amplifier edit action authorization for production
     * Passing amplifier list value, edit amplifier value to BaseActionAccess method actionAccessScriptForAmplifierModule_Edit
     */
    @Test(priority = 13)
    public void TC211_productionUser_AmplifierEditAction() {
        CommonUtility.logCreateTest("TC211_productionUser_AmplifierEditAction");
        actionAccessScriptForAmplifierModule_Edit(amplifiersListValue, editAmplifierValue, "PRO");

    }

    /***
     * Implementing device list authorization for production
     * Passing device list authorization value to BaseActionAccess method actionAccessScriptForDeviceModule_List
     */
    @Test(priority = 14)
    public void TC212_productionUser_DeviceListAction() {
        CommonUtility.logCreateTest("TC212_productionUser_DeviceListAction");
        actionAccessScriptForDeviceModule_List(devicesListValue);

    }

    /***
     * Implementing device create action authorization for production
     * Passing device list value, create device value to BaseActionAccess method actionAccessScriptForDeviceModule_Create
     */
    @Test(priority = 15)
    public void TC213_productionUser_DeviceCreateAction() {
        CommonUtility.logCreateTest("TC213_productionUser_DeviceCreateAction");
        actionAccessScriptForDeviceModule_Create(devicesListValue, createDeviceValue);

    }

    /***
     * Implementing device edit action authorization for production
     * Passing device list value, edit device value to BaseActionAccess method actionAccessScriptForDeviceModule_Edit
     */
    @Test(priority = 16)
    public void TC214_productionUser_DeviceEditAction() {
        CommonUtility.logCreateTest("TC214_productionUser_DeviceEditAction");
        actionAccessScriptForDeviceModule_Edit(devicesListValue, editDeviceValue);

    }

    /***
     * Implementing user list authorization for production
     * Passing user list authorization value to BaseActionAccess method actionAccessScriptForUserModule_List
     */
    @Test(priority = 18)
    public void TC216_productionUser_UserListAction() {
        CommonUtility.logCreateTest("TC216_productionUser_UserListAction");
        actionAccessScriptForUserModule_List(usersListValue);

    }

    /***
     * Implementing user create action authorization for production
     * Passing user list value, create user value to BaseActionAccess method actionAccessScriptForUserModule_Create
     */
    @Test(priority = 19)
    public void TC217_productionUser_UserCreateAction() {
        CommonUtility.logCreateTest("TC217_productionUser_UserCreateAction");
        actionAccessScriptForUserModule_Create(usersListValue, createUserValue, "PRO");

    }

    /***
     * Implementing user edit action authorization for production
     * Passing user list value, edit user value to BaseActionAccess method actionAccessScriptForUserModule_Edit
     */
    @Test(priority = 20)
    public void TC218_productionUser_UserEditAction() {
        CommonUtility.logCreateTest("TC218_productionUser_UserEditAction");
        actionAccessScriptForUserModule_Edit(usersListValue, editUserValue, "PRO");

    }

    /***
     * Implementing user sign out for Production user
     */
    @Test(priority = 23)
    public void afterTest_productionUser_SignOut() {
        CommonUtility.logCreateTest("t03_productionUser_SignOut");
        actionAccessScriptForUserSignOut();

    }

    /***
     * For deleting Amplifiers that have been created using test execution
     */
    @Test(priority = 21)
    public void afterTest_productionUser_AmplifierDeletion() {
        CommonUtility.logCreateTest("t01_productionUser_AmplifierDeletion");
        amplifierDeletion(editAmplifierValue);

    }

    /***
     * For deleting Devices that have been created using test execution
     */
    @Test(priority = 22)
    public void afterTest_productionUser_DeviceDeletion() {
        CommonUtility.logCreateTest("t02_productionUser_DeviceDeletion");
        deviceDeletion(editDeviceValue);

    }

}
