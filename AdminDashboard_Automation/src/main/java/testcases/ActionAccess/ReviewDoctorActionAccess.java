package testcases.ActionAccess;

import dataprovider.DataProviderClass;
import helper.CommonUtility;
import helper.Logger;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class ReviewDoctorActionAccess extends BaseActionAccess {

    String userRoleValue;
    String emailValue;
    String passwordValue;
    String dashboardValue;
    String infoBoxValue;
    String latestStudiesValue;
    String patientsListValue;
    String createPatientValue;
    String editPatientValue;
    String importStudyValue;
    String patientDocumentsTabValue;
    String patientAuditTrailTabValue;
    String createDeviceValue;
    String editDeviceValue;
    String devicesListValue;
    String deviceLogsValue;
    String changeDeviceFacilityValue;
    String amplifiersListValue;
    String createAmplifierValue;
    String editAmplifierValue;
    String facilitiesListValue;
    String createFacilityValue;
    String editFacilityValue;
    String studiesListValue;
    String openStudyValue;
    String auditLogsValue;
    String sharedUsersValue;
    String usersListValue;
    String createUserValue;
    String editUserValue;

    /***
     * Getting credentials for ReviewDoctor
     * @param userRole User Role
     * @param email User Email
     * @param password User Password
     */
    @Test(dataProvider = "getReviewDoctorCredentials", dataProviderClass = DataProviderClass.class, priority = 1)
    public void getCredentialsForReviewDoctor(String userRole, String email, String password) {
        CommonUtility.logCreateTest("getCredentialsForReviewDoctor");
        userRoleValue = userRole;
        emailValue = email;
        passwordValue = password;

    }

    /***
     * Getting data from csv file for ReviewDoctor
     * parameters : each page & action access value
     */
    @Test(dataProvider = "getReviewDoctorData", dataProviderClass = DataProviderClass.class, priority = 2)
    public void getDataForReviewDoctor(String userRole, String dashboard, String infoBox, String latestStudies,
                                       String patientsList, String createPatient, String editPatient, String importStudy, String patientDocumentsTab, String patientAuditTrailTab,
                                       String createDevice, String editDevice, String devicesList, String deviceLogs, String changeDeviceFacility, String amplifiersList, String createAmplifier,
                                       String editAmplifier, String facilitiesList, String createFacility, String editFacility,
                                       String studiesList, String openStudy, String auditLogs, String sharedUsers, String usersList, String createUser, String editUser, String userAuditLogs) {
        CommonUtility.logCreateTest("getDataForReviewDoctor");
        userRoleValue = userRole;
        dashboardValue = dashboard;
        infoBoxValue = infoBox;
        latestStudiesValue = latestStudies;
        patientsListValue = patientsList;
        createPatientValue = createPatient;
        editPatientValue = editPatient;
        importStudyValue = importStudy;
        patientDocumentsTabValue = patientDocumentsTab;
        patientAuditTrailTabValue = patientAuditTrailTab;
        createDeviceValue = createDevice;
        editDeviceValue = editDevice;
        devicesListValue = devicesList;
        deviceLogsValue = deviceLogs;
        changeDeviceFacilityValue = changeDeviceFacility;
        amplifiersListValue = amplifiersList;
        createAmplifierValue = createAmplifier;
        editAmplifierValue = editAmplifier;
        facilitiesListValue = facilitiesList;
        createFacilityValue = createFacility;
        editFacilityValue = editFacility;
        studiesListValue = studiesList;
        openStudyValue = openStudy;
        auditLogsValue = auditLogs;
        sharedUsersValue = sharedUsers;
        usersListValue = usersList;
        createUserValue = createUser;
        editUserValue = editUser;

    }

    /***
     * Implementing login and user role verification for ReviewDoctor
     * Passing email and password to BaseActionAccess method actionAccessScriptForLogin
     * Passing user role authorization value to BaseActionAccess method actionAccessScriptForUserRole
     */
    @Test(priority = 3)
    public void reviewDoctorUser_LoginRoleVerification() {
        CommonUtility.logCreateTest("TC237_reviewDoctorUser_LoginRoleVerification");
        actionAccessScriptForLogin(emailValue, passwordValue);
        actionAccessScriptForUserRole(userRoleValue);

    }

    /***
     * Implementing patient list authorization for ReviewDoctor
     * Passing patient list authorization value to BaseActionAccess method actionAccessScriptForPatientModule_List
     */
    @Test(priority = 4)
    public void TC238_reviewDoctorUser_PatientListAction() {
        CommonUtility.logCreateTest("TC238_reviewDoctorUser_PatientListAction");
        actionAccessScriptForPatientModule_List(patientsListValue);

    }

    /***
     * Implementing patient create action authorization for ReviewDoctor
     * Passing patient list value, create patient value to BaseActionAccess method actionAccessScriptForPatientModule_Create
     */
    @Test(priority = 5)
    public void TC239_reviewDoctorUser_PatientCreateAction() {
        CommonUtility.logCreateTest("TC239_reviewDoctorUser_PatientCreateAction");
        actionAccessScriptForPatientModule_Create(patientsListValue, createPatientValue);

    }

    /***
     * Implementing patient edit action authorization for ReviewDoctor
     * Passing patient list value, edit patient value to BaseActionAccess method actionAccessScriptForPatientModule_Edit
     */
    @Test(priority = 6)
    public void TC240_reviewDoctorUser_PatientEditAction() {
        CommonUtility.logCreateTest("TC240_reviewDoctorUser_PatientEditAction");
        actionAccessScriptForPatientModule_Edit(patientsListValue, editPatientValue);

    }

    /***
     * Implementing import study action authorization for ReviewDoctor
     */
    @Test(priority = 7, groups = "importStudy")
    public void TC241_reviewDoctorUser_ImportStudyAction() {
        CommonUtility.logCreateTest("TC241_reviewDoctorUser_ImportStudyAction");
        actionAccessScriptForPatientModule_ImportStudy(patientsListValue, importStudyValue);

    }

    /***
     * Implementing facility list authorization for ReviewDoctor
     * Passing facility list authorization value to BaseActionAccess method actionAccessScriptForFacilityModule_List
     */
    @Test(priority = 8)
    public void TC242_reviewDoctorUser_FacilityListAction() {
        CommonUtility.logCreateTest("TC242_reviewDoctorUser_FacilityListAction");
        actionAccessScriptForFacilityModule_List(facilitiesListValue);

    }

    /***
     * Implementing facility create action authorization for ReviewDoctor
     * Passing facility list value, create facility value to BaseActionAccess method actionAccessScriptForFacilityModule_Create
     */
    @Test(priority = 9)
    public void TC243_reviewDoctorUser_FacilityCreateAction() {
        CommonUtility.logCreateTest("TC243_reviewDoctorUser_FacilityCreateAction");
        actionAccessScriptForFacilityModule_Create(facilitiesListValue, createFacilityValue, "RD");

    }

    /***
     * Implementing facility edit action authorization for ReviewDoctor
     * Passing facility list value, edit facility value to BaseActionAccess method actionAccessScriptForFacilityModule_Edit
     */
    @Test(priority = 10)
    public void TC244_reviewDoctorUser_FacilityEditAction() {
        CommonUtility.logCreateTest("TC244_reviewDoctorUser_FacilityEditAction");
        actionAccessScriptForFacilityModule_Edit(facilitiesListValue, editFacilityValue, "RD");

    }

    /***
     * Implementing amplifier list authorization for ReviewDoctor
     * Passing amplifier list authorization value to BaseActionAccess method actionAccessScriptForAmplifierModule_List
     */
    @Test(priority = 11)
    public void TC245_reviewDoctorUser_AmplifierListAction() {
        CommonUtility.logCreateTest("TC245_reviewDoctorUser_AmplifierListAction");
        actionAccessScriptForAmplifierModule_List(amplifiersListValue);

    }

    /***
     * Implementing amplifier create action authorization for ReviewDoctor
     * Passing amplifer list value, create amplifier value to BaseActionAccess method actionAccessScriptForAmplifierModule_Create
     */
    @Test(priority = 12)
    public void TC246_reviewDoctorUser_AmplifierCreateAction() {
        CommonUtility.logCreateTest("TC246_reviewDoctorUser_AmplifierCreateAction");
        actionAccessScriptForAmplifierModule_Create(amplifiersListValue, createAmplifierValue, "RD");

    }

    /***
     * Implementing amplifier edit action authorization for ReviewDoctor
     * Passing amplifier list value, edit amplifier value to BaseActionAccess method actionAccessScriptForAmplifierModule_Edit
     */
    @Test(priority = 13)
    public void TC247_reviewDoctorUser_AmplifierEditAction() {
        CommonUtility.logCreateTest("TC247_reviewDoctorUser_AmplifierEditAction");
        actionAccessScriptForAmplifierModule_Edit(amplifiersListValue, editAmplifierValue, "RD");

    }

    /***
     * Implementing device list authorization for ReviewDoctor
     * Passing device list authorization value to BaseActionAccess method actionAccessScriptForDeviceModule_List
     */
    @Test(priority = 14)
    public void TC248_reviewDoctorUser_DeviceListAction() {
        CommonUtility.logCreateTest("TC248_reviewDoctorUser_DeviceListAction");
        actionAccessScriptForDeviceModule_List(devicesListValue);

    }

    /***
     * Implementing device create action authorization for ReviewDoctor
     * Passing device list value, create device value to BaseActionAccess method actionAccessScriptForDeviceModule_Create
     */
    @Test(priority = 15)
    public void TC249_reviewDoctorUser_DeviceCreateAction() {
        CommonUtility.logCreateTest("TC249_reviewDoctorUser_DeviceCreateAction");
        actionAccessScriptForDeviceModule_Create(devicesListValue, createDeviceValue);

    }

    /***
     * Implementing device edit action authorization for ReviewDoctor
     * Passing device list value, edit device value to BaseActionAccess method actionAccessScriptForDeviceModule_Edit
     */
    @Test(priority = 16)
    public void TC250_reviewDoctorUser_DeviceEditAction() {
        CommonUtility.logCreateTest("TC252_reviewDoctorUser_UserListAction");
        actionAccessScriptForDeviceModule_Edit(devicesListValue, editDeviceValue);

    }

    /***
     * Implementing user list authorization for ReviewDoctor
     * Passing user list authorization value to BaseActionAccess method actionAccessScriptForUserModule_List
     */
    @Test(priority = 18)
    public void TC252_reviewDoctorUser_UserListAction() {
        CommonUtility.logCreateTest("TC252_reviewDoctorUser_UserListAction");
        actionAccessScriptForUserModule_List(usersListValue);

    }

    /***
     * Implementing user create action authorization for ReviewDoctor
     * Passing user list value, create user value to BaseActionAccess method actionAccessScriptForUserModule_Create
     */
    @Test(priority = 19)
    public void TC253_reviewDoctorUser_UserCreateAction() {
        CommonUtility.logCreateTest("TC253_reviewDoctorUser_UserCreateAction");
        actionAccessScriptForUserModule_Create(usersListValue, createUserValue, "RD");

    }

    /***
     * Implementing user edit action authorization for ReviewDoctor
     * Passing user list value, edit user value to BaseActionAccess method actionAccessScriptForUserModule_Edit
     */
    @Test(priority = 20)
    public void TC254_reviewDoctorUser_UserEditAction() {
        CommonUtility.logCreateTest("TC254_reviewDoctorUser_UserEditAction");
        actionAccessScriptForUserModule_Edit(usersListValue, editUserValue, "RD");

    }

}
