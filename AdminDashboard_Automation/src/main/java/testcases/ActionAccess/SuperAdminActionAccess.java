package testcases.ActionAccess;

import dataprovider.DataProviderClass;
import helper.CommonUtility;
import helper.Logger;
import org.testng.Reporter;
import org.testng.annotations.*;

public class SuperAdminActionAccess extends BaseActionAccess {

    String userRoleValue;
    String emailValue;
    String passwordValue;
    String dashboardValue;
    String infoBoxValue;
    String latestStudiesValue;
    String patientsListValue;
    String createPatientValue;
    String editPatientValue;
    String importStudyValue;
    String patientDocumentsTabValue;
    String patientAuditTrailTabValue;
    String createDeviceValue;
    String editDeviceValue;
    String devicesListValue;
    String deviceLogsValue;
    String changeDeviceFacilityValue;
    String amplifiersListValue;
    String createAmplifierValue;
    String editAmplifierValue;
    String facilitiesListValue;
    String createFacilityValue;
    String editFacilityValue;
    String studiesListValue;
    String openStudyValue;
    String auditLogsValue;
    String sharedUsersValue;
    String usersListValue;
    String createUserValue;
    String editUserValue;

    /***
     * Getting credentials for SuperAdmin
     * @param userRole User Role
     * @param email User Email
     * @param password User Password
     */
    @Test(dataProvider = "getSuperAdminCredentials", dataProviderClass = DataProviderClass.class, priority = 1)
    public void getCredentialsForSuperAdmin(String userRole, String email, String password) {
        CommonUtility.logCreateTest("getCredentialsForSuperAdmin");
        userRoleValue = userRole;
        emailValue = email;
        passwordValue = password;

    }

    /***
     * Getting data from csv file for superAdmin
     * parameters : each page & action access value
     */
    @Test(dataProvider = "getSuperAdminData", dataProviderClass = DataProviderClass.class, priority = 2)
    public void getDataForSuperAdmin(String userRole, String dashboard, String infoBox, String latestStudies,
                                     String patientsList, String createPatient, String editPatient, String importStudy, String patientDocumentsTab, String patientAuditTrailTab,
                                     String createDevice, String editDevice, String devicesList, String deviceLogs, String changeDeviceFacility, String amplifiersList, String createAmplifier,
                                     String editAmplifier, String facilitiesList, String createFacility, String editFacility,
                                     String studiesList, String openStudy, String auditLogs, String sharedUsers, String usersList, String createUser, String editUser, String userAuditLogs) {
        CommonUtility.logCreateTest("getDataForSuperAdmin");
        userRoleValue = userRole;
        dashboardValue = dashboard;
        infoBoxValue = infoBox;
        latestStudiesValue = latestStudies;
        patientsListValue = patientsList;
        createPatientValue = createPatient;
        editPatientValue = editPatient;
        importStudyValue = importStudy;
        patientDocumentsTabValue = patientDocumentsTab;
        patientAuditTrailTabValue = patientAuditTrailTab;
        createDeviceValue = createDevice;
        editDeviceValue = editDevice;
        devicesListValue = devicesList;
        deviceLogsValue = deviceLogs;
        changeDeviceFacilityValue = changeDeviceFacility;
        amplifiersListValue = amplifiersList;
        createAmplifierValue = createAmplifier;
        editAmplifierValue = editAmplifier;
        facilitiesListValue = facilitiesList;
        createFacilityValue = createFacility;
        editFacilityValue = editFacility;
        studiesListValue = studiesList;
        openStudyValue = openStudy;
        auditLogsValue = auditLogs;
        sharedUsersValue = sharedUsers;
        usersListValue = usersList;
        createUserValue = createUser;
        editUserValue = editUser;

    }

    /***
     * Implementing login and user role verification for superAdmin
     * Passing email and password to BaseActionAccess method actionAccessScriptForLogin
     * Passing user role authorization value to BaseActionAccess method actionAccessScriptForUserRole
     */
    @Test(priority = 3)
    public void superAdminUser_LoginRoleVerification() {
        CommonUtility.logCreateTest("TC164_superAdminUser_LoginRoleVerification");
        actionAccessScriptForLogin(emailValue, passwordValue);
        actionAccessScriptForUserRole(userRoleValue);

    }

    /***
     * Implementing patient list authorization for superAdmin
     * Passing patient list authorization value to BaseActionAccess method actionAccessScriptForPatientModule_List
     */
    @Test(priority = 4)
    public void TC165_superAdminUser_PatientListAction() {
        CommonUtility.logCreateTest("TC165_superAdminUser_PatientListAction");
        actionAccessScriptForPatientModule_List(patientsListValue);

    }

    /***
     * Implementing patient create action authorization for superAdmin
     * Passing patient list value, create patient value to BaseActionAccess method actionAccessScriptForPatientModule_Create
     */
    @Test(priority = 5)
    public void TC166_superAdminUser_PatientCreateAction() {
        CommonUtility.logCreateTest("TC166_superAdminUser_PatientCreateAction");
        actionAccessScriptForPatientModule_Create(patientsListValue, createPatientValue);

    }

    /***
     * Implementing patient edit action authorization for superAdmin
     * Passing patient list value, edit patient value to BaseActionAccess method actionAccessScriptForPatientModule_Edit
     */
    @Test(priority = 6)
    public void TC167_superAdminUser_PatientEditAction() {
        CommonUtility.logCreateTest("TC167_superAdminUser_PatientEditAction");
        actionAccessScriptForPatientModule_Edit(patientsListValue, editPatientValue);

    }

    /***
     * Implementing import study action authorization for SuperAdmin
     */
    @Test(priority = 7, groups = "importStudy")
    public void TC168_superAdminUser_ImportStudyAction() {
        CommonUtility.logCreateTest("TC168_superAdminUser_ImportStudyAction");
        actionAccessScriptForPatientModule_ImportStudy(patientsListValue, importStudyValue);

    }

    /***
     * Implementing facility list authorization for superAdmin
     * Passing facility list authorization value to BaseActionAccess method actionAccessScriptForFacilityModule_List
     */
    @Test(priority = 8)
    public void TC169_superAdminUser_FacilityListAction() {
        CommonUtility.logCreateTest("TC169_superAdminUser_FacilityListAction");
        actionAccessScriptForFacilityModule_List(facilitiesListValue);

    }

    /***
     * Implementing facility create action authorization for superAdmin
     * Passing facility list value, create facility value to BaseActionAccess method actionAccessScriptForFacilityModule_Create
     */
    @Test(priority = 9)
    public void TC170_superAdminUser_FacilityCreateAction() {
        CommonUtility.logCreateTest("TC170_superAdminUser_FacilityCreateAction");
        actionAccessScriptForFacilityModule_Create(facilitiesListValue, createFacilityValue, "SA");

    }

    /***
     * Implementing facility edit action authorization for superAdmin
     * Passing facility list value, edit facility value to BaseActionAccess method actionAccessScriptForFacilityModule_Edit
     */
    @Test(priority = 10)
    public void TC171_superAdminUser_FacilityEditAction() {
        CommonUtility.logCreateTest("TC171_superAdminUser_FacilityEditAction");
        actionAccessScriptForFacilityModule_Edit(facilitiesListValue, editFacilityValue, "SA");

    }

    /***
     * Implementing amplifier list authorization for superAdmin
     * Passing amplifier list authorization value to BaseActionAccess method actionAccessScriptForAmplifierModule_List
     */
    @Test(priority = 11)
    public void TC172_superAdminUser_AmplifierListAction() {
        CommonUtility.logCreateTest("TC172_superAdminUser_AmplifierListAction");
        actionAccessScriptForAmplifierModule_List(amplifiersListValue);

    }

    /***
     * Implementing amplifier create action authorization for superAdmin
     * Passing amplifer list value, create amplifier value to BaseActionAccess method actionAccessScriptForAmplifierModule_Create
     */
    @Test(priority = 12)
    public void TC173_superAdminUser_AmplifierCreateAction() {
        CommonUtility.logCreateTest("TC173_superAdminUser_AmplifierCreateAction");
        actionAccessScriptForAmplifierModule_Create(amplifiersListValue, createAmplifierValue, "SA");

    }

    /***
     * Implementing amplifier edit action authorization for superAdmin
     * Passing amplifier list value, edit amplifier value to BaseActionAccess method actionAccessScriptForAmplifierModule_Edit
     */
    @Test(priority = 13)
    public void TC174_superAdminUser_AmplifierEditAction() {
        CommonUtility.logCreateTest("TC174_superAdminUser_AmplifierEditAction");
        actionAccessScriptForAmplifierModule_Edit(amplifiersListValue, editAmplifierValue, "SA");

    }

    /***
     * Implementing device list authorization for superAdmin
     * Passing device list authorization value to BaseActionAccess method actionAccessScriptForDeviceModule_List
     */
    @Test(priority = 14)
    public void TC175_superAdminUser_DeviceListAction() {
        CommonUtility.logCreateTest("TC175_superAdminUser_DeviceListAction");
        actionAccessScriptForDeviceModule_List(devicesListValue);

    }

    /***
     * Implementing device create action authorization for superAdmin
     * Passing device list value, create device value to BaseActionAccess method actionAccessScriptForDeviceModule_Create
     */
    @Test(priority = 15)
    public void TC176_superAdminUser_DeviceCreateAction() {
        CommonUtility.logCreateTest("TC176_superAdminUser_DeviceCreateAction");
        actionAccessScriptForDeviceModule_Create(devicesListValue, createDeviceValue);

    }

    /***
     * Implementing device edit action authorization for superAdmin
     * Passing device list value, edit device value to BaseActionAccess method actionAccessScriptForDeviceModule_Edit
     */
    @Test(priority = 16)
    public void TC177_superAdminUser_DeviceEditAction() {
        CommonUtility.logCreateTest("TC177_superAdminUser_DeviceEditAction");
        actionAccessScriptForDeviceModule_Edit(devicesListValue, editDeviceValue);

    }

    /***
     * Implementing user list authorization for superAdmin
     * Passing user list authorization value to BaseActionAccess method actionAccessScriptForUserModule_List
     */
    @Test(priority = 18)
    public void TC179_superAdminUser_UserListAction() {
        CommonUtility.logCreateTest("TC179_superAdminUser_UserListAction");
        actionAccessScriptForUserModule_List(usersListValue);

    }

    /***
     * Implementing user create action authorization for superAdmin
     * Passing user list value, create user value to BaseActionAccess method actionAccessScriptForUserModule_Create
     */
    @Test(priority = 19)
    public void TC180_superAdminUser_UserCreateAction() {
        CommonUtility.logCreateTest("TC180_superAdminUser_UserCreateAction");
        actionAccessScriptForUserModule_Create(usersListValue, createUserValue, "SA");

    }

    /***
     * Implementing user edit action authorization for superAdmin
     * Passing user list value, edit user value to BaseActionAccess method actionAccessScriptForUserModule_Edit
     */
    @Test(priority = 20)
    public void TC181_superAdminUser_UserEditAction() {
        CommonUtility.logCreateTest("TC181_superAdminUser_UserEditAction");
        actionAccessScriptForUserModule_Edit(usersListValue, editUserValue, "SA");

    }

    /***
     * Implementing user sign out for SuperAdmin
     */
    @Test(priority = 21)
    public void afterTest_superAdminUser_SignOut() {
        CommonUtility.logCreateTest("TC182_superAdminUser_SignOut");
        actionAccessScriptForUserSignOut();

    }

    /***
     * For deleting Amplifiers that have been created during test execution
     */
    @Test(groups = "ToBeExcludedForOtherUsers", priority = 22)
    public void afterTest_superAdminUser_AmplifierDeletion() {
        CommonUtility.logCreateTest("afterTest_superAdminUser_AmplifierDeletion");
        amplifierDeletion(editAmplifierValue);

    }

    /**
     * For deleting Devices that have been created during test execution
     */
    @Test(groups = "ToBeExcludedForOtherUsers", priority = 23)
    public void afterTest_superAdminUser_DeviceDeletion() {
        CommonUtility.logCreateTest("afterTest_superAdminUser_DeviceDeletion");
        deviceDeletion(editDeviceValue);

    }

    /***
     * For deleting Patients that have been created during test execution
     */
    @Test(groups = "ToBeExcludedForOtherUsers", priority = 24)
    public void afterTest_superAdminUser_PatientDeletion() {
        CommonUtility.logCreateTest("afterTest_superAdminUser_PatientDeletion");
        patientDeletion(editPatientValue);

    }

    /***
     * For deleting Users that have been created during test execution
     */
    @Test(groups = "ToBeExcludedForOtherUsers", priority = 25)
    public void afterTest_superAdminUser_UserDeletion() {
        CommonUtility.logCreateTest("afterTest_superAdminUser_UserDeletion");
        userDeletion(editUserValue);

    }

    /***
     * For deleting Facilities that have been created during test execution
     */
    @Test(groups = "ToBeExcludedForOtherUsers", priority = 26)
    public void afterTest_superAdminUser_FacilityDeletion() {
        CommonUtility.logCreateTest("afterTest_superAdminUser_FacilityDeletion");
        facilityDeletion(editFacilityValue);

    }

}
