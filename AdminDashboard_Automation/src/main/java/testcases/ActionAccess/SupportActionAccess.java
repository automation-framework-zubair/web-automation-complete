package testcases.ActionAccess;

import dataprovider.DataProviderClass;
import helper.CommonUtility;
import helper.Logger;
import org.testng.Reporter;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

public class SupportActionAccess extends BaseActionAccess {

    String userRoleValue;
    String emailValue;
    String passwordValue;
    String dashboardValue;
    String infoBoxValue;
    String latestStudiesValue;
    String patientsListValue;
    String createPatientValue;
    String editPatientValue;
    String importStudyValue;
    String patientDocumentsTabValue;
    String patientAuditTrailTabValue;
    String createDeviceValue;
    String editDeviceValue;
    String devicesListValue;
    String deviceLogsValue;
    String changeDeviceFacilityValue;
    String amplifiersListValue;
    String createAmplifierValue;
    String editAmplifierValue;
    String facilitiesListValue;
    String createFacilityValue;
    String editFacilityValue;
    String studiesListValue;
    String openStudyValue;
    String auditLogsValue;
    String sharedUsersValue;
    String usersListValue;
    String createUserValue;
    String editUserValue;
    /***
     * Getting credentials for Support
     * @param userRole User Role
     * @param email User Email
     * @param password User Password
     */
    @Test(dataProvider = "getSupportCredentials", dataProviderClass = DataProviderClass.class, priority = 1)
    public void getCredentialsForSupport(String userRole, String email, String password) {
        CommonUtility.logCreateTest("getCredentialsForSupport");
        userRoleValue = userRole;
        emailValue = email;
        passwordValue = password;

    }

    /***
     * Getting data from csv file for support
     * parameters : each page & action access value
     */
    @Test(dataProvider = "getSupportData", dataProviderClass = DataProviderClass.class, priority = 2)
    public void getDataForSupport(String userRole, String dashboard, String infoBox, String latestStudies,
                                  String patientsList, String createPatient, String editPatient, String importStudy, String patientDocumentsTab, String patientAuditTrailTab,
                                  String createDevice, String editDevice, String devicesList, String deviceLogs, String changeDeviceFacility, String amplifiersList, String createAmplifier,
                                  String editAmplifier, String facilitiesList, String createFacility, String editFacility,
                                  String studiesList, String openStudy, String auditLogs, String sharedUsers, String usersList, String createUser, String editUser, String userAuditLogs) {
        CommonUtility.logCreateTest("getDataForSupport");
        userRoleValue = userRole;
        dashboardValue = dashboard;
        infoBoxValue = infoBox;
        latestStudiesValue = latestStudies;
        patientsListValue = patientsList;
        createPatientValue = createPatient;
        editPatientValue = editPatient;
        importStudyValue = importStudy;
        patientDocumentsTabValue = patientDocumentsTab;
        patientAuditTrailTabValue = patientAuditTrailTab;
        createDeviceValue = createDevice;
        editDeviceValue = editDevice;
        devicesListValue = devicesList;
        deviceLogsValue = deviceLogs;
        changeDeviceFacilityValue = changeDeviceFacility;
        amplifiersListValue = amplifiersList;
        createAmplifierValue = createAmplifier;
        editAmplifierValue = editAmplifier;
        facilitiesListValue = facilitiesList;
        createFacilityValue = createFacility;
        editFacilityValue = editFacility;
        studiesListValue = studiesList;
        openStudyValue = openStudy;
        auditLogsValue = auditLogs;
        sharedUsersValue = sharedUsers;
        usersListValue = usersList;
        createUserValue = createUser;
        editUserValue = editUser;

    }

    /***
     * Implementing login and user role verification for support
     * Passing email and password to BaseActionAccess method actionAccessScriptForLogin
     * Passing user role authorization value to BaseActionAccess method actionAccessScriptForUserRole
     */
    @Test(priority = 3)
    public void supportUser_LoginRoleVerification() {
        CommonUtility.logCreateTest("TC183_supportUser_LoginRoleVerification");
        actionAccessScriptForLogin(emailValue, passwordValue);
        actionAccessScriptForUserRole(userRoleValue);

    }

    /***
     * Implementing patient list authorization for support
     * Passing patient list authorization value to BaseActionAccess method actionAccessScriptForPatientModule_List
     */
    @Test(priority = 4)
    public void TC184_supportUser_PatientListAction() {
        CommonUtility.logCreateTest("TC184_supportUser_PatientListAction");
        actionAccessScriptForPatientModule_List(patientsListValue);

    }

    /***
     * Implementing patient create action authorization for support
     * Passing patient list value, create patient value to BaseActionAccess method actionAccessScriptForPatientModule_Create
     */
    @Test(priority = 5)
    public void TC185_supportUser_PatientCreateAction() {
        CommonUtility.logCreateTest("TC185_supportUser_PatientCreateAction");
        actionAccessScriptForPatientModule_Create(patientsListValue, createPatientValue);

    }

    /***
     * Implementing patient edit action authorization for support
     * Passing patient list value, edit patient value to BaseActionAccess method actionAccessScriptForPatientModule_Edit
     */
    @Test(priority = 6)
    public void TC186_supportUser_PatientEditAction() {
        CommonUtility.logCreateTest("TC186_supportUser_PatientEditAction");
        actionAccessScriptForPatientModule_Edit(patientsListValue, editPatientValue);

    }

    /***
     * Implementing import study action authorization for Support
     */
    @Test(priority = 7, groups = "importStudy")
    public void TC187_supportUser_ImportStudyAction() {
        CommonUtility.logCreateTest("TC187_supportUser_ImportStudyAction");
        actionAccessScriptForPatientModule_ImportStudy(patientsListValue, importStudyValue);

    }

    /***
     * Implementing facility list authorization for support
     * Passing facility list authorization value to BaseActionAccess method actionAccessScriptForFacilityModule_List
     */
    @Test(priority = 8)
    public void TC188_supportUser_FacilityListAction() {
        CommonUtility.logCreateTest("TC188_supportUser_FacilityListAction");
        actionAccessScriptForFacilityModule_List(facilitiesListValue);

    }

    /***
     * Implementing facility create action authorization for support
     * Passing facility list value, create facility value to BaseActionAccess method actionAccessScriptForFacilityModule_Create
     */
    @Test(priority = 9)
    public void TC189_supportUser_FacilityCreateAction() {
        CommonUtility.logCreateTest("TC189_supportUser_FacilityCreateAction");
        actionAccessScriptForFacilityModule_Create(facilitiesListValue, createFacilityValue, "SUP");

    }

    /***
     * Implementing facility edit action authorization for support
     * Passing facility list value, edit facility value to BaseActionAccess method actionAccessScriptForFacilityModule_Edit
     */
    @Test(priority = 10)
    public void TC190_supportUser_FacilityEditAction() {
        CommonUtility.logCreateTest("TC190_supportUser_FacilityEditAction");
        actionAccessScriptForFacilityModule_Edit(facilitiesListValue, editFacilityValue, "SUP");

    }

    /***
     * Implementing amplifier list authorization for support
     * Passing amplifier list authorization value to BaseActionAccess method actionAccessScriptForAmplifierModule_List
     */
    @Test(priority = 11)
    public void TC191_supportUser_AmplifierListAction() {
        CommonUtility.logCreateTest("TC191_supportUser_AmplifierListAction");
        actionAccessScriptForAmplifierModule_List(amplifiersListValue);

    }

    /***
     * Implementing amplifier create action authorization for support
     * Passing amplifer list value, create amplifier value to BaseActionAccess method actionAccessScriptForAmplifierModule_Create
     */
    @Test(priority = 12)
    public void TC192_supportUser_AmplifierCreateAction() {
        CommonUtility.logCreateTest("TC192_supportUser_AmplifierCreateAction");
        actionAccessScriptForAmplifierModule_Create(amplifiersListValue, createAmplifierValue, "SUP");

    }

    /***
     * Implementing amplifier edit action authorization for support
     * Passing amplifier list value, edit amplifier value to BaseActionAccess method actionAccessScriptForAmplifierModule_Edit
     */
    @Test(priority = 13)
    public void TC193_supportUser_AmplifierEditAction() {
        CommonUtility.logCreateTest("TC193_supportUser_AmplifierEditAction");
        actionAccessScriptForAmplifierModule_Edit(amplifiersListValue, editAmplifierValue, "SUP");

    }

    /***
     * Implementing device list authorization for support
     * Passing device list authorization value to BaseActionAccess method actionAccessScriptForDeviceModule_List
     */
    @Test(priority = 14)
    public void TC194_supportUser_DeviceListAction() {
        CommonUtility.logCreateTest("TC194_supportUser_DeviceListAction");
        actionAccessScriptForDeviceModule_List(devicesListValue);

    }

    /***
     * Implementing device create action authorization for support
     * Passing device list value, create device value to BaseActionAccess method actionAccessScriptForDeviceModule_Create
     */
    @Test(priority = 15)
    public void TC195_supportUser_DeviceCreateAction() {
        CommonUtility.logCreateTest("TC195_supportUser_DeviceCreateAction");
        actionAccessScriptForDeviceModule_Create(devicesListValue, createDeviceValue);

    }

    /***
     * Implementing device edit action authorization for support
     * Passing device list value, edit device value to BaseActionAccess method actionAccessScriptForDeviceModule_Edit
     */
    @Test(priority = 16)
    public void TC196_supportUser_DeviceEditAction() {
        CommonUtility.logCreateTest("TC196_supportUser_DeviceEditAction");
        actionAccessScriptForDeviceModule_Edit(devicesListValue, editDeviceValue);

    }

    /***
     * Implementing user list authorization for support
     * Passing user list authorization value to BaseActionAccess method actionAccessScriptForUserModule_List
     */
    @Test(priority = 18)
    public void TC198_supportUser_UserListAction() {
        CommonUtility.logCreateTest("TC198_supportUser_UserListAction");
        actionAccessScriptForUserModule_List(usersListValue);

    }

    /***
     * Implementing user create action authorization for support
     * Passing user list value, create user value to BaseActionAccess method actionAccessScriptForUserModule_Create
     */
    @Test(priority = 19)
    public void TC199_supportUser_UserCreateAction() {
        CommonUtility.logCreateTest("TC199_supportUser_UserCreateAction");
        actionAccessScriptForUserModule_Create(usersListValue, createUserValue, "SUP");

    }

    /***
     * Implementing user edit action authorization for support
     * Passing user list value, edit user value to BaseActionAccess method actionAccessScriptForUserModule_Edit
     */
    @Test(priority = 20)
    public void TC200_supportUser_UserEditAction() {
        CommonUtility.logCreateTest("TC200_supportUser_UserEditAction");
        actionAccessScriptForUserModule_Edit(usersListValue, editUserValue, "SUP");

    }

    /***
     * Implementing user sign out for Support user
     */
    @Test(priority = 23)
    public void afterTest_supportUser_SignOut() {
        CommonUtility.logCreateTest("t03_supportUser_SignOut");
        actionAccessScriptForUserSignOut();

    }

    /***
     * For deleting Patients that have been created during test execution
     */
    @Test(priority = 21)
    public void afterTest_supportUser_PatientDeletion() {
        CommonUtility.logCreateTest("t01_supportUser_PatientDeletion");
        patientDeletion(editPatientValue);

    }

    /***
     * For deleting users that have been created that during test execution
     */
    @Test(priority = 22)
    public void afterTest_supportUser_UserDeletion() {
        CommonUtility.logCreateTest("t02_supportUser_UserDeletion");
        userDeletion(editUserValue);

    }

}
