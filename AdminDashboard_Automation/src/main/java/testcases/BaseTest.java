package testcases;

import browserutility.Browser;

import helper.*;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.*;
import pages.PagesFactory;
import pages.ui.PageClasses.LoginPage;
import testcases.ActionAccess.BaseActionAccess;

import java.io.*;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class BaseTest {

    SpreadsheetUtility spreadsheetUtility = new SpreadsheetUtility();
    ConfigReader configReader = new ConfigReader();
    static String reportPath;
    static String suiteName;

    /***
     * Method to capture screenshot
     * @return imageName : Returns the name of the screenshot
     */
    private String captureScreenshot() throws IOException {

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd_hhmmss");
        Date date = new Date();
        String strDate= formatter.format(date);
        File srcfileObj = ((TakesScreenshot) Browser.getWebDriver()).getScreenshotAs(OutputType.FILE);
        String imageName = strDate + ".png";
        System.out.println(reportPath);
        String screenshotPath = reportPath + imageName;
        File DestFileObj = new File(screenshotPath);
        FileUtils.copyFile(srcfileObj, DestFileObj);
        return imageName;

    }

    /***
     * Implementing test setup functionality
     * Browser will be launched and will navigate to the portal URL
     */
    @BeforeSuite(alwaysRun = true)
    public void setUp(ITestContext iTestContext) throws IOException {
        String url = configReader.url;
        suiteName = iTestContext.getCurrentXmlTest().getSuite().getName();
        ExtentReportManager.setPathForReportGeneration(suiteName);
        reportPath = ExtentReportManager.getPathForReportGeneration();
        ExtentReportManager.generateExtentHtmlReport(reportPath, suiteName);
        CommonUtility.logCreateTest("setUp");
//        CommonUtility.logCreateNode("setup");
//        CommonUtility.logCreateSubNode("setup");
        CommonUtility.logMessagesAndAddThemToReport("TEST EXECUTION STARTED", "info");
        Browser.launchBrowser("chrome");
        CommonUtility.logMessagesAndAddThemToReport("Chrome Browser Launched", "info");
        Browser.goToUrl(url);
        CommonUtility.logMessagesAndAddThemToReport("Typed in url for Portal", "info");

    }

    /***
     * Implementing test tear down functionality
     * Browser focus will be closed and will quit the browser
     */
    @AfterSuite(alwaysRun = true)
    public void tearDown() {
        CommonUtility.logCreateTest("tearDown");
        Browser.closeFocusedScreen();
        Browser.quitBrowser();
        CommonUtility.logMessagesAndAddThemToReport("Browser Closed", "info");
        CommonUtility.logMessagesAndAddThemToReport("TEST EXECUTION FINISHED", "info");
        CommonUtility.logMessagesAndAddThemToReport("############################################", "info");
        ExtentReportManager.flushReport();

    }


    /***
     * Method to log a test case as passed/failed and add it to google sheets. Also add screenshots to report in case of failure
     * @param results result of the test method
     */
    @AfterMethod(alwaysRun = true)
    public void afterTestMethod(ITestResult results, ITestContext iTestContext) throws IOException{
        CommonUtility.logMessagesAndAddThemToReport("Suite Name : " + suiteName, "info");
        String testMethodName = results.getMethod().getMethodName();
        CommonUtility.logMessagesAndAddThemToReport("Method Name:" + testMethodName, "info");
        String sheetName = spreadsheetUtility.getSheetNameForSuiteName(suiteName, testMethodName) + "!";

        String tcID = getTCIDFromTestMethodName(testMethodName);
        String spreadSheetID = getSpreadsheetIDFromSpreadsheetUrl(configReader.spreadSheetUrl);
        CommonUtility.logMessagesAndAddThemToReport("TC ID : " + tcID, "info");
        if(ITestResult.FAILURE == results.getStatus()){
            if(tcID.contains("TC")) {
                ExtentReportManager.onFailureAddAssertionExceptionsToReport(results);
                ExtentReportManager.logFail(" TC failed");
                String imageName = captureScreenshot();
                ExtentReportManager.addScreenshot(imageName);
                spreadsheetUtility.updateTestResultToSpreadSheet(tcID, "Failed", spreadSheetID, sheetName);
            }
        }
        else if (ITestResult.SUCCESS == results.getStatus()){
            if(tcID.contains("TC")) {
                ExtentReportManager.markATestCaseAsPassed("TC passed");
                spreadsheetUtility.updateTestResultToSpreadSheet(tcID, "Passed", spreadSheetID, sheetName);
            }
        }
        else if(ITestResult.SKIP == results.getStatus()) {
            if(tcID.contains("TC")) {
                ExtentReportManager.logWarn("TC ignored");
                spreadsheetUtility.updateTestResultToSpreadSheet(tcID, "Untested", spreadSheetID, sheetName);
            }
        }

    }


    /***
     * Method to get TC ID from te test method name
     * @param testMethodName name of @test method
     * @return TC ID
     */
    public String getTCIDFromTestMethodName(String testMethodName) {
        String[] tcID = new String[10];
        if(testMethodName.contains("_")) {
            tcID = testMethodName.split("_");
            if(!tcID[0].contains("TC")) {
                CommonUtility.logMessagesAndAddThemToReport("Test method name does not contain TC ID", "info");
                tcID[0] = " ";
            }
        } else {
            CommonUtility.logMessagesAndAddThemToReport("Test method name does not contain TC ID", "info");
            tcID[0] = " ";
        }
        return tcID[0];

    }

    public String getSpreadsheetIDFromSpreadsheetUrl(String spreadsheetUrl) {
        Pattern p = Pattern.compile("/spreadsheets/d/([a-zA-Z0-9-_]+)");
        Matcher m = p.matcher(spreadsheetUrl);
        String spreadsheetID = "";
        if(m.find()) {
            spreadsheetID = m.group(1);
            CommonUtility.logMessagesAndAddThemToReport("spreadsheet ID : " + spreadsheetID, "info");
        }
        return spreadsheetID;
    }

    /***
     * Method to close error toast message in case of failure
     * @param results result of the test method
     */
    @AfterMethod(alwaysRun = true)
    public void closeErrorToastMessage(ITestResult results) {
        if(ITestResult.FAILURE == results.getStatus()) {
            CommonUtility.logCreateNode("closeErrorToastMessageOnFailure");
            BaseActionAccess baseActionAccess = new BaseActionAccess();
            baseActionAccess.verifyPresenceOfErrorToastMessage();
            baseActionAccess.clickOnErrorToastMessage();
            Assert.assertTrue(baseActionAccess.verifyAbsenceOfErrorToastMessage());
        }
    }

    /***
     * Method to close second tab if a test case fails during import study
     * @param results result of the test method
     */
    @AfterMethod(dependsOnGroups = "importStudy")
    public void closeSecondTabInCaseOfFailure(ITestResult results) {
        if(ITestResult.FAILURE == results.getStatus()) {
            CommonUtility.logCreateNode("closeSecondTabInCaseOfFailure");
            BaseActionAccess baseActionAccess = new BaseActionAccess();
            baseActionAccess.closeSecondTab();
        }
    }

    /***
     * Method for a user to log in after he/she was logged out accidentally
     * @param iTestResult result of the test method
     */
    @AfterMethod(alwaysRun = true)
    public void userLogsInAgainAfterGettingLoggedOutAccidentally(ITestResult iTestResult) {
        if(ITestResult.FAILURE == iTestResult.getStatus()) {
            CommonUtility.logCreateNode("userLogsInAgainAfterGettingLoggedOutAccidentally");
            BaseActionAccess baseActionAccess = new BaseActionAccess();
            LoginPage loginPage = PagesFactory.getLoginPage();
            if(loginPage.verifyLoginPageAppearance()) {
                if(CommonUtility.checkIfLoggedInUserDetailsFileExists()) {
                    CommonUtility.logMessagesAndAddThemToReport("User logged out", "warn");
                    String lastLoggedInUserEmail = CommonUtility.readLoggedInUserDetailsFile("email");
                    String lastLoggedInUserPassword = CommonUtility.readLoggedInUserDetailsFile("password");
                    baseActionAccess.actionAccessScriptForLogin(lastLoggedInUserEmail, lastLoggedInUserPassword);
                    CommonUtility.logMessagesAndAddThemToReport("User logged back in", "info");
                }
                else {
                    CommonUtility.logMessagesAndAddThemToReport("Logged in user details file does not exist", "info");
                }

            }
            else {
                CommonUtility.logMessagesAndAddThemToReport("User was not logged out", "info");
            }
        }

    }

}
