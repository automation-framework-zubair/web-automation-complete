package testcases.CharacterConstraints;

import dataprovider.DataProviderClass;
import helper.CommonUtility;
import helper.Logger;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import pages.PagesFactory;
import pages.ui.BaseClass;
import pages.ui.PageClasses.CreateEditPages.AmplifierCreatePage;
import pages.ui.PageClasses.CreateEditPages.AmplifierEditPage;
import pages.ui.PageClasses.DashboardPage;
import pages.ui.PageClasses.ListPages.AmplifierPage;
import pages.ui.PageClasses.LoginPage;
import pages.ui.PageClasses.MenuSidebarPage;
import testcases.ActionAccess.BaseActionAccess;
import testcases.BaseTest;

public class AmplifierModuleFields extends BaseTest {

    String userRoleValue;
    String emailValue;
    String passwordValue;
    String invalidCharacterLength = "testtesttesttesttesttesttesttesttesttesttesttesttesttesttest"; // 60 characters

    /***
     * Getting credentials for SuperAdmin
     * @param userRole User Role
     * @param email User Email
     * @param password User Password
     */
    @Test(dataProvider = "getSuperAdminCredentials", dataProviderClass = DataProviderClass.class, priority = 1)
    public void getCredentialsForSuperAdmin(String userRole, String email, String password) {
        CommonUtility.logCreateTest("getCredentialsForSuperAdmin");
        userRoleValue = userRole;
        emailValue = email;
        passwordValue = password;

    }

    LoginPage loginPage;
    DashboardPage dashboardPage;
    MenuSidebarPage menuSidebarPage;
    AmplifierPage amplifierPage;
    AmplifierCreatePage amplifierCreatePage;
    AmplifierEditPage amplifierEditPage;

    /***
     * Implementing test case to validate character constrains of create amplifier page
     */
    @Test(priority = 2)
    public void TC309_CreateAmplifierCharacterConstraintsVerification(){
        CommonUtility.logCreateTest("TC309_CreateAmplifierCharacterConstraintsVerification");

        loginPage = PagesFactory.getLoginPage();
        loginPage.verifyLoginPage();
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        dashboardPage = PagesFactory.getDashboardPage();
        amplifierPage = PagesFactory.getAmplifierPage();
        amplifierCreatePage = PagesFactory.getAmplifierCreatePage();

        loginPage.typeUserName(emailValue);
        loginPage.typePassword(passwordValue);
        CommonUtility.logMessagesAndAddThemToReport("User Credentials Given", "info");
        loginPage.clickLoginButton();
        loginPage.loginButtonAppearance();
        CommonUtility.saveLoggedInUserDetailsToAFile(emailValue, passwordValue);

        dashboardPage.verifyDashboardPageNavigation();

        menuSidebarPage.goToAmplifiersPage();

        amplifierPage.verifyAmplifierPageNavigation();
        amplifierPage.goToCreateAmplifierPage();

        amplifierCreatePage.verifyCreateAmplifierPageNavigation();
        amplifierCreatePage.typeAmplifierName(invalidCharacterLength);
        amplifierCreatePage.typeSerialNumber(invalidCharacterLength);
        amplifierCreatePage.verifyCharacterLengthCreateAmplifier("Name", 50);
        amplifierCreatePage.verifyCharacterLengthCreateAmplifier("Serial Number", 16);
        Logger.endTestCase("CreateAmplifier_CharacterConstraints_Ends");

    }

    /***
     * Implementing test case to validate character constrains of edit amplifier page
     */
    @Test(priority = 3)
    public void TC310_EditAmplifierCharacterConstraintsVerification(){
        CommonUtility.logCreateTest("TC310_EditAmplifierCharacterConstraintsVerification");
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        amplifierPage = PagesFactory.getAmplifierPage();
        amplifierEditPage = PagesFactory.getAmplifierEditPage();

        menuSidebarPage.goToAmplifiersPage();
        amplifierPage.verifyAmplifierPageNavigation();
        amplifierPage.goToEditAmplifierPage();

        amplifierEditPage.verifyEditAmplifierPageNavigation();
        amplifierEditPage.typeAmplifierName(invalidCharacterLength);
        amplifierEditPage.typeSerialNumber(invalidCharacterLength);
        amplifierEditPage.verifyCharacterLengthEditAmplifier("Name", 50);
        amplifierEditPage.verifyCharacterLengthEditAmplifier("Serial Number", 16);

        Logger.endTestCase("EditAmplifier_CharacterConstraints_Ends");

    }

    /***
     * Implementing user sign out
     */
    @Test(priority = 50)
    public void afterTest_user_SignOut() {
        CommonUtility.logCreateTest("afterTest_user_SignOut");
        BaseActionAccess Action = new BaseActionAccess();
        Action.actionAccessScriptForUserSignOut();

    }

}
