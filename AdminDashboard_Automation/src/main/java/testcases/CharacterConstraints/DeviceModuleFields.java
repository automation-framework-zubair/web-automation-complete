package testcases.CharacterConstraints;

import dataprovider.DataProviderClass;
import helper.CommonUtility;
import helper.Logger;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import pages.PagesFactory;
import pages.ui.BaseClass;
import pages.ui.PageClasses.CreateEditPages.DeviceCreatePage;
import pages.ui.PageClasses.CreateEditPages.DeviceEditPage;
import pages.ui.PageClasses.DashboardPage;
import pages.ui.PageClasses.ListPages.DevicePage;
import pages.ui.PageClasses.LoginPage;
import pages.ui.PageClasses.MenuSidebarPage;
import testcases.ActionAccess.BaseActionAccess;
import testcases.BaseTest;

public class DeviceModuleFields extends BaseTest {

    String userRoleValue;
    String emailValue;
    String passwordValue;
    String invalidCharacterLength = "testtesttesttesttesttesttesttesttesttesttesttesttesttesttest"; // 60 characters

    /***
     * Getting credentials for SuperAdmin
     * @param userRole User Role
     * @param email User Email
     * @param password User Password
     */
    @Test(dataProvider = "getSuperAdminCredentials", dataProviderClass = DataProviderClass.class, priority = 1)
    public void getCredentialsForSuperAdmin(String userRole, String email, String password) {
        CommonUtility.logCreateTest("getCredentialsForSuperAdmin");
        userRoleValue = userRole;
        emailValue = email;
        passwordValue = password;

    }

    LoginPage loginPage;
    DashboardPage dashboardPage;
    MenuSidebarPage menuSidebarPage;
    DevicePage devicePage;
    DeviceCreatePage deviceCreatePage;
    DeviceEditPage deviceEditPage;

    /***
     * Implementing test case to validate character constrains of create device page
     */
    @Test(priority = 2)
    public void TC311_CreateDeviceCharacterConstraintsVerification(){
        CommonUtility.logCreateTest("TC311_CreateDeviceCharacterConstraintsVerification");

        loginPage = PagesFactory.getLoginPage();
        loginPage.verifyLoginPage();
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        dashboardPage = PagesFactory.getDashboardPage();
        devicePage = PagesFactory.getDevicePage();
        deviceCreatePage = PagesFactory.getCreateDevicePage();

        loginPage.typeUserName(emailValue);
        loginPage.typePassword(passwordValue);
        CommonUtility.logMessagesAndAddThemToReport("User Credentials Given", "info");
        loginPage.clickLoginButton();
        loginPage.loginButtonAppearance();
        CommonUtility.saveLoggedInUserDetailsToAFile(emailValue, passwordValue);

        dashboardPage.verifyDashboardPageNavigation();

        menuSidebarPage.goToDevicesPage();

        devicePage.verifyDevicesPageNavigation();
        devicePage.goToCreateDevicePage();

        deviceCreatePage.verifyCreateDevicePageNavigation();
        deviceCreatePage.typeName(invalidCharacterLength);
        deviceCreatePage.typePartNumber(invalidCharacterLength);
        deviceCreatePage.typeConfiguration(invalidCharacterLength);
        deviceCreatePage.typeSerialNumber(invalidCharacterLength);
        deviceCreatePage.verifyCharacterLengthCreateDevice("Name", 50);
        deviceCreatePage.verifyCharacterLengthCreateDevice("Part Number", 50);
        deviceCreatePage.verifyCharacterLengthCreateDevice("Configuration", 50);
        deviceCreatePage.verifyCharacterLengthCreateDevice("Serial Number", 50);

        Logger.endTestCase("CreateDevice_CharacterConstraints_Ends");

    }

    /***
     * Implementing test case to validate character constrains of edit device page
     */
    @Test(priority = 3)
    public void TC312_EditDeviceCharacterConstraintsVerification(){

        CommonUtility.logCreateTest("TC312_EditDeviceCharacterConstraintsVerification");

        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        devicePage = PagesFactory.getDevicePage();
        deviceEditPage = PagesFactory.getEditDevicePage();

        menuSidebarPage.goToDevicesPage();

        devicePage.verifyDevicesPageNavigation();
        devicePage.goToEditDevicePage();

        deviceEditPage.verifyEditDevicePageNavigation();
        deviceEditPage.typeName(invalidCharacterLength);
        deviceEditPage.typePartNumber(invalidCharacterLength);
        deviceEditPage.typeConfiguration(invalidCharacterLength);
        deviceEditPage.typeSerialNumber(invalidCharacterLength);
        deviceEditPage.verifyCharacterLengthEditDevice("Name", 50);
        deviceEditPage.verifyCharacterLengthEditDevice("Part Number", 50);
        deviceEditPage.verifyCharacterLengthEditDevice("Configuration", 50);
        deviceEditPage.verifyCharacterLengthEditDevice("Serial Number", 50);

        Logger.endTestCase("EditDevice_CharacterConstraints_Ends");

    }

    /***
     * Implementing user sign out
     */
    @Test(priority = 50)
    public void afterTest_user_SignOut() {
        CommonUtility.logCreateTest("afterTest_user_SignOut");
        BaseActionAccess Action = new BaseActionAccess();
        Action.actionAccessScriptForUserSignOut();

    }

}
