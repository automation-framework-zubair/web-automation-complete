package testcases.CharacterConstraints;
import dataprovider.DataProviderClass;
import helper.CommonUtility;
import helper.Logger;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import pages.PagesFactory;
import pages.ui.BaseClass;
import pages.ui.PageClasses.CreateEditPages.FacilityCreatePage;
import pages.ui.PageClasses.CreateEditPages.FacilityEditPage;
import pages.ui.PageClasses.DashboardPage;
import pages.ui.PageClasses.ListPages.FacilityPage;
import pages.ui.PageClasses.LoginPage;
import pages.ui.PageClasses.MenuSidebarPage;
import testcases.ActionAccess.BaseActionAccess;
import testcases.BaseTest;

public class FacilityModuleFields extends BaseTest {

    String userRoleValue;
    String emailValue;
    String passwordValue;
    String invalidCharacterLength = "testtesttesttesttesttesttesttesttesttesttesttesttesttesttest"; // 60 characters

    /***
     * Getting credentials for SuperAdmin
     * @param userRole User Role
     * @param email User Email
     * @param password User Password
     */
    @Test(dataProvider = "getSuperAdminCredentials", dataProviderClass = DataProviderClass.class, priority = 1)
    public void getCredentialsForSuperAdmin(String userRole, String email, String password) {
        CommonUtility.logCreateTest("getCredentialsForSuperAdmin");
        userRoleValue = userRole;
        emailValue = email;
        passwordValue = password;

    }

    LoginPage loginPage;
    DashboardPage dashboardPage;
    MenuSidebarPage menuSidebarPage;
    FacilityPage facilityPage;
    FacilityCreatePage facilityCreatePage;
    FacilityEditPage facilityEditPage;

    /***
     * Implementing test case to validate character constrains of create facility page
     */
    @Test(priority = 2)
    public void TC313_CreateFacilityCharacterConstraintsVerification(){
        CommonUtility.logCreateTest("TC313_CreateFacilityCharacterConstraintsVerification");

        loginPage = PagesFactory.getLoginPage();
        loginPage.verifyLoginPage();
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        dashboardPage = PagesFactory.getDashboardPage();
        facilityPage = PagesFactory.getFacilityPage();
        facilityCreatePage = PagesFactory.getFacilityCreatePage();

        loginPage.typeUserName(emailValue);
        loginPage.typePassword(passwordValue);
        CommonUtility.logMessagesAndAddThemToReport("User Credentials Given", "info");
        loginPage.clickLoginButton();
        loginPage.loginButtonAppearance();
        CommonUtility.saveLoggedInUserDetailsToAFile(emailValue, passwordValue);

        dashboardPage.verifyDashboardPageNavigation();

        menuSidebarPage.goToFacilitiesPage();

        facilityPage.verifyFacilityPageNavigation();
        facilityPage.goToCreateFacilityPage();

        facilityCreatePage.verifyCreateFacilityPageNavigation();
        facilityCreatePage.typeFacilityName(invalidCharacterLength + invalidCharacterLength + invalidCharacterLength + invalidCharacterLength);
        facilityCreatePage.typeDomainName(invalidCharacterLength + invalidCharacterLength + invalidCharacterLength + invalidCharacterLength);
        facilityCreatePage.verifyCharacterLengthCreateFacility("Facility", 200);
        facilityCreatePage.verifyCharacterLengthCreateFacility("Domain", 200);

        Logger.endTestCase("CreateFacility_CharacterConstraints_Ends");

    }

    /***
     * Implementing test case to validate character constrains of edit facility page
     */
    @Test(priority = 3)
    public void TC314_EditFacilityCharacterConstraintsVerification(){
        CommonUtility.logCreateTest("TC314_EditFacilityCharacterConstraintsVerification");

        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        facilityPage = PagesFactory.getFacilityPage();
        facilityEditPage = PagesFactory.getFacilityEditPage();

        menuSidebarPage.goToFacilitiesPage();
        facilityPage.verifyFacilityPageNavigation();
        facilityPage.goToEditFacilityPage();

        facilityEditPage.verifyEditFacilityPageNavigation();
        facilityEditPage.typeFacilityName(invalidCharacterLength + invalidCharacterLength + invalidCharacterLength + invalidCharacterLength);
        facilityEditPage.typeDomainName(invalidCharacterLength + invalidCharacterLength + invalidCharacterLength + invalidCharacterLength);
        facilityEditPage.typeStudyNotes(invalidCharacterLength + invalidCharacterLength + invalidCharacterLength + invalidCharacterLength +
                                                        invalidCharacterLength + invalidCharacterLength + invalidCharacterLength + invalidCharacterLength +
                                                        invalidCharacterLength + invalidCharacterLength + invalidCharacterLength + invalidCharacterLength +
                                                        invalidCharacterLength + invalidCharacterLength + invalidCharacterLength + invalidCharacterLength +
                                                        invalidCharacterLength + invalidCharacterLength + invalidCharacterLength + invalidCharacterLength +
                                                        invalidCharacterLength + invalidCharacterLength + invalidCharacterLength + invalidCharacterLength +
                                                        invalidCharacterLength + invalidCharacterLength + invalidCharacterLength + invalidCharacterLength +
                                                        invalidCharacterLength + invalidCharacterLength + invalidCharacterLength + invalidCharacterLength +
                                                        invalidCharacterLength + invalidCharacterLength + invalidCharacterLength + invalidCharacterLength +
                                                        invalidCharacterLength + invalidCharacterLength + invalidCharacterLength + invalidCharacterLength );
        facilityEditPage.verifyCharacterLengthEditFacility("Facility", 200);
        facilityEditPage.verifyCharacterLengthEditFacility("Domain", 200);
        facilityEditPage.verifyCharacterLengthEditFacility("Notes", 2000);

        Logger.endTestCase("EditFacility_CharacterConstraints_Ends");

    }

    /***
     * Implementing user sign out
     */
    @Test(priority = 50)
    public void afterTest_user_SignOut() {
        CommonUtility.logCreateTest("afterTest_user_SignOut");
        BaseActionAccess Action = new BaseActionAccess();
        Action.actionAccessScriptForUserSignOut();

    }

}
