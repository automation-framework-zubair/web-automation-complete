package testcases.CharacterConstraints;
import dataprovider.DataProviderClass;
import helper.CommonUtility;
import helper.Logger;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import pages.PagesFactory;
import pages.ui.BaseClass;
import pages.ui.PageClasses.CreateEditPages.PatientCreatePage;
import pages.ui.PageClasses.CreateEditPages.PatientEditPage;
import pages.ui.PageClasses.DashboardPage;
import pages.ui.PageClasses.ListPages.PatientsPage;
import pages.ui.PageClasses.LoginPage;
import pages.ui.PageClasses.MenuSidebarPage;
import testcases.ActionAccess.BaseActionAccess;
import testcases.BaseTest;

public class PatientModuleFields extends BaseTest {

    String userRoleValue;
    String emailValue;
    String passwordValue;
    String invalidCharacterLength = "testtesttesttesttesttesttesttesttesttesttesttesttesttesttest"; // 60 characters
    String validCharacterLength = "testtesttesttesttesttesttesttesttesttesttesttestte"; //50 characters
    String validCharacterLengthForID = "testtesttesttesttesttesttestte"; //30 characters

    /***
     * Getting credentials for SuperAdmin
     * @param userRole User Role
     * @param email User Email
     * @param password User Password
     */
    @Test(dataProvider = "getSuperAdminCredentials", dataProviderClass = DataProviderClass.class, priority = 1)
    public void getCredentialsForSuperAdmin(String userRole, String email, String password) {
        CommonUtility.logCreateTest("getCredentialsForSuperAdmin");
        userRoleValue = userRole;
        emailValue = email;
        passwordValue = password;

    }

    LoginPage loginPage;
    DashboardPage dashboardPage;
    MenuSidebarPage menuSidebarPage;
    PatientsPage patientsPage;
    PatientCreatePage patientCreatePage;
    PatientEditPage patientEditPage;

    /***
     * Implementing test case to validate character constrains of create patient page
     */
    @Test(priority = 2)
    public void TC315_CreatePatientCharacterConstraintsVerification(){
        CommonUtility.logCreateTest("TC315_CreatePatientCharacterConstraintsVerification");

        loginPage = PagesFactory.getLoginPage();
        loginPage.verifyLoginPage();
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        dashboardPage = PagesFactory.getDashboardPage();
        patientsPage = PagesFactory.getPatientsPage();
        patientCreatePage = PagesFactory.getPatientCreatePage();


        loginPage.typeUserName(emailValue);
        loginPage.typePassword(passwordValue);
        CommonUtility.logMessagesAndAddThemToReport("User Credentials Given", "info");
        loginPage.clickLoginButton();
        loginPage.loginButtonAppearance();
        CommonUtility.saveLoggedInUserDetailsToAFile(emailValue, passwordValue);

        dashboardPage.verifyDashboardPageNavigation();

        menuSidebarPage.goToPatientsPage();

        patientsPage.verifyPatientPageNavigation();
        patientsPage.goToCreatePatientPage();

        patientCreatePage.verifyCreatePatientPageNavigation();
        patientCreatePage.typeFirstName(invalidCharacterLength);
        patientCreatePage.typeMiddleName(invalidCharacterLength);
        patientCreatePage.typeLastName(invalidCharacterLength);
        patientCreatePage.typePatientID(invalidCharacterLength);
        patientCreatePage.clickSaveButton();
        patientCreatePage.verifyPresenceOfClientSideMessage("A first name is required");
        patientCreatePage.verifyPresenceOfClientSideMessage("If a middle name is provided, it must be 50 characters or less");
        patientCreatePage.verifyPresenceOfClientSideMessage("A last name is required");
        patientCreatePage.verifyPresenceOfClientSideMessage("If a patient ID is provided, it must be 30 characters or less");
        patientCreatePage.typeFirstName(validCharacterLength);
        patientCreatePage.typeMiddleName(validCharacterLength);
        patientCreatePage.typeLastName(validCharacterLength);
        patientCreatePage.typePatientID(validCharacterLengthForID);
        patientCreatePage.clickSaveButton();
        patientCreatePage.verifyAbsenceOfClientSideMessage("A first name is required");
        patientCreatePage.verifyAbsenceOfClientSideMessage("If a middle name is provided, it must be 50 characters or less");
        patientCreatePage.verifyAbsenceOfClientSideMessage("A last name is required");
        patientCreatePage.verifyAbsenceOfClientSideMessage("If a patient ID is provided, it must be 30 characters or less");

        Logger.endTestCase("CreatePatient_CharacterConstraints_Ends");


    }


    /***
     * Implementing test case to validate character constrains of edit patient page
     */
    @Test(priority = 3)
    public void TC316_EditPatientCharacterConstraintsVerification(){
        CommonUtility.logCreateTest("TC316_EditPatientCharacterConstraintsVerification");

        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        patientsPage = PagesFactory.getPatientsPage();
        patientEditPage = PagesFactory.getPatientEditPage();

        menuSidebarPage.goToPatientsPage();

        patientsPage.verifyPatientPageNavigation();
        patientsPage.goToEditPatientPage();

        patientEditPage.verifyEditPatientPageNavigation();
        patientEditPage.typeFirstName(invalidCharacterLength);
        patientEditPage.typeMiddleName(invalidCharacterLength);
        patientEditPage.typeLastName(invalidCharacterLength);
        patientEditPage.typePatientID(invalidCharacterLength);
        patientEditPage.clickSaveButton();
        patientEditPage.verifyPresenceOfClientSideMessage("A first name is required");
        patientEditPage.verifyPresenceOfClientSideMessage("If a middle name is provided, it must be 50 characters or less");
        patientEditPage.verifyPresenceOfClientSideMessage("A last name is required");
        patientEditPage.verifyPresenceOfClientSideMessage("If a patient ID is provided, it must be 30 characters or less");
        patientEditPage.typeFirstName(validCharacterLength);
        patientEditPage.typeMiddleName(validCharacterLength);
        patientEditPage.typeLastName(validCharacterLength);
        patientEditPage.typePatientID(validCharacterLengthForID);
        patientEditPage.clickSaveButton();
        patientEditPage.verifyAbsenceOfClientSideMessage("A first name is required");
        patientEditPage.verifyAbsenceOfClientSideMessage("If a middle name is provided, it must be 50 characters or less");
        patientEditPage.verifyAbsenceOfClientSideMessage("A last name is required");
        patientEditPage.verifyAbsenceOfClientSideMessage("If a patient ID is provided, it must be 30 characters or less");

        Logger.endTestCase("EditPatient_CharacterConstraints_Ends");

    }

    /***
     * Implementing user sign out
     */
    @Test(priority = 50)
    public void afterTest_user_SignOut() {
        CommonUtility.logCreateTest("afterTest_user_SignOut");
        BaseActionAccess Action = new BaseActionAccess();
        Action.actionAccessScriptForUserSignOut();

    }

}
