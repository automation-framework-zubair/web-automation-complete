package testcases.CharacterConstraints;

import dataprovider.DataProviderClass;
import helper.CommonUtility;
import helper.Logger;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import pages.PagesFactory;
import pages.ui.BaseClass;
import pages.ui.PageClasses.CreateEditPages.UserCreatePage;
import pages.ui.PageClasses.CreateEditPages.UserEditPage;
import pages.ui.PageClasses.DashboardPage;
import pages.ui.PageClasses.ListPages.UserPage;
import pages.ui.PageClasses.LoginPage;
import pages.ui.PageClasses.MenuSidebarPage;
import testcases.ActionAccess.BaseActionAccess;
import testcases.BaseTest;

public class UserModuleFields extends BaseTest {

    String userRoleValue;
    String emailValue;
    String passwordValue;
    String invalidCharacterLength = "testtesttesttesttesttesttesttesttesttesttesttesttesttesttest"; // 60 characters
    String invalidPassword = "asdthasdqwemnasdoqweuasdnasqweuasdasdkijqweasdohqgwelqkjwehbqljwehquweyasdoiqwyeqywgekipjasdbnq;wuqwiqwoiejuqweiqweqweja"; // 120 characters
    String validPassword = "asdhgqpoweipoajhsdoquygwepasujdhiqweasdpiuqhweqpwbgepasdiugqhwpequwheijaspodhuqwoiyegqwpeojiqwehjqw"; //99 characters
    String invalidPassword2 = "Eno12"; //5 characters
    String unMatchedPassword1 = "ENosisi1234sd";
    String unMatchedPassword2 = "Eno234234sudn";

    /***
     * Getting credentials for SuperAdmin
     * @param userRole User Role
     * @param email User Email
     * @param password User Password
     */
    @Test(dataProvider = "getSuperAdminCredentials", dataProviderClass = DataProviderClass.class, priority = 1)
    public void getCredentialsForSuperAdmin(String userRole, String email, String password) {
        CommonUtility.logCreateTest("getCredentialsForSuperAdmin");
        userRoleValue = userRole;
        emailValue = email;
        passwordValue = password;

    }

    LoginPage loginPage;
    DashboardPage dashboardPage;
    MenuSidebarPage menuSidebarPage;
    UserPage userPage;
    UserCreatePage userCreatePage;
    UserEditPage userEditPage;

    /***
     * Implementing test case to validate character constrains of create user page
     */
    @Test(priority = 2)
    public void TC317_CreateUserCharacterConstraintsVerification(){
        CommonUtility.logCreateTest("TC317_CreateUserCharacterConstraintsVerification");

        loginPage = PagesFactory.getLoginPage();
        loginPage.verifyLoginPage();
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        dashboardPage = PagesFactory.getDashboardPage();
        userPage = PagesFactory.getUserPage();
        userCreatePage = PagesFactory.getUserCreatePage();

        loginPage.typeUserName(emailValue);
        loginPage.typePassword(passwordValue);
        CommonUtility.logMessagesAndAddThemToReport("User Credentials Given", "info");
        loginPage.clickLoginButton();
        loginPage.loginButtonAppearance();
        CommonUtility.saveLoggedInUserDetailsToAFile(emailValue, passwordValue);

        dashboardPage.verifyDashboardPageNavigation();

        menuSidebarPage.goToUsersPage();

        userPage.verifyUserPageNavigation();
        userPage.goToCreateUserPage();

        userCreatePage.verifyCreateUserPageNavigation();
        userCreatePage.typeTitle(invalidCharacterLength + invalidCharacterLength);
        userCreatePage.typeFirstName(invalidCharacterLength + invalidCharacterLength + invalidCharacterLength + invalidCharacterLength);
        userCreatePage.typeMiddleName(invalidCharacterLength + invalidCharacterLength + invalidCharacterLength + invalidCharacterLength);
        userCreatePage.typeLastName(invalidCharacterLength + invalidCharacterLength + invalidCharacterLength + invalidCharacterLength);
        userCreatePage.typeSuffix(invalidCharacterLength + invalidCharacterLength);
        userCreatePage.typeEmail(invalidCharacterLength + invalidCharacterLength + invalidCharacterLength + invalidCharacterLength + invalidCharacterLength);
        userCreatePage.typePassword(invalidPassword);
        userCreatePage.typeConfirmPassword(invalidCharacterLength);
        userCreatePage.verifyCharacterLengthCreateUser("Title", 100);
        userCreatePage.verifyCharacterLengthCreateUser("First Name", 200);
        userCreatePage.verifyCharacterLengthCreateUser("Middle Name", 200);
        userCreatePage.verifyCharacterLengthCreateUser("Last Name", 200);
        userCreatePage.verifyCharacterLengthCreateUser("Suffix", 100);
        userCreatePage.verifyCharacterLengthCreateUser("Email", 256);

        Logger.endTestCase("CreateUser_CharacterConstraints_Ends");

    }

    /***
     * Test case to check if proper error message is displayed if the text of Password and Confirm password fields do not match
     */
    @Test(priority = 5)
    public void TC343_checkProperErrorMessageDisplayed_IfPasswordAndConfirmPasswordFieldsDoNotMatch() {
        CommonUtility.logCreateTest("TC343_checkProperErrorMessageDisplayed_IfPasswordAndConfirmPasswordFieldsDoNotMatch");
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        userPage = PagesFactory.getUserPage();
        userCreatePage = PagesFactory.getUserCreatePage();

        menuSidebarPage.goToUsersPage();
        userPage.verifyUserPageNavigation();
        userPage.goToCreateUserPage();
        userCreatePage.verifyCreateUserPageNavigation();

        userCreatePage.typeFirstName("First");
        userCreatePage.typeLastName("Last");
        userCreatePage.typeEmail("testesdemail@gmail.com");
        userCreatePage.typePassword(unMatchedPassword1);
        userCreatePage.typeConfirmPassword(unMatchedPassword2);
        userCreatePage.selectValueFromDropDown("SuperAdmin");
        userCreatePage.verifyPresenceOfClientSideMessage("Does not match Password");

    }


    /***
     * Test case to check if proper error message is displayed if the password field contains less than 8 characters
     */
    @Test(priority = 6)
    public void TC344_checkProperErrorMessageDisplayedForEnteringPasswordLessThan8Chars() {
        CommonUtility.logCreateTest("TC347_checkProperErrorMessageDisplayedForEnteringPasswordLessThan8Chars");
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        userPage = PagesFactory.getUserPage();
        userCreatePage = PagesFactory.getUserCreatePage();

        menuSidebarPage.goToUsersPage();
        userPage.verifyUserPageNavigation();
        userPage.goToCreateUserPage();
        userCreatePage.verifyCreateUserPageNavigation();

        userCreatePage.typeFirstName("First");
        userCreatePage.typeLastName("Last");
        userCreatePage.typeEmail("testesdemail@gmail.com");
        userCreatePage.typePassword(invalidPassword2);
        userCreatePage.typeConfirmPassword(invalidPassword2);
        userCreatePage.verifyPresenceOfClientSideMessage("Must be 8 to 100 characters long");

    }

    /***
     * Test case to check save button is disabled if the password field contains less than 8 characters
     */
    @Test(priority = 7)
    public void TC345_checkSaveButtonIsDisabledForEnteringPasswordLessThan8Chars() {
        CommonUtility.logCreateTest("TC348_checkSaveButtonIsDisabledForEnteringPasswordLessThan8Chars");
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        userPage = PagesFactory.getUserPage();
        userCreatePage = PagesFactory.getUserCreatePage();

        menuSidebarPage.goToUsersPage();
        userPage.verifyUserPageNavigation();
        userPage.goToCreateUserPage();
        userCreatePage.verifyCreateUserPageNavigation();

        userCreatePage.typeFirstName("First");
        userCreatePage.typeLastName("Last");
        userCreatePage.typeEmail("testesdemail@gmail.com");
        userCreatePage.typePassword(invalidPassword2);
        userCreatePage.typeConfirmPassword(invalidPassword2);
        userCreatePage.verifyPresenceOfClientSideMessage("Must be 8 to 100 characters long");
        userCreatePage.selectValueFromDropDown("SuperAdmin");
        userCreatePage.checkSaveButtonIsDisabled();
        userCreatePage.saveButtonClick();
        userCreatePage.checkToastMessageDidNotAppear("User created!");

    }


    /***
     * Test case to check if proper error message is displayed if the password field contains more than 100 characters
     */
    @Test(priority = 8)
    public void TC346_checkProperErrorMessageDisplayedForEnteringPasswordGreaterThan100Chars() {
        CommonUtility.logCreateTest("TC349_checkProperErrorMessageDisplayedForEnteringPasswordGreaterThan100Chars");
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        userPage = PagesFactory.getUserPage();
        userCreatePage = PagesFactory.getUserCreatePage();

        menuSidebarPage.goToUsersPage();
        userPage.verifyUserPageNavigation();
        userPage.goToCreateUserPage();
        userCreatePage.verifyCreateUserPageNavigation();

        userCreatePage.typeFirstName("First");
        userCreatePage.typeLastName("Last");
        userCreatePage.typeEmail("testesdemail@gmail.com");
        userCreatePage.typePassword(invalidPassword);
        userCreatePage.typeConfirmPassword(invalidPassword);
        userCreatePage.verifyPresenceOfClientSideMessage("Must be 8 to 100 characters long");

    }

    /***
     * Test case to check save button is disabled if the password field contains more than 100 characters
     */
    @Test(priority = 10)
    public void TC347_checkSaveButtonIsDisabledForEnteringPasswordGreaterThan100Chars() {
        CommonUtility.logCreateTest("TC350_checkSaveButtonIsDisabledForEnteringPasswordGreaterThan100Chars");
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        userPage = PagesFactory.getUserPage();
        userCreatePage = PagesFactory.getUserCreatePage();

        menuSidebarPage.goToUsersPage();
        userPage.verifyUserPageNavigation();
        userPage.goToCreateUserPage();
        userCreatePage.verifyCreateUserPageNavigation();

        userCreatePage.typeFirstName("First");
        userCreatePage.typeLastName("Last");
        userCreatePage.typeEmail("testesdemail@gmail.com");
        userCreatePage.typePassword(invalidPassword);
        userCreatePage.typeConfirmPassword(invalidPassword);
        userCreatePage.verifyPresenceOfClientSideMessage("Must be 8 to 100 characters long");
        userCreatePage.selectValueFromDropDown("SuperAdmin");
        userCreatePage.checkSaveButtonIsDisabled();
        userCreatePage.saveButtonClick();
        userCreatePage.checkToastMessageDidNotAppear("User created!");

    }

    /***
     * Implementing test case to validate character constrains of edit user page
     */
    @Test(priority = 15)
    public void TC318_EditUserCharacterConstraintsVerification(){
        CommonUtility.logCreateTest("TC318_EditUserCharacterConstraintsVerification");

        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        userPage = PagesFactory.getUserPage();
        userEditPage = PagesFactory.getUserEditPage();

        menuSidebarPage.goToUsersPage();

        userPage.verifyUserPageNavigation();
        userPage.goToEditUserPage();

        userEditPage.verifyEditUserPageNavigation();
        userEditPage.typeTitle(invalidCharacterLength + invalidCharacterLength);
        userEditPage.typeFirstName(invalidCharacterLength + invalidCharacterLength + invalidCharacterLength + invalidCharacterLength);
        userEditPage.typeMiddleName(invalidCharacterLength + invalidCharacterLength + invalidCharacterLength + invalidCharacterLength);
        userEditPage.typeLastName(invalidCharacterLength + invalidCharacterLength + invalidCharacterLength + invalidCharacterLength);
        userEditPage.typeSuffix(invalidCharacterLength + invalidCharacterLength);
        userEditPage.typeEmail(invalidCharacterLength + invalidCharacterLength + invalidCharacterLength + invalidCharacterLength + invalidCharacterLength);
        userEditPage.verifyCharacterLengthEditUser("Title", 100);
        userEditPage.verifyCharacterLengthEditUser("First Name", 200);
        userEditPage.verifyCharacterLengthEditUser("Middle Name", 200);
        userEditPage.verifyCharacterLengthEditUser("Last Name", 200);
        userEditPage.verifyCharacterLengthEditUser("Suffix", 100);
        userEditPage.verifyCharacterLengthEditUser("Email", 256);

        Logger.endTestCase("EditUser_CharacterConstraints_Ends");

    }

    /***
     * Implementing user sign out
     */
    @Test(priority = 50)
    public void afterTest_user_SignOut() {
        CommonUtility.logCreateTest("afterTest_user_SignOut");
        BaseActionAccess Action = new BaseActionAccess();
        Action.actionAccessScriptForUserSignOut();

    }

}
