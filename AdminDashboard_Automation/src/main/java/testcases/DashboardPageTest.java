package testcases;

import dataprovider.DataProviderClass;
import helper.CommonUtility;
import helper.Logger;
import org.testng.Reporter;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import pages.PagesFactory;
import pages.ui.PageClasses.CreateEditPages.UserCreatePage;
import pages.ui.PageClasses.DashboardPage;
import pages.ui.PageClasses.HeaderPanel;
import pages.ui.PageClasses.ListPages.UserPage;
import pages.ui.PageClasses.LoginPage;
import pages.ui.PageClasses.MenuSidebarPage;
import pages.ui.PageClasses.ProfilePage;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DashboardPageTest extends BaseTest {
    LoginPage loginPage;
    DashboardPage dashboardPage;
    MenuSidebarPage menuSidebarPage;
    HeaderPanel headerPanel;
    ProfilePage profilePage;
    UserPage userPage;
    UserCreatePage userCreatePage;

    String superAdminRole;
    String superAdminEmail;
    String superAdminPassword;

    Format f = new SimpleDateFormat("MMddyyyy_hhmmss");
    String dateTime = f.format(new Date());

    String newPassword = "Enosis123New";
    String password = "Enosis123";
    String email;

    DataDeletion dataDeletion = new DataDeletion();

    /***
     * Getting credentials for SuperAdmin
     * @param userRole role of the user
     * @param email Email of the user
     * @param password Password of the user
     */
    @Test(dataProvider = "getSuperAdminCredentials", dataProviderClass = DataProviderClass.class, priority = 1)
    public void getCredentialsForSuperAdmin(String userRole, String email, String password) {
        CommonUtility.logCreateTest("getCredentialsForSuperAdmin");
        superAdminRole = userRole;
        superAdminEmail = email;
        superAdminPassword = password;

    }

    /***
     * Test case to check Dashboard is displayed after logging in
     */
    @Test(priority = 2)
    public void TC84_checkDashboardDisplayedAfterLogIn() {
      CommonUtility.logCreateTest("TC84_checkDashboardDisplayedAfterLogIn");

      loginPage = PagesFactory.getLoginPage();
      dashboardPage = PagesFactory.getDashboardPage();

      loginPage.verifyLoginPage();
      loginPage.typeUserName(superAdminEmail);
      loginPage.typePassword(superAdminPassword);
      CommonUtility.logMessagesAndAddThemToReport("User Credentials Given", "info");
      loginPage.clickLoginButton();
      CommonUtility.saveLoggedInUserDetailsToAFile(superAdminEmail, superAdminPassword);

      dashboardPage.verifyDashboardPageNavigation();

      Logger.endTestCase("checkDashboardDisplayedAfterLogIn");

    }

    /***
     * Test case to check In Progress studies are displayed after clicking on the In Progress card
     */
    @Test(priority = 3)
    public void TC85_checkInProgressStudiesDisplayedAfterClickingInProgressCard() {
        CommonUtility.logCreateTest("TC85_checkInProgressStudiesDisplayedAfterClickingInProgressCard");

        dashboardPage = PagesFactory.getDashboardPage();
        menuSidebarPage = PagesFactory.getMenuSideBarPage();

        dashboardPage.verifyDashboardPageNavigation();
        dashboardPage.verifyClickingInProgressCard();

        menuSidebarPage.goToDashboardPage();

        Logger.endTestCase("checkInProgressStudiesDisplayedAfterClickingInProgressCard");

    }

    /***
     * Test case to check Pending review studies are displayed after clicking on the Studies For Review card
     */
    @Test(priority = 4)
    public void TC86_checkPendingReviewStudiesDisplayedAfterClickingStudiesForReviewCard() {
        CommonUtility.logCreateTest("TC86_checkPendingReviewStudiesDisplayedAfterClickingStudiesForReviewCard");

        dashboardPage = PagesFactory.getDashboardPage();
        menuSidebarPage = PagesFactory.getMenuSideBarPage();

        dashboardPage.verifyDashboardPageNavigation();
        dashboardPage.verifyClickingStudiesForReviewCard();

        menuSidebarPage.goToDashboardPage();

        Logger.endTestCase("checkPendingReviewStudiesDisplayedAfterClickingStudiesForReviewCard");

    }

    /***
     * Test case to check all studies are displayed after clicking on the Total Studies card
     */
    @Test(priority = 5)
    public void TC87_checkAllStudiesDisplayedAfterClickingTotalStudiesCard() {
        CommonUtility.logCreateTest("TC87_checkAllStudiesDisplayedAfterClickingTotalStudiesCard");

        dashboardPage = PagesFactory.getDashboardPage();
        menuSidebarPage = PagesFactory.getMenuSideBarPage();

        dashboardPage.verifyDashboardPageNavigation();
        dashboardPage.verifyClickingTotalStudiesCard();

        menuSidebarPage.goToDashboardPage();

        Logger.endTestCase("checkAllStudiesDisplayedAfterClickingTotalStudiesCard");

    }

    /***
     * Test case to check if profile is clickable
     */
    @Test(priority = 6)
    public void TC88_checkProfileIsClickable() {
        CommonUtility.logCreateTest("TC88_checkProfileIsClickable");

        headerPanel = PagesFactory.getheaderPanel();
        profilePage = PagesFactory.getProfilePage();
        menuSidebarPage = PagesFactory.getMenuSideBarPage();

        headerPanel.yourProfileButtonClick();
        profilePage.verifyProfilePageNavigation();

        Logger.endTestCase("checkProfileIsClickable");

    }

    /***
     * Test case to check that Profile page contains user's role
     */
    @Test(priority = 7)
    public void TC89_checkProfilePageContainsUserRole() {
        CommonUtility.logCreateTest("TC89_checkProfilePageContainsUserRole");

        profilePage = PagesFactory.getProfilePage();
        profilePage.verifyUserRole(superAdminRole);

        Logger.endTestCase("checkProfilePageContainsUserRole");

    }

    /***
     * Creating a user that can be used to test the forgot password functionality
     */
    @Test(priority = 8)
    public void createUser() {
        CommonUtility.logCreateTest("createUser");

        dashboardPage = PagesFactory.getDashboardPage();
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        userPage = PagesFactory.getUserPage();
        userCreatePage = PagesFactory.getUserCreatePage();
        headerPanel = PagesFactory.getheaderPanel();

        dashboardPage.verifyDashboardPageNavigation();

        menuSidebarPage.goToUsersPage();

        userPage.verifyUserPageNavigation();
        userPage.goToCreateUserPage();

        email = "test" + dateTime + "@gmail.com";
        userCreatePage.verifyCreateUserPageNavigation();
        userCreatePage.createUser("yes", "", dateTime, "", dateTime, "", email, password, password, "SuperAdmin", "User created!");

        headerPanel.signOut();
        loginPage.verifyLoginPage();

    }

    /***
     * Navigating to profile page of the newly created user
     */
    @Test(priority = 9)
    public void navigateToProfilePageForNewlyCreatedUser() {
        CommonUtility.logCreateTest("TC90_navigateToProfilePageForNewlyCreatedUser");

        loginPage = PagesFactory.getLoginPage();
        dashboardPage = PagesFactory.getDashboardPage();
        headerPanel = PagesFactory.getheaderPanel();
        profilePage = PagesFactory.getProfilePage();

        loginPage.typeUserName(email);
        loginPage.typePassword(password);
        CommonUtility.logMessagesAndAddThemToReport("User Credentials Given", "info");
        loginPage.clickLoginButton();

        dashboardPage.verifyDashboardPageNavigation();

        headerPanel.yourProfileButtonClick();
        profilePage.verifyProfilePageNavigation();

        Logger.endTestCase("logInWithNewlyCreatedUser");

    }

    /***
     * Test case to check show password buttons are present in Profile page
     */
    @Test(priority = 10)
    public void checkShowPasswordButtonsArePresentInProfilePage() {
        CommonUtility.logCreateTest("TC91_checkShowPasswordButtonsArePresentInProfilePage");

        profilePage = PagesFactory.getProfilePage();
        profilePage.verifyShowPasswordButtonsArePresent();

        Logger.endTestCase("checkShowPasswordButtonsArePresentInProfilePage");

    }

    /***
     * Test case to check password strength validation is present in new password field
     */
    @Test(priority = 11)
    public void TC92_checkPasswordStrengthValidationPresentInNewPasswordField() {
        CommonUtility.logCreateTest("TC92_checkPasswordStrengthValidationPresentInNewPasswordField");

        profilePage = PagesFactory.getProfilePage();
        profilePage.typeNewPassword("aaaaaaa");
        profilePage.typeConfirmPassword("tochange");
        profilePage.verifyErrorMessageForRepeatedText();
        profilePage.typeNewPassword("asdfghj");
        profilePage.typeConfirmPassword("tochange");
        profilePage.verifyErrorMessageForCommonPassword();

        Logger.endTestCase("checkPasswordStrengthValidationPresentInNewPasswordField");

    }

    /***
     * Test Case to check password matching functionality is present for new and confirm password fields
     */
    @Test(priority = 12)
    public void TC93_checkMatchingFunctionalityPresentForNewAndConfirmPasswordFields() {
        CommonUtility.logCreateTest("TC93_checkMatchingFunctionalityPresentForNewAndConfirmPasswordFields");

        profilePage = PagesFactory.getProfilePage();
        profilePage.typeNewPassword(password);
        profilePage.typeConfirmPassword("Different");
        profilePage.typeCurrentPassword("tochange");
        profilePage.verifyErrorMessageForMismatchedPasswordOfNewAndConfirmPasswordFields();
        profilePage.typeNewPassword(password);
        profilePage.typeConfirmPassword(password);
        profilePage.typeCurrentPassword("tochange");
        profilePage.verifyErrorMessageDoesNotAppearForMatchedPasswords();

        Logger.endTestCase("checkMatchingFunctionalityPresentForNewAndConfirmPasswordFields");

    }

    /***
     * Test case to check error message appears for incorrect current password
     */
    @Test(priority = 13)
    public void TC94_checkErrorMessageAppearsForIncorrectCurrentPassword() {
        CommonUtility.logCreateTest("TC94_checkErrorMessageAppearsForIncorrectCurrentPassword");

        profilePage = PagesFactory.getProfilePage();
        profilePage.typeNewPassword(newPassword);
        profilePage.typeConfirmPassword(newPassword);
        profilePage.typeCurrentPassword("NotRightPassword");
        profilePage.clickOnChangePasswordButton();
        profilePage.verifyErrorMessageForIncorrectCurrentPassword();

        Logger.endTestCase("checkErrorMessageAppearsForIncorrectCurrentPassword");

    }

    /***
     * Test case to check that user's password can be changed
     */
    @Test(priority = 14)
    public void TC95_checkUsersCanChangeTheirOwnPassword() {
        CommonUtility.logCreateTest("TC95_checkUsersCanChangeTheirOwnPassword");

        profilePage = PagesFactory.getProfilePage();
        headerPanel = PagesFactory.getheaderPanel();
        loginPage = PagesFactory.getLoginPage();

        profilePage.typeNewPassword(newPassword);
        profilePage.typeConfirmPassword(newPassword);
        profilePage.typeCurrentPassword(password);
        profilePage.clickOnChangePasswordButton();
        profilePage.verifyToastMessageForSuccessfulPasswordChange();

        headerPanel.signOut();
        loginPage.verifyLoginPage();

        Logger.endTestCase("checkUsersCanChangeTheirOwnPassword");

    }

    /***
     * Test case to check user cannot log in with the old password
     */
    @Test(priority = 15)
    public void TC96_checkUserCannotLogInWithOldPassword() {
        CommonUtility.logCreateTest("TC96_checkUserCannotLogInWithOldPassword");

        loginPage = PagesFactory.getLoginPage();

        loginPage.typeUserName(email);
        loginPage.typePassword(password);
        loginPage.clickLoginButton();
        loginPage.checkInvalidCredentialsMessage();

        Logger.endTestCase("checkUserCannotLogInWithOldPassword");

    }

    /***
     * Test case to check that the user can log in with the new password
     */
    @Test(priority = 16)
    public void TC97_checkUserCanLogInWithNewPassword() {
        CommonUtility.logCreateTest("TC97_checkUserCanLogInWithNewPassword");

        loginPage = PagesFactory.getLoginPage();
        dashboardPage = PagesFactory.getDashboardPage();

        loginPage.typeUserName(email);
        loginPage.typePassword(newPassword);
        loginPage.clickLoginButton();

        dashboardPage.verifyDashboardPageNavigation();

        Logger.endTestCase("checkUserCanLogInWithNewPassword");

    }

    /***
     * Test Case to check if sign out works properly
     */
    @Test(priority = 17)
    public void TC98_checkSignOutWorks() {
        CommonUtility.logCreateTest("TC98_checkSignOutWorks");

        headerPanel = PagesFactory.getheaderPanel();
        loginPage = PagesFactory.getLoginPage();

        headerPanel.signOut();
        loginPage.verifyLoginPage();

        Logger.endTestCase("checkSignOutWorks");

    }

    /***
     * To log in with Super Admin role
     */
    @Test(priority = 18)
    public void logInAsSuperAdmin() {
        CommonUtility.logCreateTest("TC99_logInAsSuperAdmin");

        loginPage = PagesFactory.getLoginPage();
        dashboardPage = PagesFactory.getDashboardPage();

        loginPage.typeUserName(superAdminEmail);
        loginPage.typePassword(superAdminPassword);
        loginPage.clickLoginButton();

        dashboardPage.verifyDashboardPageNavigation();

        Logger.endTestCase("logInAsSuperAdmin");

    }

    /***
     * For deleting user that have been created during test execution
     */
    @Test(priority = 100)
    public void afterTest_userTestDataDeletion() {
        CommonUtility.logCreateTest("afterTest_userTestDataDeletion");
        dataDeletion.userDeletion("yes", dateTime);

    }

}
