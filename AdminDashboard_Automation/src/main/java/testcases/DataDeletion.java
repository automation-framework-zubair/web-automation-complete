package testcases;

import pages.PagesFactory;
import pages.ui.PageClasses.CreateEditPages.*;
import pages.ui.PageClasses.DataDeletionUsingDB;
import pages.ui.PageClasses.ListPages.*;
import pages.ui.PageClasses.MenuSidebarPage;

public class DataDeletion {

    MenuSidebarPage menuSidebarPage;
    PatientsPage patientsPage;
    PatientEditPage patientEditPage;
    FacilityPage facilityPage;
    FacilityEditPage facilityEditPage;
    AmplifierPage amplifierPage;
    AmplifierEditPage amplifierEditPage;
    DevicePage devicePage;
    DeviceEditPage deviceEditPage;
    UserPage userPage;
    UserEditPage userEditPage;

    /***
     * For deleting patients that were created during test execution
     * @param editPatient If user is allowed access to Edit Patient page
     * @param nameOfPatient Name of the patient to be deleted
     */
    public void patientDeletion(String editPatient, String nameOfPatient) {
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        patientsPage = PagesFactory.getPatientsPage();
        patientEditPage = PagesFactory.getPatientEditPage();

        menuSidebarPage.goToPatientsPage();

        patientsPage.verifyPatientPageNavigation();
        patientsPage.clickOnEditIconForSelectedItem(editPatient, nameOfPatient, "Patients");

        patientEditPage.verifyEditPatientPageNavigation();

        String id = patientEditPage.getID("Patient/");
        DataDeletionUsingDB.patientDelete(id);

    }

    /***
     * For deleting amplifiers that were created during test execution
     * @param editAmplifier IF user is allowed to access Edit Amplifier page
     * @param nameOfAmplifier Name of the amplifier to be deleted
     * */
    public void amplifierDeletion(String editAmplifier, String nameOfAmplifier) {
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        amplifierPage = PagesFactory.getAmplifierPage();
        amplifierEditPage = PagesFactory.getAmplifierEditPage();

        menuSidebarPage.goToAmplifiersPage();

        amplifierPage.verifyAmplifierPageNavigation();
        amplifierPage.clickEditIconByInstanceName(editAmplifier, nameOfAmplifier, "Amplifiers");

        String id = amplifierEditPage.getID("Amplifier/");
        DataDeletionUsingDB.amplifierDelete(id);

    }

    /***
     * For deleting devices that were created during test execution
     * @param editDevice if user is allowed access to edit device page
     * @param nameOfDevice Name of the device to be deleted
     */
    public void deviceDeletion(String editDevice, String nameOfDevice) {
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        devicePage = PagesFactory.getDevicePage();
        deviceEditPage = PagesFactory.getEditDevicePage();

        menuSidebarPage.goToDevicesPage();

        devicePage.verifyDevicesPageNavigation();
        devicePage.clickEditIconByInstanceName(editDevice, nameOfDevice, "Devices");

        deviceEditPage.verifyEditDevicePageNavigation(editDevice);

        String id = deviceEditPage.getID("Device/");
        DataDeletionUsingDB.deviceDelete(id);

    }

    /***
     * For deleting Facilities that were created during test execution
     * @param editFacility if user allowed access to edit facility page
     * @param nameOfFacility Name of the facility to be deleted
     */
    public void facilityDeletion(String editFacility, String nameOfFacility) {
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        facilityPage = PagesFactory.getFacilityPage();
        facilityEditPage = PagesFactory.getFacilityEditPage();

        menuSidebarPage.goToFacilitiesPage();

        facilityPage.verifyFacilityPageNavigation();
        facilityPage.clickEditIconByInstanceName(editFacility, nameOfFacility, "Facilities");

        facilityEditPage.verifyEditFacilityPageNavigation(editFacility);

        String id = facilityEditPage.getID("Facility/");
        DataDeletionUsingDB.facilityDelete(id);

    }

    /***
     * For deleting users that were created during test execution
     * @param editUser if user is allowed access to edit user page
     * @param nameOfUser Name of the user to be deleted
     */
    public void userDeletion(String editUser, String nameOfUser) {
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        userPage = PagesFactory.getUserPage();
        userEditPage = PagesFactory.getUserEditPage();

        menuSidebarPage.goToUsersPage();

        userPage.verifyUserPageNavigation();
        userPage.clickEditIconByInstanceName(editUser, nameOfUser, "Users");

        userEditPage.verifyEditUserPageNavigation(editUser);

        String id = userEditPage.getID("User/");
        DataDeletionUsingDB.userDelete(id);

    }

}
