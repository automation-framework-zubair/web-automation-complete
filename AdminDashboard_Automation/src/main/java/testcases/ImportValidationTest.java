package testcases;

import dataprovider.DataProviderClass;
import helper.CommonUtility;
import helper.Logger;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import pages.PagesFactory;
import pages.ui.PageClasses.CreateEditPages.PatientCreatePage;
import pages.ui.PageClasses.DashboardPage;
import pages.ui.PageClasses.ImportStudyPage;
import pages.ui.PageClasses.ListPages.HelpPage;
import pages.ui.PageClasses.ListPages.PatientsPage;
import pages.ui.PageClasses.LoginPage;
import pages.ui.PageClasses.MenuSidebarPage;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ImportValidationTest extends BaseTest{

    LoginPage loginPage;
    DashboardPage dashboardPage;
    MenuSidebarPage menuSidebarPage;
    PatientsPage patientsPage;
    PatientCreatePage patientCreatePage;
    ImportStudyPage importStudyPage;
    HelpPage helpPage;

    String superAdminRole;
    String superAdminEmail;
    String superAdminPassword;

    Format f = new SimpleDateFormat("MMddyyyy_hhmm");
    String dateTime = f.format(new Date());

    DataDeletion dataDeletion = new DataDeletion();

    /***
     * Getting credentials for SuperAdmin
     * @param userRole User Role
     * @param email User Email
     * @param password User Password
     */
    @Test(dataProvider = "getSuperAdminCredentials", dataProviderClass = DataProviderClass.class, priority = 1)
    public void getCredentialsForSuperAdmin(String userRole, String email, String password) {
        CommonUtility.logCreateTest("getCredentialsForSuperAdmin");
        superAdminRole = userRole;
        superAdminEmail = email;
        superAdminPassword = password;

    }

    /***
     * Test case to implement log in to Portal as Super Admin
     */
    @Test(priority = 3)
    public void logInAsSuperAdmin() {
        CommonUtility.logCreateTest("logInAsSuperAdmin");

        loginPage = PagesFactory.getLoginPage();
        dashboardPage = PagesFactory.getDashboardPage();

        loginPage.verifyLoginPage();
        loginPage.typeUserName(superAdminEmail);
        loginPage.typePassword(superAdminPassword);
        CommonUtility.logMessagesAndAddThemToReport("User Credentials Given", "info");
        loginPage.clickLoginButton();
        CommonUtility.saveLoggedInUserDetailsToAFile(superAdminEmail, superAdminPassword);

        dashboardPage.verifyDashboardPageNavigation();

        Logger.endTestCase("logInAsSuperAdmin");

    }

    /***
     * Test case to create a patient for importing a study
     */
    @Test(priority = 4)
    public void createPatientForImportStudy() {
        CommonUtility.logCreateTest("createPatientForImportStudy");

        dashboardPage = PagesFactory.getDashboardPage();
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        patientsPage = PagesFactory.getPatientsPage();
        patientCreatePage = PagesFactory.getPatientCreatePage();

        dashboardPage.verifyDashboardPageNavigation();
        menuSidebarPage.goToPatientsPage();
        patientsPage.verifyPatientPageNavigation();
        patientsPage.goToCreatePatientPage();

        patientCreatePage.verifyCreatePatientPageNavigation();
        patientCreatePage.createPatient("yes", dateTime, dateTime, dateTime + "LN",
                                        "Male", "331472787", "", "Patient created!");

        Logger.endTestCase("createPatientForImportStudy");

    }

    /***
     * Test case to check if import study button is present in the patient page
     */
    @Test(priority = 5)
    public void TC01_checkImportStudyButtonIsPresent() {
        CommonUtility.logCreateTest("TC01_checkImportStudyButtonIsPresent");

        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        patientsPage = PagesFactory.getPatientsPage();

        menuSidebarPage.goToPatientsPage();
        patientsPage.verifyPatientPageNavigation();
        patientsPage.verifyImportButtonIsPresent();

        Logger.endTestCase("checkImportStudyButtonIsPresent");

    }

    /***
     * Test case to check if clicking on the import button opens a separate tab
     */
    @Test(priority = 6)
    public void TC02_checkClickingOnImportIconOpensSeparateTab() {
        CommonUtility.logCreateTest("TC02_checkClickingOnImportIconOpensSeparateTab");

        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        patientsPage = PagesFactory.getPatientsPage();
        importStudyPage = PagesFactory.getImportStudyPage();

        menuSidebarPage.goToPatientsPage();
        patientsPage.verifyPatientPageNavigation();
        int openedTabs = patientsPage.getNumberOfOpenedTabs();
        patientsPage.verifyClickOnImportIcon("yes");
        importStudyPage.verifyImportStudyPageNavigation();
        int openedTabsAfterOpeningImportStudy = patientsPage.getNumberOfOpenedTabs();
        Assert.assertEquals(openedTabsAfterOpeningImportStudy, openedTabs + 1, "Import Page Not Opened in a separate tab");
        importStudyPage.closeImportStudyPage();
        Logger.endTestCase("checkClickingOnImportIconOpensSeparateTab");

    }

    public void navigationToImportStudyPageAndVerification(String patientName) {
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        patientsPage = PagesFactory.getPatientsPage();
        importStudyPage = PagesFactory.getImportStudyPage();

        menuSidebarPage.goToPatientsPage();
        patientsPage.verifyPatientPageNavigation();
        patientsPage.clickOnImportIconByPatientName("yes", patientName);
        importStudyPage.verifyImportStudyPageNavigation();

    }

    /***
     * Test case to check that a button exists that lets the user select a folder for import
     */
    @Test(priority = 7)
    public void TC03_checkButtonExistsForSelectingFolderForImport() {
        CommonUtility.logCreateTest("TC03_checkButtonExistsForSelectingFolderForImport");

        importStudyPage = PagesFactory.getImportStudyPage();

        navigationToImportStudyPageAndVerification(dateTime);
        importStudyPage.verifyButtonExistsForSelectingFolderForImport();
        importStudyPage.closeImportStudyPage();
        Logger.endTestCase("checkButtonExistsForSelectingFolderForImport");

    }

    /***
     * Test case to check if error message is shown when uploading a folder which contains both EDF and BDF files and that the folder is rejected for import
     */
    @Test(priority = 8)
    public void TC04_checkErrorMessageDisplayedForUploading_BothEDFAndBDF() {
        CommonUtility.logCreateTest("TC04_checkErrorMessageDisplayedForUploading_BothEDFAndBDF");

        importStudyPage = PagesFactory.getImportStudyPage();

        navigationToImportStudyPageAndVerification(dateTime);
        importStudyPage.uploadFilesForImportStudy("ImportValidation_Studies\\EDF&BDF");
        importStudyPage.verifyErrorMessageForBothEDFAndBDF();
        importStudyPage.verifyFolderRejectedForImport();
        importStudyPage.closeImportStudyPage();

        Logger.endTestCase("checkErrorMessageDisplayedForUploading_BothEDFAndBDF");

    }

    /***
     * Test case to check if error message is shown for uploading both .Mp4 and .avi files and that the folder is rejected for import
     */
    @Test(priority = 9)
    public void TC05_checkErrorMessageForUploading_BothMP4AndAVI() {
        CommonUtility.logCreateTest("TC05_checkErrorMessageForUploading_BothMP4AndAVI");

        importStudyPage = PagesFactory.getImportStudyPage();

        navigationToImportStudyPageAndVerification(dateTime);
        importStudyPage.uploadFilesForImportStudy("ImportValidation_Studies\\ET-4097");
        importStudyPage.verifyErrorMessageForBothMP4AndAVI();
        importStudyPage.verifyFolderRejectedForImport();
        importStudyPage.closeImportStudyPage();

        Logger.endTestCase("checkErrorMessageForUploading_BothMP4AndAVI");

    }

    /***
     * Test case to check if error message is displayed for uploading video file with different file name than the TVS file and that the folder is rejected for import
     */
    @Test(priority = 10)
    public void TC06_checkErrorMessageForUploading_TVSFileWithDifferentNameThanVideo() {
        CommonUtility.logCreateTest("TC06_checkErrorMessageForUploading_TVSFileWithDifferentNameThanVideo");

        importStudyPage = PagesFactory.getImportStudyPage();

        navigationToImportStudyPageAndVerification(dateTime);
        importStudyPage.uploadFilesForImportStudy("ImportValidation_Studies\\ET-4102\\AVI with TVS name different");
        importStudyPage.verifyErrorMessageForTVSFileWithDifferentNameThanVideo();
        importStudyPage.uploadFilesForImportStudy("ImportValidation_Studies\\ET-4102\\MP4 with TVS name different");
        importStudyPage.verifyErrorMessageForTVSFileWithDifferentNameThanVideo();
        importStudyPage.verifyFolderRejectedForImport();
        importStudyPage.closeImportStudyPage();

        Logger.endTestCase("checkErrorMessageForUploading_TVSFileWithDifferentNameThanVideo");

    }

    /***
     * Test case to check if error messages are displayed for uploading a folder with tvs file but no video file and that the folder is rejected for import
     */
    @Test(priority = 11)
    public void TC07_checkErrorMessageForUploading_TVSWithNoVideoFile() {
        CommonUtility.logCreateTest("TC07_checkErrorMessageForUploading_TVSWithNoVideoFile");

        importStudyPage = PagesFactory.getImportStudyPage();

        navigationToImportStudyPageAndVerification(dateTime);
        importStudyPage.uploadFilesForImportStudy("ImportValidation_Studies\\ET-4102\\No video bdf");
        importStudyPage.verifyErrorMessageForTVSWithNoVideo();
        importStudyPage.uploadFilesForImportStudy("ImportValidation_Studies\\ET-4102\\No video edf");
        importStudyPage.verifyErrorMessageForTVSWithNoVideo();
        importStudyPage.verifyFolderRejectedForImport();
        importStudyPage.closeImportStudyPage();

        Logger.endTestCase("checkErrorMessageForUploading_TVSWithNoVideoFile");

    }

    /***
     * Test case to check if error message is displayed for uploading multiple tvs files with one video file and that the folder is rejected for import
     */
    @Test(priority = 12)
    public void TC08_checkErrorMessageForUploading_MultipleTVSFilesWithOneVideo() {
        CommonUtility.logCreateTest("TC08_checkErrorMessageForUploading_MultipleTVSFilesWithOneVideo");

        importStudyPage = PagesFactory.getImportStudyPage();

        navigationToImportStudyPageAndVerification(dateTime);
        importStudyPage.uploadFilesForImportStudy("ImportValidation_Studies\\ET-4102\\Multiple tvs with one avi file");
        importStudyPage.verifyErrorMessageForMultipleTVSFilesWithOneVideo();
        importStudyPage.uploadFilesForImportStudy("ImportValidation_Studies\\ET-4102\\Multiple tvs file for one mp4 file");
        importStudyPage.verifyErrorMessageForMultipleTVSFilesWithOneVideo();
        importStudyPage.verifyFolderRejectedForImport();
        importStudyPage.closeImportStudyPage();

        Logger.endTestCase("checkErrorMessageForUploading_MultipleTVSFilesWithOneVideo");

    }

    /***
     * Test case to check error message is displayed for uploading multiple EDF files and that the folder is rejected for import
     */
    @Test(priority = 13)
    public void TC09_checkErrorMessageForUploading_MultipleEDFFiles() {
        CommonUtility.logCreateTest("TC09_checkErrorMessageForUploading_MultipleEDFFiles");

        importStudyPage = PagesFactory.getImportStudyPage();

        navigationToImportStudyPageAndVerification(dateTime);
        importStudyPage.uploadFilesForImportStudy("ImportValidation_Studies\\ET-4098\\Single folder 2 EDF files");
        importStudyPage.verifyErrorMessageForMultipleEDFFiles();
        importStudyPage.verifyFolderRejectedForImport();
        importStudyPage.closeImportStudyPage();

        Logger.endTestCase("checkErrorMessageForUploading_MultipleEDFFiles");

    }

    /***
     * Test case to check error message is displayed for uploading multiple sub-folders of EDF files and that the folder is rejected for import
     */
    @Test(priority = 14)
    public void TC10_checkErrorMessageForUploading_MultipleSubFoldersOfEDFFiles() {
        CommonUtility.logCreateTest("TC10_checkErrorMessageForUploading_MultipleSubFoldersOfEDFFiles");

        importStudyPage = PagesFactory.getImportStudyPage();

        navigationToImportStudyPageAndVerification(dateTime);
        importStudyPage.uploadFilesForImportStudy("ImportValidation_Studies\\ET-4098\\SubFolders 2 EDF files");
        importStudyPage.verifyErrorMessageForMultipleEDFFiles();
        importStudyPage.verifyFolderRejectedForImport();
        importStudyPage.closeImportStudyPage();

        Logger.endTestCase("checkErrorMessageForUploading_MultipleSubFoldersOfEDFFiles");

    }

    /***
     * Test case to verify error message for uploading a study with multiple photic channels and that the folder is rejected for import
     */
    @Test(priority = 15)
    public void TC11_checkErrorMessageForUploading_StudyWithMultiplePhoticChannels() {
        CommonUtility.logCreateTest("TC11_checkErrorMessageForUploading_StudyWithMultiplePhoticChannels");

        importStudyPage = PagesFactory.getImportStudyPage();

        navigationToImportStudyPageAndVerification(dateTime);
        importStudyPage.uploadFilesForImportStudy("ImportValidation_Studies\\ET-4099\\Additional Photic Channel");
        importStudyPage.verifyErrorMessageForMultiplePhoticChannels();
        importStudyPage.verifyFolderRejectedForImport();
        importStudyPage.closeImportStudyPage();

        Logger.endTestCase("checkErrorMessageForUploading_StudyWithMultiplePhoticChannels");

    }

    /***
     * Test case to check that an error message is shown if the uploaded folder is empty and that the folder is rejected for import
     */
    @Test(priority = 16)
    public void TC12_checkErrorMessageForUploading_EmptyFolder() {
        CommonUtility.logCreateTest("TC12_checkErrorMessageForUploading_EmptyFolder");

        importStudyPage = PagesFactory.getImportStudyPage();

        navigationToImportStudyPageAndVerification(dateTime);
        importStudyPage.uploadFilesForImportStudy("ImportValidation_Studies\\Empty");
        importStudyPage.verifyErrorMessageForNotHavingEDForBDFFile();
        importStudyPage.verifyFolderRejectedForImport();
        importStudyPage.closeImportStudyPage();

        Logger.endTestCase("checkErrorMessageForUploading_EmptyFolder");

    }

    /***
     * Test case to check that an error message is displayed for not having at least 1 EDF or BDF file and that the folder is rejected for import
     */
    @Test(priority = 17)
    public void TC13_checkErrorMessageForUploading_FolderNotHavingEDFOrBDFFile() {
        CommonUtility.logCreateTest("TC13_checkErrorMessageForUploading_FolderNotHavingEDFOrBDFFile");

        importStudyPage = PagesFactory.getImportStudyPage();

        navigationToImportStudyPageAndVerification(dateTime);
        importStudyPage.uploadFilesForImportStudy("ImportValidation_Studies\\ET -4100\\Different Format");
        importStudyPage.verifyErrorMessageForNotHavingEDForBDFFile();
        importStudyPage.verifyFolderRejectedForImport();
        importStudyPage.closeImportStudyPage();

        Logger.endTestCase("checkErrorMessageForUploading_FolderNotHavingEDFOrBDFFile");

    }

    /***
     * Test case to check that a folder is rejected for import and an error message is shown if the headers of TEV/TVX file does not match with EDF/BDF file
     */
    @Test(priority = 18)
    public void TC321_checkFolderRejectedForImport_IfHeaderOfTEVTVXDoesNotMatchWithDataFile() {
        CommonUtility.logCreateTest("TC321_checkFolderRejectedForImport_IfHeaderOfTEVTVXDoesNotMatchWithDataFile");

        importStudyPage = PagesFactory.getImportStudyPage();

        navigationToImportStudyPageAndVerification(dateTime);
        importStudyPage.uploadFilesForImportStudy("ImportValidation_Studies\\ET-4105\\Modified Patient ID for TEV (BDF)");
        importStudyPage.verifyErrorMessageForHeadersOfTEVOrTVXNotMatchingWithEDFOrBDF();
        importStudyPage.verifyFolderRejectedForImport();
        importStudyPage.closeImportStudyPage();

        Logger.endTestCase("TC321_checkFolderRejectedForImport_IfHeaderOfTEVTVXDoesNotMatchWithDataFile");

    }

    /***
     * Test case to check that an error message is displayed if the patient ID of TEV/TVX file does not match with EDF or BDF file and that the folder is rejected for import
     */
    @Test(priority = 18)
    public void TC14_checkErrorMessage_IfPatientIDOfTEVOrTVXDoesNotMatchWithEDFOrBDF() {
        CommonUtility.logCreateTest("TC14_checkErrorMessage_IfPatientIDOfTEVOrTVXDoesNotMatchWithEDFOrBDF");

        importStudyPage = PagesFactory.getImportStudyPage();

        navigationToImportStudyPageAndVerification(dateTime);
        importStudyPage.uploadFilesForImportStudy("ImportValidation_Studies\\ET-4105\\Modified Patient ID for TEV (BDF)");
        importStudyPage.verifyErrorMessageForHeadersOfTEVOrTVXNotMatchingWithEDFOrBDF();
        importStudyPage.uploadFilesForImportStudy("ImportValidation_Studies\\ET-4105\\Modified Patient ID for TEV (EDF)");
        importStudyPage.verifyErrorMessageForHeadersOfTEVOrTVXNotMatchingWithEDFOrBDF();
        importStudyPage.uploadFilesForImportStudy("ImportValidation_Studies\\ET-4105\\Modified Patient ID for TVX");
        importStudyPage.verifyErrorMessageForHeadersOfTEVOrTVXNotMatchingWithEDFOrBDF();
        importStudyPage.verifyFolderRejectedForImport();
        importStudyPage.closeImportStudyPage();

        Logger.endTestCase("checkErrorMessage_IfPatientIDOfTEVOrTVXDoesNotMatchWithEDFOrBDF");
    }

    /***
     * Test case to check that an error message is displayed if the Record ID of TEV/TVX file does not match with EDF or BDF file and that the folder is rejected for import
     */
    @Test(priority = 19)
    public void TC15_checkErrorMessage_IfRecordIDOfTEVOrTVXDoesNotMatchWithEDFOrBDF() {
        CommonUtility.logCreateTest("TC15_checkErrorMessage_IfRecordIDOfTEVOrTVXDoesNotMatchWithEDFOrBDF");

        importStudyPage = PagesFactory.getImportStudyPage();

        navigationToImportStudyPageAndVerification(dateTime);
        importStudyPage.uploadFilesForImportStudy("ImportValidation_Studies\\ET-4105\\Modified Record ID for TEV (BDF)");
        importStudyPage.verifyErrorMessageForHeadersOfTEVOrTVXNotMatchingWithEDFOrBDF();
        importStudyPage.uploadFilesForImportStudy("ImportValidation_Studies\\ET-4105\\Modified Record ID for TEV (EDF)");
        importStudyPage.verifyErrorMessageForHeadersOfTEVOrTVXNotMatchingWithEDFOrBDF();
        importStudyPage.uploadFilesForImportStudy("ImportValidation_Studies\\ET-4105\\Modified Record ID for TVX");
        importStudyPage.verifyErrorMessageForHeadersOfTEVOrTVXNotMatchingWithEDFOrBDF();
        importStudyPage.verifyFolderRejectedForImport();
        importStudyPage.closeImportStudyPage();

        Logger.endTestCase("checkErrorMessage_IfRecordIDOfTEVOrTVXDoesNotMatchWithEDFOrBDF");
    }

    /***
     * Test case to check a folder is rejected for import if a BDF file header does not match with another BDF file's header
     */
    @Test(priority = 20)
    public void TC322_checkFolderRejectedForImport_IfBDFFileHeaderDoesNotMatchWithAnotherBDFFile() {
        CommonUtility.logCreateTest("TC322_checkFolderRejectedForImport_IfBDFFileHeaderDoesNotMatchWithAnotherBDFFile");

        importStudyPage = PagesFactory.getImportStudyPage();

        navigationToImportStudyPageAndVerification(dateTime);
        importStudyPage.uploadFilesForImportStudy("ImportValidation_Studies\\ET-4106\\Modified Signal count");
        importStudyPage.verifyFolderRejectedForImport();
        importStudyPage.closeImportStudyPage();

        Logger.endTestCase("TC322_checkFolderRejectedForImport_IfBDFFileHeaderDoesNotMatchWithAnotherBDFFile");

    }

    /***
     * Test case to check that an error message if the signal count is the same for all the BDF files
     */
    @Test(priority = 20)
    public void TC16_checkErrorMessage_IfSignalCountNotSameForAllBDFFiles() {
        CommonUtility.logCreateTest("TC16_checkErrorMessage_IfSignalCountNotSameForAllBDFFiles");

        importStudyPage = PagesFactory.getImportStudyPage();

        navigationToImportStudyPageAndVerification(dateTime);
        importStudyPage.uploadFilesForImportStudy("ImportValidation_Studies\\ET-4106\\Modified Signal count");
        importStudyPage.verifyErrorMessageIfSignalCountNotSameForAllBDFFiles();
        importStudyPage.verifyFolderRejectedForImport();
        importStudyPage.closeImportStudyPage();

        Logger.endTestCase("checkErrorMessage_IfSignalCountNotSameForAllBDFFiles");

    }

    /***
     * Test case to check that an error message is displayed if the patient ID is not same for all the BDF files
     */
    @Test(priority = 21)
    public void TC17_checkErrorMessage_IfPatientIDNotSameForAllBDFFiles() {
        CommonUtility.logCreateTest("TC17_checkErrorMessage_IfPatientIDNotSameForAllBDFFiles");

        importStudyPage = PagesFactory.getImportStudyPage();

        navigationToImportStudyPageAndVerification(dateTime);
        importStudyPage.verifyImportStudyPageNavigation();
        importStudyPage.uploadFilesForImportStudy("ImportValidation_Studies\\ET-4106\\Modified patient name");
        importStudyPage.verifyErrorMessageIfPatientIDNotSameForAllBDFFiles();
        importStudyPage.verifyFolderRejectedForImport();
        importStudyPage.closeImportStudyPage();

        Logger.endTestCase("checkErrorMessage_IfPatientIDNotSameForAllBDFFiles");

    }

    /***
     * Test case to check that an error message is displayed if the Record ID is not same for all the BDF files
     */
    @Test(priority = 22)
    public void TC18_checkErrorMessage_IfRecordIDNotSameForAllBDFFiles() {
        CommonUtility.logCreateTest("TC18_checkErrorMessage_IfRecordIDNotSameForAllBDFFiles");

        importStudyPage = PagesFactory.getImportStudyPage();

        navigationToImportStudyPageAndVerification(dateTime);
        importStudyPage.uploadFilesForImportStudy("ImportValidation_Studies\\ET-4106\\Modified Record ID");
        importStudyPage.verifyErrorMessageIfRecordIDNotSameForAllBDFFiles();
        importStudyPage.verifyFolderRejectedForImport();
        importStudyPage.closeImportStudyPage();

        Logger.endTestCase("checkErrorMessage_IfRecordIDNotSameForAllBDFFiles");

    }

    /***
     * Test case to check that an error message is displayed if the Data Version Format is not same for all the BDF files
     */
    @Test(priority = 23)
    public void TC19_checkErrorMessage_IfDataVersionFormatNotSameForAllBDFFiles() {
        CommonUtility.logCreateTest("TC19_checkErrorMessage_IfDataVersionFormatNotSameForAllBDFFiles");

        importStudyPage = PagesFactory.getImportStudyPage();

        navigationToImportStudyPageAndVerification(dateTime);
        importStudyPage.uploadFilesForImportStudy("ImportValidation_Studies\\ET-4106\\Modified Data version format");
        importStudyPage.verifyErrorMessageIfDataVersionFormatNotSameForAllBDFFiles();
        importStudyPage.verifyFolderRejectedForImport();
        importStudyPage.closeImportStudyPage();

        Logger.endTestCase("checkErrorMessage_IfDataVersionFormatNotSameForAllBDFFiles");

    }

    /***
     * Test case to check that an error message is displayed if the Data Record Duration is not same for all the BDF files
     */
    @Test(priority = 24)
    public void TC20_checkErrorMessage_IfDataRecordDurationNotSameForAllBDFFiles() {
        CommonUtility.logCreateTest("TC20_checkErrorMessage_IfDataRecordDurationNotSameForAllBDFFiles");

        importStudyPage = PagesFactory.getImportStudyPage();

        navigationToImportStudyPageAndVerification(dateTime);
        importStudyPage.uploadFilesForImportStudy("ImportValidation_Studies\\ET-4106\\Modified Data Record duration");
        importStudyPage.verifyErrorMessageIfDataRecordDurationNotSameForAllBDFFiles();
        importStudyPage.verifyFolderRejectedForImport();
        importStudyPage.closeImportStudyPage();

        Logger.endTestCase("checkErrorMessage_IfDataRecordDurationNotSameForAllBDFFiles");

    }

    /***
     * Test case to check that an error message is displayed if the Samples Per Data Record is not same for all the BDF files
     */
    @Test(priority = 25)
    public void TC21_checkErrorMessage_IfSamplesPerDataRecordNotSameForAllBDFFiles() {
        CommonUtility.logCreateTest("TC21_checkErrorMessage_IfSamplesPerDataRecordNotSameForAllBDFFiles");

        importStudyPage = PagesFactory.getImportStudyPage();

        navigationToImportStudyPageAndVerification(dateTime);
        importStudyPage.uploadFilesForImportStudy("ImportValidation_Studies\\ET-4106\\Modified sample rate");
        importStudyPage.verifyErrorMessageIfSamplesPerDataRecordNotSameForAllBDFFiles();
        importStudyPage.verifyFolderRejectedForImport();
        importStudyPage.closeImportStudyPage();

        Logger.endTestCase("checkErrorMessage_IfSamplesPerDataRecordNotSameForAllBDFFiles");

    }

    /***
     * Test case to check error message is displayed if channels sample rate does not match with with channels sample rate in EDF file and the folder is rejected for import
     */
    @Test(priority = 26)
    public void TC22_checkErrorMessage_IfSampleRateDoesNotMatchWithOtherChannelsEDF() {
        CommonUtility.logCreateTest("TC22_checkErrorMessage_IfSampleRateDoesNotMatchWithOtherChannelsEDF");

        importStudyPage = PagesFactory.getImportStudyPage();

        navigationToImportStudyPageAndVerification(dateTime);
        importStudyPage.uploadFilesForImportStudy("ImportValidation_Studies\\ET- 4107\\Modified sample data rate EDF");
        importStudyPage.verifyErrorMessageIfSampleRateDoesNotMatchWithOtherChannels();
        importStudyPage.verifyFolderRejectedForImport();
        importStudyPage.closeImportStudyPage();

        Logger.endTestCase("checkErrorMessage_IfSampleRateDoesNotMatchWithOtherChannelsEDF");

    }

    /***
     * Test case to check error message is displayed if channels sample rate does not match with with channels sample rate in BDF file and the folder is rejected for import
     */
    @Test(priority = 27)
    public void TC23_checkErrorMessage_IfSampleRateDoesNotMatchWithOtherChannelsBDF() {
        CommonUtility.logCreateTest("TC23_checkErrorMessage_IfSampleRateDoesNotMatchWithOtherChannelsBDF");

        importStudyPage = PagesFactory.getImportStudyPage();

        navigationToImportStudyPageAndVerification(dateTime);
        importStudyPage.uploadFilesForImportStudy("ImportValidation_Studies\\ET- 4107\\Modified sample rate BDF");
        importStudyPage.verifyErrorMessageIfSampleRateDoesNotMatchWithOtherChannels();
        importStudyPage.verifyFolderRejectedForImport();
        importStudyPage.closeImportStudyPage();

        Logger.endTestCase("checkErrorMessage_IfSampleRateDoesNotMatchWithOtherChannelsBDF");

    }

    /***
     * Test case to check error message is displayed if channels sample rate does not match with with channels sample rate in BDF file and the folder is rejected for import
     */
    @Test(priority = 28)
    public void TC24_checkErrorMessage_IfStudyContainsChannelsMoreThanTheMaxOf32() {
        CommonUtility.logCreateTest("TC24_checkErrorMessage_IfStudyContainsChannelsMoreThanTheMaxOf32");

        importStudyPage = PagesFactory.getImportStudyPage();

        navigationToImportStudyPageAndVerification(dateTime);
        importStudyPage.uploadFilesForImportStudy("ImportValidation_Studies\\ET-4109\\Trackit_MoreThan32ChannelsBDF");
        importStudyPage.verifyErrorMessageForStudyContaining33Channels();
        importStudyPage.uploadFilesForImportStudy("ImportValidation_Studies\\ET-4109\\Trackit_MoreThan32ChannelsEDF");
        importStudyPage.verifyErrorMessageForStudyContaining33Channels();
        importStudyPage.verifyFolderRejectedForImport();
        importStudyPage.closeImportStudyPage();

        Logger.endTestCase("checkErrorMessage_IfStudyContainsChannelsMoreThanTheMaxOf32");

    }

    /***
     * Test case to check that a folder is rejected for import if multiple BDF files do not follow the naming convention
     */
    @Test(priority = 29)
    public void TC323_checkFolderRejectedForImport_IfBDFFilesDoNotFollowNamingConvention() {
        CommonUtility.logCreateTest("TC323_checkFolderRejectedForImport_IfBDFFilesDoNotFollowNamingConvention");

        importStudyPage = PagesFactory.getImportStudyPage();

        navigationToImportStudyPageAndVerification(dateTime);
        importStudyPage.uploadFilesForImportStudy("ImportValidation_Studies\\ET-4103\\MultiBDF_Firstfile");
        importStudyPage.verifyFolderRejectedForImport();
        importStudyPage.closeImportStudyPage();

        Logger.endTestCase("TC323_checkFolderRejectedForImport_IfBDFFilesDoNotFollowNamingConvention");
    }

    /***
     * Test case to check error message if the first file has wrong naming convention for a study with multiple BDF files
     */
    @Test(priority = 29)
    public void TC25_checkErrorMessage_ForWrongFirstFileNamingConventionForMultipleBDF() {
        CommonUtility.logCreateTest("TC25_checkErrorMessage_ForWrongFirstFileNamingConventionForMultipleBDF");

        importStudyPage = PagesFactory.getImportStudyPage();

        navigationToImportStudyPageAndVerification(dateTime);
        importStudyPage.uploadFilesForImportStudy("ImportValidation_Studies\\ET-4103\\MultiBDF_Firstfile");
        importStudyPage.verifyErrorMessageForWrongFileNamingConvention();
        importStudyPage.verifyFolderRejectedForImport();
        importStudyPage.closeImportStudyPage();

        Logger.endTestCase("checkErrorMessage_ForWrongFirstFileNamingConventionForMultipleBDF");

    }

    /***
     * Test case to check error message if the 2nd to 9th file has wrong naming convention for a study with multiple BDF files
     */
    @Test(priority = 30)
    public void TC26_checkErrorMessage_ifSecondToNinthFileHasWrongNamingConventionForMultipleBDF() {
        CommonUtility.logCreateTest("TC26_checkErrorMessage_ifSecondToNinthFileHasWrongNamingConventionForMultipleBDF");

        importStudyPage = PagesFactory.getImportStudyPage();

        navigationToImportStudyPageAndVerification(dateTime);
        importStudyPage.uploadFilesForImportStudy("ImportValidation_Studies\\ET-4103\\MultiBDF_2ndto9thFile");
        importStudyPage.verifyErrorMessageForWrongFileNamingConvention();
        importStudyPage.verifyFolderRejectedForImport();
        importStudyPage.closeImportStudyPage();

        Logger.endTestCase("checkErrorMessage_ifSecondToNinthFileIsMissingForMultipleBDF");

    }

    /***
     * Test case to check error message if the 11th file has wrong naming convention for a study with multiple BDF files
     */
    @Test(priority = 31)
    public void TC27_checkErrorMessage_ForWrongEleventhFileNamingConventionForMultipleBDF() {
        CommonUtility.logCreateTest("TC27_checkErrorMessage_ForWrongEleventhFileNamingConventionForMultipleBDF");

        importStudyPage = PagesFactory.getImportStudyPage();

        navigationToImportStudyPageAndVerification(dateTime);
        importStudyPage.uploadFilesForImportStudy("ImportValidation_Studies\\ET-4103\\MultiBDF_After10Files");
        importStudyPage.verifyErrorMessageForWrongFileNamingConvention();
        importStudyPage.verifyFolderRejectedForImport();
        importStudyPage.closeImportStudyPage();

        Logger.endTestCase("checkErrorMessage_ForWrongEleventhNamingConventionForMultipleBDF");

    }

    /***
     * Test case to check error message if folder does not contain .TEV file with .BDF file
     */
    @Test(priority = 32)
    public void TC28_checkErrorMessageForUploading_FolderWithNoTEVWithBDF() {
        CommonUtility.logCreateTest("TC28_checkErrorMessageForUploading_FolderWithNoTEVWithBDF");

        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        patientsPage = PagesFactory.getPatientsPage();
        importStudyPage = PagesFactory.getImportStudyPage();

        menuSidebarPage.goToPatientsPage();
        patientsPage.verifyPatientPageNavigation();
        patientsPage.clickOnImportIconByPatientName("yes", dateTime);
        importStudyPage.verifyImportStudyPageNavigation();
        importStudyPage.uploadFilesForImportStudy("ImportValidation_Studies\\ET -4101\\BDF format\\No TEV File BDF");
        importStudyPage.verifyErrorMessageForEDFOrBDFHavingNoTEVFile();
        importStudyPage.verifyButtonExistsForSelectingFolderForImport();
        importStudyPage.closeImportStudyPage();

        Logger.endTestCase("checkErrorMessageForUploading_FolderWithNoTEVWithBDF");

    }

    /***
     * Test case to check error message if BDF file has different name than TEV
     */
    @Test(priority = 33)
    public void TC29_checkErrorMessageForUploading_BDFWithDifferentNameThanTEV() {
        CommonUtility.logCreateTest("TC29_checkErrorMessageForUploading_BDFWithDifferentNameThanTEV");

        importStudyPage = PagesFactory.getImportStudyPage();

        navigationToImportStudyPageAndVerification(dateTime);
        importStudyPage.uploadFilesForImportStudy("ImportValidation_Studies\\ET -4101\\BDF format\\Different Name of TEV");
        importStudyPage.verifyErrorMessageForEDFOrBDFHavingDifferentNamesThanTEV();
        importStudyPage.verifyFolderRejectedForImport();
        importStudyPage.closeImportStudyPage();

        Logger.endTestCase("checkErrorMessageForUploading_BDFWithDifferentNameThanTEV");

    }

    /***
     * Test case to check error message if BDF file has different name than TEV and TVX
     */
    @Test(priority = 34)
    public void TC30_checkErrorMessageForUploading_BDFWithDifferentNameThanTEVAndTVX() {
        CommonUtility.logCreateTest("TC30_checkErrorMessageForUploading_BDFWithDifferentNameThanTEVAndTVX");

        importStudyPage = PagesFactory.getImportStudyPage();

        navigationToImportStudyPageAndVerification(dateTime);
        importStudyPage.uploadFilesForImportStudy("ImportValidation_Studies\\ET -4101\\BDF format\\Different Name TEV & TVX For BDF");
        importStudyPage.verifyErrorMessageForBDFHavingDifferentNamesThanTEVAndTVX();
        importStudyPage.verifyFolderRejectedForImport();
        importStudyPage.closeImportStudyPage();

        Logger.endTestCase("checkErrorMessageForUploading_BDFWithDifferentNameThanTEVAndTVX");

    }

    /***
     * Test case to check error message if BDF file has different name than TVX
     */
    @Test(priority = 35)
    public void TC31_checkErrorMessageForUploading_BDFWithDifferentNameThanTVX() {
        CommonUtility.logCreateTest("TC31_checkErrorMessageForUploading_BDFWithDifferentNameThanTVX");

        importStudyPage = PagesFactory.getImportStudyPage();

        navigationToImportStudyPageAndVerification(dateTime);
        importStudyPage.uploadFilesForImportStudy("ImportValidation_Studies\\ET -4101\\BDF format\\Different Name TVX For BDF");
        importStudyPage.verifyErrorMessageForBDFHavingDifferentNameThanTVX();
        importStudyPage.verifyFolderRejectedForImport();
        importStudyPage.closeImportStudyPage();

        Logger.endTestCase("checkErrorMessageForUploading_BDFWithDifferentNameThanTVX");

    }

    /***
     * Test case to check error message if folder does not contain BDF file
     */
    @Test(priority = 36)
    public void TC32_checkErrorMessageForUploading_FolderWithNoBDF() {
        CommonUtility.logCreateTest("TC32_checkErrorMessageForUploading_FolderWithNoBDF");

        importStudyPage = PagesFactory.getImportStudyPage();

        navigationToImportStudyPageAndVerification(dateTime);
        importStudyPage.uploadFilesForImportStudy("ImportValidation_Studies\\ET -4101\\BDF format\\NO BDF file");
        importStudyPage.verifyErrorMessageForFolderNotHavingDataFile();
        importStudyPage.verifyFolderRejectedForImport();
        importStudyPage.closeImportStudyPage();

        Logger.endTestCase("checkErrorMessageForUploading_FolderWithNoBDF");

    }

    /***
     * Test case to check error message if folder does not contain .TEV file with .EDF file
     */
    @Test(priority = 37)
    public void TC33_checkErrorMessageForUploading_FolderWithNoTEVWithEDF() {
        CommonUtility.logCreateTest("TC33_checkErrorMessageForUploading_FolderWithNoTEVWithEDF");

        importStudyPage = PagesFactory.getImportStudyPage();

        navigationToImportStudyPageAndVerification(dateTime);
        importStudyPage.uploadFilesForImportStudy("ImportValidation_Studies\\ET -4101\\EDF format\\NO TEV file for EDF");
        importStudyPage.verifyErrorMessageForEDFOrBDFHavingNoTEVFile();
        importStudyPage.uploadFilesForImportStudy("ImportValidation_Studies\\ET -4101\\EDF format\\No TEV and TVX file");
        importStudyPage.verifyErrorMessageForEDFOrBDFHavingNoTEVFile();
        importStudyPage.verifyFolderRejectedForImport();
        importStudyPage.closeImportStudyPage();

        Logger.endTestCase("checkErrorMessageForUploading_FolderWithNoTEVWithEDF");

    }

    /***
     * Test case to check error message if EDF file has different name than TEV
     */
    @Test(priority = 38)
    public void TC34_checkErrorMessageForUploading_EDFWithDifferentNameThanTEV() {
        CommonUtility.logCreateTest("TC34_checkErrorMessageForUploading_EDFWithDifferentNameThanTEV");

        importStudyPage = PagesFactory.getImportStudyPage();

        navigationToImportStudyPageAndVerification(dateTime);
        importStudyPage.uploadFilesForImportStudy("ImportValidation_Studies\\ET -4101\\EDF format\\Different name TEV for EDF");
        importStudyPage.verifyErrorMessageForEDFOrBDFHavingDifferentNamesThanTEV();
        importStudyPage.verifyFolderRejectedForImport();
        importStudyPage.closeImportStudyPage();

        Logger.endTestCase("checkErrorMessageForUploading_EDFWithDifferentNameThanTEV");

    }

    /***
     * Test case to check that folder is rejected for import if the EDF/BDF has a different name than TEV
     */
    @Test(priority = 39)
    public void TC324_checkFolderRejectedForImport_IFEDFBDFDifferentThanTEV() {
        CommonUtility.logCreateTest("TC324_checkFolderRejectedForImport_IFEDFBDFDifferentThanTEV");

        importStudyPage = PagesFactory.getImportStudyPage();

        navigationToImportStudyPageAndVerification(dateTime);
        importStudyPage.uploadFilesForImportStudy("ImportValidation_Studies\\ET -4101\\EDF format\\Different name TEV for EDF");
        importStudyPage.verifyFolderRejectedForImport();
        importStudyPage.uploadFilesForImportStudy("ImportValidation_Studies\\ET -4101\\BDF format\\Different Name of TEV");
        importStudyPage.verifyFolderRejectedForImport();
        importStudyPage.closeImportStudyPage();

        Logger.endTestCase("TC324_checkFolderRejectedForImport_IFEDFBDFDifferentThanTEV");

    }

//    /***
//     * Test case to check error message for uploading a folder that contains different TEV and TVX names
//     */
//    @Test(priority = 39)
//    public void TC325_checkErrorMessageForUploading_FilesWithDifferentTEVAndTVXNames() {
//        CommonUtility.logCreateTest("TC325_checkErrorMessageForUploading_FilesWithDifferentTEVAndTVXNames");
//
//        importStudyPage = PagesFactory.getImportStudyPage();
//
//        navigationToImportStudyPageAndVerification(dateTime);
//        importStudyPage.uploadFilesForImportStudy("ImportValidation_Studies\\ET -4101\\EDF format\\Different name TVX and TEV for EDF");
//        importStudyPage.verifyErrorMessageForDifferentTEVTVXNames();
//        importStudyPage.verifyFolderRejectedForImport();
//        importStudyPage.closeImportStudyPage();
//
//        Logger.endTestCase("TC325_checkErrorMessageForUploading_FilesWithDifferentTEVAndTVXNames");
//
//    }

    /***
     * Test case to check error message if EDF file has different name than TEV and TVX
     */
    @Test(priority = 39)
    public void TC35_checkErrorMessageForUploading_EDFWithDifferentNameThanTEVAndTVX() {
        CommonUtility.logCreateTest("TC35_checkErrorMessageForUploading_EDFWithDifferentNameThanTEVAndTVX");

        importStudyPage = PagesFactory.getImportStudyPage();

        navigationToImportStudyPageAndVerification(dateTime);
        importStudyPage.uploadFilesForImportStudy("ImportValidation_Studies\\ET -4101\\EDF format\\Different name TVX and TEV for EDF");
        importStudyPage.verifyErrorMessageForEDFHavingDifferentNamesThanTEVAndTVX();
        importStudyPage.verifyFolderRejectedForImport();
        importStudyPage.closeImportStudyPage();

        Logger.endTestCase("checkErrorMessageForUploading_EDFWithDifferentNameThanTEVAndTVX");

    }

    /***
     * Test case to check error message if EDF file has different name than TVX
     */
    @Test(priority = 40)
    public void TC36_checkErrorMessageForUploading_EDFWithDifferentNameThanTVX() {
        CommonUtility.logCreateTest("TC36_checkErrorMessageForUploading_EDFWithDifferentNameThanTVX");

        importStudyPage = PagesFactory.getImportStudyPage();

        navigationToImportStudyPageAndVerification(dateTime);
        importStudyPage.uploadFilesForImportStudy("ImportValidation_Studies\\ET -4101\\EDF format\\Different name TVX for EDF");
        importStudyPage.verifyErrorMessageForEDFHavingDifferentNameThanTVX();
        importStudyPage.verifyFolderRejectedForImport();
        importStudyPage.closeImportStudyPage();

        Logger.endTestCase("checkErrorMessageForUploading_EDFWithDifferentNameThanTVX");

    }

    /***
     * Test case to check error message if folder does not contain EDF file
     */
    @Test(priority = 41)
    public void TC37_checkErrorMessageForUploading_FolderWithNoEDF() {
        CommonUtility.logCreateTest("TC37_checkErrorMessageForUploading_FolderWithNoEDF");

        importStudyPage = PagesFactory.getImportStudyPage();

        navigationToImportStudyPageAndVerification(dateTime);
        importStudyPage.uploadFilesForImportStudy("ImportValidation_Studies\\ET -4101\\EDF format\\NO edf file");
        importStudyPage.verifyErrorMessageForFolderNotHavingDataFile();
        importStudyPage.verifyFolderRejectedForImport();
        importStudyPage.closeImportStudyPage();

        Logger.endTestCase("checkErrorMessageForUploading_FolderWithNoEDF");

    }

    /***
     * Test case to check error message if folder contains multiple TVX files
     */
    @Test(priority = 42)
    public void TC38_checkErrorMessageForUploading_FolderWithMultipleTVXWithEDF() {
        CommonUtility.logCreateTest("TC38_checkErrorMessageForUploading_FolderWithMultipleTVXWithEDF");

        importStudyPage = PagesFactory.getImportStudyPage();

        navigationToImportStudyPageAndVerification(dateTime);
        importStudyPage.uploadFilesForImportStudy("ImportValidation_Studies\\ET - 4280\\EDF\\More than one TVX file with EDF");
        importStudyPage.verifyErrorMessageForFolderHavingMultipleTVXFiles();
        importStudyPage.verifyFolderRejectedForImport();
        importStudyPage.closeImportStudyPage();

        Logger.endTestCase("checkErrorMessageForUploading_FolderWithMultipleTVXWithEDF");

    }

    /***
     * Test case to check error message if folder contains contains more than 1 TEV file per EDF file
     */
    @Test(priority = 43)
    public void TC39_checkErrorMessageForUploading_FolderWithMoreThanOneTEVPerEDF() {
        CommonUtility.logCreateTest("TC39_checkErrorMessageForUploading_FolderWithMoreThanOneTEVPerEDF");

        importStudyPage = PagesFactory.getImportStudyPage();

        navigationToImportStudyPageAndVerification(dateTime);
        importStudyPage.uploadFilesForImportStudy("ImportValidation_Studies\\ET - 4280\\EDF\\More than one TEV file with EDF");
        importStudyPage.verifyErrorMessageForFolderHavingMoreThanOneTEVFilePerDataFile();
        importStudyPage.verifyFolderRejectedForImport();
        importStudyPage.closeImportStudyPage();

        Logger.endTestCase("checkErrorMessageForUploading_FolderWithMoreThanOneTEVPerEDF");

    }

    /***
     * Test case to check error message if folder contains multiple TVX files
     */
    @Test(priority = 44)
    public void TC40_checkErrorMessageForUploading_FolderWithMultipleTVXWithBDF() {
        CommonUtility.logCreateTest("TC40_checkErrorMessageForUploading_FolderWithMultipleTVXWithBDF");

        importStudyPage = PagesFactory.getImportStudyPage();

        navigationToImportStudyPageAndVerification(dateTime);
        importStudyPage.uploadFilesForImportStudy("ImportValidation_Studies\\ET - 4280\\BDF\\More than one TVX file with BDF");
        importStudyPage.verifyErrorMessageForFolderHavingMultipleTVXFiles();
        importStudyPage.verifyFolderRejectedForImport();
        importStudyPage.closeImportStudyPage();

        Logger.endTestCase("checkErrorMessageForUploading_FolderWithMultipleTVXWithBDF");

    }

    /***
     * Test case to check error message if folder contains contains more than 1 TEV file per BDF file
     */
    @Test(priority = 45)
    public void TC41_checkErrorMessageForUploading_FolderWithMoreThanOneTEVPerBDF() {
        CommonUtility.logCreateTest("TC41_checkErrorMessageForUploading_FolderWithMoreThanOneTEVPerBDF");

        importStudyPage = PagesFactory.getImportStudyPage();

        navigationToImportStudyPageAndVerification(dateTime);
        importStudyPage.uploadFilesForImportStudy("ImportValidation_Studies\\ET - 4280\\BDF\\More than one TEV file with BDF");
        importStudyPage.verifyErrorMessageForFolderHavingMoreThanOneTEVFilePerDataFile();
        importStudyPage.verifyFolderRejectedForImport();
        importStudyPage.closeImportStudyPage();

        Logger.endTestCase("checkErrorMessageForUploading_FolderWithMoreThanOneTEVPerBDF");

    }

    /***
     * Test case to check that user is allowed to proceed with import if there are no TVX file
     */
    @Test(priority = 46)
    public void TC42_checkUser_AllowedToProceedWithImportIfThereIsNoTVX() {
        CommonUtility.logCreateTest("TC42_checkUser_AllowedToProceedWithImportIfThereIsNoTVX");

        importStudyPage = PagesFactory.getImportStudyPage();

        navigationToImportStudyPageAndVerification(dateTime);
        importStudyPage.uploadFilesForImportStudy("ImportValidation_Studies\\ET - 4280\\BDF\\Multiple BDF withput tvx");
        importStudyPage.verifyPatientAndStudyInformationIsDisplayed();
        importStudyPage.closeImportStudyPage();

        Logger.endTestCase("checkUser_AllowedToProceedWithImportIfThereIsNoTVX");

    }

    /***
     * Test case to check that Portal allows all types of channel orders for upload
     */
    @Test(priority = 47)
    public void TC43_checkPortalAllowsAllTypesOfChannelOrdersForUpload() {
        CommonUtility.logCreateTest("TC43_checkPortalAllowsAllTypesOfChannelOrdersForUpload");

        importStudyPage = PagesFactory.getImportStudyPage();

        navigationToImportStudyPageAndVerification(dateTime);
        importStudyPage.uploadFilesForImportStudy("ImportValidation_Studies\\ET-4059");
        importStudyPage.verifyPatientAndStudyInformationIsDisplayed();
        importStudyPage.closeImportStudyPage();

        Logger.endTestCase("checkPortalAllowsAllTypesOfChannelOrdersForUpload ");

    }

    /***
     * Test case to check presence of contact support message in case of Import Rejection
     */
    @Test(priority = 48)
    public void TC44_checkContactSupportMessageWhenFolderRejectedForImport() {
        CommonUtility.logCreateTest("TC44_checkContactSupportMessageWhenFolderRejectedForImport");

        importStudyPage = PagesFactory.getImportStudyPage();

        navigationToImportStudyPageAndVerification(dateTime);
        importStudyPage.uploadFilesForImportStudy("ImportValidation_Studies\\EDF&BDF");
        importStudyPage.verifyFolderRejectedForImport();
        importStudyPage.verifyContactSupportMessageForImportRejection();
        importStudyPage.closeImportStudyPage();

        Logger.endTestCase("checkContactSupportMessageWhenFolderRejectedForImport ");

    }

    /***
     * Test case to check click On contact support link navigates to help page
     */
    @Test(priority = 49)
    public void TC45_checkClickOnContactSupportNavigatesToHelpPage() {
        CommonUtility.logCreateTest("TC45_checkClickOnContactSupportNavigatesToHelpPage");

        importStudyPage = PagesFactory.getImportStudyPage();
        helpPage = PagesFactory.getHelpPage();

        navigationToImportStudyPageAndVerification(dateTime);
        importStudyPage.uploadFilesForImportStudy("ImportValidation_Studies\\EDF&BDF");
        importStudyPage.verifyFolderRejectedForImport();
        importStudyPage.verifyContactSupportMessageForImportRejection();
        importStudyPage.clickOnContactSupportLink();
        helpPage.verifyHelpPageNavigation();

        Logger.endTestCase("checkClickOnContactSupportNavigatesToHelpPage ");

    }

    /***
     * Test case to check click On contact support link navigates to help page
     */
    @Test(priority = 50)
    public void TC46_checkBrowserBackButtonTakesUserToImportStudyPage() {
        CommonUtility.logCreateTest("TC46_checkBrowserBackButtonTakesUserToImportStudyPage");

        importStudyPage = PagesFactory.getImportStudyPage();
        helpPage = PagesFactory.getHelpPage();

        helpPage.navigateBackwardsByUsingBrowserBackButton();
        importStudyPage.verifyImportStudyPageNavigation();
        importStudyPage.closeImportStudyPage();

        Logger.endTestCase("checkBrowserBackButtonTakesUserToImportStudyPage ");

    }

    /***
     * Test case to check if a folder contains BDF & TEV with same name but different case, it is accepted for import
     */
    @Test(priority = 51)
    public void TC47_checkFolderAcceptedForImport_ForBDFAndTEVWithSameNameButDifferentCase() {
        CommonUtility.logCreateTest("TC47_checkFolderAcceptedForImport_ForBDFAndTEVWithSameNameButDifferentCase");

        importStudyPage = PagesFactory.getImportStudyPage();

        navigationToImportStudyPageAndVerification(dateTime);
        importStudyPage.uploadFilesForImportStudy("ImportValidation_Studies\\ET-4391\\BDF with TEV");
        importStudyPage.verifyPatientAndStudyInformationIsDisplayed();
        importStudyPage.closeImportStudyPage();

        Logger.endTestCase("checkFolderAcceptedForImport_ForBDFAndTEVWithSameNameButDifferentCase ");

    }

    /***
     * Test case to check if a folder contains BDF & TVX with same name but different case, it is accepted for import
     */
    @Test(priority = 52)
    public void TC48_checkFolderAcceptedForImport_ForBDFAndTVXWithSameNameButDifferentCase() {
        CommonUtility.logCreateTest("TC48_checkFolderAcceptedForImport_ForBDFAndTVXWithSameNameButDifferentCase");

        importStudyPage = PagesFactory.getImportStudyPage();

        navigationToImportStudyPageAndVerification(dateTime);
        importStudyPage.uploadFilesForImportStudy("ImportValidation_Studies\\ET-4391\\BDF with TEV & TVX");
        importStudyPage.verifyPatientAndStudyInformationIsDisplayed();
        importStudyPage.closeImportStudyPage();

        Logger.endTestCase("checkFolderAcceptedForImport_ForBDFAndTVXWithSameNameButDifferentCase ");

    }

    /***
     * Test case to check if a folder contains EDF & TEV with same name but different case, it is accepted for import
     */
    @Test(priority = 53)
    public void TC49_checkFolderAcceptedForImport_ForEDFAndTEVWithSameNameButDifferentCase() {
        CommonUtility.logCreateTest("TC49_checkFolderAcceptedForImport_ForEDFAndTEVWithSameNameButDifferentCase");

        importStudyPage = PagesFactory.getImportStudyPage();

        navigationToImportStudyPageAndVerification(dateTime);
        importStudyPage.uploadFilesForImportStudy("ImportValidation_Studies\\ET-4391\\EDF with TEV");
        importStudyPage.verifyPatientAndStudyInformationIsDisplayed();
        importStudyPage.closeImportStudyPage();

        Logger.endTestCase("checkFolderAcceptedForImport_ForEDFAndTEVWithSameNameButDifferentCase ");

    }

    /***
     * Test case to check if a folder contains EDF & TVX with same name but different case, it is accepted for import
     */
    @Test(priority = 54)
    public void TC50_checkFolderAcceptedForImport_ForEDFAndTVXWithSameNameButDifferentCase() {
        CommonUtility.logCreateTest("TC50_checkFolderAcceptedForImport_ForEDFAndTVXWithSameNameButDifferentCase");

        importStudyPage = PagesFactory.getImportStudyPage();

        navigationToImportStudyPageAndVerification(dateTime);
        importStudyPage.uploadFilesForImportStudy("ImportValidation_Studies\\ET-4391\\EDF with TEV & TVX");
        importStudyPage.verifyPatientAndStudyInformationIsDisplayed();
        importStudyPage.closeImportStudyPage();

        Logger.endTestCase("checkFolderAcceptedForImport_ForEDFAndTVXWithSameNameButDifferentCase ");

    }

    /***
     * Test case to check warning is displayed for patient name not matching with BDF header
     */
    @Test(priority = 55)
    public void TC51_checkWarningDisplayedForPatientNameNotMatchingWithBDF() {
        CommonUtility.logCreateTest("TC51_checkWarningDisplayedForPatientNameNotMatchingWithBDF");

        importStudyPage = PagesFactory.getImportStudyPage();

        navigationToImportStudyPageAndVerification(dateTime);
        importStudyPage.uploadFilesForImportStudy("ImportValidation_Studies\\WarningMessageForPatientNameMisMatch\\BDF with TEV");
        importStudyPage.verifyPatientAndStudyInformationIsDisplayed();
        importStudyPage.verifyWarningForPatientNameNotMatchingWithBDF();
        importStudyPage.closeImportStudyPage();

        Logger.endTestCase("checkWarningDisplayedForPatientNameNotMatchingWithBDF ");


    }

    /***
     * Test case to check warning is displayed for patient name not matching with EDF header
     */
    @Test(priority = 56)
    public void TC52_checkWarningDisplayedForPatientNameNotMatchingWithEDF() {
        CommonUtility.logCreateTest("TC52_checkWarningDisplayedForPatientNameNotMatchingWithEDF");

        importStudyPage = PagesFactory.getImportStudyPage();

        navigationToImportStudyPageAndVerification(dateTime);
        importStudyPage.uploadFilesForImportStudy("ImportValidation_Studies\\WarningMessageForPatientNameMisMatch\\EDF with TEV");
        importStudyPage.verifyPatientAndStudyInformationIsDisplayed();
        importStudyPage.verifyWarningForPatientNameNotMatchingWithEDF();
        importStudyPage.closeImportStudyPage();

        Logger.endTestCase("checkWarningDisplayedForPatientNameNotMatchingWithEDF ");

    }

    /***
     * Test case to check that a folder is not rejected for import if an EDF file does not contain any TVX file
     */
    @Test(priority = 57)
    public void TC319_checkFolderIsAcceptedForImportIfEDFFileDoesNotHaveAnyTVXFile() {
        CommonUtility.logCreateTest("TC319_checkFolderIsAcceptedForImportIfEDFFileDoesNotHaveAnyTVXFile");

        importStudyPage = PagesFactory.getImportStudyPage();

        navigationToImportStudyPageAndVerification(dateTime);
        importStudyPage.uploadFilesForImportStudy("ImportValidation_Studies\\ET - 4280\\EDF\\EDF with same TEV");
        importStudyPage.verifyPatientAndStudyInformationIsDisplayed();
        importStudyPage.closeImportStudyPage();

    }

    /***
     * Test case to check that a folder is not rejected for import if multiple BDF files does not contain any TVX file
     */
    @Test(priority = 57)
    public void TC320_checkFolderIsAcceptedForImportIfBDFFilesDoesNotHaveAnyTVXFile() {
        CommonUtility.logCreateTest("TC320_checkFolderIsAcceptedForImportIfBDFFilesDoesNotHaveAnyTVXFile");

        importStudyPage = PagesFactory.getImportStudyPage();

        navigationToImportStudyPageAndVerification(dateTime);
        importStudyPage.uploadFilesForImportStudy("ImportValidation_Studies\\ET - 4280\\BDF\\BDF with multiple same TEV");
        importStudyPage.verifyPatientAndStudyInformationIsDisplayed();
        importStudyPage.closeImportStudyPage();

    }

    /***
     * To delete patient created during test execution
     */
    @Test(priority = 100)
    public void afterTest_patientDeletion() {
        CommonUtility.logCreateTest("afterTest_patientDeletion");
        dataDeletion.patientDeletion("yes", dateTime);

    }

}
