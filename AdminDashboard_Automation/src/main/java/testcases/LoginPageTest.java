package testcases;

import browserutility.Browser;
import dataprovider.DataProviderClass;
import helper.CommonUtility;
import helper.ConfigReader;
import helper.Logger;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import pages.PagesFactory;
import pages.ui.PageClasses.CreateEditPages.UserCreatePage;
import pages.ui.PageClasses.DashboardPage;
import pages.ui.PageClasses.HeaderPanel;
import pages.ui.PageClasses.ListPages.EmailService;
import pages.ui.PageClasses.ListPages.UserPage;
import pages.ui.PageClasses.LoginPage;
import pages.ui.PageClasses.MenuSidebarPage;
import java.io.IOException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class LoginPageTest extends BaseTest{
    LoginPage loginPage;
    DashboardPage dashboardPage;
    HeaderPanel headerPanel;
    MenuSidebarPage menuSidebarPage;
    UserPage userPage;
    UserCreatePage userCreatePage;
    String SuperAdminRole;
    String SuperAdminEmail;
    String SuperAdminPassword;

    Format f = new SimpleDateFormat("MM-dd-yyyy_hh:mm:ss");
    String dateTime = f.format(new Date());

    DataDeletion dataDeletion = new DataDeletion();
    EmailService emailService = new EmailService();
    ConfigReader configReader = new ConfigReader();
    String newPassword = "Enosis123New";
    String password = "Enosis123";
    String accountRecoveryLink = "";


    /***
     * Getting credentials for SuperAdmin
     * @param userRole User Role
     * @param email User Email
     * @param password User Password
     */
    @Test(dataProvider = "getSuperAdminCredentials", dataProviderClass = DataProviderClass.class, priority = 1)
    public void getCredentialsForSuperAdmin(String userRole, String email, String password) {
        CommonUtility.logCreateTest("getCredentialsForSuperAdmin");
        SuperAdminRole = userRole;
        SuperAdminEmail = email;
        SuperAdminPassword = password;

    }

    /***
     * Implementing test case to validate login functionality
     */
    @Test(priority = 2)
    public void TC53_checkUserSuccessfullyLogsInForValidEmailValidPassword(){
        CommonUtility.logCreateTest("TC53_checkUserSuccessfullyLogsInForValidEmailValidPassword");

        loginPage = PagesFactory.getLoginPage();
        dashboardPage = PagesFactory.getDashboardPage();
        headerPanel = PagesFactory.getheaderPanel();

        loginPage.verifyLoginPage();
        loginPage.typeUserName(SuperAdminEmail);
        loginPage.typePassword(SuperAdminPassword);
        CommonUtility.logMessagesAndAddThemToReport("User Credentials Given", "info");
        loginPage.clickLoginButton();
        CommonUtility.saveLoggedInUserDetailsToAFile(SuperAdminEmail, SuperAdminPassword);

        dashboardPage.verifyDashboardPageNavigation();

        headerPanel.signOut();
        Logger.endTestCase("Login_functionality");

    }

    /***
     * Implementing test case to validate invalid login credentials
     */
    @Test(priority = 3)
    public void TC54_checkUserNotAllowedToLoginWithInvalidEmailInvalidPassword(){
        CommonUtility.logCreateTest("TC54_checkUserNotAllowedToLoginWithInvalidEmailInvalidPassword");

        loginPage = PagesFactory.getLoginPage();

        loginPage.verifyLoginPage();
        loginPage.typeUserName("rendrsupesdfwer@yahoo.com");
        loginPage.typePassword("Enosisadsads123");
        CommonUtility.logMessagesAndAddThemToReport("User Credentials Given", "info");
        loginPage.clickLoginButton();
        loginPage.checkInvalidCredentialsMessage();
        Logger.endTestCase("InvalidLoginFunctionality");

    }

    /***
     * Test case to check if user is disallowed to login with invalid email and valid password
     */
    @Test(priority = 4)
    public void TC55_checkUserNotAllowedToLoginWithInvalidEmailValidPassword() {

        CommonUtility.logCreateTest("TC55_checkUserNotAllowedToLoginWithInvalidEmailValidPassword");

        loginPage = PagesFactory.getLoginPage();

        loginPage.verifyLoginPage();
        loginPage.typeUserName("rendrsuper123@yahoo.com");
        loginPage.typePassword(SuperAdminPassword);
        loginPage.clickLoginButton();
        loginPage.checkInvalidCredentialsMessage();
        Logger.endTestCase("checkUserNotAllowedToLoginWithInvalidEmailValidPassword");

    }

    /***
     * Test case to check if user is disallowed to login with valid email and invalid password
     */
    @Test(priority = 5)
    public void TC56_checkUserNotAllowedToLoginWithValidEmailInvalidPassword() {

        CommonUtility.logCreateTest("TC56_checkUserNotAllowedToLoginWithValidEmailInvalidPassword");

        loginPage = PagesFactory.getLoginPage();

        loginPage.verifyLoginPage();
        loginPage.typeUserName(SuperAdminEmail);
        loginPage.typePassword("ENosis123as");
        loginPage.clickLoginButton();
        loginPage.checkInvalidCredentialsMessage();
        Logger.endTestCase("checkUserNotAllowedToLoginWithValidEmailInvalidPassword");

    }

    /***
     * Test case to check if client side error messages are shown for empty email and password fields
     */
    @Test(priority = 6)
    public void TC57_checkErrorMessagesDisplayedForEmptyEmailAndPassword() {
        CommonUtility.logCreateTest("TC57_checkErrorMessagesDisplayedForEmptyEmailAndPassword");

        loginPage = PagesFactory.getLoginPage();

        loginPage.verifyLoginPage();
        loginPage.typeUserName("");
        loginPage.typePassword("");
        loginPage.clickLoginButton();
        loginPage.verifyClientSideErrorMessageForInvalidEmail();
        loginPage.verifyClientSideErrorMessageForInvalidPassword();
        Logger.endTestCase("checkErrorMessagesDisplayedForEmptyEmailAndPassword");

    }

    /***
     * Test case to check if client side error message is shown for invalid email
     */
    @Test(priority = 7)
    public void TC58_checkErrorMessageDisplayedForInvalidEmail() {
        CommonUtility.logCreateTest("TC58_checkErrorMessageDisplayedForInvalidEmail");

        loginPage = PagesFactory.getLoginPage();

        loginPage.verifyLoginPage();
        loginPage.typeUserName("test123456");
        loginPage.typePassword(SuperAdminPassword);
        loginPage.clickLoginButton();
        loginPage.verifyClientSideErrorMessageForInvalidEmail();
        Logger.endTestCase("checkErrorMessageDisplayedForInvalidEmail");

    }

    /***
     * Test case to check if rendr hyperlink on the bottom page works or not
     */
    @Test(priority = 8)
    public void TC59_checkRendrHyperLink() throws IOException {
        String rendrUrl = configReader.rendrUrl;

        CommonUtility.logCreateTest("TC59_checkRendrHyperLink");

        loginPage = PagesFactory.getLoginPage();

        loginPage.verifyLoginPage();
        loginPage.verifyClickOnRendrHyperLink();
        loginPage.changeTabFocus(1);
        loginPage.verifyCurrentUrlMatchesRendrUrl(rendrUrl);
        loginPage.changeTabFocus(0);

    }

    /***
     * Test Case to check that password is not visible
     */
    @Test(priority = 9)
    public void TC60_checkPasswordIsNotVisible() {
        CommonUtility.logCreateTest("TC60_checkPasswordIsNotVisible");

        loginPage = PagesFactory.getLoginPage();
        loginPage.checkPasswordNotVisible();
        Logger.endTestCase("checkPasswordIsNotVisible");

    }

    /***
     * Test Case to check password is visible after clicking on show password button
     */
    @Test(priority = 10)
    public void checkPasswordIsVisibleAfterClickingOnShowPasswordButton() {
        CommonUtility.logCreateTest("TC61_checkPasswordIsVisibleAfterClickingOnShowPasswordButton");

        loginPage = PagesFactory.getLoginPage();
        loginPage.showPasswordButtonClick();
        loginPage.checkPasswordIsVisible();
        Logger.endTestCase("checkPasswordIsVisibleAfterClickingOnShowPasswordButton");

    }

    /***
     * Test case to check password is not visible after clicking on the hide password button
     */
    @Test(priority = 11)
    public void checkPasswordNotVisibleAfterClickingHidePasswordButton() {
        CommonUtility.logCreateTest("TC62_checkPasswordNotVisibleAfterClickingHidePasswordButton");

        loginPage = PagesFactory.getLoginPage();
        loginPage.hidePasswordButtonClick();
        loginPage.checkPasswordNotVisible();
        Logger.endTestCase("checkPasswordNotVisibleAfterClickingHidePasswordButton");

    }

    /***
     * Test case to check that the rendr icon is disabled
     */
    @Test(priority = 12)
    public void TC63_checkRendrIconIsDisabled() {
       CommonUtility.logCreateTest("TC63_checkRendrIconIsDisabled");

       loginPage = PagesFactory.getLoginPage();
       loginPage.checkRendrIconDisabled();
       Logger.endTestCase("checkRendrIconIsDisabled");

    }

    /***
     * Logging in as SuperAdmin so that a user can be created
     */
    @Test(priority = 13)
    public void loginWithSuperAdminRole(){
        CommonUtility.logCreateTest("loginWithSuperAdminRole");

        loginPage = PagesFactory.getLoginPage();

        loginPage.verifyLoginPage();
        dashboardPage = PagesFactory.getDashboardPage();

        loginPage.typeUserName(SuperAdminEmail);
        loginPage.typePassword(SuperAdminPassword);
        loginPage.clickLoginButton();


        dashboardPage.verifyDashboardPageNavigation();

    }

    /***
     * Creating a user that can be used to test the forgot password functionality
     */
    @Test(priority = 14)
    public void createUser() throws IOException {
        CommonUtility.logCreateTest("createUser");
        String forgotPasswordEmail = configReader.forgotPasswordEmail;

        dashboardPage = PagesFactory.getDashboardPage();
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        userPage = PagesFactory.getUserPage();
        userCreatePage = PagesFactory.getUserCreatePage();
        headerPanel = PagesFactory.getheaderPanel();

        dashboardPage.verifyDashboardPageNavigation();

        menuSidebarPage.goToUsersPage();

        userPage.verifyUserPageNavigation();
        userPage.goToCreateUserPage();

        userCreatePage.verifyCreateUserPageNavigation();
        userCreatePage.createUser("yes", "", dateTime + "Enosis", "", dateTime + "MMT", "", forgotPasswordEmail, "Enosis123", "Enosis123", "SuperAdmin", "User created!");

        headerPanel.signOut();
        loginPage.verifyLoginPage();

    }

    /***
     * Test case to check if forgot password link is visible
     */
    @Test(priority = 15)
    public void TC64_checkForgotPasswordLinkIsVisible() {
        CommonUtility.logCreateTest("TC64_checkForgotPasswordLinkIsVisible");

        loginPage = PagesFactory.getLoginPage();
        loginPage.verifyForgotPasswordLinkVisible();
        Logger.endTestCase("checkForgotPasswordLinkIsVisible");

    }

    /***
     * Test case to verify Recover Your Account page navigation
     */
    @Test(priority = 16)
    public void TC65_checkNavigationToRecoverYourAccountPage() {
        CommonUtility.logCreateTest("TC65_checkNavigationToRecoverYourAccountPage");

        loginPage = PagesFactory.getLoginPage();
        loginPage.clickForgotPasswordLink();
        loginPage.verifyForgotPasswordPage();
        Logger.endTestCase("checkNavigationToRecoverYourAccountPage");

    }

    /***
     * Test case to check send reset button is disabled for invalid email
     */
    @Test(priority = 17)
    public void TC66_checkSendResetButtonIsDisabledForInvalidEmail() {
        CommonUtility.logCreateTest("TC66_checkSendResetButtonIsDisabledForInvalidEmail");

        loginPage = PagesFactory.getLoginPage();
        loginPage.verifyForgotPasswordPage();
        loginPage.typeEmailAddress("");
        loginPage.verifySendResetButtonDisabled("Send Reset Button disabled for empty email field");
        loginPage.typeEmailAddress("testtest");
        loginPage.verifySendResetButtonDisabled("Send Reset Button disabled for invalid email");
        Logger.endTestCase("checkSendResetButtonIsDisabledForInvalidEmail");

    }

    /***
     * Test case to send email to the user who forgot their password
     */
    @Test(priority = 18)
    public void checkSuccessfulMessageAppearsForClickingSendReset() {
        CommonUtility.logCreateTest("TC67_checkSuccessfulMessageAppearsForClickingSendReset");
        String forgotPasswordEmail = configReader.forgotPasswordEmail;

        loginPage = PagesFactory.getLoginPage();
        loginPage.verifyForgotPasswordPage();
        loginPage.typeEmailAddress(forgotPasswordEmail);
        loginPage.sendResetButtonClick();
        loginPage.verifySuccessfulMessageForForgotPassword(forgotPasswordEmail);
        Logger.endTestCase("checkSuccessfulMessageAppearsForClickingSendReset");

    }

    /***
     * Test case to check if an email is generated to the user
     */
    @Test(priority = 19)
    public void TC68_checkIfEmailIsGeneratedToUser() throws InterruptedException {
        CommonUtility.logCreateTest("TC68_checkIfEmailIsGeneratedToUser");
        String forgotPasswordEmail = configReader.forgotPasswordEmail;

        TimeUnit.SECONDS.sleep(20);
        String messageID = emailService.getMessageID(forgotPasswordEmail, "account recovery");
        String messageBody = emailService.getPlainTextMessage(forgotPasswordEmail, messageID);
        Boolean checkSubString = messageBody.contains("You have requested to reset your password.");
        Assert.assertTrue(checkSubString, "The message body did not have the expected string");
        Logger.endTestCase("checkIfEmailIsGeneratedToUser");

    }

    /***
     * Test case to check if user is navigated to Account Recovery page after clicking on the link
     */
    @Test(priority = 20)
    public void TC69_checkIfUserIsNavigatedToAccountRecoveryPage() {
        CommonUtility.logCreateTest("TC69_checkIfUserIsNavigatedToAccountRecoveryPage");
        String forgotPasswordEmail = configReader.forgotPasswordEmail;

        String messageID = emailService.getMessageID(forgotPasswordEmail, "account recovery");
        accountRecoveryLink = emailService.getAccountRecoveryLink(forgotPasswordEmail, messageID);
        Browser.goToUrl(accountRecoveryLink);
        loginPage = PagesFactory.getLoginPage();

        loginPage.verifyAccountRecoveryPageNavigation();
        Logger.endTestCase("checkIfUserIsNavigatedToAccountRecoveryPage");

    }

    /***
     * Test case to check if user can reset his password
     */
    @Test(priority = 21)
    public void TC70_checkIfUserCanResetHisPassword() throws InterruptedException {
        CommonUtility.logCreateTest("TC70_checkIfUserCanResetHisPassword");

        loginPage = PagesFactory.getLoginPage();

        loginPage.verifyAccountRecoveryPageNavigation();
        loginPage.typePassword(newPassword);
        loginPage.typeConfirmPassword(newPassword);
        loginPage.clickOnResetPasswordButton();
        loginPage.verifySuccessfulResetPasswordPageNavigation();
        TimeUnit.SECONDS.sleep(20);

        Logger.endTestCase("checkIfUserCanResetHisPassword");

    }

    /***
     * Test case to check if user can log in with new password
     */
    @Test(priority = 22)
    public void TC71_checkUserCanLogInWithNewPassword() {
        CommonUtility.logCreateTest("TC71_checkUserCanLogInWithNewPassword");
        String forgotPasswordEmail = configReader.forgotPasswordEmail;
        String url = configReader.url;

        loginPage = PagesFactory.getLoginPage();
        dashboardPage = PagesFactory.getDashboardPage();
        headerPanel = PagesFactory.getheaderPanel();

        Browser.goToUrl(url);
        loginPage.typeUserName(forgotPasswordEmail);
        loginPage.typePassword(newPassword);
        loginPage.clickLoginButton();

        dashboardPage.verifyDashboardPageNavigation();
        headerPanel.signOut();
        loginPage.verifyLoginPage();
        Logger.endTestCase("checkUserCanLogInWithNewPassword");

    }

    /***
     * Test Case to check the link does not exist after the user has reset his/her password
     */
    @Test(priority = 23)
    public void TC72_checkLinkDoesNotExistAfterResettingPassword() {
        CommonUtility.logCreateTest("TC72_checkLinkDoesNotExistAfterResettingPassword");

        Browser.goToUrl(accountRecoveryLink);
        loginPage = PagesFactory.getLoginPage();
        loginPage.verifyAccountRecoveryPageIsNotVisible();

    }


    /***
     * Test Case to check if user is navigated to dashboard when the user logs in from a different tab
     */
    @Test(priority = 24)
    public void TC73_checkUserGetsLoggedInDifferentTab() throws IOException {
        CommonUtility.logCreateTest("TC73_checkUserGetsLoggedInDifferentTab");
        String url = configReader.url;

        Browser.goToUrl(url);
        loginPage = PagesFactory.getLoginPage();
        loginPage.verifyLoginPage();
        loginPage.openLoginScreenInNewTab(url);
        loginPage.changeTabFocus(0);
        loginPage.typeUserName(SuperAdminEmail);
        loginPage.typePassword(SuperAdminPassword);
        loginPage.clickLoginButton();
        loginPage.changeTabFocus(1);
        loginPage.verifyLoginPageNotVisible();

        dashboardPage = PagesFactory.getDashboardPage();
        dashboardPage.verifyDashboardPageNavigation();
        dashboardPage.closeCurrentTab();
        System.out.println("here!");
        Logger.endTestCase("checkUserGetsLoggedInDifferentTab");

    }

    /***
     * Deleting the user that has been created
     */
    @Test(priority = 100)
    public void afterTest_userDeletion() {
        CommonUtility.logCreateTest("userDeletion");

        dashboardPage = PagesFactory.getDashboardPage();
        dashboardPage.changeTabFocus(0);
        dashboardPage.verifyDashboardPageNavigation();
        dataDeletion.userDeletion("yes", dateTime);

    }

}
