package testcases.PageAccess;

import browserutility.Browser;
import com.gargoylesoftware.htmlunit.Page;
import helper.CommonUtility;
import helper.Logger;
import org.testng.Assert;
import org.testng.Reporter;
import pages.PagesFactory;
import pages.ui.PageClasses.*;
import pages.ui.PageClasses.CreateEditPages.*;
import pages.ui.PageClasses.ListPages.*;
import testcases.BaseTest;

public class BasePageAccess extends BaseTest {

    LoginPage loginPage;
    DashboardPage dashboardPage;
    ProfilePage profilePage;
    MenuSidebarPage menuSidebarPage;
    PatientsPage patientsPage;
    HeaderPanel headerPanel;
    StudiesPage studiesPage;
    SharedUsersPage sharedUsersPage;
    AuditLogsPage auditLogsPage;
    PatientCreatePage patientCreatePage;
    PatientEditPage patientEditPage;
    ImportStudyPage importStudyPage;
    RemoteViewerPage remoteViewerPage;
    FacilityPage facilityPage;
    FacilityCreatePage facilityCreatePage;
    FacilityEditPage facilityEditPage;
    AmplifierPage amplifierPage;
    AmplifierCreatePage amplifierCreatepage;
    AmplifierEditPage amplifierEditPage;
    DevicePage devicePage;
    DeviceCreatePage deviceCreatePage;
    DeviceEditPage deviceEditPage;
    DeviceLogsPage deviceLogsPage;
    ChangeDeviceFacilityPage changeDeviceFacilityPage;
    UserPage userPage;
    UserCreatePage userCreatePage;
    UserEditPage userEditPage;
    UserAuditLogsPage userAuditLogsPage;

    /***
     * Implementing test case to validate Login page
     * @param email
     * @param password
     */
    public void pageAccessScriptForLogin(String email, String password)
    {
        loginPage = PagesFactory.getLoginPage();

        loginPage.verifyLoginPage();
        loginPage.typeUserName(email);
        loginPage.typePassword(password);
        CommonUtility.logMessagesAndAddThemToReport("User Credentials Given", "info");
        CommonUtility.logMessagesAndAddThemToReport("User email : " + email, "info");
        loginPage.clickLoginButton();
        loginPage.loginButtonAppearance();
        CommonUtility.saveLoggedInUserDetailsToAFile(email, password);

    }

    /***
     * Implementing test case to validate user role
     * @param userRole
     * @param dashboard
     */
    public void pageAccessScriptForUserRole(String userRole, String dashboard)
    {
        dashboardPage = PagesFactory.getDashboardPage();
        headerPanel = PagesFactory.getheaderPanel();
        profilePage = PagesFactory.getProfilePage();

        headerPanel.yourProfileButtonClick();

        profilePage.verifyProfilePageNavigation();
        profilePage.verifyUserRole(userRole);

    }

    /***
     * Implementing test case to validate patient module access
     * @param patientsList
     * @param createPatient
     * @param editPatient
     * @param patientDocumentsTab
     * @param patientAuditTrailTab
     */
    public void pageAccessScriptForPatientModule(String patientsList, String createPatient, String editPatient, String patientDocumentsTab, String patientAuditTrailTab, String importStudy)
    {
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        patientsPage = PagesFactory.getPatientsPage();
        patientCreatePage = PagesFactory.getPatientCreatePage();
        patientEditPage = PagesFactory.getPatientEditPage();
        importStudyPage = PagesFactory.getImportStudyPage();

        menuSidebarPage.patientPageVerification(patientsList);

        patientsPage.verifyPatientPageNavigation(patientsList);
        patientsPage.verifyCreateButtonClick(createPatient);

        patientCreatePage.verifyCreatePatientPageNavigation(createPatient);

        menuSidebarPage.patientPageVerification(patientsList);

        patientsPage.verifyPatientPageNavigation(patientsList);
        patientsPage.verifyEditButtonClick(editPatient);

        patientEditPage.verifyEditPatientPageNavigation(editPatient);

        menuSidebarPage.patientPageVerification(patientsList);

        if(patientDocumentsTab.toLowerCase().equals("yes")) {
            patientsPage.verifyPatientDocumentTabIsPresent();
            patientsPage.verifyClickOnPatientDocumentsTab();
        } else if(patientDocumentsTab.toLowerCase().equals("no")) {
            patientsPage.verifyPatientDocumentTabIsNotPresent();
        } else {
            CommonUtility.logMessagesAndAddThemToReport("CSV file input wrong", "fail");
            Assert.fail("CSV file input wrong");
        }

        if(patientAuditTrailTab.toLowerCase().equals("yes")) {
            patientsPage.verifyAuditTrailTabIsPresent();
            patientsPage.verifyClickOnAuditTrailTab();
            patientsPage.verifyClickOnPatientDocumentsPanelInAuditTrailTab();
            patientsPage.verifyClickOnPatientRecordsPanel();
        } else if(patientAuditTrailTab.toLowerCase().equals("no")) {
            patientsPage.verifyAuditTrailTabIsNotPresent();
        } else {
            CommonUtility.logMessagesAndAddThemToReport("CSV file input wrong", "fail");
            Assert.fail("CSV file input wrong");
        }

        patientsPage.verifyClickOnImportIcon(importStudy);
        importStudyPage.verifyImportStudyPageNavigation(importStudy);
        importStudyPage.closeImportStudyPage(importStudy);

    }

    /***
     * Implementing test case to validate study module access
     * @param studiesList
     * @param openStudy
     */
    public void pageAccessScriptForStudyModule(String studiesList, String openStudy, String auditLogs, String allAuditLogsPage)
    {
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        studiesPage = PagesFactory.getStudiesPage();
        remoteViewerPage = PagesFactory.getRemoteViewerPage();
        auditLogsPage = PagesFactory.getAuditLogsPage();
        sharedUsersPage = PagesFactory.getSharedUsersPage();

        menuSidebarPage.studyPageVerification(studiesList);

        studiesPage.verifyStudiesPageNavigation(studiesList);
        studiesPage.verifyOpenStudyButtonClick(openStudy);

        remoteViewerPage.verifyRemoteViewerNavigation(openStudy);
        remoteViewerPage.goBackToStudiesPage(openStudy);

        studiesPage.verifyStudiesPageNavigation(studiesList);

        studiesPage.verifyAuditLogsButtonClick(auditLogs);
        auditLogsPage.verifyAuditLogsPageNavigation(auditLogs);
        auditLogsPage.clickOnAllAuditLogsButton(allAuditLogsPage);
        auditLogsPage.allAuditLogsPageNavigation(allAuditLogsPage);

    }

    /***
     * Implementing test case to validate facility module access
     * @param facilitiesList
     * @param createFacility
     * @param editFacility
     */
    public void pageAccessScriptForFacilityModule(String facilitiesList, String createFacility, String editFacility)
    {
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        facilityPage = PagesFactory.getFacilityPage();
        facilityCreatePage = PagesFactory.getFacilityCreatePage();
        facilityEditPage = PagesFactory.getFacilityEditPage();

        menuSidebarPage.facilityPageVerification(facilitiesList);

        facilityPage.verifyFacilityPageNavigation(facilitiesList);
        facilityPage.verifyCreateButtonClick(createFacility);

        facilityCreatePage.verifyCreateFacilityPageNavigation(createFacility);

        menuSidebarPage.facilityPageVerification(facilitiesList);

        facilityPage.verifyFacilityPageNavigation(facilitiesList);
        facilityPage.verifyEditButtonClick(editFacility);

        facilityEditPage.verifyEditFacilityPageNavigation(editFacility);

    }

    /***
     * Implementing test case to validate amplifier module access
     * @param amplifiersList
     * @param createAmplifier
     * @param editAmplifier
     */
    public void pageAccessScriptForAmplifierModule(String amplifiersList, String createAmplifier, String editAmplifier)
    {
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        amplifierPage = PagesFactory.getAmplifierPage();
        amplifierCreatepage = PagesFactory.getAmplifierCreatePage();
        amplifierEditPage = PagesFactory.getAmplifierEditPage();

        menuSidebarPage.amplifierPageVerification(amplifiersList);

        amplifierPage.verifyAmplifierPageNavigation(amplifiersList);
        amplifierPage.verifyCreateButtonClick(createAmplifier);

        amplifierCreatepage.verifyCreateAmplifierPageNavigation(createAmplifier);

        menuSidebarPage.amplifierPageVerification(amplifiersList);

        amplifierPage.verifyAmplifierPageNavigation(amplifiersList);
        amplifierPage.verifyEditButtonClick(editAmplifier);

        amplifierEditPage.verifyEditAmplifierPageNavigation(editAmplifier);

    }

    /***
     * Implementing test case to validate device module access
     * @param devicesList
     * @param createDevice
     * @param editDevice
     */
    public void pageAccessScriptForDeviceModule(String devicesList, String createDevice, String editDevice, String deviceLogs, String changeDeviceFacility)
    {
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        devicePage = PagesFactory.getDevicePage();
        deviceCreatePage = PagesFactory.getCreateDevicePage();
        deviceEditPage = PagesFactory.getEditDevicePage();
        deviceLogsPage = PagesFactory.getDeviceLogsPage();
        changeDeviceFacilityPage = PagesFactory.getChangeDeviceFacilityPage();

        menuSidebarPage.devicePageVerification(devicesList);

        devicePage.verifyDevicesPageNavigation(devicesList);
        devicePage.verifyCreateButtonClick(createDevice);

        deviceCreatePage.verifyCreateDevicePageNavigation(createDevice);

        menuSidebarPage.devicePageVerification(devicesList);

        devicePage.verifyDevicesPageNavigation(devicesList);
        devicePage.verifyEditButtonClick(editDevice);

        deviceEditPage.verifyEditDevicePageNavigation(editDevice);

        menuSidebarPage.devicePageVerification(devicesList);
        devicePage.verifyDevicesPageNavigation(devicesList);
        devicePage.verifyDeviceLogsPage(deviceLogs);
        deviceLogsPage.verifyDeviceLogsPageNavigation(deviceLogs);

        menuSidebarPage.devicePageVerification(devicesList);
        devicePage.verifyDevicesPageNavigation(devicesList);

    }

    /***
     * Implementing test case to validate user module access
     * @param usersList
     * @param createUser
     * @param editUser
     */
    public void pageAccessScriptForUserModule(String usersList, String createUser, String editUser, String userAuditLogs)
    {
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        userPage = PagesFactory.getUserPage();
        userCreatePage = PagesFactory.getUserCreatePage();
        userEditPage = PagesFactory.getUserEditPage();
        userAuditLogsPage = PagesFactory.getUserAuditLogsPage();

        menuSidebarPage.userPageVerification(usersList);

        userPage.verifyUserPageNavigation(usersList);
        userPage.verifyCreateButtonClick(createUser);

        userCreatePage.verifyCreateUserPageNavigation(createUser);

        menuSidebarPage.userPageVerification(usersList);

        userPage.verifyUserPageNavigation(usersList);
        userPage.verifyEditButtonClick(editUser);

        userEditPage.verifyEditUserPageNavigation(editUser);

        menuSidebarPage.userPageVerification(usersList);
        userPage.verifyUserPageNavigation(usersList);
        userPage.verifyAuditLogsButtonClick(userAuditLogs);
        userAuditLogsPage.verifyUserAuditLogsPageNavigation(userAuditLogs);

    }

    /***
     * Method to verify that user was navigated to user audit logs page for current user
     * @param monitoredStudies (yes/no) if user has access ot monitored studies/ user audit logs page
     */
    public void pageAccessScriptForMonitoredStudiesPage(String monitoredStudies)
    {
        headerPanel = PagesFactory.getheaderPanel();
        profilePage = PagesFactory.getProfilePage();
        userAuditLogsPage = PagesFactory.getUserAuditLogsPage();

        headerPanel.yourProfileButtonClick();

        profilePage.verifyProfilePageNavigation();
        String userLastName = profilePage.getLastNameOfTheCurrentlyLoggedInUser();
        profilePage.clickOnMonitoredStudiesButton(monitoredStudies);
        userAuditLogsPage.verifyNavigationToUserAuditLogsPageOfCurrentUser(userLastName, monitoredStudies);

    }


    /***
     * Implementing test case to validate dashboard element access
     * @param dashboard
     * @param infoBox
     * @param latestStudies
     */
    public void pageAccessScriptForDashboard(String dashboard, String infoBox, String latestStudies)
    {
        dashboardPage = PagesFactory.getDashboardPage();
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        headerPanel = PagesFactory.getheaderPanel();

        menuSidebarPage.dashboardPageVerification(dashboard);

        headerPanel.selectFacilityFromDropdown("Lifelines Neuro Company");

        dashboardPage.verifyDashboardPageNavigation();
        dashboardPage.verifyInfoBox(infoBox);
        dashboardPage.verifyLatestStudiesCard(latestStudies);

    }

}
