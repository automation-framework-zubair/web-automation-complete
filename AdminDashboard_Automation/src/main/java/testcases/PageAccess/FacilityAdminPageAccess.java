package testcases.PageAccess;

import dataprovider.DataProviderClass;
import helper.CommonUtility;
import helper.Logger;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import pages.ui.BaseClass;
import testcases.ActionAccess.BaseActionAccess;

public class FacilityAdminPageAccess extends BasePageAccess {

    String userRoleValue;
    String emailValue;
    String passwordValue;
    String dashboardValue;
    String infoBoxValue;
    String latestStudiesValue;
    String patientsListValue;
    String createPatientValue;
    String editPatientValue;
    String importStudyValue;
    String patientDocumentsTabValue;
    String patientAuditTrailTabValue;
    String createDeviceValue;
    String editDeviceValue;
    String devicesListValue;
    String deviceLogsValue;
    String changeDeviceFacilityValue;
    String amplifiersListValue;
    String createAmplifierValue;
    String editAmplifierValue;
    String facilitiesListValue;
    String createFacilityValue;
    String editFacilityValue;
    String studiesListValue;
    String openStudyValue;
    String auditLogsValue;
    String allAuditLogsValue;
    String usersListValue;
    String createUserValue;
    String editUserValue;
    String userAuditLogsValue;

    /***
     * Getting credentials for FacilityAdmin
     * @param userRole User Role
     * @param email User Email
     * @param password User Password
     */
    @Test(dataProvider = "getFacilityAdminCredentials", dataProviderClass = DataProviderClass.class, priority = 1)
    public void getCredentialsForFacilityAdmin(String userRole, String email, String password) {
        CommonUtility.logCreateTest("getCredentialsForFacilityAdmin");
        userRoleValue = userRole;
        emailValue = email;
        passwordValue = password;

    }

    /***
     * Getting data from csv file for facilityAdmin
     * parameters : each page & action access value
     */
    @Test(dataProvider = "getFacilityAdminData", dataProviderClass = DataProviderClass.class, priority = 2)
    public void getDataForFacilityAdmin(String userRole, String dashboard, String infoBox, String latestStudies,
                                        String patientsList, String createPatient, String editPatient, String importStudy, String patientDocumentsTab, String patientAuditTrailTab,
                                        String createDevice, String editDevice, String devicesList, String deviceLogs, String changeDeviceFacility, String amplifiersList, String createAmplifier,
                                        String editAmplifier, String facilitiesList, String createFacility, String editFacility,
                                        String studiesList, String openStudy, String auditLogs, String allAuditLogs, String usersList, String createUser, String editUser, String userAuditLogs) {
        CommonUtility.logCreateTest("getDataForFacilityAdmin");
        userRoleValue = userRole;
        dashboardValue = dashboard;
        infoBoxValue = infoBox;
        latestStudiesValue = latestStudies;
        patientsListValue = patientsList;
        createPatientValue = createPatient;
        editPatientValue = editPatient;
        importStudyValue = importStudy;
        patientDocumentsTabValue = patientDocumentsTab;
        patientAuditTrailTabValue = patientAuditTrailTab;
        createDeviceValue = createDevice;
        editDeviceValue = editDevice;
        devicesListValue = devicesList;
        deviceLogsValue = deviceLogs;
        changeDeviceFacilityValue = changeDeviceFacility;
        amplifiersListValue = amplifiersList;
        createAmplifierValue = createAmplifier;
        editAmplifierValue = editAmplifier;
        facilitiesListValue = facilitiesList;
        createFacilityValue = createFacility;
        editFacilityValue = editFacility;
        studiesListValue = studiesList;
        openStudyValue = openStudy;
        auditLogsValue = auditLogs;
        allAuditLogsValue = allAuditLogs;
        usersListValue = usersList;
        createUserValue = createUser;
        editUserValue = editUser;
        userAuditLogsValue = userAuditLogs;

    }

    /***
     * Implementing login and user role verification for facilityAdmin
     * Passing email and password to BasePageAccess method pageAccessScriptForLogin
     * Passing user role and dashboard authorization value to BasePageAcces method pageAccessScriptForUserRole
     */
    @Test(priority = 3)
    public void facilityAdminUser_LoginRoleVerification() {
        CommonUtility.logCreateTest("TC124_facilityAdminUser_LoginRoleVerification");
        pageAccessScriptForLogin(emailValue, passwordValue);
        pageAccessScriptForUserRole(userRoleValue, dashboardValue);

    }

    /***
     * Implementing dashboard element access verification for facilityAdmin
     * Passing dashboard authorization value, infoBox value, latestStudies value to BasePageAccess method pageAccessScriptForDashboard
     */
    @Test(priority = 4)
    public void TC125_facilityAdminUser_DashboardVerification() {
        CommonUtility.logCreateTest("TC125_facilityAdminUser_DashboardVerification");
        pageAccessScriptForDashboard(dashboardValue, infoBoxValue, latestStudiesValue);

    }

    /***
     * Implementing patient module access authorization for facilityAdmin
     * Passing patient list value, create patient value, edit patient value to BasePageAccess method pageAccessScriptForPatientModule
     */
    @Test(priority = 5)
    public void TC126_facilityAdminUser_PatientModule() {
        CommonUtility.logCreateTest("TC126_facilityAdminUser_PatientModule");
        pageAccessScriptForPatientModule(patientsListValue, createPatientValue, editPatientValue, patientDocumentsTabValue, patientAuditTrailTabValue, importStudyValue);

    }

    /***
     * Implementing study module access authorization for facilityAdmin
     * Passing study list value, open study value to BasePageAccess method pageAccessScriptForStudyModule
     */
    @Test(priority = 6)
    public void TC127_facilityAdminUser_StudyModule() {
        CommonUtility.logCreateTest("TC127_facilityAdminUser_StudyModule");
        pageAccessScriptForStudyModule(studiesListValue, openStudyValue, auditLogsValue, allAuditLogsValue);

    }

    /***
     * Implementing facility module access authorization for facilityAdmin
     * Passing facility list value, create facility value, edit facility value to BasePageAccess method pageAccessScriptForFacilityModule
     */
    @Test(priority = 7)
    public void TC128_facilityAdminUser_FacilityModule() {
        CommonUtility.logCreateTest("TC128_facilityAdminUser_FacilityModule");
        pageAccessScriptForFacilityModule(facilitiesListValue, createFacilityValue, editFacilityValue);

    }

    /***
     * Implementing amplifier module access authorization for facilityAdmin
     * Passing amplifier list value, create amplifier value, edit amplifier value to BasePageAccess method pageAccessScriptForAmplifierModule
     */
    @Test(priority = 8)
    public void TC129_facilityAdminUser_AmplifierModule() {
        CommonUtility.logCreateTest("TC129_facilityAdminUser_AmplifierModule");
        pageAccessScriptForAmplifierModule(amplifiersListValue, createAmplifierValue, editAmplifierValue);

    }

    /***
     * Implementing device module access authorization for facilityAdmin
     * Passing device list value, create device value, edit device value to BasePageAccess method pageAccessScriptForDeviceModule
     */
    @Test(priority = 9)
    public void TC130_facilityAdminUser_DeviceModule() {
        CommonUtility.logCreateTest("TC130_facilityAdminUser_DeviceModule");
        pageAccessScriptForDeviceModule(devicesListValue, createDeviceValue, editDeviceValue, deviceLogsValue, changeDeviceFacilityValue);

    }

    /***
     * Implementing user module access authorization for facilityAdmin
     * Passing user list value, create user value, edit user value to BasePageAccess method pageAccessScriptForUserModule
     */
    @Test(priority = 10)
    public void TC131_facilityAdminUser_UserModule() {
        CommonUtility.logCreateTest("TC131_facilityAdminUser_UserModule");
        pageAccessScriptForUserModule(usersListValue, createUserValue, editUserValue, userAuditLogsValue);
        pageAccessScriptForMonitoredStudiesPage(userAuditLogsValue);

    }

    /***
     * Implementing user sign out for facilityAdmin
     */
    @Test(priority = 30)
    public void facilityAdminUser_SignOut() {
        CommonUtility.logCreateTest("facilityAdminUser_SignOut");
        BaseActionAccess Action = new BaseActionAccess();
        Action.actionAccessScriptForUserSignOut();

    }

}
