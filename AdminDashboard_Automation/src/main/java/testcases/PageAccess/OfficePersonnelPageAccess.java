package testcases.PageAccess;

import dataprovider.DataProviderClass;
import helper.CommonUtility;
import helper.Logger;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import pages.ui.BaseClass;
import testcases.ActionAccess.BaseActionAccess;

public class OfficePersonnelPageAccess extends BasePageAccess {

    String userRoleValue;
    String emailValue;
    String passwordValue;
    String dashboardValue;
    String infoBoxValue;
    String latestStudiesValue;
    String patientsListValue;
    String createPatientValue;
    String editPatientValue;
    String importStudyValue;
    String patientDocumentsTabValue;
    String patientAuditTrailTabValue;
    String createDeviceValue;
    String editDeviceValue;
    String devicesListValue;
    String deviceLogsValue;
    String changeDeviceFacilityValue;
    String amplifiersListValue;
    String createAmplifierValue;
    String editAmplifierValue;
    String facilitiesListValue;
    String createFacilityValue;
    String editFacilityValue;
    String studiesListValue;
    String openStudyValue;
    String auditLogsValue;
    String allAuditLogsValue;
    String usersListValue;
    String createUserValue;
    String editUserValue;
    String userAuditLogsValue;

    /***
     * Getting credentials for OfficePersonnel
     * @param userRole User Role
     * @param email User Email
     * @param password User Password
     */
    @Test(dataProvider = "getOfficePersonnelCredentials", dataProviderClass = DataProviderClass.class, priority = 1)
    public void getCredentialsForOfficePersonnel(String userRole, String email, String password) {
        CommonUtility.logCreateTest("getCredentialsForOfficePersonnel");
        userRoleValue = userRole;
        emailValue = email;
        passwordValue = password;

    }

    /***
     * Getting data from csv file for officePersonnel
     * parameters : each page & action access value
     */
    @Test(dataProvider = "getOfficePersonnelData", dataProviderClass = DataProviderClass.class, priority = 2)
    public void getDataForOfficePersonnel(String userRole, String dashboard, String infoBox, String latestStudies,
                                          String patientsList, String createPatient, String editPatient, String importStudy, String patientDocumentsTab, String patientAuditTrailTab,
                                          String createDevice, String editDevice, String devicesList, String deviceLogs, String changeDeviceFacility, String amplifiersList, String createAmplifier,
                                          String editAmplifier, String facilitiesList, String createFacility, String editFacility,
                                          String studiesList, String openStudy, String auditLogs, String allAuditLogs, String usersList, String createUser, String editUser, String userAuditLogs) {
        CommonUtility.logCreateTest("getDataForOfficePersonnel");
        userRoleValue = userRole;
        dashboardValue = dashboard;
        infoBoxValue = infoBox;
        latestStudiesValue = latestStudies;
        patientsListValue = patientsList;
        createPatientValue = createPatient;
        editPatientValue = editPatient;
        importStudyValue = importStudy;
        patientDocumentsTabValue = patientDocumentsTab;
        patientAuditTrailTabValue = patientAuditTrailTab;
        createDeviceValue = createDevice;
        editDeviceValue = editDevice;
        devicesListValue = devicesList;
        deviceLogsValue = deviceLogs;
        changeDeviceFacilityValue = changeDeviceFacility;
        amplifiersListValue = amplifiersList;
        createAmplifierValue = createAmplifier;
        editAmplifierValue = editAmplifier;
        facilitiesListValue = facilitiesList;
        createFacilityValue = createFacility;
        editFacilityValue = editFacility;
        studiesListValue = studiesList;
        openStudyValue = openStudy;
        auditLogsValue = auditLogs;
        allAuditLogsValue = allAuditLogs;
        usersListValue = usersList;
        createUserValue = createUser;
        editUserValue = editUser;
        userAuditLogsValue = userAuditLogs;

    }

    /***
     * Implementing login and user role verification for officePersonnel
     * Passing email and password to BasePageAccess method pageAccessScriptForLogin
     * Passing user role and dashboard authorization value to BasePageAcces method pageAccessScriptForUserRole
     */
    @Test(priority = 3)
    public void officePersonnelUser_LoginRoleVerification() {
        CommonUtility.logCreateTest("TC156_officePersonnelUser_LoginRoleVerification");
        pageAccessScriptForLogin(emailValue, passwordValue);
        pageAccessScriptForUserRole(userRoleValue, dashboardValue);

    }

    /***
     * Implementing dashboard element access verification for officePersonnel
     * Passing dashboard authorization value, infoBox value, latestStudies value to BasePageAccess method pageAccessScriptForDashboard
     */
    @Test(priority = 4)
    public void TC157_officePersonnelUser_DashboardVerification() {
        CommonUtility.logCreateTest("TC157_officePersonnelUser_DashboardVerification");
        pageAccessScriptForDashboard(dashboardValue, infoBoxValue, latestStudiesValue);

    }

    /***
     * Implementing patient module access authorization for officePersonnel
     * Passing patient list value, create patient value, edit patient value to BasePageAccess method pageAccessScriptForPatientModule
     */
    @Test(priority = 5)
    public void TC158_officePersonnelUser_PatientModule() {
        CommonUtility.logCreateTest("TC158_officePersonnelUser_PatientModule");
        pageAccessScriptForPatientModule(patientsListValue, createPatientValue, editPatientValue, patientDocumentsTabValue, patientAuditTrailTabValue, importStudyValue);

    }

    /***
     * Implementing study module access authorization for officePersonnel
     * Passing study list value, open study value to BasePageAccess method pageAccessScriptForStudyModule
     */
    @Test(priority = 6)
    public void TC159_officePersonnelUser_StudyModule() {
        CommonUtility.logCreateTest("TC159_officePersonnelUser_StudyModule");
        pageAccessScriptForStudyModule(studiesListValue, openStudyValue, auditLogsValue, allAuditLogsValue);

    }

    /***
     * Implementing facility module access authorization for officePersonnel
     * Passing facility list value, create facility value, edit facility value to BasePageAccess method pageAccessScriptForFacilityModule
     */
    @Test(priority = 7)
    public void TC160_officePersonnelUser_FacilityModule() {
        CommonUtility.logCreateTest("TC160_officePersonnelUser_FacilityModule");
        pageAccessScriptForFacilityModule(facilitiesListValue, createFacilityValue, editFacilityValue);

    }

    /***
     * Implementing amplifier module access authorization for officePersonnel
     * Passing amplifier list value, create amplifier value, edit amplifier value to BasePageAccess method pageAccessScriptForAmplifierModule
     */
    @Test(priority = 8)
    public void TC161_officePersonnelUser_AmplifierModule() {
        CommonUtility.logCreateTest("TC161_officePersonnelUser_AmplifierModule");
        pageAccessScriptForAmplifierModule(amplifiersListValue, createAmplifierValue, editAmplifierValue);

    }

    /***
     * Implementing device module access authorization for officePersonnel
     * Passing device list value, create device value, edit device value to BasePageAccess method pageAccessScriptForDeviceModule
     */
    @Test(priority = 9)
    public void TC162_officePersonnelUser_DeviceModule() {
        CommonUtility.logCreateTest("TC162_officePersonnelUser_DeviceModule");
        pageAccessScriptForDeviceModule(devicesListValue, createDeviceValue, editDeviceValue, deviceLogsValue, changeDeviceFacilityValue);

    }

    /***
     * Implementing user module access authorization for officePersonnel
     * Passing user list value, create user value, edit user value to BasePageAccess method pageAccessScriptForUserModule
     */
    @Test(priority = 10)
    public void TC163_officePersonnelUser_UserModule() {
        CommonUtility.logCreateTest("TC163_officePersonnelUser_UserModule");
        pageAccessScriptForUserModule(usersListValue, createUserValue, editUserValue, userAuditLogsValue);
        pageAccessScriptForMonitoredStudiesPage(userAuditLogsValue);

    }

    /***
     * Implementing user sign out for officePersonnel
     */
    @Test(priority = 30)
    public void officePersonnelUser_SignOut() {
        CommonUtility.logCreateTest("officePersonnelUser_SignOut");
        BaseActionAccess Action = new BaseActionAccess();
        Action.actionAccessScriptForUserSignOut();

    }

}
