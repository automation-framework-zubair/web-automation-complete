package testcases.PageAccess;

import dataprovider.DataProviderClass;
import helper.CommonUtility;
import helper.Logger;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import pages.ui.BaseClass;
import testcases.ActionAccess.BaseActionAccess;

public class SupportPageAccess extends BasePageAccess {

    String userRoleValue;
    String emailValue;
    String passwordValue;
    String dashboardValue;
    String infoBoxValue;
    String latestStudiesValue;
    String patientsListValue;
    String createPatientValue;
    String editPatientValue;
    String importStudyValue;
    String patientDocumentsTabValue;
    String patientAuditTrailTabValue;
    String createDeviceValue;
    String editDeviceValue;
    String devicesListValue;
    String deviceLogsValue;
    String changeDeviceFacilityValue;
    String amplifiersListValue;
    String createAmplifierValue;
    String editAmplifierValue;
    String facilitiesListValue;
    String createFacilityValue;
    String editFacilityValue;
    String studiesListValue;
    String openStudyValue;
    String auditLogsValue;
    String allAuditLogsValue;
    String usersListValue;
    String createUserValue;
    String editUserValue;
    String userAuditLogsValue;

    /***
     * Getting credentials for Support
     * @param userRole User Role
     * @param email User Email
     * @param password User Password
     */
    @Test(dataProvider = "getSupportCredentials", dataProviderClass = DataProviderClass.class, priority = 1)
    public void getCredentialsForSupport(String userRole, String email, String password) {
        CommonUtility.logCreateTest("getCredentialsForSupport");
        userRoleValue = userRole;
        emailValue = email;
        passwordValue = password;

    }

    /***
     * Getting data from csv file for support
     * parameters : each page & action access value
     */
    @Test(dataProvider = "getSupportData", dataProviderClass = DataProviderClass.class, priority = 2)
    public void getDataForSupport(String userRole, String dashboard, String infoBox, String latestStudies,
                                  String patientsList, String createPatient, String editPatient, String importStudy, String patientDocumentsTab, String patientAuditTrailTab,
                                  String createDevice, String editDevice, String devicesList, String deviceLogs, String changeDeviceFacility, String amplifiersList, String createAmplifier,
                                  String editAmplifier, String facilitiesList, String createFacility, String editFacility,
                                  String studiesList, String openStudy, String auditLogs, String allAuditLogs, String usersList, String createUser, String editUser, String userAuditLogs) {
        CommonUtility.logCreateTest("getDataForSupport");
        userRoleValue = userRole;
        dashboardValue = dashboard;
        infoBoxValue = infoBox;
        latestStudiesValue = latestStudies;
        patientsListValue = patientsList;
        createPatientValue = createPatient;
        editPatientValue = editPatient;
        importStudyValue = importStudy;
        patientDocumentsTabValue = patientDocumentsTab;
        patientAuditTrailTabValue = patientAuditTrailTab;
        createDeviceValue = createDevice;
        editDeviceValue = editDevice;
        devicesListValue = devicesList;
        deviceLogsValue = deviceLogs;
        changeDeviceFacilityValue = changeDeviceFacility;
        amplifiersListValue = amplifiersList;
        createAmplifierValue = createAmplifier;
        editAmplifierValue = editAmplifier;
        facilitiesListValue = facilitiesList;
        createFacilityValue = createFacility;
        editFacilityValue = editFacility;
        studiesListValue = studiesList;
        openStudyValue = openStudy;
        auditLogsValue = auditLogs;
        allAuditLogsValue = allAuditLogs;
        usersListValue = usersList;
        createUserValue = createUser;
        editUserValue = editUser;
        userAuditLogsValue = userAuditLogs;

    }

    /***
     * Implementing login and user role verification for support
     * Passing email and password to BasePageAccess method pageAccessScriptForLogin
     * Passing user role and dashboard authorization value to BasePageAcces method pageAccessScriptForUserRole
     */
    @Test(priority = 3)
    public void supportUser_LoginRoleVerification() {
        CommonUtility.logCreateTest("TC108_supportUser_LoginRoleVerification");
        pageAccessScriptForLogin(emailValue, passwordValue);
        pageAccessScriptForUserRole(userRoleValue, dashboardValue);

    }

    /***
     * Implementing dashboard element access verification for support
     * Passing dashboard authorization value, infoBox value, latestStudies value to BasePageAccess method pageAccessScriptForDashboard
     */
    @Test(priority = 4)
    public void TC109_supportUser_DashboardVerification() {
        CommonUtility.logCreateTest("TC109_supportUser_DashboardVerification");
        pageAccessScriptForDashboard(dashboardValue, infoBoxValue, latestStudiesValue);

    }

    /***
     * Implementing patient module access authorization for support
     * Passing patient list value, create patient value, edit patient value to BasePageAccess method pageAccessScriptForPatientModule
     */
    @Test(priority = 5)
    public void TC110_supportUser_PatientModule() {
        CommonUtility.logCreateTest("TC110_supportUser_PatientModule");
        pageAccessScriptForPatientModule(patientsListValue, createPatientValue, editPatientValue, patientDocumentsTabValue, patientAuditTrailTabValue, importStudyValue);

    }

    /***
     * Implementing study module access authorization for support
     * Passing study list value, open study value to BasePageAccess method pageAccessScriptForStudyModule
     */
    @Test(priority = 6)
    public void TC111_supportUser_StudyModule() {
        CommonUtility.logCreateTest("TC111_supportUser_StudyModule");
        pageAccessScriptForStudyModule(studiesListValue, openStudyValue, auditLogsValue, allAuditLogsValue);

    }

    /***
     * Implementing facility module access authorization for support
     * Passing facility list value, create facility value, edit facility value to BasePageAccess method pageAccessScriptForFacilityModule
     */
    @Test(priority = 7)
    public void TC112_supportUser_FacilityModule() {
        CommonUtility.logCreateTest("TC112_supportUser_FacilityModule");
        pageAccessScriptForFacilityModule(facilitiesListValue, createFacilityValue, editFacilityValue);

    }

    /***
     * Implementing amplifier module access authorization for support
     * Passing amplifier list value, create amplifier value, edit amplifier value to BasePageAccess method pageAccessScriptForAmplifierModule
     */
    @Test(priority = 8)
    public void TC113_supportUser_AmplifierModule() {
        CommonUtility.logCreateTest("TC113_supportUser_AmplifierModule");
        pageAccessScriptForAmplifierModule(amplifiersListValue, createAmplifierValue, editAmplifierValue);

    }

    /***
     * Implementing device module access authorization for support
     * Passing device list value, create device value, edit device value to BasePageAccess method pageAccessScriptForDeviceModule
     */
    @Test(priority = 9)
    public void TC114_supportUser_DeviceModule() {
        CommonUtility.logCreateTest("TC114_supportUser_DeviceModule");
        pageAccessScriptForDeviceModule(devicesListValue, createDeviceValue, editDeviceValue, deviceLogsValue, changeDeviceFacilityValue);

    }

    /***
     * Implementing user module access authorization for support
     * Passing user list value, create user value, edit user value to BasePageAccess method pageAccessScriptForUserModule
     */
    @Test(priority = 10)
    public void TC115_supportUser_UserModule() {
        CommonUtility.logCreateTest("TC115_supportUser_UserModule");
        pageAccessScriptForUserModule(usersListValue, createUserValue, editUserValue, userAuditLogsValue);
        pageAccessScriptForMonitoredStudiesPage(userAuditLogsValue);

    }

    /***
     * Implementing user sign out for support
     */
    @Test(priority = 30)
    public void supportUser_SignOut() {
        CommonUtility.logCreateTest("supportUser_SignOut");
        BaseActionAccess Action = new BaseActionAccess();
        Action.actionAccessScriptForUserSignOut();

    }

}
