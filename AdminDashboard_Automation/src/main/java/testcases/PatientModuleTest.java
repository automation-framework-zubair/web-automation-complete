package testcases;

import dataprovider.DataProviderClass;
import helper.CommonUtility;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import pages.PagesFactory;
import pages.ui.PageClasses.*;
import pages.ui.PageClasses.CreateEditPages.PatientCreatePage;
import pages.ui.PageClasses.CreateEditPages.PatientEditPage;
import pages.ui.PageClasses.ListPages.PatientsPage;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PatientModuleTest extends BaseTest {

    LoginPage loginPage;
    DashboardPage dashboardPage;
    MenuSidebarPage menuSidebarPage;
    PatientsPage patientsPage;
    PatientCreatePage patientCreatePage;
    PatientEditPage patientEditPage;

    Format f = new SimpleDateFormat("MMddyyyy_hhmm");
    String dateTime = f.format(new Date());
    String userRoleValue;
    String emailValue;
    String passwordValue;

    DataDeletion dataDeletion = new DataDeletion();

    Format fo = new SimpleDateFormat("MM/dd/yyyy");
    String currentDate = fo.format(new Date());

    /***
     * Getting credentials for SuperAdmin
     * @param userRole User Role
     * @param email User Email
     * @param password User Password
     */
    @Test(dataProvider = "getSuperAdminCredentials", dataProviderClass = DataProviderClass.class, priority = 1)
    public void getCredentialsForSuperAdmin(String userRole, String email, String password) {
        CommonUtility.logCreateTest("getCredentialsForSuperAdmin");
        userRoleValue = userRole;
        emailValue = email;
        passwordValue = password;

    }

    /***
     * Implementing test case to verify create patient functionality
     */
    @Test(priority = 3)
    public void TC74_PatientCreateVerification(){
        CommonUtility.logCreateTest("TC74_PatientCreateVerification");
        patientsPage = PagesFactory.getPatientsPage();
        patientCreatePage = PagesFactory.getPatientCreatePage();
        dashboardPage = PagesFactory.getDashboardPage();
        menuSidebarPage = PagesFactory.getMenuSideBarPage();

        loginPage = PagesFactory.getLoginPage();
        loginPage.verifyLoginPage();
        loginPage.typeUserName(emailValue);
        loginPage.typePassword(passwordValue);
        CommonUtility.logMessagesAndAddThemToReport("User Credentials ", "info");
        loginPage.clickLoginButton();
        loginPage.loginButtonAppearance();
        CommonUtility.saveLoggedInUserDetailsToAFile(emailValue, passwordValue);

        dashboardPage.isDashboardMenuVisible("Yes");


        menuSidebarPage.goToPatientsPage();

        patientsPage.goToCreatePatientPage();

        patientCreatePage.verifyCreatePatientPageNavigation();
        patientCreatePage.typeFirstName(dateTime+"_First_Name");
        CommonUtility.logMessagesAndAddThemToReport("First name typed", "info");
        patientCreatePage.typeMiddleName(dateTime+"_Middle_Name");
        CommonUtility.logMessagesAndAddThemToReport("Middle name typed", "info");
        patientCreatePage.typeLastName(dateTime+"_Last_Name");
        CommonUtility.logMessagesAndAddThemToReport("Last name typed", "info");
        patientCreatePage.selectSexValueFromDropdown("Male");
        CommonUtility.logMessagesAndAddThemToReport("Male selected from drop down", "info");
        patientCreatePage.typeSSN("159753842");
        CommonUtility.logMessagesAndAddThemToReport("SSN typed", "info");
        patientCreatePage.typePatientID(dateTime+"_PatientID");
        CommonUtility.logMessagesAndAddThemToReport("Patient ID typed", "info");
        patientCreatePage.typeDOB(currentDate);
        CommonUtility.logMessagesAndAddThemToReport("DOB typed", "info");
        patientCreatePage.clickSaveButton();
        patientCreatePage.MatchToastMessageValue("Patient created!");
    }

    /***
     * Implementing test case to search a patient
     */
    @Test(priority = 4)
    public void TC75_PatientSearchFunctionality(){
        CommonUtility.logCreateTest("TC75_PatientSearchFunctionality");
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        patientsPage = PagesFactory.getPatientsPage();
        patientCreatePage = PagesFactory.getPatientCreatePage();


        menuSidebarPage.goToPatientsPage();

        patientsPage.verifyPatientPageNavigation();
        patientsPage.searchForPatient(dateTime+"_First_Name");
        CommonUtility.logMessagesAndAddThemToReport("Patient searched by First name", "info");
        patientsPage.verifySearchedPatientName(dateTime+"_First_Name", dateTime);
        patientsPage.searchForPatient(dateTime+"_Middle_Name");
        CommonUtility.logMessagesAndAddThemToReport("Patient searched by Middle name", "info");
        patientsPage.verifySearchedPatientName(dateTime+"_Middle_Name", dateTime);
        patientsPage.searchForPatient(dateTime+"_Last_Name");
        CommonUtility.logMessagesAndAddThemToReport("Patient searched by Last name", "info");
        patientsPage.verifySearchedPatientName(dateTime+"_Last_Name", dateTime);
        patientsPage.searchForPatient("3842");
        CommonUtility.logMessagesAndAddThemToReport("Patient searched by SSN", "info");
        patientsPage.verifySearchedPatientByKeyword("3842", dateTime, "SSN");
        patientsPage.searchForPatient(currentDate);
        CommonUtility.logMessagesAndAddThemToReport("Patient searched by DOB", "info");
        patientsPage.verifySearchedPatientByKeyword(currentDate, dateTime, "Born");
        patientsPage.searchForPatient(dateTime+"_PatientID");
        patientsPage.verifySearchedPatientByKeyword(dateTime+"_PatientID", dateTime, "ID");

    }

    /***
     * Implementing test case to verify edit patient functionality
     */
    @Test(priority = 5)
    public void TC76_PatientEditFunctionality(){
        CommonUtility.logCreateTest("TC76_PatientEditFunctionality");
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        patientsPage = PagesFactory.getPatientsPage();
        patientEditPage = PagesFactory.getPatientEditPage();
        patientCreatePage = PagesFactory.getPatientCreatePage();


        menuSidebarPage.goToPatientsPage();

        patientsPage.verifyPatientPageNavigation();

        patientsPage.searchForPatient(dateTime+"_First");
        CommonUtility.logMessagesAndAddThemToReport("Patient searched by First name", "info");
        patientsPage.verifySearchedPatientName(dateTime+"_First_Name", dateTime);
        patientsPage.clickOnEditButton(dateTime+"_First_Name");

        patientEditPage.verifyEditPatientPageNavigation();
        patientEditPage.typeFirstName(dateTime + "_Edited");
        CommonUtility.logMessagesAndAddThemToReport("First name typed", "info");
        patientEditPage.typeMiddleName(dateTime + "_Edited");
        CommonUtility.logMessagesAndAddThemToReport("Middle name typed", "info");
        patientEditPage.typeLastName(dateTime + "_Edited");
        CommonUtility.logMessagesAndAddThemToReport("Last name typed", "info");
        patientEditPage.selectSexValueFromDropdown("Female");
        CommonUtility.logMessagesAndAddThemToReport("Male selected from drop down", "info");
        patientEditPage.typeSSN("213738840");
        CommonUtility.logMessagesAndAddThemToReport("SSN typed", "info");
        patientEditPage.typePatientID("");
        CommonUtility.logMessagesAndAddThemToReport("Patient ID typed", "info");
        patientEditPage.typeDOB();
        CommonUtility.logMessagesAndAddThemToReport("DOB typed", "info");
        patientEditPage.clickSaveButton();
        patientEditPage.MatchToastMessageValue("Patient saved!");

        menuSidebarPage.goToPatientsPage();

        patientsPage.verifyPatientPageNavigation();
        patientsPage.searchForPatient(dateTime + "_Edited");
        CommonUtility.logMessagesAndAddThemToReport("Patient searched by First name", "info");
        patientsPage.verifySearchedPatientName(dateTime + "_Edited", dateTime);

    }


    /***
     * For deleting patients that have been created during test execution
     */
    @Test(priority = 100)
    public void afterTest_patientTestDataDeletion() {
        CommonUtility.logCreateTest("afterTest_patientTestDataDeletion");
        dataDeletion.patientDeletion("yes", dateTime);

    }

}
