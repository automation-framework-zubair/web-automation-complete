package testcases.UserModule;

import helper.CommonUtility;
import org.testng.annotations.AfterClass;
import pages.PagesFactory;
import pages.ui.PageClasses.CreateEditPages.UserCreatePage;
import pages.ui.PageClasses.CreateEditPages.UserEditPage;
import pages.ui.PageClasses.DashboardPage;
import pages.ui.PageClasses.HeaderPanel;
import pages.ui.PageClasses.ListPages.UserPage;
import pages.ui.PageClasses.LoginPage;
import pages.ui.PageClasses.MenuSidebarPage;
import testcases.BaseTest;

public class BaseUserModule extends BaseTest {
    LoginPage loginPage;
    MenuSidebarPage menuSidebarPage;
    UserPage userPage;
    UserCreatePage userCreatePage;
    UserEditPage userEditPage;
    HeaderPanel headerPanel;
    DashboardPage dashboardPage;

    /***
     * Method to login by email and password
     * @param email Email of the user
     * @param password Password of the user
     */
    public void userLogin(String email, String password) {
        loginPage = PagesFactory.getLoginPage();
        dashboardPage = PagesFactory.getDashboardPage();

        loginPage.verifyLoginPage();
        loginPage.typeUserName(email);
        loginPage.typePassword(password);
        CommonUtility.logMessagesAndAddThemToReport("User Credentials Given", "info");
        loginPage.clickLoginButton();
        dashboardPage.verifyDashboardPageNavigation();
        CommonUtility.saveLoggedInUserDetailsToAFile(email, password);

    }

    /***
     * Method for navigating to the create user page
     */
    public void navigationToCreateUserPageAndVerification() {
        userPage = PagesFactory.getUserPage();
        userCreatePage = PagesFactory.getUserCreatePage();
        menuSidebarPage = PagesFactory.getMenuSideBarPage();

        menuSidebarPage.goToUsersPage();
        userPage.verifyUserPageNavigation();
        userPage.goToCreateUserPage();
        userCreatePage.verifyCreateUserPageNavigation();

    }

    /***
     * Method to navigate to Edit User page by user's last name
     * @param userLastName Last name of the user
     */
    public void navigationToEditUserPageByUserLastName(String userLastName) {
        userPage = PagesFactory.getUserPage();
        userEditPage = PagesFactory.getUserEditPage();
        menuSidebarPage = PagesFactory.getMenuSideBarPage();

        menuSidebarPage.goToUsersPage();
        userPage.verifyUserPageNavigation();
        userPage.clickEditIconByInstanceName("yes", userLastName, "Users");
        userEditPage.verifyEditUserPageNavigation();

    }

    /***
     * Method for navigating to create user page and creating a user
     * @param firstName First name of the user
     * @param lastName Last name of the user
     * @param password Password of the user
     * @param email Email address of th user
     * @param role Role of the user
     */
    public void createUser(String firstName, String lastName, String email, String password, String role) {
        navigationToCreateUserPageAndVerification();
        userCreatePage.createUser("yes", "", firstName, "", lastName,
                "", email, password, password, role, "User created!");
        userCreatePage.successfulToastMessageDisappear();
        menuSidebarPage.goToUsersPage();

    }

    /***
     * Method to delete user by last name
     * @param userLastName Last name of the user
     */
    public void deleteUserByLastName(String userLastName) {
        userPage.clickOnDeleteIconByUserLastName(userLastName);
        userPage.checkThatAParticularModalIsPresentWithTitle("Confirm User Deletion");
        userPage.clickOnButton("Yes");
        userPage.MatchToastMessageValue("User has been deleted");

    }

    /***
     * Method for user sign out
     */
    public void userSignOut() {
        headerPanel = PagesFactory.getheaderPanel();
        loginPage = PagesFactory.getLoginPage();
        CommonUtility.logMessagesAndAddThemToReport("Now signing out", "info");
        headerPanel.signOut();
        loginPage.verifyLoginPage();

    }

    /***
     * Method for signing out the user after each class
     */
    @AfterClass(alwaysRun = true)
    public void afterClass_userSignOut() {
        userSignOut();

    }

}
