package testcases.UserModule;

import dataprovider.DataProviderClass;
import helper.CommonUtility;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.PagesFactory;
import pages.ui.PageClasses.*;
import pages.ui.PageClasses.CreateEditPages.FacilityCreatePage;
import pages.ui.PageClasses.CreateEditPages.UserCreatePage;
import pages.ui.PageClasses.CreateEditPages.UserEditPage;
import pages.ui.PageClasses.ListPages.FacilityPage;
import pages.ui.PageClasses.ListPages.UserPage;
import testcases.DataDeletion;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

public class UserCreatePageTest extends BaseUserModule{
    LoginPage loginPage;
    MenuSidebarPage menuSidebarPage;
    UserPage userPage;
    UserCreatePage userCreatePage;
    UserEditPage userEditPage;
    HeaderPanel headerPanel;
    DashboardPage dashboardPage;
    ProfilePage profilePage;
    FacilityPage facilityPage;
    FacilityCreatePage facilityCreatePage;
    Format f = new SimpleDateFormat("MMddyyyy_hhmm");
    String dateTime = f.format(new Date());
    String email = "test" + dateTime + "@gmail.com";
    String password = "Enosis123";
    String SuperAdminRole;
    String SuperAdminEmail;
    String SuperAdminPassword;
    String userLastNameWithWhitespaces;
    String userLastNameSuperAdminForEditUser;
    String userLastNameForPasswordFields;
    String userLastNameForEditUserPageHeader;
    String userLastNameForProductionUser;
    String emailAddressForProductionUser;
    String facilityName;
    String userLastNameSuperAdmin;
    String userLastNameSupport;
    String userLastNameProduction;
    String userLastNameFacilityAdmin;
    String userLastNameReviewDoctor;
    String userLastNameLeadTech;
    String userLastNameFieldTech;
    String userLastNameOfficePersonnel;

    DataDeletion dataDeletion = new DataDeletion();

    /***
     * Getting credentials for SuperAdmin
     * @param userRole User Role
     * @param email User Email
     * @param password User Password
     */
    @Test(dataProvider = "getSuperAdminCredentials", dataProviderClass = DataProviderClass.class, priority = 1)
    public void getCredentialsForSuperAdmin(String userRole, String email, String password) {
        CommonUtility.logCreateTest("BeforeTestMethods");
        CommonUtility.logCreateNode("getCredentialsForSuperAdmin");
        SuperAdminRole = userRole;
        SuperAdminEmail = email;
        SuperAdminPassword = password;

    }


    /***
     * Login with SuperAdmin role and verifying if login was successful
     */
    @Test(priority = 2)
    public void loginRoleVerification() {
        CommonUtility.logCreateNode("loginRoleVerification");
        loginPage = PagesFactory.getLoginPage();
        dashboardPage = PagesFactory.getDashboardPage();

        userLogin(SuperAdminEmail, SuperAdminPassword);

    }


    /***
     * Test case to verify that a new user cannot be added without assigning a role
     */
    @Test(priority = 6)
    public void TC326_checkNewUserCannotBeAddedWithoutAssigningARole() {
        CommonUtility.logCreateTest("TC326_checkNewUserCannotBeAddedWithoutAssigningARole");

        userCreatePage = PagesFactory.getUserCreatePage();
        navigationToCreateUserPageAndVerification();

        userCreatePage.verifyCreateUserPageNavigation();
        userCreatePage.typeFirstName("Test");
        userCreatePage.typeLastName("LastName_test");
        userCreatePage.typeEmail("emailtest@gmail.com");
        userCreatePage.typePassword("Enosis123");
        userCreatePage.typeConfirmPassword("Enosis123");
        userCreatePage.checkSaveButtonIsDisabled();
        userCreatePage.saveButtonClick();
        userCreatePage.checkToastMessageDidNotAppear("User created!");

    }

    /***
     * Test case to verify that error message is displayed for duplicate email when creating a new user
     */
    @Test(priority = 7)
    public void TC327_checkErrorMessageIsDisplayed_ForDuplicateEmailWhenCreatingUser() {
        CommonUtility.logCreateTest("TC327_checkErrorMessageIsDisplayed_ForDuplicateEmailWhenCreatingUser");
        userCreatePage = PagesFactory.getUserCreatePage();
        navigationToCreateUserPageAndVerification();

        userCreatePage.typeFirstName("Test");
        userCreatePage.typeLastName("LastName_test");
        userCreatePage.typeEmail(SuperAdminEmail);
        userCreatePage.typePassword(password);
        userCreatePage.typeConfirmPassword(password);
        userCreatePage.selectValueFromDropDown("SuperAdmin");
        userCreatePage.saveButtonClick();
        userCreatePage.checkErrorMessageForDuplicateEmail();

    }


    /***
     * Test case to verify that an error message is displayed for invalid email format
     */
    @Test(priority = 8)
    public void TC328_checkErrorMessageIsDisplayed_ForInvalidEmailFormat() {
        CommonUtility.logCreateTest("TC328_checkErrorMessageIsDisplayed_ForInvalidEmailFormat");
        userCreatePage = PagesFactory.getUserCreatePage();
        navigationToCreateUserPageAndVerification();

        userCreatePage.typeEmail("random Text");
        userCreatePage.typePassword(" ");
        userCreatePage.verifyPresenceOfClientSideMessage("Valid email address required");
        userCreatePage.typeEmail("abc.gmam.com");
        userCreatePage.typePassword(" ");
        userCreatePage.verifyPresenceOfClientSideMessage("Valid email address required");
        userCreatePage.typeEmail("a@b@c@example.com");
        userCreatePage.typePassword(" ");
        userCreatePage.verifyPresenceOfClientSideMessage("Valid email address required");
        userCreatePage.typeEmail("example@123");
        userCreatePage.typePassword(" ");
        userCreatePage.verifyPresenceOfClientSideMessage("Valid email address required");
        userCreatePage.typeEmail("example@.com");
        userCreatePage.typePassword(" ");
        userCreatePage.verifyPresenceOfClientSideMessage("Valid email address required");
        userCreatePage.typeEmail("@example.org");
        userCreatePage.typePassword(" ");
        userCreatePage.verifyPresenceOfClientSideMessage("Valid email address required");
        userCreatePage.typeEmail("example@&*(&^!_+");
        userCreatePage.typePassword(" ");
        userCreatePage.verifyPresenceOfClientSideMessage("Valid email address required");

    }

    /***
     * Test case to check that the name field is now separated to Title, First Name, Middle Name, Last Name & Suffix
     */
    @Test(priority = 9)
    public void TC329_checkCreateUserContainsSeparateTextFieldsForNames() {
        CommonUtility.logCreateTest("TC329_checkCreateUserContainsSeparateTextFieldsForNames");
        userCreatePage = PagesFactory.getUserCreatePage();
        navigationToCreateUserPageAndVerification();

        userCreatePage.verifyCreateUserPageContainsSeparateNameFields();

    }

    /***
     * Test case to check that the label of required fields contain asterisks
     */
    @Test(priority = 10)
    public void TC330_checkAsterisksAreShownForRequiredFields() {
        CommonUtility.logCreateTest("TC330_checkAsterisksAreShownForRequiredFields");
        userCreatePage = PagesFactory.getUserCreatePage();
        navigationToCreateUserPageAndVerification();

        Assert.assertTrue(userCreatePage.verifyLabelOfRequiredFieldContainsAsterisk("FirstName"));
        Assert.assertTrue(userCreatePage.verifyLabelOfRequiredFieldContainsAsterisk("LastName"));
        Assert.assertTrue(userCreatePage.verifyLabelOfRequiredFieldContainsAsterisk("Email"));
        Assert.assertTrue(userCreatePage.verifyLabelOfRequiredFieldContainsAsterisk("Password"));
        Assert.assertTrue(userCreatePage.verifyLabelOfRequiredFieldContainsAsterisk("ConfirmPassword"));
        Assert.assertTrue(userCreatePage.verifyLabelOfRequiredFieldContainsAsterisk("RoleId"));

    }

    /***
     * Test case to check that a user cannot be created with only whitespaces in the name fields
     */
    @Test(priority = 11)
    public void TC331_checkNewUserCannotBeCreatedWithOnlyWhitespaces() {
        CommonUtility.logCreateTest("TC331_checkNewUserCannotBeCreatedWithOnlyWhitespaces");
        userCreatePage = PagesFactory.getUserCreatePage();
        navigationToCreateUserPageAndVerification();
        userLastNameWithWhitespaces = "       ";

        userCreatePage.typeFirstName("      ");
        userCreatePage.typeLastName(userLastNameWithWhitespaces);
        userCreatePage.typeEmail(dateTime + "test@gmail.com");
        userCreatePage.typePassword(password);
        userCreatePage.typeConfirmPassword(password);
        userCreatePage.selectValueFromDropDown("SuperAdmin");
        userCreatePage.saveButtonClick();
        userCreatePage.checkToastMessageDidNotAppear("User created!");

    }

    /***
     * Test case to check that Create user page becomes Edit User page after creating a user
     */
    @Test(priority = 12)
    public void TC332_checkCreateUserPageBecomesEditUserPageAfterCreatingUser() {
        CommonUtility.logCreateTest("TC332_checkCreateUserPageBecomesEditUserPageAfterCreatingUser");
        userCreatePage = PagesFactory.getUserCreatePage();
        userEditPage = PagesFactory.getUserEditPage();
        navigationToCreateUserPageAndVerification();
        userLastNameSuperAdminForEditUser = "TC332" + dateTime;

        userCreatePage.typeFirstName("Test");
        userCreatePage.typeLastName(userLastNameSuperAdminForEditUser);
        userCreatePage.typeEmail(dateTime + "test332@gmail.com");
        userCreatePage.typePassword(password);
        userCreatePage.typeConfirmPassword(password);
        userCreatePage.selectValueFromDropDown("SuperAdmin");
        userCreatePage.saveButtonClick();
        userEditPage.verifyEditUserPageNavigation();

    }

    /***
     * Test case to check that password & confirm password fields disappear after creating a user
     */
    @Test(priority = 13)
    public void TC333_checkPasswordFieldsDisappearAfterCreatingUser() {
        CommonUtility.logCreateTest("TC333_checkPasswordFieldsDisappearAfterCreatingUser");
        userCreatePage = PagesFactory.getUserCreatePage();
        userEditPage = PagesFactory.getUserEditPage();
        navigationToCreateUserPageAndVerification();
        userLastNameForPasswordFields = "TC333";

        userCreatePage.typeFirstName("Test");
        userCreatePage.typeLastName(userLastNameForPasswordFields);
        userCreatePage.typeEmail(dateTime + "test333@gmail.com");
        userCreatePage.typePassword(password);
        userCreatePage.typeConfirmPassword(password);
        userCreatePage.selectValueFromDropDown("SuperAdmin");
        userCreatePage.saveButtonClick();
        userEditPage.verifyPasswordFieldsAreNotPresent();

    }

    /***
     * Test case to check that roles are displayed in a particular order in create user page
     */
    @Test(priority = 14)
    public void TC334_checkRolesAreDisplayedInAParticularOrderInCreateUserPage() {
        CommonUtility.logCreateTest("TC334_checkRolesAreDisplayedInAParticularOrderInCreateUserPage");
        userCreatePage = PagesFactory.getUserCreatePage();
        userEditPage = PagesFactory.getUserEditPage();
        navigationToCreateUserPageAndVerification();

        userCreatePage.checkRolesAreDisplayedInAParticularOrderInDropdown();

    }


    /***
     * Test case to check that a created user is assigned to the selected role
     */
    @Test(priority = 15)
    public void TC335_checkThatCreatedUserIsAssignedToTheSelectedRole() {
        CommonUtility.logCreateTest("TC335_checkThatCreateUserIsAssignedToTheSelectedRole");
        dashboardPage = PagesFactory.getDashboardPage();
        headerPanel = PagesFactory.getheaderPanel();
        profilePage = PagesFactory.getProfilePage();
        loginPage = PagesFactory.getLoginPage();

        createUser("First_name", dateTime, email, password, "SuperAdmin");
        userSignOut();
        loginPage.typeUserName(email);
        loginPage.typePassword(password);
        loginPage.clickLoginButton();
        dashboardPage.verifyDashboardPageNavigation();
        headerPanel.yourProfileButtonClick();

        profilePage.verifyProfilePageNavigation();
        profilePage.verifyUserRole("Super Admin");
        userSignOut();
        loginRoleVerification();

    }

    /***
     * Method to create a new facility for creating users
     */
    @Test(priority = 16)
    public void createFacility() {
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        facilityPage = PagesFactory.getFacilityPage();
        facilityCreatePage = PagesFactory.getFacilityCreatePage();
        facilityName = dateTime;

        menuSidebarPage.goToFacilitiesPage();

        facilityPage.verifyFacilityPageNavigation();
        facilityPage.goToCreateFacilityPage();

        facilityCreatePage.verifyCreateFacilityPageNavigation();
        facilityCreatePage.createFacility("yes", facilityName, "www.domain.com", "Facility created!");

    }

    /***
     * Test case to check if SuperAdmin/Support/Production users are assigned to Lifelines Neuro Facility regardless of the selected facility
     */
    @Test(priority = 17)
    public void TC336_checkSuperAdminSupportOrProductionUserIsAssignedToLifelinesNeuro() {
        CommonUtility.logCreateTest("TC336_checkSuperAdminSupportOrProductionUserIsAssignedToLifelinesNeuro");
        headerPanel = PagesFactory.getheaderPanel();
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        userPage = PagesFactory.getUserPage();

        userLastNameSuperAdmin = "SuperAdmin" + dateTime;
        userLastNameSupport = "Support" + dateTime;
        userLastNameProduction = "Production" + dateTime;

        menuSidebarPage.goToUsersPage();
        headerPanel.selectFacilityFromDropdown(facilityName);
        createUser("Test", userLastNameSuperAdmin, dateTime + "testsuperAdmin@gmail.com", password, "SuperAdmin");
        createUser("Test", userLastNameSupport, dateTime + "testsupport@gmail.com", password, "Support");
        createUser("Test", userLastNameProduction, dateTime + "testproduction@gmail.com", password, "Production");
        headerPanel.selectFacilityFromDropdown("Lifelines Neuro Company");
        userPage.verifyUserExistsInUsersList(userLastNameSuperAdmin);
        userPage.verifyUserExistsInUsersList(userLastNameSupport);
        userPage.verifyUserExistsInUsersList(userLastNameProduction);

    }

    /***
     * Test case to check if a newly created user is assigned to the selected facility if the user role is not SuperAdmin/Support
     */
    @Test(priority = 18)
    public void TC337_checkNewlyCreatedUserAssignedToSelectedFacilityIfRoleNotSuperAdminSupportOrProduction() {
        CommonUtility.logCreateTest("TC337_checkNewlyCreatedUserAssignedToSelectedFacilityIfRoleNotSuperAdminSupportOrProduction");
        headerPanel = PagesFactory.getheaderPanel();
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        userPage = PagesFactory.getUserPage();

        userLastNameFacilityAdmin = "FacilityAdmin" + dateTime;
        userLastNameReviewDoctor = "ReviewDoctor" + dateTime;
        userLastNameLeadTech = "LeadTech" + dateTime;
        userLastNameFieldTech = "FieldTech" + dateTime;
        userLastNameOfficePersonnel = "OfficePersonnel" + dateTime;

        headerPanel.selectFacilityFromDropdown(facilityName);
        createUser("Test", userLastNameFacilityAdmin, dateTime + "testfacilityadmin@gmail.com", password, "FacilityAdmin");
        createUser("Test", userLastNameReviewDoctor, dateTime + "testreviewdoctor@gmail.com", password, "ReviewDoctor");
        createUser("Test", userLastNameLeadTech, dateTime + "testleadtech@gmail.com", password, "LeadTech");
        createUser("Test", userLastNameFieldTech, dateTime + "testfieldtech@gmail.com", password, "FieldTech");
        createUser("Test", userLastNameOfficePersonnel, dateTime + "testofficePersonnel@gmail.com", password, "OfficePersonnel");
        userPage.verifyUserExistsInUsersList(userLastNameFacilityAdmin);
        userPage.verifyUserExistsInUsersList(userLastNameReviewDoctor);
        userPage.verifyUserExistsInUsersList(userLastNameLeadTech);
        userPage.verifyUserExistsInUsersList(userLastNameFieldTech);
        userPage.verifyUserExistsInUsersList(userLastNameOfficePersonnel);
        headerPanel.selectFacilityFromDropdown("Lifelines Neuro Company");

    }


    /***
     * Test case to check that save button remains disabled initially in Create user page
     */
    @Test(priority = 20)
    public void TC338_checkSaveButtonRemainsDisabledInitially() {
        CommonUtility.logCreateTest("TC338_checkSaveButtonRemainsDisabledInitially");
        userCreatePage = PagesFactory.getUserCreatePage();
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        menuSidebarPage.goToUsersPage();
        headerPanel.selectFacilityFromDropdown("Lifelines Neuro Company");
        navigationToCreateUserPageAndVerification();

        userCreatePage.checkSaveButtonIsDisabled();

    }

    /***
     * Test case to check that save button becomes disabled after all the data is erased from the text fields
     */
    @Test(priority = 21)
    public void TC339_checkSaveButtonBecomesDisabledAfterAllDataHasBeenErased() {
        CommonUtility.logCreateTest("TC339_checkSaveButtonBecomesDisabledAfterAllDataHasBeenErased");
        userCreatePage = PagesFactory.getUserCreatePage();
        navigationToCreateUserPageAndVerification();

        userCreatePage.typeTitle("Title");
        userCreatePage.typeFirstName("Test_first");
        userCreatePage.typeMiddleName("Test_middle");
        userCreatePage.typeLastName("TEst_Last");
        userCreatePage.typeSuffix("Test");
        userCreatePage.typePassword("test");
        userCreatePage.typeConfirmPassword("test");
        userCreatePage.typeEmail("test@gmasol.com");
        userCreatePage.selectValueFromDropDown("SuperAdmin");
        userCreatePage.clearTitleTextField();
        userCreatePage.clearSuffixTextField();
        userCreatePage.clearFirstNameTextField();
        userCreatePage.clearMiddleNameTextField();
        userCreatePage.clearLastNameTextField();
        userCreatePage.clearPasswordTextField();
        userCreatePage.clearConfirmPasswordTextField();
        userCreatePage.clearEmailTextField();
        userCreatePage.checkSaveButtonIsDisabled();

    }

    /***
     * Test case to check that the page header becomes Edit User after creating a user
     */
    @Test(priority = 25)
    public void TC340_checkPageHeaderBecomesEditUserAfterCreatingUser() {
        CommonUtility.logCreateTest("TC340_checkPageHeaderBecomesEditUserAfterCreatingUser");
        userCreatePage = PagesFactory.getUserCreatePage();
        userEditPage = PagesFactory.getUserEditPage();
        navigationToCreateUserPageAndVerification();
        userLastNameForEditUserPageHeader = "TC340";

        userCreatePage.typeFirstName("Test");
        userCreatePage.typeLastName(userLastNameForEditUserPageHeader);
        userCreatePage.typeEmail(dateTime + "test340@gmail.com");
        userCreatePage.typePassword(password);
        userCreatePage.typeConfirmPassword(password);
        userCreatePage.selectValueFromDropDown("SuperAdmin");
        userCreatePage.saveButtonClick();
        userEditPage.verifyEditUserPageNavigation();

    }

    /***
     * Test case to check that Production users can be created
     */
    @Test(priority = 26)
    public void TC341_checkThatProductionUsersCanBeCreated() {
        CommonUtility.logCreateTest("TC341_checkThatProductionUsersCanBeCreated");
        userCreatePage = PagesFactory.getUserCreatePage();
        userEditPage = PagesFactory.getUserEditPage();
        navigationToCreateUserPageAndVerification();
        userLastNameForProductionUser = "Production" + dateTime;
        emailAddressForProductionUser = dateTime + "test341@gmail.com";

        userCreatePage.typeFirstName("Production_Test" + dateTime);
        userCreatePage.typeLastName(userLastNameForProductionUser);
        userCreatePage.typeEmail(emailAddressForProductionUser);
        userCreatePage.typePassword(password);
        userCreatePage.typeConfirmPassword(password);
        userCreatePage.selectValueFromDropDown("SuperAdmin");
        userCreatePage.saveButtonClick();
        userEditPage.verifyEditUserPageNavigation();

    }

    /***
     * Test case to check that newly created Production user can log in
     */
    @Test(priority = 27)
    public void TC342_checkThatNewlyCreatedProductionUserCanLogIn() {
        CommonUtility.logCreateTest("TC342_checkThatNewlyCreatedProductionUserCanLogIn");
        loginPage = PagesFactory.getLoginPage();
        dashboardPage = PagesFactory.getDashboardPage();
        userSignOut();

        loginPage.verifyLoginPage();
        loginPage.typeUserName(emailAddressForProductionUser);
        loginPage.typePassword(password);
        CommonUtility.logMessagesAndAddThemToReport("User Credentials Given", "info");
        loginPage.clickLoginButton();
        dashboardPage.verifyDashboardPageNavigation();
        CommonUtility.saveLoggedInUserDetailsToAFile(emailAddressForProductionUser, password);
        userSignOut();
        loginRoleVerification();

    }

    /***
     * For deleting user that have been created to test disappearance of password fields after user creation
     */
    @Test(priority = 102, alwaysRun = true)
    public void afterTest_TC333DataDeletion() {
        CommonUtility.logCreateTest("afterTestDataDeletion");
        CommonUtility.logCreateNode("afterTest_TC333DataDeletion");
        dataDeletion.userDeletion("yes", userLastNameForPasswordFields);

    }

    /***
     * For deleting user that have been created to test page header of create user page changes
     */
    @Test(priority = 105, alwaysRun = true)
    public void afterTest_TC340DataDeletion() {
        CommonUtility.logCreateNode("afterTest_TC340DataDeletion");
        dataDeletion.userDeletion("yes", userLastNameForEditUserPageHeader);

    }

    /***
     * For deleting user that have been created to test creation of user with whitespaces (TC331)
     */
    @Test(priority = 106, alwaysRun = true)
    public void afterTest_userWithWhitespacesTestDataDeletion() {
        CommonUtility.logCreateNode("afterTest_userWithWhitespacesTestDataDeletion");
        dataDeletion.userDeletion("yes", userLastNameWithWhitespaces);

    }

    /***
     * For deleting user that have been created to test Production user can log in (TC341)
     */
    @Test(priority = 107, alwaysRun = true)
    public void afterTest_productionUserTestDataDeletion() {
        CommonUtility.logCreateNode("afterTest_productionUserTestDataDeletion");
        dataDeletion.userDeletion("yes", userLastNameForProductionUser);

    }

    /***
     * For deleting user that have been created to test that create user page becomes edit user page
     */
    @Test(priority = 108, alwaysRun = true)
    public void afterTest_TC332DataDeletion() {
        CommonUtility.logCreateNode("afterTest_TC332DataDeletion");
        dataDeletion.userDeletion("yes", userLastNameSuperAdminForEditUser);

    }

    /***
     * For deleting user that have been created to test SuperAdmin is assigned to Lifelines Neuro Facility (TC336)
     */
    @Test(priority = 109, alwaysRun = true)
    public void afterTest_superAdminUserDataDeletion() {
        CommonUtility.logCreateNode("afterTest_superAdminUserDataDeletion");
        dataDeletion.userDeletion("yes", userLastNameSuperAdmin);

    }

    /***
     * For deleting user that have been created to test Support is assigned to Lifelines Neuro Facility (TC336)
     */
    @Test(priority = 110, alwaysRun = true)
    public void afterTest_supportUserDataDeletion() {
        CommonUtility.logCreateNode("afterTest_supportUserDataDeletion");
        dataDeletion.userDeletion("yes", userLastNameSupport);

    }

    /***
     * For deleting user that has been created for testing which facility the user is assigned to
     */
    @Test(priority = 112, alwaysRun = true)
    public void afterTest_productionUserDataDeletion() {
        CommonUtility.logCreateNode("afterTest_productionUserDataDeletion");
        dataDeletion.userDeletion("yes", userLastNameProduction);

    }

    /***
     * For deleting user that has been created for testing which facility the user is assigned to
     */
    @Test(priority = 113, alwaysRun = true)
    public void afterTest_facilityAdminUserDataDeletion() {
        CommonUtility.logCreateNode("afterTest_facilityAdminUserDataDeletion");
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        headerPanel = PagesFactory.getheaderPanel();
        menuSidebarPage.goToUsersPage();
        headerPanel.selectFacilityFromDropdown(facilityName);
        dataDeletion.userDeletion("yes", userLastNameFacilityAdmin);

    }

    /***
     * For deleting user that has been created for testing which facility the user is assigned to
     */
    @Test(priority = 114, alwaysRun = true)
    public void afterTest_reviewDoctorUserDataDeletion() {
        CommonUtility.logCreateNode("afterTest_reviewDoctorUserDataDeletion");
        dataDeletion.userDeletion("yes", userLastNameReviewDoctor);

    }

    /***
     * For deleting user that has been created for testing which facility the user is assigned to
     */
    @Test(priority = 115, alwaysRun = true)
    public void afterTest_leadTechUserDataDeletion() {
        CommonUtility.logCreateNode("afterTest_leadTechUserDataDeletion");
        dataDeletion.userDeletion("yes", userLastNameLeadTech);

    }

    /***
     * For deleting user that has been created for testing which facility the user is assigned to
     */
    @Test(priority = 116, alwaysRun = true)
    public void afterTest_fieldTechUserDataDeletion() {
        CommonUtility.logCreateNode("afterTest_fieldTechUserDataDeletion");
        dataDeletion.userDeletion("yes", userLastNameFieldTech);

    }

    /***
     * For deleting user that has been created for testing which facility the user is assigned to
     */
    @Test(priority = 117, alwaysRun = true)
    public void afterTest_officePersonnelUserDataDeletion() {
        CommonUtility.logCreateNode("afterTest_officePersonnelUserDataDeletion");
        dataDeletion.userDeletion("yes", userLastNameOfficePersonnel);

    }

    /**
     * For deleting facility that has been created for testing which facility the created user is assigned to
     */
    @Test(priority = 120, alwaysRun = true)
    public void afterTest_facilityDataDeletion() {
        CommonUtility.logCreateNode("afterTest_facilityDataDeletion");
        menuSidebarPage.goToUsersPage();
        headerPanel.selectFacilityFromDropdown("Lifelines Neuro Company");
        dataDeletion.facilityDeletion("yes", facilityName);

    }

}
