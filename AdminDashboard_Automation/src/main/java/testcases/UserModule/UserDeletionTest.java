package testcases.UserModule;

import dataprovider.DataProviderClass;
import helper.CommonUtility;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;
import pages.PagesFactory;
import pages.ui.PageClasses.CreateEditPages.FacilityCreatePage;
import pages.ui.PageClasses.CreateEditPages.UserCreatePage;
import pages.ui.PageClasses.DashboardPage;
import pages.ui.PageClasses.HeaderPanel;
import pages.ui.PageClasses.ListPages.FacilityPage;
import pages.ui.PageClasses.ListPages.UserPage;
import pages.ui.PageClasses.MenuSidebarPage;
import testcases.DataDeletion;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

public class UserDeletionTest extends BaseUserModule{
    MenuSidebarPage menuSidebarPage;
    UserPage userPage;
    UserCreatePage userCreatePage;
    HeaderPanel headerPanel;
    DashboardPage dashboardPage;
    FacilityPage facilityPage;
    FacilityCreatePage facilityCreatePage;
    Format f = new SimpleDateFormat("MMddyyyy_hhmm");
    String dateTime = f.format(new Date());
    String password = "Enosis123";
    String facilityName;
    String SuperAdminRole;
    String SuperAdminEmail;
    String SuperAdminPassword;
    String productionRole;
    String productionEmail;
    String productionPassword;
    String reviewDoctorRole;
    String reviewDoctorEmail;
    String reviewDoctorPassword;
    String leadTechRole;
    String leadTechEmail;
    String leadTechPassword;
    String fieldTechRole;
    String fieldTechEmail;
    String fieldTechPassword;
    String officePersonnelRole;
    String officePersonnelEmail;
    String officePersonnelPassword;
    String userLastNameSuperAdmin;
    String userLastNameSupport;
    String userLastNameProduction;
    String userLastNameFacilityAdmin;
    String userLastNameReviewDoctor;
    String userLastNameLeadTech;
    String emailAddressForSuperAdmin;
    String emailAddressForSupport;
    String emailAddressForProduction;
    String emailAddressForFacilityAdmin;
    String emailAddressForReviewDoctor;
    String emailAddressForLeadTech;
    String productionUserListValue;
    String reviewDoctorUserListValue;
    String leadTechUserListValue;
    String fieldTechUserListValue;
    String officePersonnelUserListValue;

    DataDeletion dataDeletion = new DataDeletion();

    /***
     * Getting credentials for SuperAdmin
     * @param userRole User Role
     * @param email User Email
     * @param password User Password
     */
    @Test(dataProvider = "getSuperAdminCredentials", dataProviderClass = DataProviderClass.class, priority = 1)
    public void getCredentialsForSuperAdmin(String userRole, String email, String password) {
        CommonUtility.logCreateTest("BeforeTestMethods");
        CommonUtility.logCreateNode("getCredentialsForSuperAdmin");
        SuperAdminRole = userRole;
        SuperAdminEmail = email;
        SuperAdminPassword = password;

    }

    /***
     * Getting credentials for Production
     * @param userRole User Role
     * @param email User Email
     * @param password User Password
     */
    @Test(dataProvider = "getProductionCredentials", dataProviderClass = DataProviderClass.class, priority = 2)
    public void getCredentialsForProduction(String userRole, String email, String password) {
        CommonUtility.logCreateNode("getCredentialsForProduction");
        productionRole = userRole;
        productionEmail = email;
        productionPassword = password;

    }

    /***
     * Getting credentials for ReviewDoctor
     * @param userRole User Role
     * @param email User Email
     * @param password User Password
     */
    @Test(dataProvider = "getReviewDoctorCredentials", dataProviderClass = DataProviderClass.class, priority = 5)
    public void getCredentialsForReviewDoctor(String userRole, String email, String password) {
        CommonUtility.logCreateNode("getCredentialsForReviewDoctor");
        reviewDoctorRole = userRole;
        reviewDoctorEmail = email;
        reviewDoctorPassword = password;

    }

    /***
     * Getting credentials for LeadTech
     * @param userRole User Role
     * @param email User Email
     * @param password User Password
     */
    @Test(dataProvider = "getLeadTechCredentials", dataProviderClass = DataProviderClass.class, priority = 6)
    public void getCredentialsForLeadTech(String userRole, String email, String password) {
        CommonUtility.logCreateNode("getCredentialsForLeadTech");
        leadTechRole = userRole;
        leadTechEmail = email;
        leadTechPassword = password;

    }

    /***
     * Getting credentials for FieldTech
     * @param userRole User Role
     * @param email User Email
     * @param password User Password
     */
    @Test(dataProvider = "getFieldTechCredentials", dataProviderClass = DataProviderClass.class, priority = 8)
    public void getCredentialsForFieldTech(String userRole, String email, String password) {
        CommonUtility.logCreateNode("getCredentialsForFieldTech");
        fieldTechRole = userRole;
        fieldTechEmail = email;
        fieldTechPassword = password;

    }

    /***
     * Getting credentials for OfficePersonnel
     * @param userRole User Role
     * @param email User Email
     * @param password User Password
     */
    @Test(dataProvider = "getOfficePersonnelCredentials", dataProviderClass = DataProviderClass.class, priority = 9)
    public void getCredentialsForOfficePersonnel(String userRole, String email, String password) {
        CommonUtility.logCreateNode("getCredentialsForOfficePersonnel");
        officePersonnelRole = userRole;
        officePersonnelEmail = email;
        officePersonnelPassword = password;

    }

    /***
     * Getting data from csv file for production
     * parameters : each page & action access value
     */
    @Test(dataProvider = "getProductionData", dataProviderClass = DataProviderClass.class, priority = 2)
    public void getDataForProduction(String userRole, String dashboard, String infoBox, String latestStudies,
                                     String patientsList, String createPatient, String editPatient, String importStudy, String patientDocumentsTab, String patientAuditTrailTab,
                                     String createDevice, String editDevice, String devicesList, String deviceLogs, String changeDeviceFacility, String amplifiersList, String createAmplifier,
                                     String editAmplifier, String facilitiesList, String createFacility, String editFacility,
                                     String studiesList, String openStudy, String auditLogs, String sharedUsers, String usersList, String createUser, String editUser, String userAuditLogs) {
        CommonUtility.logCreateNode("getProductionData");
        productionUserListValue = usersList;

    }

    /***
     * Getting data from csv file for reviewDoctor
     * parameters : each page & action access value
     */
    @Test(dataProvider = "getReviewDoctorData", dataProviderClass = DataProviderClass.class, priority = 4)
    public void getDataForReviewDoctor(String userRole, String dashboard, String infoBox, String latestStudies,
                                       String patientsList, String createPatient, String editPatient, String importStudy, String patientDocumentsTab, String patientAuditTrailTab,
                                       String createDevice, String editDevice, String devicesList, String deviceLogs, String changeDeviceFacility, String amplifiersList, String createAmplifier,
                                       String editAmplifier, String facilitiesList, String createFacility, String editFacility,
                                       String studiesList, String openStudy, String auditLogs, String sharedUsers, String usersList, String createUser, String editUser, String userAuditLogs) {
        CommonUtility.logCreateNode("getReviewDoctorData");
        reviewDoctorUserListValue = usersList;

    }

    /***
     * Getting data from csv file for leadTech
     * parameters : each page & action access value
     */
    @Test(dataProvider = "getLeadTechData", dataProviderClass = DataProviderClass.class, priority = 5)
    public void getDataForLeadTech(String userRole, String dashboard, String infoBox, String latestStudies,
                                   String patientsList, String createPatient, String editPatient, String importStudy, String patientDocumentsTab, String patientAuditTrailTab,
                                   String createDevice, String editDevice, String devicesList, String deviceLogs, String changeDeviceFacility, String amplifiersList, String createAmplifier,
                                   String editAmplifier, String facilitiesList, String createFacility, String editFacility,
                                   String studiesList, String openStudy, String auditLogs, String sharedUsers, String usersList, String createUser, String editUser, String userAuditLogs) {
        CommonUtility.logCreateNode("getLeadTechData");
        leadTechUserListValue = usersList;

    }

    /***
     * Getting data from csv file for leadTech
     * parameters : each page & action access value
     */
    @Test(dataProvider = "getFieldTechData", dataProviderClass = DataProviderClass.class, priority = 6)
    public void getDataForFieldTech(String userRole, String dashboard, String infoBox, String latestStudies,
                                    String patientsList, String createPatient, String editPatient, String importStudy, String patientDocumentsTab, String patientAuditTrailTab,
                                    String createDevice, String editDevice, String devicesList, String deviceLogs, String changeDeviceFacility, String amplifiersList, String createAmplifier,
                                    String editAmplifier, String facilitiesList, String createFacility, String editFacility,
                                    String studiesList, String openStudy, String auditLogs, String sharedUsers, String usersList, String createUser, String editUser, String userAuditLogs) {
        CommonUtility.logCreateNode("getFieldTechData");
        fieldTechUserListValue = usersList;

    }

    /***
     * Getting data from csv file for leadTech
     * parameters : each page & action access value
     */
    @Test(dataProvider = "getOfficePersonnelData", dataProviderClass = DataProviderClass.class, priority = 7)
    public void getDataForOfficePersonnel(String userRole, String dashboard, String infoBox, String latestStudies,
                                          String patientsList, String createPatient, String editPatient, String importStudy, String patientDocumentsTab, String patientAuditTrailTab,
                                          String createDevice, String editDevice, String devicesList, String deviceLogs, String changeDeviceFacility, String amplifiersList, String createAmplifier,
                                          String editAmplifier, String facilitiesList, String createFacility, String editFacility,
                                          String studiesList, String openStudy, String auditLogs, String sharedUsers, String usersList, String createUser, String editUser, String userAuditLogs) {
        CommonUtility.logCreateNode("getOfficePersonnelData");
        officePersonnelUserListValue = usersList;

    }

    /***
     * Method for SuperAdmin to log in
     */
    @Test(priority = 10)
    public void superAdminUserLogin() {
        CommonUtility.logCreateNode("superAdminUserLogin");
        userLogin(SuperAdminEmail, SuperAdminPassword);

    }

    /***
     * Method to create test users in LNC facility
     */
    @Test(priority = 12)
    public void createTestUsersInLNCFacility() {
        CommonUtility.logCreateNode("createTestUsersInLNCFacility");
        userPage = PagesFactory.getUserPage();
        userCreatePage = PagesFactory.getUserCreatePage();
        headerPanel = PagesFactory.getheaderPanel();

        userLastNameSuperAdmin = "SuperAdmin" + dateTime;
        userLastNameProduction = "Production" + dateTime + "testtesttesttesttesttesttesttesttesttesttesttesttesttesttest" + "testtesttesttesttesttesttesttesttesttesttesttesttesttesttest";
        userLastNameFacilityAdmin = "FacilityAdmin" + dateTime;
        userLastNameReviewDoctor = "ReviewDoctor" + dateTime;

        String userFirstNameProduction = "Production " + "testtesttesttesttesttesttesttesttesttesttesttesttesttesttest" + "testtesttesttesttesttesttesttesttesttesttesttesttesttesttest";

        emailAddressForSuperAdmin = "SuperAdmin" + dateTime + "@gmail.com";
        emailAddressForSupport = "Support" + dateTime + "@gmail.com";
        emailAddressForProduction = "Production" + dateTime + "@gmail.com";
        emailAddressForFacilityAdmin = "FacilityAdmin" + dateTime + "@gmail.com";
        emailAddressForReviewDoctor = "ReviewDoctor" + dateTime + "@gmail.com";

        headerPanel.selectFacilityFromDropdown("Lifelines Neuro Company");
        createUser("SuperAdmin", userLastNameSuperAdmin, emailAddressForSuperAdmin, password, "SuperAdmin");
        createUser("SuperAdmin", userLastNameSupport, emailAddressForSupport, password, "Support");
        createUser(userFirstNameProduction, userLastNameProduction, emailAddressForProduction, password, "Production");
        createUser("FacilityAdmin", userLastNameFacilityAdmin, emailAddressForFacilityAdmin, password, "FacilityAdmin");
        createUser("ReviewDoctor", userLastNameReviewDoctor, emailAddressForReviewDoctor, password, "ReviewDoctor");

    }

    /***
     * Method to create a new facility for creating users
     */
    @Test(priority = 13)
    public void createFacility() {
        CommonUtility.logCreateNode("createFacility");
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        facilityPage = PagesFactory.getFacilityPage();
        facilityCreatePage = PagesFactory.getFacilityCreatePage();
        facilityName = dateTime;

        menuSidebarPage.goToFacilitiesPage();

        facilityPage.verifyFacilityPageNavigation();
        facilityPage.goToCreateFacilityPage();

        facilityCreatePage.verifyCreateFacilityPageNavigation();
        facilityCreatePage.createFacility("yes", facilityName, "www.domain.com", "Facility created!");

    }

    /***
     * Method to create test users in a newly created facility
     */
    @Test(priority = 14)
    public void createTestUsersInNewlyCreatedFacility() {
        CommonUtility.logCreateNode("createTestUsersInNewlyCreatedFacility");
        userPage = PagesFactory.getUserPage();
        userCreatePage = PagesFactory.getUserCreatePage();
        headerPanel = PagesFactory.getheaderPanel();
        menuSidebarPage = PagesFactory.getMenuSideBarPage();

        userLastNameLeadTech = "LeadTech" + dateTime;
        emailAddressForLeadTech = "LeadTech" + dateTime + "@gmail.com";

        menuSidebarPage.goToUsersPage();
        userPage.verifyUserPageNavigation();
        headerPanel.selectFacilityFromDropdown(facilityName);
        createUser("LeadTech", userLastNameLeadTech, emailAddressForLeadTech, password, "LeadTech");
        menuSidebarPage.goToUsersPage();
        userPage.verifyUserPageNavigation();
        headerPanel.selectFacilityFromDropdown("Lifelines Neuro Company");

    }

    /***
     * Method to check that delete user button is not visible to user
     * @param userPageAccess (yes/no) if user has access to users page
     * @param userLastName Last name of the user
     */
    public void checkThatDeleteUserButtonIsNotVisibleToUser(String userPageAccess, String userLastName) {
        userPage = PagesFactory.getUserPage();
        menuSidebarPage = PagesFactory.getMenuSideBarPage();

        if(userPageAccess.toLowerCase().equals("yes")) {
            menuSidebarPage.goToUsersPage();
            userPage.verifyUserPageNavigation();
            userPage.checkThatDeleteIconIsNotPresentForUser(userLastName);
        }
        else if(userPageAccess.toLowerCase().equals("no")) {
            menuSidebarPage.userPageVerification(userPageAccess);
            userPage.verifyUserPageNavigation(userPageAccess);
        }

    }

    /***
     * Test case to check that users other than SuperAdmin, Support & FacilityAdmin cannot delete users
     */
    @Test(priority = 15)
    public void TC398_checkThatUsersOtherThan_SuperAdminSupportFacilityAdmin_CannotDeleteUsers() {
        CommonUtility.logCreateTest("TC398_checkThatUsersOtherThan_SuperAdminSupportFacilityAdmin_CannotDeleteUsers");
        userPage = PagesFactory.getUserPage();
        menuSidebarPage = PagesFactory.getMenuSideBarPage();

        menuSidebarPage.goToUsersPage();
        userPage.verifyUserPageNavigation();
        headerPanel.selectFacilityFromDropdown("Lifelines Neuro Company");
        userSignOut();
        userLogin(productionEmail, productionPassword);
        checkThatDeleteUserButtonIsNotVisibleToUser(productionUserListValue, userLastNameReviewDoctor);

        userSignOut();
        userLogin(reviewDoctorEmail, reviewDoctorPassword);
        checkThatDeleteUserButtonIsNotVisibleToUser(reviewDoctorUserListValue, userLastNameReviewDoctor);

        userSignOut();
        userLogin(leadTechEmail, leadTechPassword);
        checkThatDeleteUserButtonIsNotVisibleToUser(leadTechUserListValue, userLastNameReviewDoctor);

        userSignOut();
        userLogin(fieldTechEmail, fieldTechPassword);
        checkThatDeleteUserButtonIsNotVisibleToUser(fieldTechUserListValue, userLastNameReviewDoctor);

        userSignOut();
        userLogin(officePersonnelEmail, officePersonnelPassword);
        checkThatDeleteUserButtonIsNotVisibleToUser(officePersonnelUserListValue, userLastNameReviewDoctor);

    }

    /***
     * Test case to check deleted user can log in
     */
    @Test(priority = 18)
    public void TC399_checkThatDeletedUserCanLogIn() {
        CommonUtility.logCreateTest("TC399_checkThatDeletedUserCanLogIn");
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        userPage = PagesFactory.getUserPage();
        dashboardPage = PagesFactory.getDashboardPage();

        userSignOut();
        userLogin(SuperAdminEmail, SuperAdminPassword);

        menuSidebarPage.goToUsersPage();
        userPage.verifyUserPageNavigation();
        deleteUserByLastName(userLastNameReviewDoctor);

        userSignOut();
        userLogin(emailAddressForReviewDoctor, password);
        dashboardPage.checkThatUserNotAssociatedWithAnyFacilityMessageAppears();

    }

    /***
     * Test case to check that User cannot delete himself
     */
    @Test(priority = 20)
    public void TC400_checkThatUserCannotDeleteHimself() {
        CommonUtility.logCreateTest("TC400_checkThatUserCannotDeleteHimself");
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        userPage = PagesFactory.getUserPage();

        userSignOut();
        userLogin(emailAddressForSuperAdmin, password);

        menuSidebarPage.goToUsersPage();
        userPage.verifyUserPageNavigation();
        userPage.clickOnDeleteIconByUserLastName(userLastNameSuperAdmin);
        userPage.checkThatAParticularModalIsPresentWithTitle("Confirm User Deletion");
        userPage.clickOnButton("Yes");
        userPage.MatchToastMessageValue("You are not allowed to self terminate");

    }

    /***
     * Test case to check that Support users are not logged out while trying to delete themselves
     */
    @Test(priority = 22)
    public void TC401_checkThatSupportUsersAreNotLoggedOutWhileTryingToDeleteThemselves() {
        CommonUtility.logCreateTest("TC401_checkThatSupportUsersAreNotLoggedOutWhileTryingToDeleteThemselves");
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        userPage = PagesFactory.getUserPage();

        userSignOut();
        userLogin(emailAddressForSupport, password);

        menuSidebarPage.goToUsersPage();
        userPage.verifyUserPageNavigation();
        userPage.clickOnDeleteIconByUserLastName(userLastNameSuperAdmin);
        userPage.checkThatAParticularModalIsPresentWithTitle("Confirm User Deletion");
        userPage.clickOnButton("Yes");
        userPage.MatchToastMessageValue("You are not allowed to self terminate");

    }

    /***
     * Test case to check that LNC facility's deleted user is shown differently
     */
    @Test(priority = 25)
    public void TC402_checkThatLNCFacilityDeletedUserIsRepresentedDifferently() {
        CommonUtility.logCreateTest("TC402_checkThatLNCFacilityDeletedUserIsRepresentedDifferently");
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        userPage = PagesFactory.getUserPage();

        userSignOut();
        userLogin(SuperAdminEmail, SuperAdminPassword);

        menuSidebarPage.goToUsersPage();
        userPage.verifyUserPageNavigation();
        deleteUserByLastName(userLastNameFacilityAdmin);
        menuSidebarPage.goToUsersPage();
        userPage.verifyUserPageNavigation();
        userPage.verifyDeletedUserExistsInUserListWithDifferentShade(userLastNameFacilityAdmin);

    }

    /***
     * Test case to check that other facility's deleted user is represented differently in LNC facility
     */
    @Test(priority = 27)
    public void TC403_checkThatOtherFacilityUsersDeletedUserIsRepresentDifferentlyInLNCFacility() {
        CommonUtility.logCreateTest("TC402_checkThatLNCFacilityDeletedUserIsRepresentedDifferently");
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        userPage = PagesFactory.getUserPage();
        headerPanel = PagesFactory.getheaderPanel();

        menuSidebarPage.goToUsersPage();
        userPage.verifyUserPageNavigation();
        headerPanel.selectFacilityFromDropdown(facilityName);
        userPage.verifyUserPageNavigation();
        deleteUserByLastName(userLastNameLeadTech);
        headerPanel.selectFacilityFromDropdown("Lifelines Neuro Company");
        userPage.verifyUserPageNavigation();
        userPage.verifyDeletedUserExistsInUserListWithDifferentShade(userLastNameLeadTech);

    }

    /***
     * Test case to check that delete user icon is not present for deleted user
     */
    @Test(priority = 30)
    public void TC404_checkThatDeleteUserIconIsNotPresentForDeletedUser() {
        CommonUtility.logCreateTest("TC404_checkThatDeleteUserIconIsNotPresentForDeletedUser");
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        userPage = PagesFactory.getUserPage();

        userSignOut();
        userLogin(SuperAdminEmail, SuperAdminPassword);

        menuSidebarPage.goToUsersPage();
        userPage.verifyUserPageNavigation();
        userPage.checkThatDeleteIconIsNotPresentForUser(userLastNameFacilityAdmin);

    }

    /***
     * Test case to check that deleting user's confirmation message is displayed while deleting user
     */
    @Test(priority = 40)
    public void TC406_checkThatUserDeletionConfirmationMessageIsDisplayedInConfirmationModal() {
        CommonUtility.logCreateTest("TC406_checkThatUserDeletionConfirmationMessageIsDisplayedInConfirmationModal");
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        userPage = PagesFactory.getUserPage();

        menuSidebarPage.goToUsersPage();
        userPage.verifyUserPageNavigation();
        userPage.clickOnDeleteIconByUserLastName(userLastNameProduction);
        userPage.checkThatAParticularModalIsPresentWithTitle("Confirm User Deletion");
        userPage.checkThatUserDeletionConfirmationMessageIsDisplayed();
        userPage.clickOnButton("Cancel");

    }

    /***
     * For deleting Super Admin user
     */
    @Test(priority = 120, alwaysRun = true)
    public void afterTest_superAdminUserDataDeletion() {
        CommonUtility.logCreateTest("afterTestDataDeletion");
        CommonUtility.logCreateNode("afterTest_superAdminUserDataDeletion");
        userSignOut();
        userLogin(SuperAdminEmail, SuperAdminPassword);
        dataDeletion.userDeletion("yes", userLastNameSuperAdmin);

    }

    /***
     * For deleting Production user
     */
    @Test(priority = 130, alwaysRun = true)
    public void afterTest_productionUserDataDeletion() {
        CommonUtility.logCreateNode("afterTest_productionUserDataDeletion");
        dataDeletion.userDeletion("yes", userLastNameProduction);

    }

    /***
     * For deleting FacilityAdmin user
     */
    @Test(priority = 135, alwaysRun = true)
    public void afterTest_facilityAdminUserDataDeletion() {
        CommonUtility.logCreateNode("afterTest_facilityAdminUserDataDeletion");
        dataDeletion.userDeletion("yes", userLastNameFacilityAdmin);

    }

    /***
     * For deleting ReviewDoctor user
     */
    @Test(priority = 138, alwaysRun = true)
    public void afterTest_reviewDoctorUserDataDeletion() {
        CommonUtility.logCreateNode("afterTest_reviewDoctorUserDataDeletion");
        dataDeletion.userDeletion("yes", userLastNameReviewDoctor);

    }

    /***
     * For deleting LeadTech user
     */
    @Test(priority = 140, alwaysRun = true)
    public void afterTest_leadTechUserDataDeletion() {
        CommonUtility.logCreateNode("afterTest_leadTechUserDataDeletion");
        dataDeletion.userDeletion("yes", userLastNameLeadTech);

    }

    /**
     * For deleting facility that has been created for testing which facility the created user is assigned to
     */
    @Test(priority = 150, alwaysRun = true)
    public void afterTest_facilityDataDeletion() {
        CommonUtility.logCreateNode("afterTest_facilityDataDeletion");
        menuSidebarPage.goToUsersPage();
        headerPanel.selectFacilityFromDropdown("Lifelines Neuro Company");
        dataDeletion.facilityDeletion("yes", facilityName);

    }

}
