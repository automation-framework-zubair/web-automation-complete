package testcases.UserModule;

import dataprovider.DataProviderClass;
import helper.CommonUtility;
import org.testng.annotations.Test;
import pages.PagesFactory;
import pages.ui.PageClasses.CreateEditPages.UserCreatePage;
import pages.ui.PageClasses.ListPages.UserPage;
import pages.ui.PageClasses.MenuSidebarPage;
import testcases.DataDeletion;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

public class UserDeletionTestForFacilityAdmin extends BaseUserModule{
    MenuSidebarPage menuSidebarPage;
    UserPage userPage;
    UserCreatePage userCreatePage;
    Format f = new SimpleDateFormat("MMddyyyy_hhmm");
    String dateTime = f.format(new Date());
    String password = "Enosis123";
    String FacilityAdminRole;
    String FacilityAdminEmail;
    String FacilityAdminPassword;
    String SuperAdminRole;
    String SuperAdminEmail;
    String SuperAdminPassword;
    String userLastNameSuperAdmin;
    String userLastNameSupport;
    String userLastNameProduction;
    String userLastNameFacilityAdmin;
    String userLastNameReviewDoctor;
    String userLastNameLeadTech;
    String userLastNameFieldTech;
    String userLastNameOfficePersonnel;
    String emailAddressForSuperAdmin;
    String emailAddressForSupport;
    String emailAddressForProduction;
    String emailAddressForFacilityAdmin;
    String emailAddressForReviewDoctor;
    String emailAddressForLeadTech;
    String emailAddressForFieldTech;
    String emailAddressForOfficePersonnel;

    DataDeletion dataDeletion = new DataDeletion();

    /***
     * Getting credentials for SuperAdmin
     * @param userRole User Role
     * @param email User Email
     * @param password User Password
     */
    @Test(dataProvider = "getSuperAdminCredentials", dataProviderClass = DataProviderClass.class, priority = 1)
    public void getCredentialsForSuperAdmin(String userRole, String email, String password) {
        CommonUtility.logCreateTest("BeforeTestMethods");
        CommonUtility.logCreateNode("getCredentialsForSuperAdmin");
        SuperAdminRole = userRole;
        SuperAdminEmail = email;
        SuperAdminPassword = password;

    }

    /***
     * Getting credentials for FacilityAdmin
     * @param userRole User Role
     * @param email User Email
     * @param password User Password
     */
    @Test(dataProvider = "getFacilityAdminCredentials", dataProviderClass = DataProviderClass.class, priority = 2)
    public void getCredentialsForFacilityAdmin(String userRole, String email, String password) {
        CommonUtility.logCreateNode("getCredentialsForFacilityAdmin");
        FacilityAdminRole = userRole;
        FacilityAdminEmail = email;
        FacilityAdminPassword = password;

    }

    /***
     * Method for SuperAdmin to log in for test data creation
     */
    @Test(priority = 3)
    public void superAdminUserLoginForTestDataCreation() {
        CommonUtility.logCreateNode("superAdminUserLoginForTestDataCreation");
        userLogin(SuperAdminEmail, SuperAdminPassword);

    }

    /***
     * Method to create test users
     */
    @Test(priority = 5)
    public void createTestUsers() {
        CommonUtility.logCreateNode("createTestUsers");
        userPage = PagesFactory.getUserPage();
        userCreatePage = PagesFactory.getUserCreatePage();

        userLastNameSuperAdmin = "SuperAdmin" + dateTime;
        userLastNameSupport = "Support" + dateTime;
        userLastNameProduction = "Production" + dateTime;
        userLastNameFacilityAdmin = "FacilityAdmin" + dateTime;
        userLastNameReviewDoctor = "ReviewDoctor" + dateTime;
        userLastNameLeadTech = "LeadTech" + dateTime;
        userLastNameFieldTech = "FieldTech" + dateTime;
        userLastNameOfficePersonnel = "OfficePersonnel" + dateTime;

        emailAddressForSuperAdmin = "SuperAdmin" + dateTime + "@gmail.com";
        emailAddressForSupport = "Support" + dateTime + "@gmail.com";
        emailAddressForProduction = "Production" + dateTime + "@gmail.com";
        emailAddressForFacilityAdmin = "FacilityAdmin" + dateTime + "@gmail.com";
        emailAddressForReviewDoctor = "ReviewDoctor" + dateTime + "@gmail.com";
        emailAddressForLeadTech = "LeadTech" + dateTime + "@gmail.com";
        emailAddressForFieldTech = "FieldTech" + dateTime + "@gmail.com";
        emailAddressForOfficePersonnel = "OfficePersonnel" + dateTime + "@gmail.com";

        createUser("SuperAdmin", userLastNameSuperAdmin, emailAddressForSuperAdmin, password, "SuperAdmin");
        createUser("Support", userLastNameSupport, emailAddressForSupport, password, "Support");
        createUser("Production", userLastNameProduction, emailAddressForProduction, password, "Production");
        createUser("FacilityAdmin", userLastNameFacilityAdmin, emailAddressForFacilityAdmin, password, "FacilityAdmin");
        createUser("ReviewDoctor", userLastNameReviewDoctor, emailAddressForReviewDoctor, password, "ReviewDoctor");
        createUser("LeadTech", userLastNameLeadTech, emailAddressForLeadTech, password, "LeadTech");
        createUser("FieldTech", userLastNameFieldTech, emailAddressForFieldTech, password, "FieldTech");
        createUser("OfficePersonnel", userLastNameOfficePersonnel, emailAddressForOfficePersonnel, password, "OfficePersonnel");

    }

    /***
     * Method for FacilityAdmin to log in
     */
    @Test(priority = 10)
    public void facilityAdminUserLogin() {
        CommonUtility.logCreateNode("facilityAdminUserLogin");
        userSignOut();
        userLogin(FacilityAdminEmail, FacilityAdminPassword);

    }

    /***
     * Test case to check that FacilityAdmin cannot delete SuperAdmin user
     */
    @Test(priority = 15)
    public void TC390_checkThatFacilityAdminCannotDeleteSuperAdminUser() {
        CommonUtility.logCreateTest("TC390_checkThatFacilityAdminCannotDeleteSuperAdminUser");
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        userPage = PagesFactory.getUserPage();

        menuSidebarPage.goToUsersPage();
        userPage.verifyUserPageNavigation();
        userPage.checkThatUserIsNotPresentInUsersList(userLastNameSuperAdmin);

    }

    /***
     * Test case to check that FacilityAdmin cannot delete Support user
     */
    @Test(priority = 18)
    public void TC391_checkThatFacilityAdminCannotDeleteSupportUser() {
        CommonUtility.logCreateTest("TC391_checkThatFacilityAdminCannotDeleteSupportUser");
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        userPage = PagesFactory.getUserPage();

        menuSidebarPage.goToUsersPage();
        userPage.verifyUserPageNavigation();
        userPage.checkThatUserIsNotPresentInUsersList(userLastNameSupport);

    }

    /***
     * Test case to check that FacilityAdmin cannot delete Production user
     */
    @Test(priority = 20)
    public void TC392_checkThatFacilityAdminCannotDeleteProductionUser() {
        CommonUtility.logCreateTest("TC392_checkThatFacilityAdminCannotDeleteProductionUser");
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        userPage = PagesFactory.getUserPage();

        menuSidebarPage.goToUsersPage();
        userPage.verifyUserPageNavigation();
        userPage.checkThatUserIsNotPresentInUsersList(userLastNameProduction);

    }

    /***
     * Test case to check that FacilityAdmin can delete another FacilityAdmin user
     */
    @Test(priority = 23)
    public void TC393_checkThatFacilityAdminCanDeleteFacilityAdminUser() {
        CommonUtility.logCreateTest("TC394_checkThatFacilityAdminCanDeleteFacilityAdminUser");
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        userPage = PagesFactory.getUserPage();

        menuSidebarPage.goToUsersPage();
        userPage.verifyUserPageNavigation();
        deleteUserByLastName(userLastNameFacilityAdmin);

    }

    /***
     * Test case to check that FacilityAdmin can delete ReviewDoctor user
     */
    @Test(priority = 25)
    public void TC394_checkThatFacilityAdminCanDeleteReviewDoctorUser() {
        CommonUtility.logCreateTest("TC395_checkThatFacilityAdminCanDeleteReviewDoctorUser");
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        userPage = PagesFactory.getUserPage();

        menuSidebarPage.goToUsersPage();
        userPage.verifyUserPageNavigation();
        deleteUserByLastName(userLastNameReviewDoctor);

    }

    /***
     * Test case to check that FacilityAdmin can delete LeadTech user
     */
    @Test(priority = 27)
    public void TC395_checkThatFacilityAdminCanDeleteLeadTechUser() {
        CommonUtility.logCreateTest("TC395_checkThatFacilityAdminCanDeleteLeadTechUser");
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        userPage = PagesFactory.getUserPage();

        menuSidebarPage.goToUsersPage();
        userPage.verifyUserPageNavigation();
        deleteUserByLastName(userLastNameLeadTech);

    }

    /***
     * Test case to check that FacilityAdmin can delete FieldTech user
     */
    @Test(priority = 30)
    public void TC396_checkThatFacilityAdminCanDeleteFieldTechUser() {
        CommonUtility.logCreateTest("TC396_checkThatFacilityAdminCanDeleteFieldTechUser");
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        userPage = PagesFactory.getUserPage();

        menuSidebarPage.goToUsersPage();
        userPage.verifyUserPageNavigation();
        deleteUserByLastName(userLastNameFieldTech);

    }

    /***
     * Test case to check that FacilityAdmin can delete OfficePersonnel user
     */
    @Test(priority = 30)
    public void TC397_checkThatFacilityAdminCanDeleteOfficePersonnelUser() {
        CommonUtility.logCreateTest("TC397_checkThatFacilityAdminCanDeleteOfficePersonnelUser");
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        userPage = PagesFactory.getUserPage();

        menuSidebarPage.goToUsersPage();
        userPage.verifyUserPageNavigation();
        deleteUserByLastName(userLastNameOfficePersonnel);

    }


    /***
     * Method for SuperAdmin to log in for test data deletion
     */
    @Test(priority = 100)
    public void superAdminUserLogin() {
        CommonUtility.logCreateTest("afterTestDataDeletion");
        CommonUtility.logCreateNode("superAdminUserLogin");
        userSignOut();
        userLogin(SuperAdminEmail, SuperAdminPassword);

    }

    /***
     * For deleting Super Admin user
     */
    @Test(priority = 120, alwaysRun = true)
    public void afterTest_superAdminUserDataDeletion() {
        CommonUtility.logCreateNode("afterTest_superAdminUserDataDeletion");
        dataDeletion.userDeletion("yes", userLastNameSuperAdmin);

    }

    /***
     * For deleting Support user
     */
    @Test(priority = 125, alwaysRun = true)
    public void afterTest_supportUserDataDeletion() {
        CommonUtility.logCreateNode("afterTest_supportUserDataDeletion");
        dataDeletion.userDeletion("yes", userLastNameSupport);

    }

    /***
     * For deleting Production user
     */
    @Test(priority = 130, alwaysRun = true)
    public void afterTest_productionUserDataDeletion() {
        CommonUtility.logCreateNode("afterTest_productionUserDataDeletion");
        dataDeletion.userDeletion("yes", userLastNameProduction);

    }

    /***
     * For deleting FacilityAdmin user
     */
    @Test(priority = 135, alwaysRun = true)
    public void afterTest_facilityAdminUserDataDeletion() {
        CommonUtility.logCreateNode("afterTest_facilityAdminUserDataDeletion");
        dataDeletion.userDeletion("yes", userLastNameFacilityAdmin);

    }

    /***
     * For deleting ReviewDoctor user
     */
    @Test(priority = 138, alwaysRun = true)
    public void afterTest_reviewDoctorUserDataDeletion() {
        CommonUtility.logCreateNode("afterTest_reviewDoctorUserDataDeletion");
        dataDeletion.userDeletion("yes", userLastNameReviewDoctor);

    }

    /***
     * For deleting LeadTech user
     */
    @Test(priority = 140, alwaysRun = true)
    public void afterTest_leadTechUserDataDeletion() {
        CommonUtility.logCreateNode("afterTest_leadTechUserDataDeletion");
        dataDeletion.userDeletion("yes", userLastNameLeadTech);

    }

    /***
     * For deleting FieldTech user
     */
    @Test(priority = 142, alwaysRun = true)
    public void afterTest_fieldTechUserDataDeletion() {
        CommonUtility.logCreateNode("afterTest_fieldTechUserDataDeletion");
        dataDeletion.userDeletion("yes", userLastNameFieldTech);

    }

    /***
     * For deleting OfficePersonnel user
     */
    @Test(priority = 145, alwaysRun = true)
    public void afterTest_officePersonnelUserDataDeletion() {
        CommonUtility.logCreateNode("afterTest_officePersonnelUserDataDeletion");
        dataDeletion.userDeletion("yes", userLastNameOfficePersonnel);

    }

}
