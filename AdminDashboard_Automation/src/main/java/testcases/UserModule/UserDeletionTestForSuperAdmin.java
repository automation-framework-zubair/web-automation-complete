package testcases.UserModule;

import dataprovider.DataProviderClass;
import helper.CommonUtility;
import org.testng.annotations.Test;
import pages.PagesFactory;
import pages.ui.PageClasses.*;
import pages.ui.PageClasses.CreateEditPages.UserCreatePage;
import pages.ui.PageClasses.ListPages.UserPage;
import testcases.DataDeletion;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

public class UserDeletionTestForSuperAdmin extends BaseUserModule{
    MenuSidebarPage menuSidebarPage;
    UserPage userPage;
    UserCreatePage userCreatePage;
    Format f = new SimpleDateFormat("MMddyyyy_hhmm");
    String dateTime = f.format(new Date());
    String password = "Enosis123";
    String SuperAdminRole;
    String SuperAdminEmail;
    String SuperAdminPassword;
    String userLastNameSuperAdmin;
    String userLastNameSupport;
    String userLastNameProduction;
    String userLastNameFacilityAdmin;
    String userLastNameReviewDoctor;
    String userLastNameLeadTech;
    String userLastNameFieldTech;
    String userLastNameOfficePersonnel;
    String emailAddressForSuperAdmin;
    String emailAddressForSupport;
    String emailAddressForProduction;
    String emailAddressForFacilityAdmin;
    String emailAddressForReviewDoctor;
    String emailAddressForLeadTech;
    String emailAddressForFieldTech;
    String emailAddressForOfficePersonnel;

    DataDeletion dataDeletion = new DataDeletion();

    /***
     * Getting credentials for SuperAdmin
     * @param userRole User Role
     * @param email User Email
     * @param password User Password
     */
    @Test(dataProvider = "getSuperAdminCredentials", dataProviderClass = DataProviderClass.class, priority = 1)
    public void getCredentialsForSuperAdmin(String userRole, String email, String password) {
        CommonUtility.logCreateTest("BeforeTestMethods");
        CommonUtility.logCreateNode("getCredentialsForSuperAdmin");
        SuperAdminRole = userRole;
        SuperAdminEmail = email;
        SuperAdminPassword = password;

    }

    /***
     * Method for SuperAdmin to log in
     */
    @Test(priority = 3)
    public void superAdminUserLogin() {
        CommonUtility.logCreateNode("superAdminUserLogin");
        userLogin(SuperAdminEmail, SuperAdminPassword);

    }

    /***
     * Method to create test users
     */
    @Test(priority = 5)
    public void createTestUsers() {
        CommonUtility.logCreateNode("createTestUsers");
        userPage = PagesFactory.getUserPage();
        userCreatePage = PagesFactory.getUserCreatePage();

        userLastNameSuperAdmin = "SuperAdmin" + dateTime;
        userLastNameSupport = "Support" + dateTime;
        userLastNameProduction = "Production" + dateTime;
        userLastNameFacilityAdmin = "FacilityAdmin" + dateTime;
        userLastNameReviewDoctor = "ReviewDoctor" + dateTime;
        userLastNameLeadTech = "LeadTech" + dateTime;
        userLastNameFieldTech = "FieldTech" + dateTime;
        userLastNameOfficePersonnel = "OfficePersonnel" + dateTime;

        emailAddressForSuperAdmin = "SuperAdmin" + dateTime + "@gmail.com";
        emailAddressForSupport = "Support" + dateTime + "@gmail.com";
        emailAddressForProduction = "Production" + dateTime + "@gmail.com";
        emailAddressForFacilityAdmin = "FacilityAdmin" + dateTime + "@gmail.com";
        emailAddressForReviewDoctor = "ReviewDoctor" + dateTime + "@gmail.com";
        emailAddressForLeadTech = "LeadTech" + dateTime + "@gmail.com";
        emailAddressForFieldTech = "FieldTech" + dateTime + "@gmail.com";
        emailAddressForOfficePersonnel = "OfficePersonnel" + dateTime + "@gmail.com";

        createUser("SuperAdmin", userLastNameSuperAdmin, emailAddressForSuperAdmin, password, "SuperAdmin");
        createUser("Support", userLastNameSupport, emailAddressForSupport, password, "Support");
        createUser("Production", userLastNameProduction, emailAddressForProduction, password, "Production");
        createUser("FacilityAdmin", userLastNameFacilityAdmin, emailAddressForFacilityAdmin, password, "FacilityAdmin");
        createUser("ReviewDoctor", userLastNameReviewDoctor, emailAddressForReviewDoctor, password, "ReviewDoctor");
        createUser("LeadTech", userLastNameLeadTech, emailAddressForLeadTech, password, "LeadTech");
        createUser("FieldTech", userLastNameFieldTech, emailAddressForFieldTech, password, "FieldTech");
        createUser("OfficePersonnel", userLastNameOfficePersonnel, emailAddressForOfficePersonnel, password, "OfficePersonnel");

    }


    /***
     * Test case to check that SuperAdmin can delete another SuperAdmin user
     */
    @Test(priority = 10)
    public void TC372_checkThatSuperAdminCanDeleteAnotherSuperAdminUser() {
        CommonUtility.logCreateTest("TC372_checkThatSuperAdminCanDeleteAnotherSuperAdminUser");
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        userPage = PagesFactory.getUserPage();

        menuSidebarPage.goToUsersPage();
        userPage.verifyUserPageNavigation();
        deleteUserByLastName(userLastNameSuperAdmin);

    }

    /***
     * Test case to check that SuperAdmin can delete Support user
     */
    @Test(priority = 12)
    public void TC373_checkThatSuperAdminCanDeleteSupportUser() {
        CommonUtility.logCreateTest("TC373_checkThatSuperAdminCanDeleteSupportUser");
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        userPage = PagesFactory.getUserPage();

        menuSidebarPage.goToUsersPage();
        userPage.verifyUserPageNavigation();
        deleteUserByLastName(userLastNameSupport);

    }

    /***
     * Test case to check that SuperAdmin can delete Production user
     */
    @Test(priority = 14)
    public void TC374_checkThatSuperAdminCanDeleteProductionUser() {
        CommonUtility.logCreateTest("TC374_checkThatSuperAdminCanDeleteProductionUser");
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        userPage = PagesFactory.getUserPage();

        menuSidebarPage.goToUsersPage();
        userPage.verifyUserPageNavigation();
        deleteUserByLastName(userLastNameProduction);

    }

    /***
     * Test case to check that SuperAdmin can delete FacilityAdmin user
     */
    @Test(priority = 16)
    public void TC375_checkThatSuperAdminCanDeleteFacilityAdminUser() {
        CommonUtility.logCreateTest("TC375_checkThatSuperAdminCanDeleteFacilityAdminUser");
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        userPage = PagesFactory.getUserPage();

        menuSidebarPage.goToUsersPage();
        userPage.verifyUserPageNavigation();
        deleteUserByLastName(userLastNameFacilityAdmin);

    }

    /***
     * Test case to check that SuperAdmin can delete ReviewDoctor user
     */
    @Test(priority = 18)
    public void TC376_checkThatSuperAdminCanDeleteReviewDoctorUser() {
        CommonUtility.logCreateTest("TC376_checkThatSuperAdminCanDeleteReviewDoctorUser");
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        userPage = PagesFactory.getUserPage();

        menuSidebarPage.goToUsersPage();
        userPage.verifyUserPageNavigation();
        deleteUserByLastName(userLastNameReviewDoctor);

    }

    /***
     * Test case to check that SuperAdmin can delete LeadTech user
     */
    @Test(priority = 20)
    public void TC377_checkThatSuperAdminCanDeleteLeadTechUser() {
        CommonUtility.logCreateTest("TC377_checkThatSuperAdminCanDeleteLeadTechUser");
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        userPage = PagesFactory.getUserPage();

        menuSidebarPage.goToUsersPage();
        userPage.verifyUserPageNavigation();
        deleteUserByLastName(userLastNameLeadTech);

    }

    /***
     * Test case to check that SuperAdmin can delete FieldTech user
     */
    @Test(priority = 24)
    public void TC378_checkThatSuperAdminCanDeleteFieldTechUser() {
        CommonUtility.logCreateTest("TC378_checkThatSuperAdminCanDeleteFieldTechUser");
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        userPage = PagesFactory.getUserPage();

        menuSidebarPage.goToUsersPage();
        userPage.verifyUserPageNavigation();
        deleteUserByLastName(userLastNameFieldTech);

    }

    /***
     * Test case to check that SuperAdmin can delete OfficePersonnel user
     */
    @Test(priority = 30)
    public void TC379_checkThatSuperAdminCanDeleteOfficePersonnelUser() {
        CommonUtility.logCreateTest("TC379_checkThatSuperAdminCanDeleteOfficePersonnelUser");
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        userPage = PagesFactory.getUserPage();

        menuSidebarPage.goToUsersPage();
        userPage.verifyUserPageNavigation();
        deleteUserByLastName(userLastNameOfficePersonnel);

    }

    /***
     * Test case to check that the deletion of Company users was permanent and that new users can be created with same email
     */
    @Test(priority = 35)
    public void TC380_checkThatDeletionOfCompanyUsersWasPermanentAndNewUsersCanBeCreatedWithSameEmail() {
        CommonUtility.logCreateTest("TC380_checkThatDeletionOfCompanyUsersWasPermanentAndNewUsersCanBeCreatedWithSameEmail");
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        userPage = PagesFactory.getUserPage();

        menuSidebarPage.goToUsersPage();
        userPage.verifyUserPageNavigation();
        userPage.checkThatUserIsNotPresentInUsersList(userLastNameSuperAdmin);
        userPage.checkThatUserIsNotPresentInUsersList(userLastNameSupport);
        userPage.checkThatUserIsNotPresentInUsersList(userLastNameProduction);

        createUser("SuperAdmin", userLastNameSuperAdmin, emailAddressForSuperAdmin, password, "SuperAdmin");
        createUser("Support", userLastNameSupport, emailAddressForSupport, password, "Support");
        createUser("Production", userLastNameProduction, emailAddressForProduction, password, "Production");

    }

    /***
     * Test case to check that proper message is displayed for successful user deletion
     */
    @Test(priority = 40)
    public void TC381_checkThatProperMessageIsDisplayedForSuccessfulUserDeletion() {
        CommonUtility.logCreateTest("TC381_checkThatProperMessageIsDisplayedForSuccessfulUserDeletion");
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        userPage = PagesFactory.getUserPage();

        menuSidebarPage.goToUsersPage();
        userPage.verifyUserPageNavigation();
        deleteUserByLastName(userLastNameSuperAdmin);
        menuSidebarPage.goToUsersPage();
        userPage.verifyUserPageNavigation();
        deleteUserByLastName(userLastNameSupport);
        menuSidebarPage.goToUsersPage();
        userPage.verifyUserPageNavigation();
        deleteUserByLastName(userLastNameProduction);

    }

    /***
     * For deleting FacilityAdmin user
     */
    @Test(priority = 135, alwaysRun = true)
    public void afterTest_facilityAdminUserDataDeletion() {
        CommonUtility.logCreateTest("afterTestDataDeletion");
        CommonUtility.logCreateNode("afterTest_facilityAdminUserDataDeletion");
        dataDeletion.userDeletion("yes", userLastNameFacilityAdmin);

    }

    /***
     * For deleting ReviewDoctor user
     */
    @Test(priority = 138, alwaysRun = true)
    public void afterTest_reviewDoctorUserDataDeletion() {
        CommonUtility.logCreateNode("afterTest_reviewDoctorUserDataDeletion");
        dataDeletion.userDeletion("yes", userLastNameReviewDoctor);

    }

    /***
     * For deleting LeadTech user
     */
    @Test(priority = 140, alwaysRun = true)
    public void afterTest_leadTechUserDataDeletion() {
        CommonUtility.logCreateNode("afterTest_leadTechUserDataDeletion");
        dataDeletion.userDeletion("yes", userLastNameLeadTech);

    }

    /***
     * For deleting FieldTech user
     */
    @Test(priority = 142, alwaysRun = true)
    public void afterTest_fieldTechUserDataDeletion() {
        CommonUtility.logCreateNode("afterTest_fieldTechUserDataDeletion");
        dataDeletion.userDeletion("yes", userLastNameFieldTech);

    }

    /***
     * For deleting OfficePersonnel user
     */
    @Test(priority = 145, alwaysRun = true)
    public void afterTest_officePersonnelUserDataDeletion() {
        CommonUtility.logCreateNode("afterTest_officePersonnelUserDataDeletion");
        dataDeletion.userDeletion("yes", userLastNameOfficePersonnel);

    }

}
