package testcases.UserModule;

import browserutility.Browser;
import dataprovider.DataProviderClass;
import helper.CommonUtility;
import org.testng.annotations.Test;
import pages.PagesFactory;
import pages.ui.PageClasses.CreateEditPages.UserCreatePage;
import pages.ui.PageClasses.CreateEditPages.UserEditPage;
import pages.ui.PageClasses.DashboardPage;
import pages.ui.PageClasses.HeaderPanel;
import pages.ui.PageClasses.ListPages.UserPage;
import pages.ui.PageClasses.LoginPage;
import pages.ui.PageClasses.MenuSidebarPage;
import testcases.DataDeletion;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

public class UserEditPageTest extends BaseUserModule {
    LoginPage loginPage;
    MenuSidebarPage menuSidebarPage;
    UserPage userPage;
    UserCreatePage userCreatePage;
    UserEditPage userEditPage;
    HeaderPanel headerPanel;
    DashboardPage dashboardPage;
    Format f = new SimpleDateFormat("MMddyyyy_hhmm");
    String dateTime = f.format(new Date());
    String password = "Enosis123";
    String SuperAdminRole;
    String SuperAdminEmail;
    String SuperAdminPassword;
    String userLastNameSuperAdminForEditUser;
    String userLastNameSupportForEditUser;
    String userLastNameProductionForEditUser;
    String userLastNameFacilityAdminForEditUser;
    String userLastNameReviewDoctorForEditUser;
    String userLastNameLeadTechForEditUser;
    String userLastNameFieldTechForEditUser;
    String userLastNameOfficePersonnelForEditUser;
    String emailAddressForSuperAdminEditUser;
    String emailAddressForSupportEditUser;
    String emailAddressForProductionEditUser;
    String emailAddressForFacilityAdminEditUser;
    String emailAddressForReviewDoctorEditUser;
    String emailAddressForLeadTechEditUser;
    String emailAddressForFieldTechEditUser;
    String emailAddressForOfficePersonnelEditUser;

    DataDeletion dataDeletion = new DataDeletion();

    /***
     * Getting credentials for SuperAdmin
     * @param userRole User Role
     * @param email User Email
     * @param password User Password
     */
    @Test(dataProvider = "getSuperAdminCredentials", dataProviderClass = DataProviderClass.class, priority = 1)
    public void getCredentialsForSuperAdmin(String userRole, String email, String password) {
        CommonUtility.logCreateTest("BeforeTestMethods");
        CommonUtility.logCreateNode("getCredentialsForSuperAdmin");
        SuperAdminRole = userRole;
        SuperAdminEmail = email;
        SuperAdminPassword = password;

    }


    /***
     * Login with SuperAdmin role and verifying if login was successful
     */
    @Test(priority = 2)
    public void loginRoleVerification() {
        CommonUtility.logCreateNode("loginRoleVerification");
        loginPage = PagesFactory.getLoginPage();
        dashboardPage = PagesFactory.getDashboardPage();

        userLogin(SuperAdminEmail, SuperAdminPassword);

    }

    /***
     * User page navigation & verification
     */
    @Test(priority = 3)
    public void userListAction() {
        CommonUtility.logCreateNode("userListAction");
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        userPage = PagesFactory.getUserPage();

        menuSidebarPage.goToUsersPage();

        userPage.verifyUserPageNavigation();

    }

    /***
     * Method to create test users for testing edit user functionality
     */
    @Test(priority = 29)
    public void createTestUsers() {
        CommonUtility.logCreateNode("createTestUsers");
        userPage = PagesFactory.getUserPage();
        userCreatePage = PagesFactory.getUserCreatePage();

        userLastNameSuperAdminForEditUser = "SuperAdmin" + dateTime;
        userLastNameSupportForEditUser = "Support" + dateTime;
        userLastNameProductionForEditUser = "Production" + dateTime;
        userLastNameFacilityAdminForEditUser = "FacilityAdmin" + dateTime;
        userLastNameReviewDoctorForEditUser = "ReviewDoctor" + dateTime;
        userLastNameLeadTechForEditUser = "LeadTech" + dateTime;
        userLastNameFieldTechForEditUser = "FieldTech" + dateTime;
        userLastNameOfficePersonnelForEditUser = "OfficePersonnel" + dateTime;

        emailAddressForSuperAdminEditUser = "SuperAdmin" + dateTime + "@gmail.com";
        emailAddressForSupportEditUser = "Support" + dateTime + "@gmail.com";
        emailAddressForProductionEditUser = "Production" + dateTime + "@gmail.com";
        emailAddressForFacilityAdminEditUser = "FacilityAdmin" + dateTime + "@gmail.com";
        emailAddressForReviewDoctorEditUser = "ReviewDoctor" + dateTime + "@gmail.com";
        emailAddressForLeadTechEditUser = "LeadTech" + dateTime + "@gmail.com";
        emailAddressForFieldTechEditUser = "FieldTech" + dateTime + "@gmail.com";
        emailAddressForOfficePersonnelEditUser = "OfficePersonnel" + dateTime + "@gmail.com";

        createUser("SuperAdmin", userLastNameSuperAdminForEditUser, emailAddressForSuperAdminEditUser, password, "SuperAdmin");
        createUser("Support", userLastNameSupportForEditUser, emailAddressForSupportEditUser, password, "Support");
        createUser("Production", userLastNameProductionForEditUser, emailAddressForProductionEditUser, password, "Production");
        createUser("FacilityAdmin", userLastNameFacilityAdminForEditUser, emailAddressForFacilityAdminEditUser, password, "FacilityAdmin");
        createUser("ReviewDoctor", userLastNameReviewDoctorForEditUser, emailAddressForReviewDoctorEditUser, password, "ReviewDoctor");
        createUser("LeadTech", userLastNameLeadTechForEditUser, emailAddressForLeadTechEditUser, password, "LeadTech");
        createUser("FieldTech", userLastNameFieldTechForEditUser, emailAddressForFieldTechEditUser, password, "FieldTech");
        createUser("OfficePersonnel", userLastNameOfficePersonnelForEditUser, emailAddressForOfficePersonnelEditUser, password, "OfficePersonnel");

    }

    /***
     * Test case to check that the user is navigated to Edit User Page after clicking on the Edit icon
     */
    @Test(priority = 30)
    public void TC348_checkThatUserIsNavigatedToEditUserPageAfterClickingOnEditIcon() {
        CommonUtility.logCreateTest("TC348_checkThatUserIsNavigatedToEditUserPageAfterClickingOnEditIcon");
        userPage = PagesFactory.getUserPage();
        userEditPage = PagesFactory.getUserEditPage();
        menuSidebarPage = PagesFactory.getMenuSideBarPage();

        menuSidebarPage.goToUsersPage();
        userPage.verifyUserPageNavigation();
        userPage.verifyEditButtonClick("yes");

    }

    /***
     * Test case to check that the user names can be edited
     */
    @Test(priority = 31)
    public void TC349_checkThatUserNamesCanBeEdited() {
        CommonUtility.logCreateTest("TC349_checkThatUserNamesCanBeEdited");
        userPage = PagesFactory.getUserPage();
        userEditPage = PagesFactory.getUserEditPage();
        menuSidebarPage = PagesFactory.getMenuSideBarPage();

        navigationToEditUserPageByUserLastName(userLastNameSuperAdminForEditUser);
        userLastNameSuperAdminForEditUser = "TC_Edited_Last" + dateTime;
        userEditPage.typeTitle("TC_Edited");
        userEditPage.typeFirstName("TC_Edited");
        userEditPage.typeMiddleName("TC_Edited");
        userEditPage.typeLastName(userLastNameSuperAdminForEditUser);
        userEditPage.typeSuffix("TC_Edited");
        userEditPage.clickOnSaveButton();
        userEditPage.MatchToastMessageValue("User saved!");

    }

    /***
     * Test case to check that the email of a user can be edited
     */
    @Test(priority = 32)
    public void TC350_checkThatEmailOfUserCanBeEdited() {
        CommonUtility.logCreateTest("TC350_checkThatEmailOfUserCanBeEdited");
        userPage = PagesFactory.getUserPage();
        userEditPage = PagesFactory.getUserEditPage();
        menuSidebarPage = PagesFactory.getMenuSideBarPage();

        navigationToEditUserPageByUserLastName(userLastNameSuperAdminForEditUser);
        userEditPage.typeEmail("test332349" + dateTime + "@gmail.com");
        userEditPage.clickOnSaveButton();
        userEditPage.MatchToastMessageValue("User saved!");

    }

    /***
     * Test case to check that the save button remains disabled if there are no changes in user data
     */
    @Test(priority = 35)
    public void TC353_checkThatSaveButtonRemainsDisabledIfThereAreNoChangesInUserData() {
        CommonUtility.logCreateTest("TC353_checkThatSaveButtonRemainsDisabledIfThereAreNoChangesInUserData");
        userPage = PagesFactory.getUserPage();
        userEditPage = PagesFactory.getUserEditPage();
        menuSidebarPage = PagesFactory.getMenuSideBarPage();

        navigationToEditUserPageByUserLastName(userLastNameSuperAdminForEditUser);
        userEditPage.checkSaveButtonIsDisabled();
        userEditPage.clickOnSaveButton();
        userEditPage.checkToastMessageDidNotAppear("User saved!");

    }

    /***
     * Test case to check that successful message appears after editing data
     */
    @Test(priority = 36)
    public void TC354_checkThatSuccessfulMessageAppearsAfterEditingUserData() {
        CommonUtility.logCreateTest("TC354_checkThatSuccessfulMessageAppearsAfterEditingUserData");
        userPage = PagesFactory.getUserPage();
        userEditPage = PagesFactory.getUserEditPage();
        menuSidebarPage = PagesFactory.getMenuSideBarPage();

        navigationToEditUserPageByUserLastName(userLastNameSuperAdminForEditUser);
        userEditPage.typeFirstName("FirstName_Edited_354");
        userEditPage.selectValueFromDropDown("SuperAdmin");
        userEditPage.clickOnSaveButton();
        userEditPage.MatchToastMessageValue("User saved!");

    }

    /***
     * Test case to check that an error message is displayed for invalid email format
     */
    @Test(priority = 39)
    public void TC355_checkThatErrorMessageDisplayedForInvalidEmailWhenEditingUser() {
        CommonUtility.logCreateTest("TC355_checkThatErrorMessageDisplayedForInvalidEmailWhenEditingUser");
        userPage = PagesFactory.getUserPage();
        userEditPage = PagesFactory.getUserEditPage();
        menuSidebarPage = PagesFactory.getMenuSideBarPage();

        navigationToEditUserPageByUserLastName(userLastNameSuperAdminForEditUser);
        userEditPage.typeEmail("random Text");
        userEditPage.typeSuffix(" ");
        userEditPage.verifyPresenceOfClientSideMessage("Valid email address required");
        userEditPage.typeEmail("abc.gmam.com");
        userEditPage.typeSuffix(" ");
        userEditPage.verifyPresenceOfClientSideMessage("Valid email address required");
        userEditPage.typeEmail("a@b@c@example.com");
        userEditPage.typeSuffix(" ");
        userEditPage.verifyPresenceOfClientSideMessage("Valid email address required");
        userEditPage.typeEmail("example@123");
        userEditPage.typeSuffix(" ");
        userEditPage.verifyPresenceOfClientSideMessage("Valid email address required");
        userEditPage.typeEmail("example@.com");
        userEditPage.typeSuffix(" ");
        userEditPage.verifyPresenceOfClientSideMessage("Valid email address required");
        userEditPage.typeEmail("@example.org");
        userEditPage.typeSuffix(" ");
        userEditPage.verifyPresenceOfClientSideMessage("Valid email address required");
        userEditPage.typeEmail("example@&*(&^!_+");
        userEditPage.typeSuffix(" ");
        userEditPage.verifyPresenceOfClientSideMessage("Valid email address required");

    }

    /***
     * Test case to check that SuperAdmin can edit any user
     */
    @Test(priority = 40)
    public void TC356_checkSuperAdminCanEditAnyUser() {
        CommonUtility.logCreateTest("TC356_checkSuperAdminCanEditAnyUser");
        userPage = PagesFactory.getUserPage();
        userEditPage = PagesFactory.getUserEditPage();
        menuSidebarPage = PagesFactory.getMenuSideBarPage();

        menuSidebarPage.goToUsersPage();
        userPage.verifyUserPageNavigation();
        userPage.checkEditIconIsPresentForUser(userLastNameSuperAdminForEditUser);
        userPage.checkEditIconIsPresentForUser(userLastNameSupportForEditUser);
        userPage.checkEditIconIsPresentForUser(userLastNameProductionForEditUser);
        userPage.checkEditIconIsPresentForUser(userLastNameFacilityAdminForEditUser);
        userPage.checkEditIconIsPresentForUser(userLastNameReviewDoctorForEditUser);
        userPage.checkEditIconIsPresentForUser(userLastNameLeadTechForEditUser);
        userPage.checkEditIconIsPresentForUser(userLastNameFieldTechForEditUser);
        userPage.checkEditIconIsPresentForUser(userLastNameOfficePersonnelForEditUser);

    }

    /***
     * Test case to check that FacilityAdmin can only edit users under the same facility
     */
    @Test(priority = 41)
    public void TC357_checkFacilityAdminCanOnlyEditUsersUnderTheSameFacility() {
        CommonUtility.logCreateTest("TC357_checkFacilityAdminCanEditUsersUnderTheSameFacility");
        userPage = PagesFactory.getUserPage();
        userEditPage = PagesFactory.getUserEditPage();
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        loginPage = PagesFactory.getLoginPage();
        dashboardPage = PagesFactory.getDashboardPage();

        userSignOut();
        userLogin(emailAddressForFacilityAdminEditUser, password);

        menuSidebarPage.goToUsersPage();
        userPage.verifyUserPageNavigation();
        userPage.checkEditIconIsNotPresentForUser(userLastNameSuperAdminForEditUser);
        userPage.checkEditIconIsNotPresentForUser(userLastNameSupportForEditUser);
        userPage.checkEditIconIsNotPresentForUser(userLastNameProductionForEditUser);
        userPage.checkEditIconIsPresentForUser(userLastNameFacilityAdminForEditUser);
        userPage.checkEditIconIsPresentForUser(userLastNameReviewDoctorForEditUser);
        userPage.checkEditIconIsPresentForUser(userLastNameLeadTechForEditUser);
        userPage.checkEditIconIsPresentForUser(userLastNameFieldTechForEditUser);
        userPage.checkEditIconIsPresentForUser(userLastNameOfficePersonnelForEditUser);

    }

    /***
     * Test case to check that Support can edit any user
     */
    @Test(priority = 42)
    public void TC358_checkSupportCanEditAnyUser() {
        CommonUtility.logCreateTest("TC358_checkSupportCanEditAnyUser");
        userPage = PagesFactory.getUserPage();
        userEditPage = PagesFactory.getUserEditPage();
        menuSidebarPage = PagesFactory.getMenuSideBarPage();

        userSignOut();
        userLogin(emailAddressForSupportEditUser, password);

        menuSidebarPage.goToUsersPage();
        userPage.verifyUserPageNavigation();
        userPage.checkEditIconIsNotPresentForUser(userLastNameSuperAdminForEditUser);
        userPage.checkEditIconIsPresentForUser(userLastNameSupportForEditUser);
        userPage.checkEditIconIsPresentForUser(userLastNameProductionForEditUser);
        userPage.checkEditIconIsPresentForUser(userLastNameFacilityAdminForEditUser);
        userPage.checkEditIconIsPresentForUser(userLastNameReviewDoctorForEditUser);
        userPage.checkEditIconIsPresentForUser(userLastNameLeadTechForEditUser);
        userPage.checkEditIconIsPresentForUser(userLastNameFieldTechForEditUser);
        userPage.checkEditIconIsPresentForUser(userLastNameOfficePersonnelForEditUser);
        userPage.clickEditIconByInstanceName("yes", userLastNameLeadTechForEditUser, "Users");
        userEditPage.selectValueFromDropDown("Production");
        userEditPage.clickOnSaveButton();
        userEditPage.MatchToastMessageValue("User saved!");

    }

    /***
     * Test case to check that only SuperAdmin can change the user role to SuperAdmin
     */
    @Test(priority = 43)
    public void TC359_checkThatOnlySuperAdminCanChangeUserRoleToSuperAdmin() {
        CommonUtility.logCreateTest("TC359_checkThatOnlySuperAdminCanChangeUserRoleToSuperAdmin");
        userPage = PagesFactory.getUserPage();
        userEditPage = PagesFactory.getUserEditPage();
        menuSidebarPage = PagesFactory.getMenuSideBarPage();

        userSignOut();
        userLogin(emailAddressForFacilityAdminEditUser, password);
        navigationToEditUserPageByUserLastName(userLastNameLeadTechForEditUser);
        userEditPage.checkIfUserRoleIsNotPresentInTheDropdown("SuperAdmin");

        userSignOut();
        userLogin(emailAddressForSupportEditUser, password);
        navigationToEditUserPageByUserLastName(userLastNameLeadTechForEditUser);
        userEditPage.checkIfUserRoleIsNotPresentInTheDropdown("SuperAdmin");

        userSignOut();
        userLogin(SuperAdminEmail, SuperAdminPassword);
        navigationToEditUserPageByUserLastName(userLastNameLeadTechForEditUser);
        userEditPage.checkIfUserRoleIsPresentInTheDropdown("SuperAdmin");
        userEditPage.selectValueFromDropDown("SuperAdmin");
        userEditPage.clickOnSaveButton();
        userEditPage.MatchToastMessageValue("User saved!");
        userEditPage.selectValueFromDropDown("LeadTech");
        userEditPage.clickOnSaveButton();
        userEditPage.MatchToastMessageValue("User saved!");

    }

    /***
     * Test case to check that FacilityAdmin cannot see company roles in the drop down
     */
    @Test(priority = 44)
    public void TC360_checkThatFacilityAdminDoNotSeeCompanyRolesInDropdown() {
        CommonUtility.logCreateTest("TC360_checkThatFacilityAdminDoNotSeeCompanyRolesInDropdown");
        userPage = PagesFactory.getUserPage();
        userEditPage = PagesFactory.getUserEditPage();
        menuSidebarPage = PagesFactory.getMenuSideBarPage();

        userSignOut();
        userLogin(emailAddressForFacilityAdminEditUser, password);
        navigationToEditUserPageByUserLastName(userLastNameFieldTechForEditUser);
        userEditPage.checkIfUserRoleIsNotPresentInTheDropdown("SuperAdmin");
        userEditPage.checkIfUserRoleIsNotPresentInTheDropdown("Support");
        userEditPage.checkIfUserRoleIsNotPresentInTheDropdown("Production");

    }

    /***
     * Test case to check that FacilityAdmin can see only facility roles in the drop down
     */
    @Test(priority = 44)
    public void TC361_checkThatFacilityAdminOnlySeeFacilityRolesInDropdown() {
        CommonUtility.logCreateTest("TC361_checkThatFacilityAdminOnlySeeFacilityRolesInDropdown");
        userPage = PagesFactory.getUserPage();
        userEditPage = PagesFactory.getUserEditPage();
        menuSidebarPage = PagesFactory.getMenuSideBarPage();

        userSignOut();
        userLogin(emailAddressForFacilityAdminEditUser, password);
        navigationToEditUserPageByUserLastName(userLastNameFieldTechForEditUser);
        userEditPage.checkIfUserRoleIsPresentInTheDropdown("FacilityAdmin");
        userEditPage.checkIfUserRoleIsPresentInTheDropdown("ReviewDoctor");
        userEditPage.checkIfUserRoleIsPresentInTheDropdown("LeadTech");
        userEditPage.checkIfUserRoleIsPresentInTheDropdown("FieldTech");
        userEditPage.checkIfUserRoleIsPresentInTheDropdown("OfficePersonnel");

    }

    /***
     * Test case to check that FacilityAdmin can change facility user's role to any of the facility roles
     */
    @Test(priority = 45)
    public void TC362_checkThatFacilityAdminCanChangeFacilityUsersRoleToAnyOfTheFacilityRoles() {
        CommonUtility.logCreateTest("TC362_checkThatFacilityAdminCanChangeFacilityUsersRoleToAnyOfTheFacilityRoles");
        userPage = PagesFactory.getUserPage();
        userEditPage = PagesFactory.getUserEditPage();
        menuSidebarPage = PagesFactory.getMenuSideBarPage();

        userSignOut();
        userLogin(emailAddressForFacilityAdminEditUser, password);
        navigationToEditUserPageByUserLastName(userLastNameFieldTechForEditUser);
        userEditPage.selectValueFromDropDown("FacilityAdmin");
        userEditPage.clickOnSaveButton();
        userEditPage.MatchToastMessageValue("User saved!");
        userEditPage.selectValueFromDropDown("ReviewDoctor");
        userEditPage.clickOnSaveButton();
        userEditPage.MatchToastMessageValue("User saved!");
        userEditPage.selectValueFromDropDown("LeadTech");
        userEditPage.clickOnSaveButton();
        userEditPage.MatchToastMessageValue("User saved!");
        userEditPage.selectValueFromDropDown("OfficePersonnel");
        userEditPage.clickOnSaveButton();
        userEditPage.MatchToastMessageValue("User saved!");
        userEditPage.selectValueFromDropDown("FieldTech");
        userEditPage.clickOnSaveButton();
        userEditPage.MatchToastMessageValue("User saved!");

    }

    /***
     * Test case to check that modification of a user does not redirect the logged in user to the Users page
     */
    @Test(priority = 46)
    public void TC363_checkThatModificationOfUserDoesNotRedirectLoggedInUserToUsersPage() {
        CommonUtility.logCreateTest("TC363_checkThatModificationOfUserDoesNotRedirectLoggedInUserToUsersPage");
        userPage = PagesFactory.getUserPage();
        userEditPage = PagesFactory.getUserEditPage();
        menuSidebarPage = PagesFactory.getMenuSideBarPage();

        userSignOut();
        userLogin(SuperAdminEmail, SuperAdminPassword);
        navigationToEditUserPageByUserLastName(userLastNameFieldTechForEditUser);
        userEditPage.typeMiddleName("Middle");
        userEditPage.clickOnSaveButton();
        userEditPage.MatchToastMessageValue("User saved!");
        userPage.checkUserPageDidNotAppear();
        userEditPage.verifyEditUserPageNavigation();

    }

    /***
     * Test case to check that logged in user is not allowed to edit a user to have invalid email
     */
    @Test(priority = 47)
    public void TC364_checkThatUserCannotBeEditedToHaveInvalidEmail() {
        CommonUtility.logCreateTest("TC364_checkThatUserCannotBeEditedToHaveInvalidEmail");
        userPage = PagesFactory.getUserPage();
        userEditPage = PagesFactory.getUserEditPage();
        menuSidebarPage = PagesFactory.getMenuSideBarPage();

        navigationToEditUserPageByUserLastName(userLastNameFieldTechForEditUser);
        userEditPage.typeEmail("-example@sample.com");
        userEditPage.clickOnSaveButton();
        userEditPage.checkToastMessageDidNotAppear("User saved!");

    }

    /***
     * Test case to check if error message is displayed for duplicate email in Edit user page
     */
    @Test(priority = 47)
    public void TC365_checkThatErrorMessageIsDisplayedForDuplicateEmail() {
        CommonUtility.logCreateTest("TC365_checkThatErrorMessageIsDisplayedForDuplicateEmail");
        userPage = PagesFactory.getUserPage();
        userEditPage = PagesFactory.getUserEditPage();
        menuSidebarPage = PagesFactory.getMenuSideBarPage();

        navigationToEditUserPageByUserLastName(userLastNameFieldTechForEditUser);
        userEditPage.typeEmail(SuperAdminEmail);
        userEditPage.clickOnSaveButton();
        userEditPage.MatchToastMessageValue("Email already in use.");

    }

    /***
     * Test case to check that save button remains disabled until any changes are being made
     */
    @Test(priority = 48)
    public void TC366_checkThatSaveButtonRemainsDisabledUntilAnyChangesBeingMade() {
        CommonUtility.logCreateTest("TC366_checkThatSaveButtonRemainsDisabledUntilAnyChangesBeingMade");
        userPage = PagesFactory.getUserPage();
        userEditPage = PagesFactory.getUserEditPage();
        menuSidebarPage = PagesFactory.getMenuSideBarPage();

        navigationToEditUserPageByUserLastName(userLastNameFieldTechForEditUser);
        userEditPage.checkSaveButtonIsDisabled();
        userEditPage.typeMiddleName("Middle_366");
        userEditPage.checkThatSaveButtonIsNotDisabled();

    }

    /***
     * Test case to check that Support user cannot edit SuperAdmin user's information
     */
    @Test(priority = 49)
    public void TC367_checkThatSupportCannotEditSuperAdminUser() {
        CommonUtility.logCreateTest("TC367_checkThatSupportCannotEditSuperAdminUser");
        userPage = PagesFactory.getUserPage();
        userEditPage = PagesFactory.getUserEditPage();
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        dashboardPage = PagesFactory.getDashboardPage();

        navigationToEditUserPageByUserLastName(userLastNameSuperAdminForEditUser);
        String superAdminEditUserPageUrl = Browser.getCurrentUrl();

        userSignOut();
        userLogin(emailAddressForSupportEditUser, password);
        menuSidebarPage.goToUsersPage();
        userPage.verifyUserPageNavigation();
        userPage.checkEditIconIsNotPresentForUser(userLastNameSuperAdminForEditUser);
        Browser.goToUrl(superAdminEditUserPageUrl);
        dashboardPage.verifyDashboardPageNavigation();
        dashboardPage.MatchToastMessageValue("You have attempted to access a resource for which you are not authorized");

    }

    /***
     * Test case to check that SuperAdmin & Support users can be assigned a facility role
     */
    @Test(priority = 50)
    public void TC351_checkThatCompanyUsersCanBeAssignedFacilityRoles() {
        CommonUtility.logCreateTest("TC351_checkThatCompanyUsersCanBeAssignedFacilityRoles");
        userPage = PagesFactory.getUserPage();
        userCreatePage = PagesFactory.getUserCreatePage();

        userSignOut();
        userLogin(SuperAdminEmail, SuperAdminPassword);

        navigationToEditUserPageByUserLastName(userLastNameSuperAdminForEditUser);
        userEditPage.selectValueFromDropDown("LeadTech");
        userEditPage.clickOnSaveButton();
        userEditPage.MatchToastMessageValue("User saved!");

        navigationToEditUserPageByUserLastName(userLastNameSupportForEditUser);
        userEditPage.selectValueFromDropDown("LeadTech");
        userEditPage.clickOnSaveButton();
        userEditPage.MatchToastMessageValue("User saved!");

    }


    /***
     * For deleting Super Admin user
     */
    @Test(priority = 120, alwaysRun = true)
    public void afterTest_superAdminUserDataDeletion() {
        CommonUtility.logCreateTest("afterTestDataDeletion");
        CommonUtility.logCreateNode("afterTest_superAdminUserDataDeletion");
        userSignOut();
        userLogin(SuperAdminEmail, SuperAdminPassword);
        dataDeletion.userDeletion("yes", userLastNameSuperAdminForEditUser);

    }

    /***
     * For deleting Support user
     */
    @Test(priority = 125, alwaysRun = true)
    public void afterTest_supportUserDataDeletion() {
        CommonUtility.logCreateNode("afterTest_supportUserDataDeletion");
        dataDeletion.userDeletion("yes", userLastNameSupportForEditUser);

    }

    /***
     * For deleting Production user
     */
    @Test(priority = 130, alwaysRun = true)
    public void afterTest_productionUserDataDeletion() {
        CommonUtility.logCreateNode("afterTest_productionUserDataDeletion");
        dataDeletion.userDeletion("yes", userLastNameProductionForEditUser);

    }

    /***
     * For deleting FacilityAdmin user
     */
    @Test(priority = 135, alwaysRun = true)
    public void afterTest_facilityAdminUserDataDeletion() {
        CommonUtility.logCreateNode("afterTest_facilityAdminUserDataDeletion");
        dataDeletion.userDeletion("yes", userLastNameFacilityAdminForEditUser);

    }

    /***
     * For deleting ReviewDoctor user
     */
    @Test(priority = 138, alwaysRun = true)
    public void afterTest_reviewDoctorUserDataDeletion() {
        CommonUtility.logCreateNode("afterTest_reviewDoctorUserDataDeletion");
        dataDeletion.userDeletion("yes", userLastNameReviewDoctorForEditUser);

    }

    /***
     * For deleting LeadTech user
     */
    @Test(priority = 140, alwaysRun = true)
    public void afterTest_leadTechUserDataDeletion() {
        CommonUtility.logCreateNode("afterTest_leadTechUserDataDeletion");
        dataDeletion.userDeletion("yes", userLastNameLeadTechForEditUser);

    }

    /***
     * For deleting FieldTech user
     */
    @Test(priority = 142, alwaysRun = true)
    public void afterTest_fieldTechUserDataDeletion() {
        CommonUtility.logCreateNode("afterTest_fieldTechUserDataDeletion");
        dataDeletion.userDeletion("yes", userLastNameFieldTechForEditUser);

    }

    /***
     * For deleting OfficePersonnel user
     */
    @Test(priority = 145, alwaysRun = true)
    public void afterTest_officePersonnelUserDataDeletion() {
        CommonUtility.logCreateNode("afterTest_officePersonnelUserDataDeletion");
        dataDeletion.userDeletion("yes", userLastNameOfficePersonnelForEditUser);

    }

}
