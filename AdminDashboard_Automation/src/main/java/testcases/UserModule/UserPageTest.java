package testcases.UserModule;

import browserutility.Browser;
import dataprovider.DataProviderClass;
import helper.CommonUtility;
import helper.Logger;
import org.testng.annotations.Test;
import pages.PagesFactory;
import pages.ui.PageClasses.*;
import pages.ui.PageClasses.CreateEditPages.FacilityCreatePage;
import pages.ui.PageClasses.CreateEditPages.UserCreatePage;
import pages.ui.PageClasses.CreateEditPages.UserEditPage;
import pages.ui.PageClasses.ListPages.FacilityPage;
import pages.ui.PageClasses.ListPages.UserPage;
import testcases.DataDeletion;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

public class UserPageTest extends BaseUserModule {
    LoginPage loginPage;
    MenuSidebarPage menuSidebarPage;
    UserPage userPage;
    UserCreatePage userCreatePage;
    UserEditPage userEditPage;
    HeaderPanel headerPanel;
    DashboardPage dashboardPage;
    ProfilePage profilePage;
    FacilityPage facilityPage;
    FacilityCreatePage facilityCreatePage;
    Format f = new SimpleDateFormat("MMddyyyy_hhmm");
    String dateTime = f.format(new Date());
    String email = "test" + dateTime + "@gmail.com";
    String firstName = "User" + dateTime;
    String lastName = "Reset" + dateTime;
    String middleName = "Middle" + dateTime;
    String password = "Enosis123";
    String userRole = "SuperAdmin";
    String resetPassword = "";
    String facilityName;
    String SuperAdminRole;
    String SuperAdminEmail;
    String SuperAdminPassword;
    String userLastNameSuperAdmin;
    String userLastNameSupport;
    String userLastNameProduction;
    String userLastNameFacilityAdmin;
    String userLastNameReviewDoctor;
    String userLastNameLeadTech;
    String userLastNameFieldTech;
    String userLastNameOfficePersonnel;
    String emailAddressForSuperAdmin;
    String emailAddressForSupport;
    String emailAddressForProduction;
    String emailAddressForFacilityAdmin;
    String emailAddressForReviewDoctor;
    String emailAddressForLeadTech;
    String emailAddressForFieldTech;
    String emailAddressForOfficePersonnel;
    String superAdminUserListValue;
    String supportUserListValue;
    String productionUserListValue;
    String facilityAdminUserListValue;
    String reviewDoctorUserListValue;
    String leadTechUserListValue;
    String fieldTechUserListValue;
    String officePersonnelUserListValue;
    String emailAddressForLeadTechInNewFacility;
    String emailAddressForFieldTechInNewFacility;
    String userLastNameLeadTechInNewFacility;
    String userLastNameFieldTechInNewFacility;


    DataDeletion dataDeletion = new DataDeletion();

    /***
     * Getting credentials for SuperAdmin
     * @param userRole User Role
     * @param email User Email
     * @param password User Password
     */
    @Test(dataProvider = "getSuperAdminCredentials", dataProviderClass = DataProviderClass.class, priority = 1)
    public void getCredentialsForSuperAdmin(String userRole, String email, String password) {
        CommonUtility.logCreateTest("getUserTestData");
        CommonUtility.logCreateNode("getCredentialsForSuperAdmin");
        SuperAdminRole = userRole;
        SuperAdminEmail = email;
        SuperAdminPassword = password;

    }

    /***
     * Getting data from csv file for superAdmin
     * parameters : each page & action access value
     */
    @Test(dataProvider = "getSuperAdminData", dataProviderClass = DataProviderClass.class, priority = 2)
    public void getDataForSuperAdmin(String userRole, String dashboard, String infoBox, String latestStudies,
                                     String patientsList, String createPatient, String editPatient, String importStudy, String patientDocumentsTab, String patientAuditTrailTab,
                                     String createDevice, String editDevice, String devicesList, String deviceLogs, String changeDeviceFacility, String amplifiersList, String createAmplifier,
                                     String editAmplifier, String facilitiesList, String createFacility, String editFacility,
                                     String studiesList, String openStudy, String auditLogs, String sharedUsers, String usersList, String createUser, String editUser, String userAuditLogs) {
        CommonUtility.logCreateNode("getDataForSuperAdmin");
        superAdminUserListValue = usersList;

    }

    /***
     * Getting data from csv file for support
     * parameters : each page & action access value
     */
    @Test(dataProvider = "getSupportData", dataProviderClass = DataProviderClass.class, priority = 2)
    public void getDataForSupport(String userRole, String dashboard, String infoBox, String latestStudies,
                                  String patientsList, String createPatient, String editPatient, String importStudy, String patientDocumentsTab, String patientAuditTrailTab,
                                  String createDevice, String editDevice, String devicesList, String deviceLogs, String changeDeviceFacility, String amplifiersList, String createAmplifier,
                                  String editAmplifier, String facilitiesList, String createFacility, String editFacility,
                                  String studiesList, String openStudy, String auditLogs, String sharedUsers, String usersList, String createUser, String editUser, String userAuditLogs) {
        CommonUtility.logCreateNode("getDataForSupport");
        supportUserListValue = usersList;


    }

    /***
     * Getting data from csv file for production
     * parameters : each page & action access value
     */
    @Test(dataProvider = "getProductionData", dataProviderClass = DataProviderClass.class, priority = 2)
    public void getDataForProduction(String userRole, String dashboard, String infoBox, String latestStudies,
                                     String patientsList, String createPatient, String editPatient, String importStudy, String patientDocumentsTab, String patientAuditTrailTab,
                                     String createDevice, String editDevice, String devicesList, String deviceLogs, String changeDeviceFacility, String amplifiersList, String createAmplifier,
                                     String editAmplifier, String facilitiesList, String createFacility, String editFacility,
                                     String studiesList, String openStudy, String auditLogs, String sharedUsers, String usersList, String createUser, String editUser, String userAuditLogs) {
        CommonUtility.logCreateNode("getProductionData");
        productionUserListValue = usersList;

    }

    /***
     * Getting data from csv file for facilityAdmin
     * parameters : each page & action access value
     */
    @Test(dataProvider = "getFacilityAdminData", dataProviderClass = DataProviderClass.class, priority = 3)
    public void getDataForFacilityAdmin(String userRole, String dashboard, String infoBox, String latestStudies,
                                     String patientsList, String createPatient, String editPatient, String importStudy, String patientDocumentsTab, String patientAuditTrailTab,
                                     String createDevice, String editDevice, String devicesList, String deviceLogs, String changeDeviceFacility, String amplifiersList, String createAmplifier,
                                     String editAmplifier, String facilitiesList, String createFacility, String editFacility,
                                     String studiesList, String openStudy, String auditLogs, String sharedUsers, String usersList, String createUser, String editUser, String userAuditLogs) {
        CommonUtility.logCreateNode("getFacilityAdminData");
        facilityAdminUserListValue = usersList;

    }

    /***
     * Getting data from csv file for reviewDoctor
     * parameters : each page & action access value
     */
    @Test(dataProvider = "getReviewDoctorData", dataProviderClass = DataProviderClass.class, priority = 4)
    public void getDataForReviewDoctor(String userRole, String dashboard, String infoBox, String latestStudies,
                                       String patientsList, String createPatient, String editPatient, String importStudy, String patientDocumentsTab, String patientAuditTrailTab,
                                       String createDevice, String editDevice, String devicesList, String deviceLogs, String changeDeviceFacility, String amplifiersList, String createAmplifier,
                                       String editAmplifier, String facilitiesList, String createFacility, String editFacility,
                                       String studiesList, String openStudy, String auditLogs, String sharedUsers, String usersList, String createUser, String editUser, String userAuditLogs) {
        CommonUtility.logCreateNode("getReviewDoctorData");
        reviewDoctorUserListValue = usersList;

    }

    /***
     * Getting data from csv file for leadTech
     * parameters : each page & action access value
     */
    @Test(dataProvider = "getLeadTechData", dataProviderClass = DataProviderClass.class, priority = 5)
    public void getDataForLeadTech(String userRole, String dashboard, String infoBox, String latestStudies,
                                   String patientsList, String createPatient, String editPatient, String importStudy, String patientDocumentsTab, String patientAuditTrailTab,
                                   String createDevice, String editDevice, String devicesList, String deviceLogs, String changeDeviceFacility, String amplifiersList, String createAmplifier,
                                   String editAmplifier, String facilitiesList, String createFacility, String editFacility,
                                   String studiesList, String openStudy, String auditLogs, String sharedUsers, String usersList, String createUser, String editUser, String userAuditLogs) {
        CommonUtility.logCreateNode("getLeadTechData");
        leadTechUserListValue = usersList;

    }

    /***
     * Getting data from csv file for leadTech
     * parameters : each page & action access value
     */
    @Test(dataProvider = "getFieldTechData", dataProviderClass = DataProviderClass.class, priority = 6)
    public void getDataForFieldTech(String userRole, String dashboard, String infoBox, String latestStudies,
                                    String patientsList, String createPatient, String editPatient, String importStudy, String patientDocumentsTab, String patientAuditTrailTab,
                                    String createDevice, String editDevice, String devicesList, String deviceLogs, String changeDeviceFacility, String amplifiersList, String createAmplifier,
                                    String editAmplifier, String facilitiesList, String createFacility, String editFacility,
                                    String studiesList, String openStudy, String auditLogs, String sharedUsers, String usersList, String createUser, String editUser, String userAuditLogs) {
        CommonUtility.logCreateNode("getFieldTechData");
        fieldTechUserListValue = usersList;

    }

    /***
     * Getting data from csv file for leadTech
     * parameters : each page & action access value
     */
    @Test(dataProvider = "getOfficePersonnelData", dataProviderClass = DataProviderClass.class, priority = 7)
    public void getDataForOfficePersonnel(String userRole, String dashboard, String infoBox, String latestStudies,
                                          String patientsList, String createPatient, String editPatient, String importStudy, String patientDocumentsTab, String patientAuditTrailTab,
                                          String createDevice, String editDevice, String devicesList, String deviceLogs, String changeDeviceFacility, String amplifiersList, String createAmplifier,
                                          String editAmplifier, String facilitiesList, String createFacility, String editFacility,
                                          String studiesList, String openStudy, String auditLogs, String sharedUsers, String usersList, String createUser, String editUser, String userAuditLogs) {
        CommonUtility.logCreateNode("getOfficePersonnelData");
        officePersonnelUserListValue = usersList;

    }


    /***
     * Login with SuperAdmin role and verifying if login was successful
     */
    @Test(priority = 9)
    public void loginRoleVerification() {
        CommonUtility.logCreateTest("beforeTestMethods");
        CommonUtility.logCreateNode("loginRoleVerification");
        loginPage = PagesFactory.getLoginPage();
        dashboardPage = PagesFactory.getDashboardPage();

        userLogin(SuperAdminEmail, SuperAdminPassword);

    }

    /***
     * User page navigation & verification
     */
    @Test(priority = 12)
    public void userListAction() {
        CommonUtility.logCreateNode("userListAction");
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        userPage = PagesFactory.getUserPage();

        menuSidebarPage.goToUsersPage();

        userPage.verifyUserPageNavigation();

    }


    /***
     * Creating user for reset password functionality
     */
    @Test(priority = 15)
    public void userCreateAction() {
        CommonUtility.logCreateNode("userCreateAction");
        userPage = PagesFactory.getUserPage();
        userCreatePage = PagesFactory.getUserCreatePage();

        userPage.verifyUserPageNavigation();
        userPage.goToCreateUserPage();

        userCreatePage.verifyCreateUserPageNavigation();
        userCreatePage.createUser("yes", "Title", firstName, middleName, lastName,
                "suffix", email, password, password, userRole, "User created!");
        userCreatePage.successfulToastMessageDisappear();

    }

    /***
     * Method to create test users in LNC facility
     */
    @Test(priority = 19)
    public void createTestUsersInLNCFacility() {
        CommonUtility.logCreateNode("createTestUsersInLNCFacility");
        userPage = PagesFactory.getUserPage();
        userCreatePage = PagesFactory.getUserCreatePage();

        userLastNameSuperAdmin = "SuperAdmin" + dateTime;
        userLastNameSupport = "Support" + dateTime;
        userLastNameProduction = "Production" + dateTime;
        userLastNameFacilityAdmin = "FacilityAdmin" + dateTime;
        userLastNameReviewDoctor = "ReviewDoctor" + dateTime;
        userLastNameLeadTech = "LeadTech" + dateTime;
        userLastNameFieldTech = "FieldTech" + dateTime;
        userLastNameOfficePersonnel = "OfficePersonnel" + dateTime;

        emailAddressForSuperAdmin = "SuperAdmin" + dateTime + "@gmail.com";
        emailAddressForSupport = "Support" + dateTime + "@gmail.com";
        emailAddressForProduction = "Production" + dateTime + "@gmail.com";
        emailAddressForFacilityAdmin = "FacilityAdmin" + dateTime + "@gmail.com";
        emailAddressForReviewDoctor = "ReviewDoctor" + dateTime + "@gmail.com";
        emailAddressForLeadTech = "LeadTech" + dateTime + "@gmail.com";
        emailAddressForFieldTech = "FieldTech" + dateTime + "@gmail.com";
        emailAddressForOfficePersonnel = "OfficePersonnel" + dateTime + "@gmail.com";

        createUser("SuperAdmin", userLastNameSuperAdmin, emailAddressForSuperAdmin, password, "SuperAdmin");
        createUser("Support", userLastNameSupport, emailAddressForSupport, password, "Support");
        createUser("Production", userLastNameProduction, emailAddressForProduction, password, "Production");
        createUser("FacilityAdmin", userLastNameFacilityAdmin, emailAddressForFacilityAdmin, password, "FacilityAdmin");
        createUser("ReviewDoctor", userLastNameReviewDoctor, emailAddressForReviewDoctor, password, "ReviewDoctor");
        createUser("LeadTech", userLastNameLeadTech, emailAddressForLeadTech, password, "LeadTech");
        createUser("FieldTech", userLastNameFieldTech, emailAddressForFieldTech, password, "FieldTech");
        createUser("OfficePersonnel", userLastNameOfficePersonnel, emailAddressForOfficePersonnel, password, "OfficePersonnel");

    }

    /***
     * Method to create a new facility for creating users
     */
    @Test(priority = 22)
    public void createFacility() {
        CommonUtility.logCreateNode("createFacility");
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        facilityPage = PagesFactory.getFacilityPage();
        facilityCreatePage = PagesFactory.getFacilityCreatePage();
        facilityName = dateTime;

        menuSidebarPage.goToFacilitiesPage();

        facilityPage.verifyFacilityPageNavigation();
        facilityPage.goToCreateFacilityPage();

        facilityCreatePage.verifyCreateFacilityPageNavigation();
        facilityCreatePage.createFacility("yes", facilityName, "www.domain.com", "Facility created!");

    }

    /***
     * Method to create test users in a newly created facility
     */
    @Test(priority = 24)
    public void createTestUsersInNewlyCreatedFacility() {
        CommonUtility.logCreateNode("createTestUsersInNewlyCreatedFacility");
        userPage = PagesFactory.getUserPage();
        userCreatePage = PagesFactory.getUserCreatePage();
        headerPanel = PagesFactory.getheaderPanel();
        menuSidebarPage = PagesFactory.getMenuSideBarPage();

        userLastNameLeadTechInNewFacility = "LeadTech_NewFacility" + dateTime;
        emailAddressForLeadTechInNewFacility = "LeadTech_NewFacility" + dateTime + "@gmail.com";
        userLastNameFieldTechInNewFacility = "FieldTech_NewFacility" + dateTime;
        emailAddressForFieldTechInNewFacility = "FieldTech_NewFacility" + dateTime + "@gmail.com";

        menuSidebarPage.goToUsersPage();
        userPage.verifyUserPageNavigation();
        Browser.refreshBrowserPage();
        userPage.verifyUserPageNavigation();
        headerPanel.selectFacilityFromDropdown(facilityName);
        createUser("LeadTech", userLastNameLeadTechInNewFacility, emailAddressForLeadTechInNewFacility, password, "LeadTech");
        createUser("FieldTech", userLastNameFieldTechInNewFacility, emailAddressForFieldTechInNewFacility, password, "FieldTech");
        menuSidebarPage.goToUsersPage();
        userPage.verifyUserPageNavigation();
        headerPanel.selectFacilityFromDropdown("Lifelines Neuro Company");

    }

    /***
     * Test case to check that clicking on reset user password icon shows confirm user reset password modal
     */
    @Test(priority = 26)
    public void TC368_checkClickingOnResetUserPasswordIconShowsConfirmResetUserPasswordModal() {
        CommonUtility.logCreateTest("TC368_checkClickingOnResetUserPasswordIconShowsConfirmResetUserPasswordModal");
        userPage = PagesFactory.getUserPage();
        menuSidebarPage = PagesFactory.getMenuSideBarPage();

        menuSidebarPage.goToUsersPage();
        userPage.verifyUserPageNavigation();
        headerPanel.selectFacilityFromDropdown("Lifelines Neuro Company");
        userPage.verifyUserPageNavigation();
        userPage.checkResetPasswordIconIsPresent();
        userPage.resetPasswordIconClick();
        userPage.checkThatAParticularModalIsPresentWithTitle("Confirm Reset User Password");
        userPage.clickOnButton("Cancel");

    }

    /***
     * Test case to check that the cancel button in reset user password modal is functional
     */
    @Test(priority = 28)
    public void TC369_checkThatCancelButtonOnResetUserPasswordModalIsFunctional() {
        CommonUtility.logCreateTest("TC370_checkThatCancelButtonOnResetUserPasswordModalIsFunctional");
        userPage = PagesFactory.getUserPage();
        menuSidebarPage = PagesFactory.getMenuSideBarPage();

        menuSidebarPage.goToUsersPage();
        userPage.verifyUserPageNavigation();
        userPage.resetPasswordIconClick();
        userPage.checkThatAParticularModalIsPresentWithTitle("Confirm Reset User Password");
        userPage.clickOnButton("Cancel");
        userPage.checkThatAParticularModalIsNotPresentWithTitle("Confirm Reset User Password");
        userPage.verifyUserPageNavigation();

    }

    /***
     * Implementing test case to check if user can reset password
     */
    @Test(priority = 50)
    public void TC80_resetPasswordFunctionality(){
        CommonUtility.logCreateTest("TC80_resetPasswordFunctionality");
        userPage = PagesFactory.getUserPage();
        menuSidebarPage = PagesFactory.getMenuSideBarPage();

        menuSidebarPage.goToUsersPage();
        userPage.verifyUserPageNavigation();

        userPage.resetPasswordIconClickByUserNames(firstName, lastName, middleName);
        userPage.checkThatAParticularModalIsPresentWithTitle("Confirm Reset User Password");
        userPage.clickOnButton("Yes");
        resetPassword = userPage.getResetPassword();
        userPage.closeResetPasswordToastMessage();

        Logger.endTestCase("resetPasswordFunctionality");

    }

    /**
     * Implementing test case to check if user is disallowed to log in with old password
     */
    @Test(priority = 52)
    public void TC82_loginWithOldPassword(){
        CommonUtility.logCreateTest("TC82_loginWithOldPassword");
        loginPage = PagesFactory.getLoginPage();
        userSignOut();

        loginPage.verifyLoginPage();
        loginPage.typeUserName(email);
        loginPage.typePassword(password);
        CommonUtility.logMessagesAndAddThemToReport("Old User Credentials Given", "info");
        loginPage.clickLoginButton();
        loginPage.checkInvalidCredentialsMessage();
        Logger.endTestCase("loginWithOldPassword");

    }

    /**
     * Implementing test case to check if user is allowed to log in with newly reset password
     */
    @Test(priority = 54)
    public void TC83_loginWithNewlyResetPassword(){
        CommonUtility.logCreateTest("TC83_loginWithNewlyResetPassword");
        loginPage = PagesFactory.getLoginPage();
        dashboardPage = PagesFactory.getDashboardPage();

        loginPage.verifyLoginPage();
        loginPage.typeUserName(email);
        loginPage.typePassword(resetPassword);
        CommonUtility.logMessagesAndAddThemToReport("New User Credentials Given", "info");
        loginPage.clickLoginButton();

        dashboardPage.verifyDashboardPageNavigation();
        Logger.endTestCase("loginWithNewlyResetPassword");


    }

    /***
     * Test case to check that Reset password icon is visible to only SuperAdmin & Support
     */
    @Test(priority = 56)
    public void TC370_checkThatResetPasswordIconIsVisibleToSuperAdminSupport() {
        CommonUtility.logCreateTest("TC370_checkThatResetPasswordIconIsVisibleToSuperAdminSupport");
        userPage = PagesFactory.getUserPage();
        menuSidebarPage = PagesFactory.getMenuSideBarPage();

        userSignOut();
        userLogin(emailAddressForSuperAdmin, password);
        menuSidebarPage.goToUsersPage();
        userPage.verifyUserPageNavigation();
        userPage.checkResetPasswordIconIsPresent();

        userSignOut();
        userLogin(emailAddressForSupport, password);
        menuSidebarPage.goToUsersPage();
        userPage.verifyUserPageNavigation();
        userPage.checkResetPasswordIconIsPresent();


    }

    /***
     * Method to check that reset password icon is not visible to user
     * @param userPageAccess (yes/no) if user has access to users page
     */
    public void checkThatResetPasswordIconIsNotVisibleToUser(String userPageAccess) {
        userPage = PagesFactory.getUserPage();
        menuSidebarPage = PagesFactory.getMenuSideBarPage();

        if(userPageAccess.toLowerCase().equals("yes")) {
            menuSidebarPage.goToUsersPage();
            userPage.verifyUserPageNavigation();
            userPage.checkResetPasswordIconIsNotPresent();
        }
        else if(userPageAccess.toLowerCase().equals("no")) {
            menuSidebarPage.userPageVerification(userPageAccess);
            userPage.verifyUserPageNavigation(userPageAccess);
        }

    }

    /***
     * Test case to check that Reset password icon is not visible to any users other than SuperAdmin & Support
     */
    @Test(priority = 60)
    public void TC371_checkThatResetPasswordIconIsNotVisibleToUsersOtherThanSuperAdminSupport() {
        CommonUtility.logCreateTest("TC370_checkThatResetPasswordIconIsNotVisibleToUsersOtherThanSuperAdminSupport");
        userPage = PagesFactory.getUserPage();
        menuSidebarPage = PagesFactory.getMenuSideBarPage();

        userSignOut();
        userLogin(emailAddressForProduction, password);
        checkThatResetPasswordIconIsNotVisibleToUser(productionUserListValue);

        userSignOut();
        userLogin(emailAddressForFacilityAdmin, password);
        checkThatResetPasswordIconIsNotVisibleToUser(facilityAdminUserListValue);

        userSignOut();
        userLogin(emailAddressForReviewDoctor, password);
        checkThatResetPasswordIconIsNotVisibleToUser(reviewDoctorUserListValue);

        userSignOut();
        userLogin(emailAddressForLeadTech, password);
        checkThatResetPasswordIconIsNotVisibleToUser(leadTechUserListValue);

        userSignOut();
        userLogin(emailAddressForFieldTech, password);
        checkThatResetPasswordIconIsNotVisibleToUser(fieldTechUserListValue);

        userSignOut();
        userLogin(emailAddressForOfficePersonnel, password);
        checkThatResetPasswordIconIsNotVisibleToUser(officePersonnelUserListValue);


    }

    /***
     * Method to check that appropriate role is displayed for user
     * @param userLastName Last name of the user
     * @param typeOfRole The type of role assigned to user (System/Facility)
     * @param userRole Role of the user
     */
    public void checkThatAppropriateRoleIsDisplayedForUser(String userLastName, String typeOfRole, String userRole) {
        if(typeOfRole.toLowerCase().equals("system")) {
            userPage.clickOnInfoIconByUserLastName(userLastName);
            userPage.checkIfAppropriateSystemRoleDisplayedForUser(userRole);
            userPage.clickOnInfoIconByUserLastName(userLastName);
        }
        else if(typeOfRole.toLowerCase().equals("facility")) {
            userPage.clickOnInfoIconByUserLastName(userLastName);
            userPage.checkIfAppropriateFacilityRoleDisplayedForUser(userRole);
            userPage.clickOnInfoIconByUserLastName(userLastName);
        }

    }

    /***
     * Test case to check that proper system role is displayed for System users
     */
    @Test(priority = 62)
    public void TC407_checkThatProperSystemRoleIsDisplayedForSystemUsers() {
        CommonUtility.logCreateTest("TC407_checkThatProperSystemRoleIsDisplayedForSystemUsers");
        userPage = PagesFactory.getUserPage();
        menuSidebarPage = PagesFactory.getMenuSideBarPage();

        userSignOut();
        userLogin(SuperAdminEmail, SuperAdminPassword);
        menuSidebarPage.goToUsersPage();
        userPage.verifyUserPageNavigation();

        checkThatAppropriateRoleIsDisplayedForUser(userLastNameSuperAdmin, "System", "Super Admin");
        checkThatAppropriateRoleIsDisplayedForUser(userLastNameSupport, "System", "Support");
        checkThatAppropriateRoleIsDisplayedForUser(userLastNameProduction, "System", "Production");

    }

    /***
     * Test case to check that proper facility role is displayed for facility users
     */
    @Test(priority = 65)
    public void TC408_checkThatProperFacilityRoleIsDisplayedForFacilityUsers() {
        CommonUtility.logCreateTest("TC408_checkThatProperFacilityRoleIsDisplayedForFacilityUsers");
        userPage = PagesFactory.getUserPage();
        menuSidebarPage = PagesFactory.getMenuSideBarPage();

        menuSidebarPage.goToUsersPage();
        userPage.verifyUserPageNavigation();
        checkThatAppropriateRoleIsDisplayedForUser(userLastNameFacilityAdmin, "Facility", "Facility Admin");
        checkThatAppropriateRoleIsDisplayedForUser(userLastNameReviewDoctor, "Facility", "Review Doctor");
        checkThatAppropriateRoleIsDisplayedForUser(userLastNameLeadTech, "Facility", "Lead Tech");
        checkThatAppropriateRoleIsDisplayedForUser(userLastNameFieldTech, "Facility", "Field Tech");
        checkThatAppropriateRoleIsDisplayedForUser(userLastNameOfficePersonnel, "Facility", "Office Personnel");

    }

    /***
     * Test case to check that facility user deletion removes the role card properly
     */
    @Test(priority = 70)
    public void TC409_checkThatFacilityUserDeletionRemovesTheRoleCardProperly() {
        CommonUtility.logCreateTest("TC409_checkThatFacilityUserDeletionRemovesTheRoleCardProperly");
        userPage = PagesFactory.getUserPage();
        menuSidebarPage = PagesFactory.getMenuSideBarPage();

        menuSidebarPage.goToUsersPage();
        userPage.verifyUserPageNavigation();
        deleteUserByLastName(userLastNameOfficePersonnel);
        userPage.clickOnInfoIconByUserLastName(userLastNameOfficePersonnel);
        userPage.checkThatFacilityUserCardIsNotDisplayed();
        userPage.clickOnInfoIconByUserLastName(userLastNameOfficePersonnel);

    }

    /***
     * Method to delete facility users in the newly created facility
     */
    @Test(priority = 72)
    public void deleteUsersInNewlyCreatedFacility() {
        CommonUtility.logCreateTest("deleteUsersInNewlyCreatedFacility");
        userPage = PagesFactory.getUserPage();
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        headerPanel = PagesFactory.getheaderPanel();

        menuSidebarPage.goToUsersPage();
        userPage.verifyUserPageNavigation();
        headerPanel.selectFacilityFromDropdown(facilityName);
        deleteUserByLastName(userLastNameLeadTechInNewFacility);
        menuSidebarPage.goToUsersPage();
        userPage.verifyUserPageNavigation();
        deleteUserByLastName(userLastNameFieldTechInNewFacility);
        menuSidebarPage.goToUsersPage();
        userPage.verifyUserPageNavigation();
        headerPanel.selectFacilityFromDropdown("Lifelines Neuro Company");

    }

    /***
     * Test case to check that user list displays system users (SuperAdmin, Support & Production) and deleted users
     * of other facilities when logged in as SuperAdmin and the selected facility is LNC
     */
    @Test(priority = 75)
    public void TC410_checkThatSystemUsersAndDeletedUsersOfOtherFacilities_AreDisplayedWhenLoggedInAsSuperAdminForLNCFacility() {
        CommonUtility.logCreateTest("TC410_checkThatSystemUsersAndDeletedUsersOfOtherFacilities_AreDisplayedWhenLoggedInAsSuperAdminForLNCFacility");
        userPage = PagesFactory.getUserPage();
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        headerPanel = PagesFactory.getheaderPanel();

        menuSidebarPage.goToUsersPage();
        userPage.verifyUserPageNavigation();
        headerPanel.selectFacilityFromDropdown("Lifelines Neuro Company");
        userPage.verifyUserExistsInUsersList(userLastNameSuperAdmin);
        userPage.verifyUserExistsInUsersList(userLastNameSupport);
        userPage.verifyUserExistsInUsersList(userLastNameProduction);
        userPage.verifyUserExistsInUsersList(userLastNameLeadTechInNewFacility);
        userPage.verifyUserExistsInUsersList(userLastNameFieldTechInNewFacility);

    }

    /***
     * Test case to check that user list displays system users (SuperAdmin, Support & Production) and deleted users
     * of other facilities when logged in as Support and the selected facility is LNC
     */
    @Test(priority = 77)
    public void TC411_checkThatSystemUsersAndDeletedUsersOfOtherFacilities_AreDisplayedWhenLoggedInAsSupportForLNCFacility() {
        CommonUtility.logCreateTest("TC411_checkThatSystemUsersAndDeletedUsersOfOtherFacilities_AreDisplayedWhenLoggedInAsSupportForLNCFacility");
        userPage = PagesFactory.getUserPage();
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        headerPanel = PagesFactory.getheaderPanel();
        dashboardPage = PagesFactory.getDashboardPage();

        userSignOut();
        userLogin(emailAddressForSupport, password);
        menuSidebarPage.goToUsersPage();
        userPage.verifyUserPageNavigation();
        userPage.verifyUserExistsInUsersList(userLastNameSuperAdmin);
        userPage.verifyUserExistsInUsersList(userLastNameSupport);
        userPage.verifyUserExistsInUsersList(userLastNameProduction);
        userPage.verifyUserExistsInUsersList(userLastNameLeadTechInNewFacility);
        userPage.verifyUserExistsInUsersList(userLastNameFieldTechInNewFacility);

    }

    /***
     * Method to check that system users (SuperAdmin, Support & Production) and deleted users of other facilities are not displayed
     * @param userPageAccess if user role has access to Users page (yes/no)
     */
    public void checkThatSystemUsersAndDeletedUsersOfOtherFacilitiesAreNotDisplayed(String userPageAccess) {
        userPage = PagesFactory.getUserPage();
        menuSidebarPage = PagesFactory.getMenuSideBarPage();

        if(userPageAccess.toLowerCase().equals("yes")) {
            menuSidebarPage.goToUsersPage();
            userPage.verifyUserPageNavigation();
            userPage.checkThatUserIsNotPresentInUsersList(userLastNameSuperAdmin);
            userPage.checkThatUserIsNotPresentInUsersList(userLastNameSupport);
            userPage.checkThatUserIsNotPresentInUsersList(userLastNameProduction);
            userPage.checkThatUserIsNotPresentInUsersList(userLastNameLeadTechInNewFacility);
            userPage.checkThatUserIsNotPresentInUsersList(userLastNameFieldTechInNewFacility);
        }
        else if(userPageAccess.toLowerCase().equals("no")) {
            menuSidebarPage.userPageVerification(userPageAccess);
            userPage.verifyUserPageNavigation(userPageAccess);
        }

    }

    /***
     * Test case to check that user list does not display system users (SuperAdmin, Support & Production) and deleted users
     * of other facilities when the logged in user is not SuperAdmin/Support and the selected facility is LNC
     */
    @Test(priority = 79)
    public void TC412_checkThatSystemUsersAndDeletedUsersOfOtherFacilities_AreNotDisplayedWhenUserRoleIsNotSuperAdminSupportForLNCFacility() {
        CommonUtility.logCreateTest("TC412_checkThatSystemUsersAndDeletedUsersOfOtherFacilities_AreNotDisplayedWhenUserRoleIsNotSuperAdminSupportForLNCFacility");
        userPage = PagesFactory.getUserPage();
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        headerPanel = PagesFactory.getheaderPanel();
        dashboardPage = PagesFactory.getDashboardPage();

        userSignOut();
        userLogin(emailAddressForProduction, password);
        checkThatSystemUsersAndDeletedUsersOfOtherFacilitiesAreNotDisplayed(productionUserListValue);

        userSignOut();
        userLogin(emailAddressForFacilityAdmin, password);
        checkThatSystemUsersAndDeletedUsersOfOtherFacilitiesAreNotDisplayed(facilityAdminUserListValue);

        userSignOut();
        userLogin(emailAddressForReviewDoctor, password);
        checkThatSystemUsersAndDeletedUsersOfOtherFacilitiesAreNotDisplayed(reviewDoctorUserListValue);

        userSignOut();
        userLogin(emailAddressForLeadTech, password);
        checkThatSystemUsersAndDeletedUsersOfOtherFacilitiesAreNotDisplayed(leadTechUserListValue);

        userSignOut();
        userLogin(emailAddressForFieldTech, password);
        checkThatSystemUsersAndDeletedUsersOfOtherFacilitiesAreNotDisplayed(fieldTechUserListValue);

        userSignOut();
        userLogin(emailAddressForOfficePersonnel, password);
        checkThatSystemUsersAndDeletedUsersOfOtherFacilitiesAreNotDisplayed(officePersonnelUserListValue);


    }

    /***
     * Test case to check that user list does not display system users (SuperAdmin, Support & Production) and deleted users
     * of other facilities when the logged in user is SuperAdmin/Support and the selected facility is NOT LNC
     */
    @Test(priority = 82)
    public void TC413_checkThatSystemUsersAndDeletedUsersOfOtherFacilities_AreNotDisplayedWhenUserRoleIsSuperAdminSupportForNewFacility() {
        CommonUtility.logCreateTest("TC413_checkThatSystemUsersAndDeletedUsersOfOtherFacilities_AreNotDisplayedWhenUserRoleIsSuperAdminSupportForNewFacility");
        userPage = PagesFactory.getUserPage();
        menuSidebarPage = PagesFactory.getMenuSideBarPage();
        headerPanel = PagesFactory.getheaderPanel();
        dashboardPage = PagesFactory.getDashboardPage();

        userSignOut();
        userLogin(emailAddressForSuperAdmin, password);
        menuSidebarPage.goToUsersPage();
        userPage.verifyUserPageNavigation();
        headerPanel.selectFacilityFromDropdown(facilityName);
        checkThatSystemUsersAndDeletedUsersOfOtherFacilitiesAreNotDisplayed(superAdminUserListValue);

        userSignOut();
        userLogin(emailAddressForSupport, password);
        menuSidebarPage.goToUsersPage();
        userPage.verifyUserPageNavigation();
        headerPanel.selectFacilityFromDropdown(facilityName);
        checkThatSystemUsersAndDeletedUsersOfOtherFacilitiesAreNotDisplayed(supportUserListValue);

    }

    /**
     * For deleting user that have been created to test reset password functionality
     */
    @Test(priority = 100)
    public void afterTest_resetPasswordUserTestDataDeletion() {
        CommonUtility.logCreateTest("afterTestDataDeletion");
        CommonUtility.logCreateNode("afterTest_userTestDataDeletion");
        userSignOut();
        userLogin(SuperAdminEmail, SuperAdminPassword);
        dataDeletion.userDeletion("yes", lastName);

    }

    /***
     * For deleting Super Admin user
     */
    @Test(priority = 120, alwaysRun = true)
    public void afterTest_superAdminUserDataDeletion() {
        CommonUtility.logCreateNode("afterTest_superAdminUserDataDeletion");
        userSignOut();
        userLogin(SuperAdminEmail, SuperAdminPassword);
        dataDeletion.userDeletion("yes", userLastNameSuperAdmin);

    }

    /***
     * For deleting Support user
     */
    @Test(priority = 125, alwaysRun = true)
    public void afterTest_supportUserDataDeletion() {
        CommonUtility.logCreateNode("afterTest_supportUserDataDeletion");
        dataDeletion.userDeletion("yes", userLastNameSupport);

    }

    /***
     * For deleting Production user
     */
    @Test(priority = 130, alwaysRun = true)
    public void afterTest_productionUserDataDeletion() {
        CommonUtility.logCreateNode("afterTest_productionUserDataDeletion");
        dataDeletion.userDeletion("yes", userLastNameProduction);

    }

    /***
     * For deleting FacilityAdmin user
     */
    @Test(priority = 135, alwaysRun = true)
    public void afterTest_facilityAdminUserDataDeletion() {
        CommonUtility.logCreateNode("afterTest_facilityAdminUserDataDeletion");
        dataDeletion.userDeletion("yes", userLastNameFacilityAdmin);

    }

    /***
     * For deleting ReviewDoctor user
     */
    @Test(priority = 138, alwaysRun = true)
    public void afterTest_reviewDoctorUserDataDeletion() {
        CommonUtility.logCreateNode("afterTest_reviewDoctorUserDataDeletion");
        dataDeletion.userDeletion("yes", userLastNameReviewDoctor);

    }

    /***
     * For deleting LeadTech user
     */
    @Test(priority = 140, alwaysRun = true)
    public void afterTest_leadTechUserDataDeletion() {
        CommonUtility.logCreateNode("afterTest_leadTechUserDataDeletion");
        dataDeletion.userDeletion("yes", userLastNameLeadTech);

    }

    /***
     * For deleting FieldTech user
     */
    @Test(priority = 142, alwaysRun = true)
    public void afterTest_fieldTechUserDataDeletion() {
        CommonUtility.logCreateNode("afterTest_fieldTechUserDataDeletion");
        dataDeletion.userDeletion("yes", userLastNameFieldTech);

    }

    /***
     * For deleting OfficePersonnel user
     */
    @Test(priority = 145, alwaysRun = true)
    public void afterTest_officePersonnelUserDataDeletion() {
        CommonUtility.logCreateNode("afterTest_officePersonnelUserDataDeletion");
        dataDeletion.userDeletion("yes", userLastNameOfficePersonnel);

    }

    /***
     * For deleting LeadTech user in the new facility
     */
    @Test(priority = 148, alwaysRun = true)
    public void afterTest_leadTechUserOfDifferentFacilityDataDeletion() {
        CommonUtility.logCreateNode("afterTest_leadTechUserOfDifferentFacilityDataDeletion");
        dataDeletion.userDeletion("yes", userLastNameLeadTechInNewFacility);

    }

     /***
     * For deleting FieldTech user in the new facility
     */
    @Test(priority = 150, alwaysRun = true)
    public void afterTest_fieldTechUserOfDifferentFacilityDataDeletion() {
        CommonUtility.logCreateNode("afterTest_fieldTechUserOfDifferentFacilityDataDeletion");
        dataDeletion.userDeletion("yes", userLastNameFieldTechInNewFacility);

    }

    /**
     * For deleting facility that has been created for testing which facility the created user is assigned to
     */
    @Test(priority = 152, alwaysRun = true)
    public void afterTest_facilityDataDeletion() {
        CommonUtility.logCreateNode("afterTest_facilityDataDeletion");
        menuSidebarPage.goToUsersPage();
        headerPanel.selectFacilityFromDropdown("Lifelines Neuro Company");
        dataDeletion.facilityDeletion("yes", facilityName);

    }

}
